/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_srvr.h
 *  Purpose:            HTTP Server header file
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *                     
 *  Author:             Peter Hillen
 *  Changes:
 *    09 Oct 2021:      Ported from rpisense
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_SRVR_H_
#define _RPI_SRVR_H_

int      SRVR_Init         (void);


#endif /* _RPI_SRVR_H_ */
