/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for main thread
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *    20 Aug 2021:      Add rpi_guard
 *    23 Jan 2022:      Add MODACQ
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIN_H_
#define _RPI_MAIN_H_

//
// External declarations
//
extern const char *pcLogfile;
extern const char *pcMapFile;
extern const char *pcPikrellCam;
extern const char *pcPiKrellEvents;
extern const char *pcMotionLoopPath;
//
typedef struct PILOOP
{
   u_int32     ulCheckSecs;
   u_int32    *pulNextSecs;
   int         iMaxFiles;
   const char *pcPath;
   const char *pcFilter;
}  PILOOP;
//
enum
{
   PIKRELL_CMD_IDLE     = 0,                    // idle
   PIKRELL_CMD_MOTION,                          // on_motion_begin
   PIKRELL_CMD_ARCHIVE,                         // on_motion_end
   //
   NUM_PIKRELL_CMDS
};
//
typedef enum _motion_acquisition_
{
   MOT_ACQ_BEGIN        = 0,                    // Waiting for ON_MOTION_BEGIN:     GLOBAL_CAM_HST_MOT
   MOT_ACQ_COMPLETED,                           // Waiting till motion completed:   GLOBAL_MOT_HST_NFY
   MOT_ACQ_END,                                 // Waiting for ON_MOTION_END:       GLOBAL_CAM_HST_ARC
   MOT_ACQ_ARCHIVED,                            // Waiting for Archiving completed: GLOBAL_ARC_HST_NFY
   MOT_ACQ_HERON,                               // Remote Heron motion detection:   GLOBAL_HST_ARC_HER
}  MOTACQ;

#endif /* _RPI_MAIN_H_ */
