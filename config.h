/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           config.h
 *  Purpose:            Configuration headerfile
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    12 Jun 2021:      Created
 *    25 Aug 2021:      Add Guard to check on theApp
 *    04 Sep 2021:      Add CL option R x
 *    10 Nov 2021:      Add HTTP Server
 *    12 Nov 2021:      Remove mail client
 *    13 Nov 2021:      Add external msmtp mail client
 *    19 Nov 2021:      Add variadic macro's for PRINTF()
 *    20 Nov 2021:      Add Hue Scenes
 *    04 Dec 2021:      Reboot if video acquisition froze
 *    10 Dec 2021:      Add system core temperature
 *    15 Dec 2021:      Add largest motion thumbnail attachment
 *    16 Dec 2021:      Archive mail jpg files also
 *    01 Jan 2022:      CR37:Add email retries on send error
 *    01 Jan 2022:      CR38:Add Ping Camera
 *    06 Jan 2022:      CR39:Fix common Log file position
 *    13 Jan 2022:      Add motion threshold for archiving
 *    16 Jan 2022:      Archive if all motion thresholds exceeded
 *    18 Jan 2022:      Debug HUE bridge implementation
 *    07 Feb 2022:      Add CT to Hue data: UPDATE DB
 *    28 Feb 2022:      Control Hue bridge for dawn/dusk
 *    01 Jun 2022:      Add Pidlist timestamps
 *    11 Jan 2023:      LOG new state on verify OKee
 *    02 Apr 2023:      Sanity check HUE midnight
 *    07 Apr 2023:      Add Date or Time HTTP filter
 *    20 May 2023:      Add Heron archiving (New DB)
 *    12 Oct 2023:      Flexible WAN retrieval
 *    10 Mar 2024:      Use common lib fixes for net clients
 *    02 Oct 2024:      Add HUE IP
 *    09 Dec 2024:      Add Xmas Outlet 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//=============================================================================
// Feature switches
//
// makefile:
// If make debug:
//    -D DEBUG_ASSERT
//    -D FEATURE_USE_PRINTF (or overrule here)
//
//#define FEATURE_USE_PRINTF              // Release mode: disable PRINTFs
//=============================================================================
//
// Verbose flags
// +-------+-------+-------+-------+
// | Level |     24 Flags          |
// +-------+-------+-------+-------+
//
#define VERBOSE_LEVEL_MASK    0xFF000000  // Verbose level mask
#define VERBOSE_LEVEL_SHIFT   24          // Verbose level shift
//
//                                           0x85: Normal setting (Persistent G_iVerbose)
//                                           |                    (Overrule: main -v 0x240)
#define FLAGS_NFY_USAGE       0x00000001  // 1  main.c         Notify PiKrellCam of archive % free (FIFO)
#define FLAGS_PIK_ARCHIVE     0x00000002  // 0  rpi_archive.c  Archiving through PiKrellCam (FIFO)
#define FLAGS_MOTION_CSV      0x00000004  // 1  rpi_archive.c  Runtime update motion CSV file
#define FLAGS_SPARE_1         0x00000008  // 0
#define FLAGS_LOG_EVENT       0x00000010  // 0  rpi_func.c     LOG Motion timestamp
#define FLAGS_LOG_MOTION      0x00000020  // 0  rpi_motion.c   Log PiKrellCam motion data
#define FLAGS_LOG_JSON        0x00000040  // 0  rpi_hue.c      LOG JSON object data
#define FLAGS_LOG_HUECFG      0x00000080  // 1  rpi_hue.c      LOG actual   Hue string configuration
#define FLAGS_LOG_BINCFG      0x00000100  // 0  rpi_hue.c      LOG actual   Hue binary configuration
#define FLAGS_PRN_HUEMOD      0x00000200  // 0  rpi_hue.c      PRN Modified Hue string configuration
#define FLAGS_PRN_HUEALL      0x00000400  // 0  rpi_hue.c      PRN All      Hue string configuration
#define FLAGS_PRN_HUEURL      0x00000800  // 0  rpi_hue.c      PRN Hue PUT URL/Payload
#define FLAGS_PRN_MOTION      0x00001000  // 0  rpi_motion.c   PRN PiKrellCam motion data
#define FLAGS_LOG_HUEURL      0x00002000  // 0  rpi_hue.c      LOG Hue PUT URL/Payload
#define FLAGS_FIF_MOTION      0x00004000  // 0  rpi_motion.c   FIFO PiKrellCam motion data
#define FLAGS_LOG_ARCHIVE     0x00008000  // 0  rpi_archive.c  LOG Archive
#define FLAGS_HUE_MOTION      0x00010000  // 0  rpi_motion.c   Use Hue to signal motion
#define FLAGS_HUE_OUTLET      0x00020000  // 0  rpi_hue.c      LOG Hue Outlet actions
//
// IO-Options are now defined in the makefile and the actual board itself:
// Rpi-Proj root: file RPIBOARD --> decimal number 1...?
//
// IO:   select define $(INOUT):
//       FEATURE_IO_NONE                  // IO : No IO used !
//       FEATURE_IO_PATRN                 // IO : Patrn.nl board
//       FEATURE_IO_IQAUDIO               // IO : IO from IQaudIO-Zero board
//

//=============================================================================
#ifdef   FEATURE_IO_NONE      // IO : NO IO board used
//=============================================================================
#define  DIO_USE_NONE         //No IO
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "Pikrellman uses NO IO"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  IO_BUTTON
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  IN(x)                FALSE
#define  OUT(x, y)
#endif

//=============================================================================
#ifdef   FEATURE_IO_PATRN     // IO : Use Patrn.nl board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "Pikrellman uses PatrnBoard"
//
#define  LED_R                0
#define  LED_Y                7
#define  LED_G                3
#define  LED_W                6
//
#define  IO_BUTTON            1
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  IN(x)                !digitalRead(x)
#define  OUT(x, y)            digitalWrite(x, y)
#endif

//=============================================================================
#ifdef   FEATURE_IO_IQAUDIO   // IO : IO from IQaudIO-Zero board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "Pikrellman uses IQaudIO-Zero"
//
#define  LED_R                5     // GPIO24 Pin #18
#define  LED_G                4     // GPIO23 Pin #16
#define  LED_Y
#define  LED_W
//
#define  IO_BUTTON            2     // GPIO27 Pin #13
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  IN(x)                !digitalRead(x)
#define  OUT(x, y)            digitalWrite(x, y)
#endif

#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          4                    // Change to clear MMAP
#define DATA_VERSION_MINOR          0                    // Does NOT clear MMAP !
//
// The build process determines RELEASE or DEBUG builds through the
// DEBUG_ASSERT compile-time switch.
//
#define BUILD                       "084"
//
#ifdef  DEBUG_ASSERT
#define VERSION                     "2.06-CR" BUILD
#define RPI_BASE_NAME               "pikrellman"
//
#else   //DEBUG_ASSERT
#define VERSION                     "2.07." BUILD
#define RPI_BASE_NAME               "pikrellman"
//
#endif  //DEBUG_ASSERT

//
// Add some Raspberry Pi application specifics
//
#if defined (BOARD_RPI12)
//
// Rpi12: Carport
//
#define RPI_APP_NR                  12
#define RPI_APP_TEXT                " (Carport)"
//
#elif defined (BOARD_RPI14)
//
// Rpi14: Garden/Pond
//
#define RPI_APP_NR                  14
#define RPI_APP_TEXT                " (Garden)"
//
#elif defined (BOARD_RPI10)
//
// Rpi10: Development
//
#define RPI_APP_NR                  10
#define RPI_APP_TEXT                "(Devel)"
//
#else
//
// Rpi other: Debug
//
#define RPI_APP_NR                  0
#define RPI_APP_TEXT                "(Debug)"
#endif
//
#define RPI_ERR_FILE                "pikmanager.err"
#define RPI_HUE_IP                  "192.168.178.107"
//
#define RPI_MAP_FILE                RPI_BASE_NAME ".map"
#define RPI_LOG_FILE                RPI_BASE_NAME ".log"
#define RPI_CSV_FILE                RPI_BASE_NAME ".csv"
#define RPI_SHELL_FILE              RPI_BASE_NAME ".txt"
#define RPI_ALL_FILES               RPI_BASE_NAME ".*"
//
// Target RAM disk for high load file access
//
#define RPI_WORK_DIR                "/mnt/rpicache/pikrellman/"
#define RPI_RAMFS_DIR               "/mnt/rpipix/"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
#define RPI_PUBLIC_WWW              "/all/public/www/"
#define RPI_PUBLIC_DEFAULT          "index.html"
//
#define RPI_ERROR_PATH              RPI_BACKUP_DIR RPI_ERR_FILE
//
#define RPI_PIKRELL_LOG             RPI_WORK_DIR RPI_LOG_FILE
#define RPI_MAP_PATH                RPI_WORK_DIR RPI_MAP_FILE
#define RPI_CSV_PATH                RPI_WORK_DIR RPI_CSV_FILE
#define RPI_MAP_NEWPATH             RPI_WORK_DIR RPI_BASE_NAME "new.map"

#endif /* _CONFIG_H_ */
