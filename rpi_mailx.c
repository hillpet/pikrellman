/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_mailx.c
 *  Purpose:            External mail client (for msmtp)
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect, msmtp, mpack
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    13 Nov 2021:      Ported from spicam
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    01 Jun 2022:      Add PidList timestamps
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_mailx.h"
//

//#define USE_PRINTF
#include <printx.h>

//
// Local prototypes
//
static bool    mailx_SendMail             (const SARG *, int);
static char  **mailx_AllocateArgList      (const SARG *, int);
static void    mailx_FreeArgList          (char **);

//
// Show the cmnd exec argumants
//
#define  FEATURE_SHOW_EXEC_PARMS
#ifdef   FEATURE_SHOW_EXEC_PARMS
static void mailx_ShowExecParms           (char *, char **);
#define  MAILX_SHOWEXECPARMS(x,y)         mailx_ShowExecParms(x,y)
#else  //FEATURE_SHOW_EXEC_PARMS
#define  MAILX_SHOWEXECPARMS(x,y)
#endif //FEATURE_SHOW_EXEC_PARMS

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   MAILX_Send
// Purpose:    
//             
// Parms:      Command struct
// Returns:    TRUE if OKee
// Note:       
//
bool MAILX_SendMail(const SARG *pstArgs, int iNumArgs)
{
   bool  fCc;

   fCc = mailx_SendMail(pstArgs, iNumArgs);
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   mailx_SendMail
// Purpose:    Split this thread and execute the desired function. Kill any ongoing
//             process first ! 
// Parms:      Command, Pipiline args list, num args (zero = no pipe)
// Returns:    TRUE if OKee
// Note:       As long as we are running the PID_CMND thread we should get Apps and Args from the Command
//             struct. As soon as we have notified command execution down the ladder, helper threads are 
//             on their own and should get additional args from the global mmap struct !
//
static bool mailx_SendMail(const SARG *pstArgs, int iNumArgs)
{
   bool     fCc=TRUE;
   pid_t    tPid;
   char   **ppcArgsList;
   
   tPid = fork();
   //
   switch(tPid)
   {
      case 0:
         GLOBAL_PidTimestamp(PID_MAILX, 0);
         // Mailx helper thread
         LOG_Report(0, "EML", "mailx-SendMail():%d args", iNumArgs);
         PRINTF("mailx-SendMail():Fork mailx helper (%d args)" CRLF, iNumArgs);
         ppcArgsList = mailx_AllocateArgList(pstArgs, iNumArgs);
         MAILX_SHOWEXECPARMS("Mailx:", ppcArgsList);
         execvp(ppcArgsList[0], ppcArgsList);
         //
         // ================================================================
         // We should never return here !!
         // ================================================================
         //
         LOG_Report(errno, "EML", "mailx-SendMail():%s thread error return:%s", ppcArgsList[0], strerror(errno));
         PRINTF("mailx-SendMail():%s completion:%s-SHOULD NOT BE HERE!!!!" CRLF, ppcArgsList[0], strerror(errno));
         //
         mailx_FreeArgList(ppcArgsList);
         exit(EXIT_CC_GEN_ERROR);
         break;
   
      case -1:
         //====================================================================
         // Error
         //====================================================================
         fCc = FALSE;
         break;

      default:
         //====================================================================
         // Parent
         //====================================================================
         GLOBAL_PidPut(PID_MAILX, tPid);
         break;
   }
   return(fCc);
}

// 
// Function:   mailx_AllocateArgList
// Purpose:    Allocate exec-arg list
// 
// Parameters: Initial NULL terminated Arg list, Max number of list entries
// Returns:    Allocated arg array
// Note:       The arg list may be extended at some later stage to add additional
//             args. Add one additional entry for the NULL terminator
//
//             The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char **mailx_AllocateArgList(const SARG *pstArg, int iNumArgs)
{
   int         x=0;
   char       *pcArg;
   char      **ppcArgList;

   PRINTF("mailx-AllocateArgList():%d Args" CRLF, iNumArgs);
   iNumArgs++;
   ppcArgList = (char **) malloc(iNumArgs * sizeof(ppcArgList));
   //
   while(x < iNumArgs)
   {
      pcArg = pstArg->pfGetArg(pstArg);
      if(pcArg)
      {
         PRINTF("mailx-AllocateArgList():Arg %d=[%s]" CRLF, x, pcArg);
         //
         // We have an allocated argument to include in the list
         //
         ppcArgList[x] = pcArg;
      }
      else PRINTF("mailx-AllocateArgList():No Arg %d" CRLF, x);
      x++;
      pstArg++;
   }
   //
   // Make sure to NULL terminate the list always
   //
   ppcArgList[x] = NULL;
   //
   PRINTF("mailx-AllocateArgList():Arglist ready." CRLF);
   return(ppcArgList);
}

// 
// Function:   mailx_FreeArgList
// Purpose:    Free applicable option list
// 
// Parameters: Arg list
// Returns:    
// Note:       We will only call this function if the exec() system call fails.
//             In other cases, the OS will free all allocated memory for the ARG list !
//             Therefor, we should NOT use safefree() here as to not confuse our mallc admin.
//
static void mailx_FreeArgList(char **ppcArgList)
{
   int   x=0;

   if(ppcArgList)
   {
      while(ppcArgList[x])
      {
         free(ppcArgList[x]);
         x++;
      }
      free(ppcArgList);
   }
}


#ifdef   FEATURE_SHOW_EXEC_PARMS
// 
// Function:   mailx_ShowExecParms
// Purpose:    Show exec parms
// 
// Parameters: Caller, Arg list
// Returns:    
// Note:       
//
static void mailx_ShowExecParms(char *pcCaller, char **ppcArgList)
{
   int   x, iLen=0;
   char *pcTemp;

   if(ppcArgList)
   {
      //
      //
      // Deternine total size
      x = 0;
      while(ppcArgList[x])
      {
         iLen += GEN_STRLEN(ppcArgList[x++]);
         // add 1 space
         iLen++;
      }
      //
      // Assemble log string as well
      //
      iLen++;
      pcTemp = (char *)safemalloc(iLen);
      //
      x = 0;
      while(ppcArgList[x])
      {
         safestrcat(pcTemp, ppcArgList[x], iLen);
         safestrcat(pcTemp, " ", iLen);
         LOG_printf("mailx_ShowExecParms(%s): %02d:[%s]" CRLF, pcCaller, x, ppcArgList[x]);
         x++;
      }
      LOG_printf("mailx_ShowExecParms(%s):%s" CRLF, pcCaller, pcTemp);
      safefree(pcTemp);
   }
}
#endif   //FEATURE_SHOW_EXEC_PARMS
