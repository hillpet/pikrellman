/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_archive.c
 *  Purpose:            Archive thread
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Jun 2021:      Created
 *    11 Jul 2021:      Archive motion as well
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    16 Dec 2021:      Archive mail jpg files also
 *    17 Dec 2021:      Archive mail jpg files to emails folder
 *    13 Jan 2022:      Add motion threshold for archiving
 *    14 Jan 2022:      Add video filename to log
 *    17 Jan 2022:      Add csv records
 *    01 Jun 2022:      Add PidList timestamps
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_main.h"
#include "rpi_func.h"
//
#include "rpi_archive.h"

#define USE_PRINTF
#include <printx.h>

extern const char   *pcNewUser;
extern const char   *pcGroupWww;
extern const char   *pcCsvFile;
//    
static bool          fThreadRunning    = TRUE;
static const char   *pcMotionYes       = "x";
static const char   *pcMotionNo        = " ";
static const char   *pcMp4Filter       = "loop*.mp4";
//
// Local prototypes
//
static int     arch_Execute               (void);
static int     arch_ArchiveMotion         (void);
static int     arch_ArchiveMotionHeron    (void);
static int     arch_ArchiveMotionLocal    (void);
static int     arch_ArchiveMotionFifo     (void);
static int     arch_ArchiveMotionSkip     (void);
static int     arch_ArchiveThreshold      (void);
static void    arch_UpdateCsvFile         (ARCCSV, void *);
//
static bool    arch_SignalRegister        (sigset_t *);
static void    arch_ReceiveSignalSegmnt   (int);
static void    arch_ReceiveSignalInt      (int);
static void    arch_ReceiveSignalTerm     (int);
static void    arch_ReceiveSignalUser1    (int);
static void    arch_ReceiveSignalUser2    (int);
//

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   ARCH_Init
// Purpose:    Handle motion archive for PiKrellCam
//
// Parms:      
// Returns:    Exit codes
// Note:       Called from main() to split off thread and let it handle the 
//             motion archiving. The main() will use SIGUSR2 to trigger archiving.
//             handling.
//
int ARCH_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_ARCHIVE, tPid);
         GLOBAL_PidSetInit(PID_ARCHIVE, ARCH_Init);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = arch_Execute();
         GLOBAL_PidPut(PID_ARCHIVE, 0);
         LOG_Report(0, "ARC", "ARCH-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("ARCH-Init(): Error!" CRLF);
         LOG_Report(errno, "ARC", "ARCH-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_ARC_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   arch_Execute
// Purpose:    Run the Archive command thread
// 
// Parameters: 
// Returns:    
// Note:       
// 
static int arch_Execute()
{
   int         iOpt, iCc=EXIT_CC_OKEE;
   sigset_t    tBlockset;

   if(arch_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "ARC", "arch-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_ARCHIVE);
   //
   // Init ready: await further actions
   //
   GLOBAL_PidTimestamp(PID_ARCHIVE, 0);
   GLOBAL_PidSaveGuard(PID_ARCHIVE, GLOBAL_ARC_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ARC_INI);
   //
   // Main loop of Archive: wait for the semaphore posted by the parent 
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_ARCHIVE, 30000);
      switch(iOpt)
      {
         case 0:
            GLOBAL_PidTimestamp(PID_ARCHIVE, 0);
            if(RPI_GetFlag(FLAGS_LOG_ARCHIVE)) LOG_Report(0, "ARC", "arch-Execute():Signal=0x%x", GLOBAL_GetSignal(PID_ARCHIVE));
            //
            // Check if HOST reports Midnight
            //
            if(GLOBAL_GetSignalNotification(PID_ARCHIVE, GLOBAL_HST_ALL_MID)) 
            {
               // Midnight
               PRINTF("arch-Execute():Midnight" CRLF);
               LOG_Report(0, "ARC", "arch-Execute():Midnight");
            }
            //
            // Check if HOST request archiving
            //
            if(GLOBAL_GetSignalNotification(PID_ARCHIVE, GLOBAL_HST_ARC_RUN)) 
            {
               //
               // This is the PiKrellCam ON_MOTION_END Event:
               //
               iCc = arch_ArchiveMotion();
               switch(iCc)
               {
                  case EXIT_CC_OKEE:
                     break;

                  default:
                     LOG_Report(0, "ARC", "arch-Execute():Archive ERROR");
                     break;
               }
               GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ARC_HST_NFY);
            }
            if(GLOBAL_GetSignalNotification(PID_ARCHIVE, GLOBAL_HST_ARC_HER))
            {
               //
               // HTTP Server received Heron Motion Detection activation.
               // Archive *.mp4 files until detection time is over.
               //
               arch_ArchiveMotionHeron();
            }
            break;

         case 1:
            //
            // Timeout: 
            //
            break;

         default:
         case -1:
            LOG_Report(errno, "ARC", "arch-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("arch-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            iCc = EXIT_CC_GEN_ERROR;
            fThreadRunning = FALSE;
            break;
      }
   }
   arch_UpdateCsvFile(ARC_STATE_CLOSE, NULL);
   GLOBAL_SemaphoreDelete(PID_ARCHIVE);
   return(iCc);
}

//
// Function:   arch_ArchiveMotion
// Purpose:    Archive this Video/Thumbnail/Motion contents
// 
// Parameters: 
// Returns:    Cc
// Note:       Get the files from the Global area
// 
static int arch_ArchiveMotion()
{
   int      iCc=EXIT_CC_GEN_ERROR;

   PRINTF("arch-ArchiveMotion():Motion Type=%s, Verbose level=%d, Flags = 0x%X" CRLF, pstMap->G_stTs.cType, RPI_GetVerboseLevel(), RPI_GetFlags());
   //
   //
   // Archive the files
   //
   // Archive all loop-files
   // PiKrellCam filenames have the format:
   //
   // loop_2018-07-27_11.15.38_a.xxx where
   //
   //    a = 0 for no motion
   //        m for motion trigger
   //  xxx = mp4    for video
   //        th.jpg for thumbnails
   //
   // Stored in pstMap->G_stTs
   //
   switch(pstMap->G_stTs.cType[0])
   {
      case 'v':
         //
         // Video mode: skip archiving
         //
         LOG_Report(0, "ARC", "arch-ArchiveMotion():Video:Skip for now");
         iCc = arch_ArchiveMotionSkip();
         break;

      case 'a':
         //
         // Audio mode: skip archiving
         //
         LOG_Report(0, "ARC", "arch-ArchiveMotion():Audio:Skip for now");
         iCc = arch_ArchiveMotionSkip();
         break;

      case 't':
         //
         // Timelapse mode: skip archiving
         //
         LOG_Report(0, "ARC", "arch-ArchiveMotion():Timelapse:Skip for now");
         iCc = arch_ArchiveMotionSkip();
         break;

      case 'e':
         //
         // External trigger mode: skip archiving
         //
         LOG_Report(0, "ARC", "arch-ArchiveMotion():External:Skip for now");
         iCc = arch_ArchiveMotionSkip();
         break;

      case '0':
      case 'm':
         if(arch_ArchiveThreshold() >= 0)
         {
            //
            // Motion trigger: Archive either local or let PiKrellCam do it 
            //
            if(RPI_GetFlag(FLAGS_PIK_ARCHIVE)) iCc = arch_ArchiveMotionFifo(); 
            else                               iCc = arch_ArchiveMotionLocal();
         }
         else 
         {
            iCc = EXIT_CC_OKEE;
         }
         break;

      default:
         iCc = arch_ArchiveMotionSkip();
         break;
   }
   return(iCc);
}

// 
// Function:   arch_ArchiveMotionFifo
// Purpose:    Have pikrellcam archive this Video/Thumbnail file
// 
// Parameters: 
// Returns:    Cc
// Note:       Get the files from the G_ area
// 
static int arch_ArchiveMotionFifo()
{
   int      iNr=0, iLen, iCc=EXIT_CC_GEN_ERROR;
   int      iFd;
   int     *piState;
   int     *piCount;
   char    *pcVideo;
   char    *pcFile;
   char    *pcFifo;
   char    *pcData;

   //
   // Get pointers to global storage locations:
   //
   pcFifo  = (char *)GLOBAL_GetParameter(PAR_FIFO);
   pcVideo = (char *)GLOBAL_GetParameter(PAR_LAST_VID);
   RPI_LogTimestamp("arch_ArchiveMotionFifo()", pcVideo, &(pstMap->G_stTs));
   pcFile  = RPI_FindFilename(pcVideo);
   //
   piState = (int *)GLOBAL_GetParameter(PAR_MOT_STATE);
   piCount = (int *)GLOBAL_GetParameter(PAR_ARC_COUNT);
   //
   iFd = safeopen(pcFifo, O_WRONLY|O_NONBLOCK);
   if(iFd > 0)
   {
      //
      // OKee: all data found: send archive cmd to FIFO
      //
      (*piCount)++;
      pcData = (char *) safemalloc(MAX_CMDLINE_LEN);
      GEN_SNPRINTF(pcData, MAX_CMDLINE_LEN, "archive_video %s %s-%s-%s\n", pcFile, pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay);
      iLen = GEN_STRLEN(pcData);
      iNr = write(iFd, pcData, iLen);
      //
      // LOG FIFO command (remove LF)
      //
      GEN_RemoveChar(pcData, LF);
      if(iNr != iLen) LOG_Report(0, "ARC", "arch-ArchiveMotionFifo():%3d:Motion=%d (FIFO-Cmd=%s) *** SIZE ERROR *** (%d i.o %d)", *piCount, *piState, pcData, iLen, iNr);
      else            LOG_Report(0, "ARC", "arch-ArchiveMotionFifo():%3d:Motion=%d (FIFO-Cmd=%s)", *piCount, *piState, pcData);
      //
      PRINTF("arch-ArchiveMotion(): FIFO <-- %s" CRLF, pcData);
      safefree(pcData);
      safeclose(iFd);
      iCc = EXIT_CC_OKEE;
   }
   else
   {
      LOG_Report(errno, "ARC", "arch-ArchiveMotionFifo():ERROR open FIFO %s", pcFifo);
   }
   return(iCc);
}

//
// Function:   arch_ArchiveMotionHeron
// Purpose:    Archive oldest Heron triggered loop-files
// 
// Parameters: 
// Returns:    Nr of archived OKee, -1 on error
// Note:       
// 
static int arch_ArchiveMotionHeron()
{
   bool     fOkee=FALSE;
   int      iNumFiles=0;
   int     *piCount;
   char    *pcLoop;
   char    *pcArchive;
   char    *pcBase=NULL;
   char    *pcDest=NULL;
   char    *pcThumb=NULL;
   char    *pcVideo=NULL;
   PIKDIR  *pstDir;
   PIKDIR  *pstTmp;
   PIKTIM   stTs;

   //
   // Get pointers to global storage locations:
   //
   pcArchive = (char *)GLOBAL_GetParameter(PAR_ARCHIVE);      // The Archive directory
   pcLoop    = (char *)GLOBAL_GetParameter(PAR_LOOP);         // The RAMFS Loop directory
   pcVideo   = safemalloc(MAX_PATH_LENZ);
   //
   // Extract the OLDEST (= alphanumeric 1st) loop file
   //
   GEN_SNPRINTF(pcVideo, MAX_PATH_LEN, "%s/%s", pcLoop, "videos");
   //pwjh pstDir = RPI_GetDirEntries(pcVideo, (char *)pcMp4Filter, &iNumFiles, NULL);
   pstDir = GEN_GetDirEntries(pcVideo, pcMp4Filter, &iNumFiles, NULL, PIK_ALPHA);
   pstTmp = pstDir;
   //
   if(iNumFiles)
   {
      PRINTF("arch-ArchiveMotionHeron():%d files in [%s]" CRLF, iNumFiles, pcVideo);
      //
      // Find the oldest file
      // 
      while(pstTmp)
      {
         if(pstTmp->fDir) pstTmp = pstTmp->pstNext;
         else
         {
            //
            // We have a file: build full Video pathname
            //
            pcThumb = safemalloc(MAX_PATH_LENZ);
            GEN_SNPRINTF(pcVideo, MAX_PATH_LEN, "%s/%s", pcLoop, pstTmp->pcName);
            PRINTF("arch-ArchiveMotionHeron():Archive [%s]" CRLF, pcVideo);
            fOkee = TRUE;
            break;
         }
      }
      if(fOkee)
      {
         //
         // All time elements in the filename have been converted: archive this loop recording
         //
         pcBase = (char *) safemalloc(MAX_PATH_LENZ);    // file base name "loop_YYYY-MM-DD_hh.mm.ss"
         pcDest = (char *) safemalloc(MAX_PATH_LENZ);    // path for destination dirs "/all/public/www/....."
         //
         // The actual file base name (loop_YYYY-MM-DD_hh.mm.ss) --> pcBase
         //
         RPI_ExtractTimestamp(pstTmp->pcName, &stTs);
         GEN_SNPRINTF(pcBase, MAX_PATH_LEN, "loop_%s-%s-%s_%s.%s.%s", 
                        stTs.cYear, stTs.cMonth,  stTs.cDay,
                        stTs.cHour, stTs.cMinute, stTs.cSecond);
         //
         RPI_LogTimestamp("arch-ArchiveMotionHeron():", pcBase, &stTs);
         //      
         PRINTF("=========================================================" CRLF);
         PRINTF("arch-ArchiveMotionHeron():Archive Event:" CRLF);
         PRINTF("Basefile : [%s]" CRLF, pcBase);
         PRINTF("Archive  : [%s]" CRLF, pcArchive);
         PRINTF("=========================================================" CRLF);
         //
         // First make sure all directories exist. If not, create and set owner/permissions
         // Permissions will be  0775 (drwxrwxr-x)
         //
         if(!RPI_DirectoryExists(pcArchive))  
         {
            // Create Archive dir "/all/public/www"
            fOkee = RPI_DirectoryCreate(pcArchive, ATTR_RWXRWXR_X, TRUE);
            if(fOkee) GEN_ChangeOwner(pcArchive, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
         }
      }
      if(fOkee)
      {
         // Check or Create Archive dir "/all/public/www/YYYY"
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s", pcArchive, stTs.cYear);
         if(!RPI_DirectoryExists(pcDest))  
         {
            fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
            if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
         }
      }
      if(fOkee)
      {
         // Check or Create Archive dir "/all/public/www/YYYY/MM"
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s", pcArchive, stTs.cYear, stTs.cMonth);
         if(!RPI_DirectoryExists(pcDest))  
         {
            fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
            if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
         }
      }
      if(fOkee)
      {
         // Check or Create Archive dir "/all/public/www/YYYY/MM/DD"
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s", pcArchive, stTs.cYear, stTs.cMonth, stTs.cDay);
         if(!RPI_DirectoryExists(pcDest))  
         {
            fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
            if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
         }
      }
      if(fOkee)
      {
         // Check or Create Archive dir "/all/public/www/YYYY/MM/DD/videos"
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/videos", pcArchive, stTs.cYear, stTs.cMonth, stTs.cDay);
         if(!RPI_DirectoryExists(pcDest))  
         {
            fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
            if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
         }
      }
      if(fOkee)
      {
         // Check or Create Archive dir "/all/public/www/YYYY/MM/DD/thumbs"
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/thumbs", pcArchive, stTs.cYear, stTs.cMonth, stTs.cDay);
         if(!RPI_DirectoryExists(pcDest))  
         {
            fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
            if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
         }
      }
      if(fOkee)
      {
         // ==========================================================================================
         // OKee: all directories exist, move file to archive
         //    pcBase is base filename:   "YYYY-MM-DD_hh.mm.dd"
         //    The cType is the motion type 0, m, a...
         // ==========================================================================================
         PRINTF("arch-ArchiveMotionHeron():All dirs OKee:Archive [%s]" CRLF, pcVideo);
         piCount = (int *)GLOBAL_GetParameter(PAR_ARC_COUNT);
         (*piCount)++;
         //
         // Move Video file to archive: 
         //    Source:        <Loop path> / "videos" / <base filename> "_m.mp4"
         //    Destination:   <Archive path> / YYYY / MM / DD / "videos" / <base filename> "_m.mp4"
         //
         GEN_SNPRINTF(pcVideo, MAX_PATH_LEN, "%s/videos/%s_%s.mp4", pcLoop, pcBase, stTs.cType);
         GEN_SNPRINTF(pcDest,  MAX_PATH_LEN, "%s/%s/%s/%s/videos/%s_%s.mp4", pcArchive, stTs.cYear, stTs.cMonth, stTs.cDay, pcBase, stTs.cType);
         //
         if(GEN_FileCopyAttributes(pcDest, pcVideo, ATTR_RW_R__R__))
         {
            PRINTF("arch-ArchiveMotionHeron():Delete [%s]" CRLF, pcVideo);
            GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RW_R__R__, TRUE);
            if(remove(pcVideo) != 0)
            {
               PRINTF("arch-ArchiveMotionHeron():Error deleting [%s]" CRLF, pcVideo);
               LOG_Report(errno, "ARC", "arch-ArchiveMotionHeron():ERROR deleting %s ", pcVideo);
            }
            else PRINTF("arch-ArchiveMotionHeron():Delete [%s]: Done" CRLF, pcVideo);
            //
            // Move Thumbs file to archive: 
            //    Source:        <Loop path> / "thumbs" / <base filename> "_m.th.jpg"
            //    Destination:   <Archive path> / YYYY / MM / DD / "thumbs" / <base filename> "_m.th.jpg"
            //
            GEN_SNPRINTF(pcThumb, MAX_PATH_LEN, "%s/thumbs/%s_%s.th.jpg", pcLoop, pcBase, stTs.cType);
            GEN_SNPRINTF(pcDest,  MAX_PATH_LEN, "%s/%s/%s/%s/thumbs/%s_%s.th.jpg", pcArchive, stTs.cYear, stTs.cMonth, stTs.cDay, pcBase, stTs.cType);
            PRINTF("arch-ArchiveMotionHeron():Archive [%s]" CRLF, pcThumb);
            //
            if(GEN_FileCopyAttributes(pcDest, pcThumb, ATTR_RW_R__R__))
            {
               GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RW_R__R__, TRUE);
               if(remove(pcThumb) != 0)
               {
                  PRINTF("arch-ArchiveMotionHeron():Error deleting [%s]" CRLF, pcThumb);
                  LOG_Report(errno, "ARC", "arch-ArchiveMotionHeron():ERROR deleting %s ", pcThumb);
               }
               else PRINTF("arch-ArchiveMotionHeron():Delete [%s]: Done" CRLF, pcThumb);
            }
            else
            {
               PRINTF("arch-ArchiveMotionHeron():Copy ERROR [%s]" CRLF, strerror(errno));
               LOG_Report(errno, "ARC", "arch-ArchiveMotionHeron():Copy ERROR [%s] to [%s]", pcThumb, pcDest);
            }
            //
            // Added to archived today
            //
            pstMap->G_iArchiveAdd++;
            //
            // Video and Thumb files moved to archive: report back to PiKrellCam through the command fifo
            //
            GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "Archived %s_%s.*", pcBase, stTs.cType);
            RPI_InformFifo(NULL, (char *)"2 3 0", pcDest, 4);
            //
            PRINTF("arch-ArchiveMotionHeron():Archive [%s_%s] Completed." CRLF, pcBase, stTs.cType);
            LOG_Report(0, "ARC", "arch-ArchiveMotionHeron():Archive [%s_%s] Completed.", pcBase, stTs.cType);
         }
         else
         {
            PRINTF("arch-ArchiveMotionHeron():Copy Video [%s] to [%s] ERROR:[%s]" CRLF, pcVideo, pcDest, strerror(errno));
            LOG_Report(errno, "ARC", "arch-ArchiveMotionHeron()Copy Video [%s] to [%s] ERROR", pcVideo, pcDest);
         }
      }
      else
      {
         PRINTF("arch-ArchiveMotionHeron():Problems archiving to [%s]" CRLF, pcArchive);
         LOG_Report(0, "ARC", "arch-ArchiveMotionHeron():Problems archiving to [%s]", pcArchive);
         iNumFiles = -1;
      }
   }
   else
   {
      PRINTF("arch-ArchiveMotionHeron():NO files in [%s]" CRLF, pcVideo);
   }
   if(pcDest)  safefree(pcDest);
   if(pcBase)  safefree(pcBase);
   if(pcThumb) safefree(pcThumb);
   if(pcVideo) safefree(pcVideo);
   //
   RPI_FreeDirEntries(pstDir);
   return(iNumFiles);
}

// 
// Function:   arch_ArchiveMotionSkip
// Purpose:    Do NOT archive this pikrellcam video/Thumbnail file
// 
// Parameters: 
// Returns:    Cc
// Note:       
// 
static int arch_ArchiveMotionSkip()
{
   int     *piState;
   int     *piCount;
   char    *pcVideo;
   char    *pcFile;

   piState = (int *)GLOBAL_GetParameter(PAR_MOT_STATE);
   piCount = (int *)GLOBAL_GetParameter(PAR_ARC_COUNT);
   //
   pcVideo = (char *)GLOBAL_GetParameter(PAR_LAST_VID);
   RPI_LogTimestamp("arch_ArchiveMotionSkip()", pcVideo, &(pstMap->G_stTs));
   //
   pcFile = RPI_FindFilename(pcVideo);
   PRINTF("arch-ArchiveMotionSkip():%s:Motion=%d" CRLF, pcFile, *piState);
   LOG_Report(0, "ARC", "arc-ArchiveMotionSkip():%3d-%s:Motion=%d", *piCount, pcFile, *piState);
   return(EXIT_CC_OKEE);
}

//
// Function:   arch_ArchiveMotionLocal
// Purpose:    Archive this video/Thumbnail file using local processing
// 
// Parameters: 
// Returns:    Cc
// Note:       The filenames (video/thumbs/motion/emails) will be constructed from the
//             initial ON_MOTION_BEGIN timestamp !
// 
static int arch_ArchiveMotionLocal()
{
   bool     fOkee=TRUE;
   int      iCc=EXIT_CC_GEN_ERROR;
   int     *piCount;
   char    *pcLoop;
   char    *pcVideo;
   char    *pcThumb;
   char    *pcArchive;
   char    *pcBase;
   char    *pcDest;

   //
   // Get pointers to global storage locations:
   //
   pcArchive = (char *)GLOBAL_GetParameter(PAR_ARCHIVE);      // The Archive directory
   pcLoop    = (char *)GLOBAL_GetParameter(PAR_LOOP);         // The RAMFS Loop directory
   pcThumb   = (char *)GLOBAL_GetParameter(PAR_LAST_THB);     // The Source thumb full filepath
   pcVideo   = (char *)GLOBAL_GetParameter(PAR_LAST_VID);     // The Source video full filepath
   //
   // All time elements in the filename have been converted: archive this
   // contents (video/thumbs/motion/emails)
   //
   pcBase = (char *) safemalloc(MAX_PATH_LENZ);    // file base name "loop_YYYY-MM-DD_hh.mm.ss"
   pcDest = (char *) safemalloc(MAX_PATH_LENZ);    // path for destination dirs "/all/public/www/....."
   //
   // The actual file base name (loop_YYYY-MM-DD_hh.mm.ss) --> pcBase
   //
   GEN_SNPRINTF(pcBase, MAX_PATH_LEN, "loop_%s-%s-%s_%s.%s.%s", 
                  pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth,  pstMap->G_stTs.cDay,
                  pstMap->G_stTs.cHour, pstMap->G_stTs.cMinute, pstMap->G_stTs.cSecond);
   //
   RPI_LogTimestamp("arch_ArchiveMotionLocal():", pcBase, &(pstMap->G_stTs));
   //      
   PRINTF("=========================================================" CRLF);
   PRINTF("arch-ArchiveMotionLocal():Archive Event:" CRLF);
   PRINTF("Basefile : [%s]" CRLF, pcBase);
   PRINTF("Archive  : [%s]" CRLF, pcArchive);
   PRINTF("=========================================================" CRLF);
   //
   // First make sure all directories exist. If not, create and set owner/permissions
   // Permissions will be  0775 (drwxrwxr-x)
   //
   if(!RPI_DirectoryExists(pcArchive))  
   {
      // Create Archive dir "/all/public/www"
      fOkee = RPI_DirectoryCreate(pcArchive, ATTR_RWXRWXR_X, TRUE);
      if(fOkee) GEN_ChangeOwner(pcArchive, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
   }
   if(fOkee)
   {
      // Check or Create Archive dir "/all/public/www/YYYY"
      GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s", pcArchive, pstMap->G_stTs.cYear);
      if(!RPI_DirectoryExists(pcDest))  
      {
         fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
         if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
      }
   }
   if(fOkee)
   {
      // Check or Create Archive dir "/all/public/www/YYYY/MM"
      GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s", pcArchive, pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth);
      if(!RPI_DirectoryExists(pcDest))  
      {
         fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
         if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
      }
   }
   if(fOkee)
   {
      // Check or Create Archive dir "/all/public/www/YYYY/MM/DD"
      GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s", pcArchive, pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay);
      if(!RPI_DirectoryExists(pcDest))  
      {
         fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
         if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
      }
   }
   if(fOkee)
   {
      // Check or Create Archive dir "/all/public/www/YYYY/MM/DD/videos"
      GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/videos", pcArchive, pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay);
      if(!RPI_DirectoryExists(pcDest))  
      {
         fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
         if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
      }
   }
   if(fOkee)
   {
      // Check or Create Archive dir "/all/public/www/YYYY/MM/DD/thumbs"
      GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/thumbs", pcArchive, pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay);
      if(!RPI_DirectoryExists(pcDest))  
      {
         fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
         if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
      }
   }
   if(fOkee)
   {
      // Check or Create Archive dir "/all/public/www/YYYY/MM/DD/emails"
      GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/emails", pcArchive, pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay);
      if(!RPI_DirectoryExists(pcDest))  
      {
         fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
         if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
      }
   }
   if(fOkee)
   {
      // Check or Create Archive dir "/all/public/www/YYYY/MM/DD/motion"
      GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/motion", pcArchive, pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay);
      if(!RPI_DirectoryExists(pcDest))  
      {
         fOkee = RPI_DirectoryCreate(pcDest, ATTR_RWXRWXR_X, TRUE);
         if(fOkee) GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RWXRWXR_X, TRUE);
      }
   }
   if(fOkee)
   {
      // ==========================================================================================
      // OKee: all directories exist, move files to archive
      //    pcBase is base filename:   "YYYY-MM-DD_hh.mm.dd"
      //    The cType is the motion type, and since we consider this motion, set it to "m"
      // ==========================================================================================
      piCount = (int *)GLOBAL_GetParameter(PAR_ARC_COUNT);
      (*piCount)++;
      //
      // Move Video file to archive: 
      //    Source:        <Loop path> / "videos" / <base filename> "_m.mp4"
      //    Destination:   <Archive path> / YYYY / MM / DD / "videos" / <base filename> "_m.mp4"
      //
      GEN_SNPRINTF(pcVideo, MAX_PATH_LEN, "%s/videos/%s_m.mp4", pcLoop, pcBase);
      GEN_SNPRINTF(pcDest,  MAX_PATH_LEN, "%s/%s/%s/%s/videos/%s_m.mp4", pcArchive, 
                     pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay, pcBase);
      if(GEN_FileCopyAttributes(pcDest, pcVideo, ATTR_RW_R__R__))
      {
         GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RW_R__R__, TRUE);
         if(remove(pcVideo) != 0)
         {
            PRINTF("arch-ArchiveMotionLocal():Error deleting [%s]" CRLF, pcVideo);
            LOG_Report(errno, "ARC", "arch-ArchiveMotionLocal():ERROR deleting %s ", pcVideo);
         }
         //
         // Move Thumbs file to archive: 
         //    Source:        <Loop path> / "thumbs" / <base filename> "_m.th.jpg"
         //    Destination:   <Archive path> / YYYY / MM / DD / "thumbs" / <base filename> "_m.th.jpg"
         //
         GEN_SNPRINTF(pcThumb, MAX_PATH_LEN, "%s/thumbs/%s_m.th.jpg", pcLoop, pcBase);
         GEN_SNPRINTF(pcDest,  MAX_PATH_LEN, "%s/%s/%s/%s/thumbs/%s_m.th.jpg", pcArchive, 
                        pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay, pcBase);
         if(GEN_FileCopyAttributes(pcDest, pcThumb, ATTR_RW_R__R__))
         {
            GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RW_R__R__, TRUE);
            if(remove(pcThumb) != 0)
            {
               PRINTF("arch-ArchiveMotionLocal():Error deleting [%s]" CRLF, pcThumb);
               LOG_Report(errno, "ARC", "arch-ArchiveMotionLocal():ERROR deleting %s ", pcThumb);
            }
         }
         else
         {
            PRINTF("arch-ArchiveMotionLocal():Copy ERROR [%s]" CRLF, strerror(errno));
            LOG_Report(errno, "ARC", "arch-ArchiveMotion():Copy ERROR [%s] to [%s]", pcThumb, pcDest);
         }
         //
         // Move JPG Mail attachment file to archive
         //    Source:        <Loop path> / "thumbs" / <base filename> ".mail.jpg"
         //    Destination:   <Archive path> / YYYY / MM / DD / "thumbs" / <base filename> ".mail.jpg"
         //
         //
         // Move JPG Mail attachment file to archive (emails) folder
         // Move : *_yyyy-mm-dd_hh.mm.ss.mail.jpg
         //
         GEN_SNPRINTF(pcThumb, MAX_PATH_LEN, "%s/thumbs/%s.mail.jpg", pcLoop, pcBase);
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/emails/%s.mail.jpg", pcArchive, 
                        pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay, pcBase);
         if(GEN_FileCopyAttributes(pcDest, pcThumb, ATTR_RW_R__R__))
         {
            GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RW_R__R__, TRUE);
            if(remove(pcThumb) != 0)
            {
               PRINTF("arch-ArchiveMotionLocal():Error deleting [%s]" CRLF, pcThumb);
               LOG_Report(errno, "ARC", "arch-ArchiveMotionLocal():ERROR deleting %s ", pcThumb);
            }
         }
         else
         {
            PRINTF("arch-ArchiveMotionLocal():Copy ERROR [%s]" CRLF, strerror(errno));
            LOG_Report(errno, "ARC", "arch-ArchiveMotion():Copy ERROR [%s] to [%s]", pcThumb, pcDest);
         }
         //
         // Save Motion data to archive
         //    Source:        <Loop path> / "motion" / <base filename> "_m.txt"
         //    Destination:   <Archive path> / YYYY / MM / DD / "motion" / <base filename> "_m.txt"
         //
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/motion/%s_m.txt", pcLoop, pcBase);
         RPI_SaveMotionToFile(pcDest);
         //
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "%s/%s/%s/%s/motion/%s_m.txt", pcArchive, 
                        pstMap->G_stTs.cYear, pstMap->G_stTs.cMonth, pstMap->G_stTs.cDay, pcBase);
         RPI_SaveMotionToFile(pcDest);
         //
         // Added to archived today
         //
         pstMap->G_iArchiveAdd++;
         //
         // Video, Thumb and Motion files moved to archive: report back to PiKrellCam through the command fifo
         //
         GEN_SNPRINTF(pcDest, MAX_PATH_LEN, "Archived %s.*", pcBase);
         RPI_InformFifo(NULL, (char *)"2 3 0", pcDest, 4);
         //
         PRINTF("arch-ArchiveMotionLocal():Archive [%s]" CRLF, pcBase);
         LOG_Report(0, "ARC", "arch-ArchiveMotionLocal():Archive [%s]", pcBase);
         iCc = EXIT_CC_OKEE;
      }
      else
      {
         PRINTF("arch-ArchiveMotionLocal():Copy Video [%s] to [%s] ERROR:[%s]" CRLF, pcVideo, pcDest, strerror(errno));
         LOG_Report(errno, "ARC", "arch-ArchiveMotionLocal()Copy Video [%s] to [%s] ERROR", pcVideo, pcDest);
      }
   }
   else
   {
      PRINTF("arch-ArchiveMotionLocal():Problems archiving to [%s]" CRLF, pcArchive);
      LOG_Report(0, "ARC", "arch-ArchiveMotionLocal():Problems archiving to [%s]", pcArchive);
   }
   safefree(pcDest);
   safefree(pcBase);
   return(iCc);
}

//
// Function:   arch_ArchiveThreshold
// Purpose:    Check if this motion was beyond the archive threshold
// 
// Parameters: 
// Returns:    Region which violates the threshold and archiving is called for.
//             -1 if not enough motion
// Note:       Motion thresholds:
//             Magnitude   G_iTholdMag
//             Count       G_iTholdCnt
//             Motion      G_iTholdMot
// 
static int arch_ArchiveThreshold()
{
   int      x, iReg=-1;
   int      iThMag, iThCnt, iThMot;
   int      iThMagMax=0, iThCntMax=0, iThMotMax=0;
   PIKREG  *pstReg;

   pstReg = pstMap->G_stMotion.stFrame.stRegion;
   //
   arch_UpdateCsvFile(ARC_STATE_INIT,        NULL);
   arch_UpdateCsvFile(ARC_STATE_TIMESTAMP, &(pstMap->G_stTs));
   arch_UpdateCsvFile(ARC_STATE_SETTINGS,    NULL);
   arch_UpdateCsvFile(ARC_STATE_FRAMES,    &(pstMap->G_stMotion));
   //
   for(x=0; x<MAX_MOTION_REGIONS; x++, pstReg++)
   {
      iThMag = pstReg->iMagnitude;
      iThCnt = pstReg->iCount;
      iThMot = iThMag * iThCnt;
      //
      if(iThMot > iThMotMax)
      {
         // Keep track of max motion
         iReg = x;
         iThMagMax = iThMag;
         iThCntMax = iThCnt;
         iThMotMax = iThMot;
      }
      arch_UpdateCsvFile(ARC_STATE_MAGCNT, pstReg);
   }
   arch_UpdateCsvFile(ARC_STATE_MOTION, &iThMotMax);
   //
   // Check if max motion exceeds the thresholds
   //
   if( (iThMagMax > pstMap->G_iTholdMag) && (iThCntMax > pstMap->G_iTholdCnt) && (iThMotMax > pstMap->G_iTholdMot) )
   {
      PRINTF("arch-ArchiveThreshold():Region-%d exceeds threshold: Mag=%d,Cnt=%d,Mot=%d" CRLF, iReg, iThMagMax, iThCntMax, iThMotMax);
      LOG_Report(0, "ARC", "arch-ArchiveThreshold():Region-%d exceeds threshold: Mag=%d,Cnt=%d,Mot=%d", iReg, iThMagMax, iThCntMax, iThMotMax);
      arch_UpdateCsvFile(ARC_STATE_TRIGGER, (void *)pcMotionYes);
   }
   else 
   {
      iReg = -1;
      //
      PRINTF("arch-ArchiveThreshold():NOT Exceeding: Mag=%d,Cnt=%d,Mot=%d" CRLF, iThMagMax, iThCntMax, iThMotMax);
      LOG_Report(0, "ARC", "arch-ArchiveThreshold():NOT Exceeding: Mag=%d,Cnt=%d,Mot=%d", iThMagMax, iThCntMax, iThMotMax);
      arch_UpdateCsvFile(ARC_STATE_TRIGGER, (void *)pcMotionNo);
   }
   return(iReg);
}

//
// Function:   arch_UpdateCsvFile
// Purpose:    Update the CSV file, if enabled
// 
// Parameters: ARCCSV state, usefull pointer
// Returns:    
// Note:       
// 
static void arch_UpdateCsvFile(ARCCSV tState, void *pvData)
{
   static FILE *ptFile=NULL;
   //
   PIKTIM  *pstTs;
   PIKREG  *pstReg;
   PIKMOT  *pstMot;

   if(RPI_GetFlag(FLAGS_MOTION_CSV))
   {
      PRINTF("arch-UpdateCsvFile():%d" CRLF, tState);
      switch(tState)
      {
         default:
         case ARC_STATE_INIT:
            // Init, waiting for initial time stamp
            if(!ptFile) 
            {
               PRINTF("arch-UpdateCsvFile():Starting" CRLF);
               LOG_Report(0, "ARC", "arch-UpdateCsvFile():Starting");
               ptFile = safefopen((char *)pcCsvFile, "w");
            }
            break;

         case ARC_STATE_TIMESTAMP:
            if(ptFile) 
            {
               // Add timestamp data
               pstTs = (PIKTIM *) pvData;
               GEN_FPRINTF(ptFile, "%s,%s,%s,%s,%s,%s", 
                  pstTs->cYear,  pstTs->cMonth,  pstTs->cDay,
                  pstTs->cHour,  pstTs->cMinute, pstTs->cSecond);
            }
            break;

         case ARC_STATE_SETTINGS:
            if(ptFile) 
            {
               // Add Settings data
               GEN_FPRINTF(ptFile, ",%d,%d,%d, ",
                  pstMap->G_iTholdMag, pstMap->G_iTholdCnt, pstMap->G_iTholdMot);
            }
            break;

         case ARC_STATE_FRAMES:
            if(ptFile) 
            {
               // Add motion data
               pstMot = (PIKMOT *)pvData;
               GEN_FPRINTF(ptFile, ",%d", pstMot->iNumFrames);
            }
            break;

         case ARC_STATE_MAGCNT:
            if(ptFile) 
            {
               // Add motion data
               pstReg = (PIKREG *)pvData;
               GEN_FPRINTF(ptFile, ",%d,%d", pstReg->iMagnitude, pstReg->iCount);
            }
            break;

         case ARC_STATE_MOTION:
            if(ptFile) 
            {
               // Add motion data
               GEN_FPRINTF(ptFile, ",%d", *(int *)pvData);
            }
            break;

         case ARC_STATE_TRIGGER:
            if(ptFile) 
            {
               // Add motion data
               GEN_FPRINTF(ptFile, ",%s\n", (const char *)pvData);
               safefflush(ptFile);

            }
            break;

         case ARC_STATE_CLOSE:
            if(ptFile) 
            {
               // Close for exit
               safefclose(ptFile);
               ptFile = NULL;
            }
            break;
      }
   }
   else  // RPI_GetFlag(FLAGS_MOTION_CSV)
   {
      if(ptFile)
      {
         safefclose(ptFile);
         ptFile = NULL;
      }
   }
}

/*----------------------------------------------------------------------
_______________SIGNAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   arch_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool arch_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &arch_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "ARC", "cmd-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &arch_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "ARC", "cmd-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &arch_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "ARC", "cmd-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &arch_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "ARC", "cmd-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &arch_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "ARC", "cmd-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   arch_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void arch_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   arch_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void arch_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "ARC", "cmd-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_ARCHIVE);
}

//
// Function:   arch_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void arch_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "ARC", "cmd-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_ARCHIVE);
}

//
// Function:   arch_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void arch_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   arch_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void arch_ReceiveSignalUser2(int iSignal)
{
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT

#endif   //COMMENT
