/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           pages.h
 *  Purpose:            Dynamic HTML snd JSON pages
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Nov 2021:      Ported from rpihue
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// The default (empty) URL page MUST be the first in this list
//
//          tUrl,             tType,         iTimeout,   iFlags,        pcUrl,               pfDynCb;
EXTRACT_DYN(DYN_HTML_EMPTY,   HTTP_HTML,     0,          DYN_FLAG_PORT, "",                  http_DynPageEmpty          )
EXTRACT_DYN(DYN_HTML_INFO,    HTTP_HTML,     0,          DYN_FLAG_PORT, "info.html",         http_DynPageDefault        )
EXTRACT_DYN(DYN_JSON_INFO,    HTTP_JSON,     0,          DYN_FLAG_PORT, "info.json",         http_DynPageDefault        )
EXTRACT_DYN(DYN_NONE_PARMS,   HTTP_JSON,     0,          DYN_FLAG_PORT, "parms",             http_DynPageParms          )
EXTRACT_DYN(DYN_JSON_PARMS,   HTTP_JSON,     0,          DYN_FLAG_PORT, "parms.json",        http_DynPageParms          )
EXTRACT_DYN(DYN_HTML_PARMS,   HTTP_HTML,     0,          DYN_FLAG_PORT, "parms.html",        http_DynPageParms          )
EXTRACT_DYN(DYN_NONE_LOG,     HTTP_HTML,     0,          DYN_FLAG_PORT, "log",               http_DynPageLog            )
EXTRACT_DYN(DYN_HTML_LOG,     HTTP_HTML,     0,          DYN_FLAG_PORT, "log.html",          http_DynPageLog            )
EXTRACT_DYN(DYN_JSON_LOG,     HTTP_JSON,     0,          DYN_FLAG_PORT, "log.json",          http_DynPageLog            )
EXTRACT_DYN(DYN_HTML_EXIT,    HTTP_HTML,     0,          DYN_FLAG_NONE, "exithue.html",      http_DynPageExitHue        )
EXTRACT_DYN(DYN_HTML_NEXIT,   HTTP_HTML,     0,          DYN_FLAG_NONE, "exit.html",         http_DynPageExit           )
EXTRACT_DYN(DYN_JSON_NEXIT,   HTTP_JSON,     0,          DYN_FLAG_PORT, "exit.json",         http_DynPageExit           )
EXTRACT_DYN(DYN_HTML_CSV,     HTTP_HTML,     0,          DYN_FLAG_PORT, "csv.html",          http_DynPageCsvFiles       )
EXTRACT_DYN(DYN_HTML_MAIL,    HTTP_HTML,     0,          DYN_FLAG_PORT, "mail.html",         http_DynPageMail           )
EXTRACT_DYN(DYN_NONE_MAIL,    HTTP_JSON,     0,          DYN_FLAG_PORT, "mail",              http_DynPageMail           )
EXTRACT_DYN(DYN_JSON_MAIL,    HTTP_JSON,     0,          DYN_FLAG_PORT, "mail.json",         http_DynPageMail           )
EXTRACT_DYN(DYN_HTML_MOT,     HTTP_HTML,     0,          DYN_FLAG_PORT, "motion.html",       http_DynPageMotion         )
EXTRACT_DYN(DYN_NONE_MOT,     HTTP_JSON,     0,          DYN_FLAG_PORT, "motion",            http_DynPageMotion         )
EXTRACT_DYN(DYN_JSON_MOT,     HTTP_JSON,     0,          DYN_FLAG_PORT, "motion.json",       http_DynPageMotion         )
EXTRACT_DYN(DYN_HTML_DATE,    HTTP_HTML,     0,          DYN_FLAG_PORT, "date.html",         http_DynPageDateTime       )
EXTRACT_DYN(DYN_NONE_DATE,    HTTP_JSON,     0,          DYN_FLAG_PORT, "date",              http_DynPageDateTime       )
EXTRACT_DYN(DYN_JSON_DATE,    HTTP_JSON,     0,          DYN_FLAG_PORT, "date.json",         http_DynPageDateTime       )
EXTRACT_DYN(DYN_HTML_TIME,    HTTP_HTML,     0,          DYN_FLAG_PORT, "time.html",         http_DynPageDateTime       )
EXTRACT_DYN(DYN_NONE_TIME,    HTTP_JSON,     0,          DYN_FLAG_PORT, "time",              http_DynPageDateTime       )
EXTRACT_DYN(DYN_JSON_TIME,    HTTP_JSON,     0,          DYN_FLAG_PORT, "time.json",         http_DynPageDateTime       )
//
EXTRACT_DYN(DYN_NONE_PIDS,    HTTP_HTML,     0,          DYN_FLAG_PORT, "pids",              http_DynPagePids           )
EXTRACT_DYN(DYN_HTML_PIDS,    HTTP_HTML,     0,          DYN_FLAG_PORT, "pids.html",         http_DynPagePids           )
EXTRACT_DYN(DYN_HTML_PIDSR,   HTTP_HTML,     0,          DYN_FLAG_PORT, "pidsref.html",      http_DynPagePids           )

