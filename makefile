#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi PiKrellMan motion/archiving utility for PiKrellCam
#
#	Copyright (c) 2022 Peter Hillen 
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): Garden Kinshi
#
	BOARD_OK=yes
	TARGET=pikrellman
	COMMON=../common
	OBJM=rpi_mailx.o rpi_mail.o
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=-lssl -lcrypto
	LIBW=
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): Development RPi3+
#
	BOARD_OK=yes
	TARGET=pikrellman
	COMMON=../common
	OBJM=rpi_mailx.o rpi_mail.o
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=
	LIBW=
endif

ifeq ($(BOARD),12)
#
# RPi#12 (Rev-4-2GB): Carport PiKrellCam motion detect
#
	BOARD_OK=yes
	TARGET=pikrellman
	COMMON=../common
	OBJM=rpi_mailx.o rpi_mail.o
	OBJX=
	DEFS=-D FEATURE_IO_IQAUDIO
	LIBM=
	LIBW=-lwiringPi -lwiringPiDev
endif

ifeq ($(BOARD),14)
#
# RPi#14 (Rev-4-2GB): Pond PiKrellCam motion detect
#
	BOARD_OK=yes
	TARGET=pikrellman
	COMMON=../common
	OBJM=rpi_mailx.o rpi_mail.o
	OBJX=
	DEFS=-D FEATURE_IO_IQAUDIO
	LIBM=
	LIBW=-lwiringPi -lwiringPiDev
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	BOARD_OK=yes
	TARGET=pikrellman
	COMMON=../common
	OBJM=rpi_mailx.o rpi_mail.o
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=
	LIBW=
endif

CC		   = gcc
CFLAGS	= -O2 -Wall -g
DEFS	   += -D BOARD_RPI$(BOARD)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= -I$(COMMON)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS		= -lrt
LIBS		+= $(LIBM) 
LIBS		+= $(LIBW) 
LIBCOM	= -lcommon
DEPS		= config.h
OBJ		= rpi_main.o rpi_archive.o rpi_motion.o rpi_guard.o globals.o rpi_func.o rpi_hue.o \
				rpi_json.o rpi_srvr.o rpi_page.o gen_http.o http_func.o rpi_ping.o
OBJ		+= $(OBJM)
OBJ		+= $(OBJX)

debug: LIBCOM = -lcommondebug
debug: DEFS += -D FEATURE_LOG_PRINTF -D DEBUG_ASSERT
debug: all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBCOM) $(LDFLGS)

# Do NOT strip debug info, used for backtrace
#	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/*.c ./
	cp -u /mnt/rpi/softw/$(TARGET)/*.h ./
	cp -u /mnt/rpi/softw/$(TARGET)/makefile ./
	cp -u /mnt/rpi/softw/$(TARGET)/pikrelltest ./
	cp -u /mnt/rpi/softw/$(TARGET)/rebuild ./
	cp -u /mnt/rpi/softw/$(TARGET)/mailxtest ./
	cp -u /mnt/rpi/softw/$(TARGET)/pikrellman.service ./

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(TARGET)/*.c ./
	cp /mnt/rpi/softw/$(TARGET)/*.h ./
	cp /mnt/rpi/softw/$(TARGET)/makefile ./
	cp /mnt/rpi/softw/$(TARGET)/pikrelltest ./
	cp /mnt/rpi/softw/$(TARGET)/rebuild ./
	cp /mnt/rpi/softw/$(TARGET)/mailxtest ./
	cp /mnt/rpi/softw/$(TARGET)/pikrellman.service ./

ziplog:
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET)/$(TARGET).log

dellog:
	@echo "Delete logfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET)/$(TARGET).log

delmap:
	@echo "Delete mapfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).map
	sudo rm /mnt/rpicache/$(TARGET)/$(TARGET).map

clean:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	rm -rf *.o
	rm -rf $(DESTDIR)/$(TARGET)

restart:
	@echo "Restart: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo systemctl stop $(TARGET).service
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	@echo `date`: Restart RPi-$(TARGET) Board=RPi#$(BOARD) >>/usr/local/share/rpi/$(TARGET).log
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log 2>/dev/null >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm -rf /usr/local/share/rpi/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET)/$(TARGET).log
	sudo systemctl restart $(TARGET).service
	sudo systemctl daemon-reload

setup:
	sudo cp -u pikrellman.service /lib/systemd/system/pikrellman.service
	sudo systemctl enable pikrellman.service

install:
	@echo "Install: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo systemctl stop $(TARGET).service
	cp $(DESTDIR)/$(TARGET) $(DESTDIR)/$(TARGET).latest
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	@echo `date`: New Install RPi-$(TARGET) Board=RPi#$(BOARD) >>/usr/local/share/rpi/$(TARGET).log
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log 2>/dev/null >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm -rf /usr/local/share/rpi/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET)/$(TARGET).log
	sudo cp $(TARGET).service /lib/systemd/system/
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo systemctl restart $(TARGET).service
	sudo systemctl daemon-reload

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
