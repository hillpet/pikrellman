/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_ping.h
 *  Purpose:            Header file for ping thread, to keep Wifi network alive
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    29 Nov 2021:      Ported from pikdog
 *    24 Jan 2022:      Add Ping struct
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_PING_H_
#define _RPI_PING_H_

#define  ONEMINUTE               (60*1000)      // Timeout millisecs
#define  PING_FREQ_MINUTES       5              // Ping minutes
#define  PING_REBOOT_THRESHOLD   10             // Max no-wifi pings per 24 hours
//
typedef struct _ping_network_
{
   bool     fLog;                                  // Log ping
   int      iPingLogged;                           // Log only transients
   int      iPingMinutes;                          // Minutes between pings
   int      iRetries;                              // Ping retries
   int      iNoWifi;                               // No wifi counter
   int      iCountOk;                              // Wifi ping Yes counter
   int      iCountNo;                              // Wifi ping No  counter
   u_int32  ulLastWifi;                            // Last seen pring reply
}  PNET;
//
int         PING_Init            (void);

#endif /* _RPI_PING_H_ */
