/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           http_func.c
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Purpose:            Misc functions
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Nov 2021:      Ported from rpihue (rpi_func.c)
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_json.h"
//
#include "http_func.h"

//#define USE_PRINTF
#include <printx.h>

//
// Enums
//
enum
{
#define  EXTRACT_DYN(a,b,c,d,e,f)           a,
#include "pages.h"
#undef EXTRACT_DYN
   NUM_PAGES_DYNS
};
//
typedef bool (*HTTPFUN)(int);
//
typedef struct httpargs
{
   int            iId;
   int            iFunction;
   const char    *pcJson;
   const char    *pcHtml;
   int            iValueOffset;
   int            iValueSize;
   int            iChangedOffset;
   HTTPFUN        pfHttpFun;
}  HTTPARGS;
//
// Prototypes Collect Callback
//
static bool http_CollectHeron                (int);
static bool http_CollectHueCommand           (int);
static bool http_CollectDebug                (int);
static bool http_CollectDateTime             (int);
//
// Index list to global Arguments G_xxxx
//
static const HTTPARGS stHttpArguments[] =
{
//
//    Macro elements:               Example
//  
//       a  Enum              x     PAR_VERSION
//       b  iFunction         x     WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL
//       c  pcJson            x     "Version"
//       d  pcHtml            x     "vrs="
//       e  pcOption                "-v"
//       f  iValueOffset      x     offsetof(RPIMAP, G_pcSwVersion)
//       g  iValueSize        x     MAX_PARM_LEN
//       h  iChangedOffset    x     ALWAYS_CHANGED or offsetof(RPIMAP, G_ubVersionChanged)
//       i  Changed                 0
//       j  pcDefault               "v1.00-cr103"
//       k  pfHttpFun         x     http_CollectDebug
//
//       a                 b                                c           d        f                             g              h                                      k
//   
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    {a,b,c,d,f,g,h,k},
#include "par_defs.h"
#undef   EXTRACT_PAR
   {     NUM_GLOBAL_DEFS,  0,                               NULL,       NULL,    0,                            0,             0,                                     NULL}
};

//
// Local prototypes
//
static bool       rpi_HandleParm          (char *, FTYPE, int);
static bool       rpi_ParmSetChanged      (const HTTPARGS *, bool);
//
#ifdef  FEATURE_JSON_SHOW_REPLY
static void rpi_ShowNetReply              (const char *, int, char *);
#define SHOW_NET_REPLY(a,b,c)             rpi_ShowNetReply(a,b,c)
#else
#define SHOW_NET_REPLY(a,b,c)
#endif  //FEATURE_JSON_SHOW_REPLY
//
static const char pcWebPageCommandBusy[]  = "<p>Rpi Error: Ongoing command</p>" NEWLINE;
static const char pcWebPageCommandBad[]   = "<p>Rpi Error: Unknown command!</p>" NEWLINE;
//
static const char   *pcAmpAnd              = "&";
static const char   *pcParmDelims          = "=:";

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
// Function:   RPI_BuildJsonMessageArgs
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, Args 
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageArgs(NETCL *pstCl, int iArgs)
{
   bool            fCc;
   GLOPAR          tParm;
   char            cTemp[32];
   RPIDATA        *pstCam=NULL;
   const HTTPARGS *pstArgs=stHttpArguments;

   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL, NULL, JE_CURLB|JE_CRLF);
   for(tParm=0; tParm<NUM_GLOBAL_DEFS; tParm++)
   {
      if(pstArgs->iFunction & iArgs)
      {
         if(pstArgs->iFunction & PAR_A)
         {
            char *pcValue;

            // These are ASCII parms
            pcValue = GLOBAL_GetParameter(tParm);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():ASCII: Offset=0x%x %s=%p-->%s" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, pcValue, pcValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue,         JE_COMMA|JE_CRLF);
         }
         if(pstArgs->iFunction & PAR_B)
         {
            int  *piValue;

            // These are Int-BCD parms
            piValue = GLOBAL_GetParameter(tParm);
            GEN_SNPRINTF(cTemp, 31, "%d", *piValue);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():BCD-I: Offset=0x%x %s=%p-->%d" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, piValue, *piValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp,         JE_COMMA|JE_CRLF);
         }
         if(pstArgs->iFunction & PAR_H)
         {
            int  *piValue;

            // These are Int-HEX parms
            piValue = GLOBAL_GetParameter(tParm);
            GEN_SNPRINTF(cTemp, 31, "0x%x", *piValue);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():BCD-H: Offset=0x%x %s=%p-->0x%x" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, piValue, *piValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp,         JE_COMMA|JE_CRLF);
         }
         if(pstArgs->iFunction & PAR_F)
         {
            double  *pflValue;

            // These are float parms
            pflValue = GLOBAL_GetParameter(tParm);
            GEN_SNPRINTF(cTemp, 31, "%f", *pflValue);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():DOUBL: Offset=0x%x %s=%p-->%f" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, pflValue, *pflValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp,         JE_COMMA|JE_CRLF);
         }
      }
      pstArgs++;
   }
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageBody
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, variables mask
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageBody(NETCL *pstCl, int iArgs)
{
   bool     fCc;

   //PRINTF("RPI_BuildJsonMessageBody()" CRLF);
   fCc = RPI_BuildJsonMessageArgs(pstCl, iArgs);
   return(fCc);
}

//
// Function:   RPI_CollectParms
// Purpose:    Collect parameters passing from HTTP-data into the MMAP global area
//
// Parms:      Client area, HTTP-type
// Returns:    Nr of parms found
// Note:       
//             pstCl->pcUrl-> "/all/public/www/cam.json?Command=snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
//                             |               |   |    |
//             iPathIdx = -----+               |   |    |
//             iFileIdx = ---------------------+   |    |
//             iExtnIdx = -------------------------+    |
//             iPaylIdx = ------------------------------+
//  
int RPI_CollectParms(NETCL *pstCl, FTYPE tType)
{
   int             iNr=0;
   char           *pcParm;
   const HTTPARGS *pstArgs=stHttpArguments;

   //
   // Set first parameter
   //
   pstCl->iNextIdx = pstCl->iPaylIdx;
   //
   // Before collecting new parms, reset all changed markers
   //
   while(pstArgs->iFunction)
   {
      rpi_ParmSetChanged(pstArgs, 0);
      pstArgs++;
   }
   //
   // Retrieve and store all arguments
   //
   do
   {
      pcParm = HTTP_CollectGetParameter(pstCl);
      if(pcParm)
      {
         iNr++;
         rpi_HandleParm(pcParm, tType, PAR_ALL);
      }
   }
   while(pcParm);
   //
   //PRINTF("RPI_CollectParms(): %d arguments found" CRLF, iNr);
   return(iNr);
}

//
// Function:   RPI_ReportBusy
// Purpose:    Report that the server thread is busy
//
// Parms:      Socket descriptor
// Returns:    
// Note:       
//             
void RPI_ReportBusy(NETCL *pstCl)
{
   int      iIdx, tType;

   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBusy);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         RPI_BuildJsonMessageBody(pstCl, pstMap->G_iHttpArgs);
         break;
   }
}

//
// Function:   RPI_ReportError
// Purpose:    Report error
//
// Parms:      Socket descriptor
// Returns:    
// Note:       
//             
void RPI_ReportError(NETCL *pstCl)
{
   int      iIdx, tType;

   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         LOG_Report(0, "RPI", "RPI_ReportError():HTML");
         PRINTF("RPI_ReportError():HTML" CRLF);
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBad);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         LOG_Report(0, "RPI", "RPI_ReportError():JSON");
         PRINTF("RPI_ReportError():JSON" CRLF);
         RPI_BuildJsonMessageBody(pstCl, pstMap->G_iHttpArgs);
         break;
   }
}



/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
// Function:   rpi_HandleParm
// Purpose:    Lookup an argement in the main Args list and copy its value (if any) into the Global mmap area
//
// Parms:      Ptr to parm "XXX=500", HTTP_HTML|HTTP_JSON|..., PAR_xxx
// Returns:    TRUE if parm OKee
// Note:       
//             
static bool rpi_HandleParm(char *pcParm, FTYPE tType, int iFunction)
{
   bool            fCc=FALSE;
   GLOPAR          tParm;
   int             iSize;
   const char     *pcArg;
   const HTTPARGS *pstArgs=stHttpArguments;
   HTTPFUN         pfCallback;

   PRINTF("rpi_HandleParm(): Parm=<%s>" CRLF, pcParm);
   //
   for(tParm=0; tParm<NUM_GLOBAL_DEFS; tParm++)
   {
      //
      // iFunction is the bitwise function-filter (PAR_xxx)
      //
      if(pstArgs->iFunction & iFunction)
      {
         switch(tType)
         {
            case HTTP_JSON:   
               pcArg = pstArgs->pcJson; 
               break;
            
            case HTTP_HTML:
            default:          
               pcArg = pstArgs->pcHtml; 
               break;
         }
         //
         // Check parameter_to_variable delimiter "=" or ":"
         //
         iSize = GEN_SizeToDelimiters(pcParm, (char *)pcParmDelims);
         if(iSize == 0)
         {
            //
            // No delimiter: check variable without value (flag, ...)
            //
            iSize = GEN_SizeToDelimiters(pcParm, (char *)pcAmpAnd);
         }
         if( (iSize > 0) && (GEN_STRNCMPI(pcParm, pcArg, iSize) == 0) )
         {
            //PRINTF("rpi_HandleParm(): cmp(%s,%s,%d) match" CRLF, pcArg, pcParm, iSize);
            //
            // Option found: copy value
            //
            pcArg = GEN_FindDelimiters(pcParm, (char *)pcParmDelims);
            if(pcArg)
            {
               if(pstArgs->iFunction & PAR_A)
               {
                  char *pcValue;

                  // These are ASCII parms
                  pcValue = GLOBAL_GetParameter(tParm);
                  GEN_CopyToDelimiter((char)*pcAmpAnd, pcValue, (char *)pcArg, pstArgs->iValueSize);
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi_HandleParm(): Ascii: Opt=<%s>:Value=<%s>" CRLF, pcParm, pcValue);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_B)
               {
                  int  *piValue;

                  // These are Int-BCD parms
                  piValue = GLOBAL_GetParameter(tParm);
                 *piValue = (int)strtol(pcArg, NULL, 10);
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi_HandleParm(): BCD-Int: Opt=<%s>:Value=%d" CRLF, pcParm, *piValue);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_H)
               {
                  int  *piValue;

                  // These are Int-HEX parms
                  piValue = GLOBAL_GetParameter(tParm);
                 *piValue = (int)strtol(pcArg, NULL, 16);
                  PRINTF("rpi_HandleParm(): BCD-Hex: Opt=<%s>:Value=0x%x" CRLF, pcParm, *piValue);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_F)
               {
                  double  *pflValue;

                  // These are float parms
                  pflValue = GLOBAL_GetParameter(tParm);
                 *pflValue = (double)strtod(pcArg, NULL);
                  PRINTF("rpi_HandleParm(): Float: Opt=<%s>:Value=%f" CRLF, pcParm, *pflValue);
                  fCc = TRUE;
               }
            }
            else
            {
               //
               // ParameterX without value:
               //    "ParameterX&ParameterY=12033"
               //    "ParameterX"
               //
               if(pstArgs->iFunction & PAR_A)
               {
                  char *pcValue;

                  // These are ASCII parms
                  pcValue = GLOBAL_GetParameter(tParm);
                  GEN_STRCPY(pcValue, "1");
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi_HandleParm(): Ascii: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_B)
               {
                  int  *piValue;

                  // These are Int-BCD parms
                  piValue = GLOBAL_GetParameter(tParm);
                 *piValue = 1;
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi_HandleParm(): BCD-Int: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_H)
               {
                  int  *piValue;

                  // These are Int-HEX parms
                  piValue = GLOBAL_GetParameter(tParm);
                 *piValue = 1;
                  PRINTF("rpi_HandleParm(): BCD-Hex: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_F)
               {
                  double  *pflValue;

                  // These are float parms
                  pflValue = GLOBAL_GetParameter(tParm);
                 *pflValue = 1.0;
                  PRINTF("rpi_HandleParm(): Double: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }
            }
            //
            // Call the parameter callback function, if any
            //
            pfCallback = pstArgs->pfHttpFun;
            if(pfCallback) 
            {
               PRINTF("rpi_HandleParm(): Id=%d" CRLF, pstArgs->iId);
               fCc = pfCallback(pstArgs->iId);
            }
            //
            // Arg found: no need to search for more !
            //
            break;
         }
         else
         {
            //PRINTF("rpi_HandleParm(): cmp(%s,%s,%d) NO match" CRLF, pcArg, pcParm, iSize);
         }
      }
      pstArgs++;
   }
   return(fCc);
}

//
// Function:   rpi_ParmSetChanged
// Purpose:    Set the parm changed marker
//
// Parms:      Parm ^, set/unset
// Returns:    TRUE if parm has changed successfully
// Note:       Some parms cannot be changed (NEVER_CHANGED)
//             Some are always volatile (ALWAYS_CHANGED)
//
static bool rpi_ParmSetChanged(const HTTPARGS *pstArgs, bool fChanged)
{
   u_int8     *pubChanged;
   int         iChangedOffset=pstArgs->iChangedOffset;

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         // Do not alter the change marker
         break;

      case NEVER_CHANGED:
         // Cannot alter the change marker
         if(fChanged)  fChanged = FALSE;
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
        *pubChanged = (u_int8)fChanged;
         fChanged   = TRUE;
         break;
   }
   return(fChanged);
}

#ifdef  FEATURE_JSON_SHOW_REPLY
/*
 * Function    : rpi_ShowNetReply
 * Description : Print out the network json payload reply
 *
 * Parameters  : Title, port, JSON Object
 * Returns     : 
 *
 */
static void rpi_ShowNetReply(const char *pcTitle, int iPort, char *pcData)
{
   LOG_printf("%s(p=%d):Data=%s" CRLF, pcTitle, iPort, pcData);
}
#endif   //FEATURE_JSON_SHOW_REPLY


/*------  Local functions separator ------------------------------------
_____________CALLBACK_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
// Function:   http_CollectHueCommand
// Purpose:    Callback function G_iHueCmd
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_CollectHueCommand(int iId)
{
   PRINTF("http-CollectHueCommand(): ID=%ld" CRLF, iId);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_SVR_HUE_RUN);
   return(TRUE);
}

//
// Function:   http_CollectDateTime
// Purpose:    Callback function dates & times
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_CollectDateTime(int iId)
{
   //
   PRINTF("http-CollectDateTime(): ID=%ld" CRLF, iId);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_SVR_HUE_DAT);
   return(TRUE);
}

//
// Function:   http_CollectDebug
// Purpose:    Callback function G_pcDebugMask
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_CollectDebug(int iId)
{
   GLOBAL_ConvertDebugMask(TRUE);
   //
   PRINTF("http-CollectDebug(): ID=%ld" CRLF, iId);
   return(TRUE);
}

//
// Function:   http_CollectHeron
// Purpose:    Callback function G_iHeronSecs
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_CollectHeron(int iId)
{
   PRINTF("http-CollectHeron(): ID=%ld" CRLF, iId);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_SVR_HST_HER);
   return(TRUE);
}




