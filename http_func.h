/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           http_func.h
 *  Purpose:            Headerfile for http_func
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Nov 2021:      Ported from rpihue (rpi_func.h)
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _HTTP_FUNC_H_
#define _HTTP_FUNC_H_

bool  RPI_BuildHtmlMessageBody               (NETCL *);
bool  RPI_BuildJsonMessageBody               (NETCL *, int);
bool  RPI_BuildJsonMessageArgs               (NETCL *, int);
bool  RPI_CheckDelete                        (void);
int   RPI_CollectParms                       (NETCL *, FTYPE);
void  RPI_ReportBusy                         (NETCL *);
void  RPI_ReportError                        (NETCL *);

#endif /* _HTTP_FUNC_H_ */
