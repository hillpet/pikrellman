/*  (c) Copyright:  2021 Patrn, Confidential Data
 *
 *  Workfile:           rpi_func.c
 *  Purpose:            Misc pikrellman functions
 *                      
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Jun 2021:      Created
 *    11 Jul 2021:      Motion data file has "_m.txt"
 *    25 Aug 2021:      Report FIFO error only once
 *    12 Nov 2021:      Remove mail client
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    14 Dec 2021:      Add RPI_AllocateArchivePath(), RPI_GetTimestampSecs()
 *    15 Dec 2021:      Add RPI_FileExists()
 *    20 May 2023:      Change FINFO_GetFileInfo()
 *    02 Oct 2024:      RPI_GetDirEntries() --> GEN_GetDirEntries()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <fnmatch.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_main.h"
#include "rpi_func.h"
//
//#define USE_PRINTF
#include <printx.h>

//#warning slash-slash-pwjh-Temp-changes 

extern const char   *pcNewUser;
extern const char   *pcGroupWww;
//
// Local functions
//
static int64   rpi_DeleteFiles            (PIKDIR *, char *, int *);
static int     rpi_ExtractData            (char, char *, char *, int);
static int64   rpi_RemoveFiles            (PIKDIR *, char *, int);
//
//
// Local arguments
//
// Video/Thumb/Motion filename template:
// Starts with the delimiter needed to synchronize template with filename
//
static const char   *pcPiKrellCamMask     = "_YYYY-MM-DD_hh.mm.ss_T.xxxxxx";
static const char   *pcInformTimeout      = "inform timeout";
static const char   *pcSepLine            = "===============================================================\n";

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   RPI_AllocateArchivePath
// Purpose:    Create an archive pathname
// 
// Parameters: Ts struct ptr, Filetype
// Returns:    Pointer to path
// Note:       Caller must free memory to returned pathname !!
// 
char *RPI_AllocateArchivePath(PIKTIM *pstTs, CTTYPE tType)
{
   bool     fCc=TRUE;
   char    *pcSubDir;
   char    *pcExt;
   char    *pcArch;
   char    *pcType;
   char    *pcPath=NULL;

   pcArch = GLOBAL_GetParameter(PAR_ARCHIVE);
   //
   switch(tType)
   {
      case CT_THUMB:
         pcSubDir = "thumbs";
         pcType   = "_m";
         pcExt    = "th.jpg";
         break;

      case CT_EMAIL:
         pcSubDir = "emails";
         pcType   = "";
         pcExt    = "mail.jpg";
         break;

      case CT_MOTION:
         pcSubDir = "motion";
         pcType   = "_m";
         pcExt    = "txt";
         break;

      case CT_VIDEO:
         pcSubDir = "videos";
         pcType   = "_m";
         pcExt    = "mp4";
         break;

      case CT_AUDIO:
         pcSubDir = "audios";
         pcType   = "_m";
         pcExt    = "mp3";
         break;

      default:
         fCc = FALSE;
         break;
   }
   //
   // All time elements in the filename have been converted
   //
   if(fCc)
   {
      pcPath = safemalloc(MAX_PATH_LENZ);
      GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s/%s/%s/%s/loop_%s-%s-%s_%s.%s.%s%s.%s", 
                        pcArch, pstTs->cYear, pstTs->cMonth, pstTs->cDay, pcSubDir,
                        pstTs->cYear, pstTs->cMonth, pstTs->cDay, pstTs->cHour, pstTs->cMinute, pstTs->cSecond, pcType, pcExt);
      //
      if(GEN_STRLEN(pcPath) >= MAX_PATH_LEN)
      {
         // Not enough space
         PRINTF("RPI-AllocateArchivePath():ERROR to little space in [%s]:" CRLF, pcPath);
         LOG_Report(0, "FUN", "RPI-AllocateArchivePath():ERROR to little space in [%s]:", pcPath);
         safefree(pcPath);
         pcPath = NULL;
      }
   }
   return(pcPath);
}

// 
// Function:   RPI_CleanupFiles
// Purpose:    Cleanup older timestamped motion files
//             
// Parameters: Dir path, file filter, NumDeleted
//             
// Returns:    Total deleted filesize
// Note:       
// 
int64 RPI_CleanupFiles(char *pcDir, char *pcFilter, int *piNumDeleted)
{
   bool     fSearchFiles=TRUE;
   int64    llSize=0;
   int      iNumFiles, iNumDirs;
   char    *pcDirCur;
   PIKDIR  *pstDir;
   PIKDIR  *pstCur;

   pcDirCur = safemalloc(MAX_PATH_LENZ);
   GEN_STRNCPY(pcDirCur, pcDir, MAX_PATH_LEN);
   do
   {
      iNumFiles = 0;
      iNumDirs  = 0;
      PRINTF("RPI-CleanupFiles():Dir=%s [filter=%s]" CRLF, pcDirCur, pcFilter);
      //pwjh pstDir = RPI_GetDirEntries(pcDirCur, pcFilter, &iNumFiles, &iNumDirs);
      pstDir = GEN_GetDirEntries(pcDirCur, pcFilter, &iNumFiles, &iNumDirs, PIK_ALPHA);
      PRINTF("RPI-CleanupFiles():%d Dirs, %d Files" CRLF, iNumDirs, iNumFiles);
      if(iNumFiles)
      {
         //
         // There are files and/or directories at this node: Delete the files first
         //
         llSize = rpi_DeleteFiles(pstDir, pcDirCur, piNumDeleted);
         if(llSize > 0)
         {
            PRINTF("RPI-CleanupFiles():%lld bytes of Files deleted from %s!" CRLF, llSize, pcDirCur);
         }
         else
         {
            PRINTF("RPI-CleanupFiles():NO Files were deleted from %s!" CRLF, pcDirCur);
         }
         fSearchFiles = FALSE;
      }
      else if(iNumDirs)
      {
         PRINTF("RPI-CleanupFiles():NO Files in %s, but %d Dirs" CRLF, pcDirCur, iNumDirs);
         //
         // There are no files at this node: Try first next node
         //
         pstCur       = pstDir;
         fSearchFiles = FALSE;
         while(pstCur && !fSearchFiles)
         {
            if(pstCur->fDir)
            {
               //
               // Another Dir node:
               //
               GEN_STRNCAT(pcDirCur, "/", MAX_PATH_LEN);
               GEN_STRNCAT(pcDirCur, pstCur->pcName, MAX_PATH_LEN);
               fSearchFiles = TRUE;
               PRINTF("RPI-CleanupFiles():Descent into Dir [%s]" CRLF, pcDirCur);
            }
            else pstCur = pstCur->pstNext;
         }
      }
      else
      {
         //
         // There are NO files and NO Dirs at this node. Delete it.
         //
         PRINTF("RPI-CleanupFiles():NO Files and NO Dirs at %s: DELETE" CRLF, pcDirCur);
         rpi_DeleteFiles(NULL, pcDirCur, NULL);
         fSearchFiles = FALSE;
      }
      RPI_FreeDirEntries(pstDir);
   }
   while(fSearchFiles);
   safefree(pcDirCur);
   return(llSize);
}

// 
// Function:   RPI_DirectoryCreate
// Purpose:    Create directory
// 
// Parameters: Dir path, Permissions, log yesno
// Returns:    TRUE if okee
// Note:       
// 
bool RPI_DirectoryCreate(char *pcDir, mode_t tPerm, bool fLog)
{
   bool  fCc=TRUE;
   int   iRes;

   if((iRes = mkdir(pcDir, tPerm)) != 0)
   {
      if(fLog)
      {
         PRINTF("RPI-DirectoryCreate():ERROR creating directory [%s]:%s" CRLF, pcDir, strerror(errno));
         LOG_Report(errno, "FUN", "RPI-DirectoryCreate():ERROR creating directory [%s]:", pcDir);
      }
      fCc = FALSE;
   }
   else 
   {
      if(fLog)
      {
         LOG_Report(0, "FUN", "RPI-DirectoryCreate():Dir [%s] created [0%o]:", pcDir, tPerm);
         PRINTF("RPI-DirectoryCreate():Dir [%s] created [0%o]" CRLF, pcDir, tPerm);
      }
   }
   return(fCc);
}

// 
// Function:   RPI_DirectoryExists
// Purpose:    Check if directory exists
// 
// Parameters: Dir path
// Returns:    TRUE if exists
// Note:       
// 
bool RPI_DirectoryExists(char *pcDir)
{
   bool        fCc=FALSE;
   int         iRes;
   struct stat stStat;

   iRes = stat(pcDir, &stStat);
   if(iRes == 0)
   {
      if(S_ISDIR(stStat.st_mode)) 
      {
         // Fine: Dir exists
         fCc = TRUE;
      }
   }
   return(fCc);
}

// 
// Function:   RPI_FileExists
// Purpose:    Check if file exists
// 
// Parameters: File path
// Returns:    TRUE if exists
// Note:       
// 
bool RPI_FileExists(char *pcFilePath)
{
   bool        fCc=FALSE;
   int         iRes;
   struct stat stStat;

   iRes = stat(pcFilePath, &stStat);
   if(iRes == 0)
   {
      if(S_ISREG(stStat.st_mode)) 
      {
         // Fine: File exists
         fCc = TRUE;
      }
   }
   return(fCc);
}

// 
// Function:   RPI_ExtractParameter
// Purpose:    Extract data from the latest pikrellcam video/thumbs filename
// 
// Parameters: Var type, file, dest, max_size
// Returns:    Dest act_size
// 
int RPI_ExtractParameter(PIKMSK eMask, char *pcFile, char *pcDest, int iMaxSize)
{
   int   iNr=0;

   switch(eMask)
   {
      case MASK_YEAR:
         iNr = rpi_ExtractData('Y', pcFile, pcDest, iMaxSize);
         break;

      case MASK_MONTH:
         iNr = rpi_ExtractData('M', pcFile, pcDest, iMaxSize);
         break;

      case MASK_DAY:
         iNr = rpi_ExtractData('D', pcFile, pcDest, iMaxSize);
         break;

      case MASK_HOUR:
         iNr = rpi_ExtractData('h', pcFile, pcDest, iMaxSize);
         break;

      case MASK_MINUTE:
         iNr = rpi_ExtractData('m', pcFile, pcDest, iMaxSize);
         break;

      case MASK_SECOND:
         iNr = rpi_ExtractData('s', pcFile, pcDest, iMaxSize);
         break;

      case MASK_TYPE:
         iNr = rpi_ExtractData('T', pcFile, pcDest, iMaxSize);
         break;

      case MASK_EXT:
         iNr = rpi_ExtractData('x', pcFile, pcDest, iMaxSize);
         break;

      default:
      case MASK_DELIM:
         break;
   }
   return(iNr);
}

// 
// Function:   RPI_ExtractTimestamp
// Purpose:    Extract timestamp data from a pikrellcam video/thumb/motion file
// 
// Parameters: Filename, struct ptr
// Returns:    TRUE if conversion is OKee
// Note:       Filename comes from the PiKrellCam format:
//             "xxxx_2021-12-15_14.45.59_x.xxx"
// 
bool RPI_ExtractTimestamp(char *pcFile, PIKTIM *pstTs)
{
   bool  fCc=TRUE;
   int   x, iNr=0;
   char *pcEnd;

   x = RPI_ExtractParameter(MASK_YEAR, pcFile, pstTs->cYear, PIKTIM_LEN);
   if(x == 4) 
   {
      pstTs->iYear = (int)strtol(pstTs->cYear, &pcEnd, 10);
      if(*pcEnd == 0) iNr += x;
      else            pstTs->iYear = -1;
   }
   else pstTs->iYear = -1;
   //
   x= RPI_ExtractParameter(MASK_MONTH, pcFile, pstTs->cMonth, PIKTIM_LEN);
   if(x == 2) 
   {
      pstTs->iMonth = (int)strtol(pstTs->cMonth, &pcEnd, 10);
      if(*pcEnd == 0) iNr += x;
      else            pstTs->iMonth = -1;
   }
   else pstTs->iMonth = -1;
   //
   x = RPI_ExtractParameter(MASK_DAY, pcFile, pstTs->cDay, PIKTIM_LEN);
   if(x == 2)
   {
      pstTs->iDay = (int)strtol(pstTs->cDay, &pcEnd, 10);
      if(*pcEnd == 0) iNr += x;
      else            pstTs->iDay = -1;
   }
   else pstTs->iDay = -1;
   //
   x = RPI_ExtractParameter(MASK_HOUR, pcFile, pstTs->cHour, PIKTIM_LEN);
   if(x == 2)
   {
      pstTs->iHour = (int)strtol(pstTs->cHour, &pcEnd, 10);
      if(*pcEnd == 0) iNr += x;
      else            pstTs->iHour = -1;
   }
   else pstTs->iHour = -1;
   //
   x = RPI_ExtractParameter(MASK_MINUTE, pcFile, pstTs->cMinute, PIKTIM_LEN);
   if(x == 2) 
   {
      pstTs->iMinute = (int)strtol(pstTs->cMinute, &pcEnd, 10);
      if(*pcEnd == 0) iNr += x;
      else            pstTs->iMinute = -1;
   }
   else pstTs->iMinute = -1;
   //
   x = RPI_ExtractParameter(MASK_SECOND, pcFile, pstTs->cSecond, PIKTIM_LEN);
   if(x == 2) 
   {
      pstTs->iSecond = (int)strtol(pstTs->cSecond, &pcEnd, 10);
      if(*pcEnd == 0) iNr += x;
      else            pstTs->iSecond = -1;
   }
   else pstTs->iSecond = -1;
   //
   iNr += RPI_ExtractParameter(MASK_TYPE, pcFile, pstTs->cType, PIKTIM_LEN);
   //
   if(iNr != 15) 
   {
      LOG_Report(0, "RPI", "RPI-ExtractTimestamp():ERROR [%s]:ONLY %d items", pcFile, iNr);
      PRINTF("RPI-ExtractTimestamp():ERROR [%s]:ONLY %d items" CRLF, pcFile, iNr);
      fCc = FALSE;
   }
   pstTs->fConverted = fCc;
   return(fCc);
}

// 
// Function:   RPI_ExtractVariables
// Purpose:    Extract data from a pikrellcam variables file
// 
// Parameters: Filename, variables struct, nr of variables
// Returns:    Number of variables retrieved
// 
int RPI_ExtractVariables(const char *pcFilename, const PIKVAR *pstVars, int iNumVars)
{
   FILE          *tFp;
   int            iIdx, iNr, iVars=0;
   char          *pcBuffer, *pcVar1, *pcVar2;
   const PIKVAR  *pstVarsTemp;

   //
   // It could be that PiKrellCam is not running, or the desired file has not (yet) been created.
   // Therefor do not safefopen the file to prevent flooding the Logfile with msgs.
   //
   if( (tFp = fopen((char *)pcFilename, "r")) != NULL)
   {
      pcBuffer = safemalloc(256); 
      pcVar1   = safemalloc(256); 
      pcVar2   = safemalloc(256); 
      //
      while(fgets(pcBuffer, 255, tFp) != NULL)
      {
         //PRINTF("RPI-ExtractVariables():[%s]", pcBuffer);
         iNr = GEN_SSCANF(pcBuffer, "%255s %255s", pcVar1, pcVar2);
         if(iNr == 2)
         {
            pstVarsTemp = pstVars;
            for(iIdx=0; iIdx<iNumVars; iIdx++, pstVarsTemp++)
            {
               if(GEN_STRCMP(pstVarsTemp->pcVar, pcVar1) == 0)
               {
                  if(GLOBAL_UpdateParameter(pstVarsTemp->tVar, pcVar2))
                  {
                     iVars++;
                     //PRINTF("RPI-ExtractVariables():store Var %2d [%s]=[%s]" CRLF, pstVarsTemp->tVar, pcVar1, pcVar2);
                     break;
                  }
               }
            }
         }
         else
         {
            //PRINTF("RPI-ExtractVariables():%d variables !", iNr);
         }
      }
      safefree(pcVar2); 
      safefree(pcVar1); 
      safefree(pcBuffer); 
      safefclose(tFp);
   }
   return(iVars);
}

// 
// Function:   RPI_FindFilename
// Purpose:    Find the filename from the pikrellcam file
// 
// Parameters: Ptr to whole path
// Returns:    Ptr to filename
// Note:       f.i: Path -> /mnt/rpipix/loop_2018-07-27_11.15.38_0.mp4
//                  Return -------------^
//
char *RPI_FindFilename(char *pcPath)
{
   char *pcFile=pcPath;

   while(*pcPath)
   {
      if(*pcPath++ == '/') pcFile = pcPath;
   }
   return(pcFile);
}

//
// Function:   RPI_FreeDirEntries
// Purpose:    Free node struct
//
// Parms:      Node struct
// Returns:    
// Note:       
//             
void RPI_FreeDirEntries(PIKDIR *pstDirs)
{
   PIKDIR  *pstCurr;

   while(pstDirs)
   {
      pstCurr = pstDirs->pstNext;
      safefree(pstDirs->pcName);
      safefree(pstDirs);
      pstDirs = pstCurr;
   }
}

#ifdef USE_RPI_GETDIRENTRIES
//
// Function:   RPI_GetDirEntries
// Purpose:    Get number of files and directories
//
// Parms:      Dir, Filter, Num Files ptr, Num Dirs ptr
// Returns:    Result struct ptr or NULL
// Note:       FINFO_GetDirEntries() are sorted on alphabetic order!
//             
PIKDIR *RPI_GetDirEntries(char *pcDirPath, char *pcFilter, int *piNumFiles, int *piNumDirs)
{
   bool     fDoStore;
   int      x, iNum, iSize;
   char     pcTemp[32];
   char    *pcFileName;
   char    *pcFilePath;
   void    *pvData;
   PIKDIR  *pstDirs=NULL;
   PIKDIR  *pstLast=NULL;
   PIKDIR  *pstCurr=NULL;

   pcFileName = safemalloc(MAX_PATH_LENZ);
   pcFilePath = safemalloc(MAX_PATH_LENZ);
   pvData     = FINFO_GetDirEntries(pcDirPath, pcFilter, &iNum);
   //PRINTF("rpi-GetDirEntries():Dir=[%s]:%d entries" CRLF, pcDirPath, iNum);
   //
   if(iNum > 0)
   {
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         GEN_SPRINTF(pcFilePath, "%s/%s", pcDirPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, pcTemp, 31);
         if(*pcTemp == 'F')
         {
            //PRINTF("rpi-GetDirEntries(): File [%s]" CRLF, pcFileName);
            if(piNumFiles) (*piNumFiles)++;
            fDoStore = TRUE;
         }
         else if( (*pcTemp == 'D') && (*pcFileName != '.'))
         {
            //PRINTF("rpi-GetDirEntries(): Dir  [%s]" CRLF, pcFileName);
            if(piNumDirs) (*piNumDirs)++;
            fDoStore = TRUE;
         }
         else
         {
            //PRINTF("rpi-GetDirEntries(): Skip [%s]" CRLF, pcFileName);
            fDoStore = FALSE;
         }
         if(fDoStore)
         {
            pstCurr = safemalloc(sizeof(PIKDIR));
            GEN_MEMSET(pstCurr, 0x00, sizeof(PIKDIR));
            //
            // Fill struct with info
            //
            iSize = GEN_STRLEN(pcFileName) + 32;
            pstCurr->pcName = safemalloc(iSize);
            GEN_STRCPY(pstCurr->pcName, pcFileName);
            pstCurr->fDir = (*pcTemp == 'D');
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, 31);
            pstCurr->llSize = strtoll(pcTemp, NULL, 10); 
            //
            // Keep the top of the linked list
            //
            if(pstDirs == NULL) 
            {
               pstDirs = pstCurr;
               pstLast = pstCurr;
            }
            else
            {
               pstLast->pstNext = pstCurr;
               pstLast          = pstCurr;
            }
         }
      }
   }
   if(pvData) FINFO_ReleaseDirEntries(pvData, iNum);
   safefree(pcFileName);
   safefree(pcFilePath);
   return(pstDirs);
}
#endif

// 
// Function:   RPI_GetFlag
// Purpose:    Decode the flag from the G_ PAR_ area
// 
// Parameters: Flag
// Returns:    Flag value
// Note:       
//
bool RPI_GetFlag(int iFlag)
{
   int      iFlags;

   iFlags = RPI_GetFlags();
   return((iFlags & iFlag) == iFlag);
}

// 
// Function:   RPI_GetFlags
// Purpose:    Get all flags from the G_ PAR_ area
// 
// Parameters: 
// Returns:    Flags
// Note:       
//
int RPI_GetFlags()
{
   int     *piFlags;
   int      iFlags;

   piFlags = GLOBAL_GetParameter(PAR_VERBOSE);
   iFlags  = *piFlags;
   iFlags &= ~VERBOSE_LEVEL_MASK;
   return(iFlags);
}

// 
// Function:   RPI_SetFlag
// Purpose:    Set/Reset the flag from the G_ PAR_ area
// 
// Parameters: Flag, on/off
// Returns:    
// Note:       
//
void RPI_SetFlag(int iFlag, bool fValue)
{
   int  *piFlags;

   piFlags = GLOBAL_GetParameter(PAR_VERBOSE);
   if(fValue) *piFlags |=  iFlag;
   else       *piFlags &= ~iFlag;
}

// 
// Function:   RPI_GetTimestampSecs
// Purpose:    Rework filename containing Date/Time to SECS-FROM-1970
// 
// Parameters: TIMPIK ptr
// Returns:    System secs
// Note:       
//
u_int32 RPI_GetTimestampSecs(PIKTIM *pstTs)
{
   u_int32  ulCurSecs;

   ulCurSecs  = RTC_GetSecs(pstTs->iYear, pstTs->iMonth, pstTs->iDay);
   ulCurSecs += (pstTs->iHour * 3600); 
   ulCurSecs += (pstTs->iMinute * 60);
   ulCurSecs +=  pstTs->iSecond;

   //pwjh if(RTC_DaylightSavingTime(ulCurSecs))
   //pwjh {
   //pwjh    //
   //pwjh    // PiKrellCam supplied the actual date/time stamp, including the DST !
   //pwjh    // Discard that here for the Secs since 1970
   //pwjh    //
   //pwjh    ulCurSecs -= ES_SECONDS_PER_HOUR;
   //pwjh }

   return(ulCurSecs);
}

// 
// Function:   RPI_GetVerboseLevel
// Purpose:    Decode the verbose level from the G_ PAR_ area
// 
// Parameters: 
// Returns:    Verbose level
// Note:       
//
int RPI_GetVerboseLevel()
{
   int     *piFlags;
   int      iVerbose, iFlags;

   piFlags   = GLOBAL_GetParameter(PAR_VERBOSE);
   iFlags    = *piFlags;
   iVerbose  = (iFlags & VERBOSE_LEVEL_MASK) >> VERBOSE_LEVEL_SHIFT;
   return(iVerbose);
}

// 
// Function:   RPI_InformFifo
// Purpose:    Send inform command to the pikrellcam FIFO
// 
// Parameters: FIFO (or NULL), Format, Message, duration
// Returns:    Cc
// Note:       Do NOT safeopen(pcFifo), since the FIFO will not be there if
//             PiKrellCam is switched OFF (through the web). It will flood the LOG file.
//
//             Build fifo cmd, f.i:
//             "inform timeout <duration>
//             "inform "AapNootMies" a b c d e"
//                      pcMsg        pcFrmt
//                                   a: (0) Row within region
//                                   b: (3) Justify (3=center)
//                                   c: (0) Font (0=small, 1=big)
//                                   d: (0) Xs
//                                   e: (0) Ys
// 
int RPI_InformFifo(const char *pcFifo, char *pcFrmt, char *pcMsg, int iDuration)
{
   static bool fInError=FALSE;
   //
   int         iLen, iCc=EXIT_CC_GEN_ERROR;
   int         iFd;
   char       *pcData;

   //
   // Get the PiKrellCam command FIFO if not given here
   //
   if(pcFifo == NULL) pcFifo = GLOBAL_GetParameter(PAR_FIFO);         
   //
	iFd = open(pcFifo, O_WRONLY|O_NONBLOCK);
   if(iFd > 0)
   {
      iLen   = GEN_STRLEN(pcInformTimeout) + GEN_STRLEN(pcMsg) + GEN_STRLEN(pcFrmt) + 64;
      pcData = (char *) safemalloc(iLen);
      //
      if(fInError)
      {
         // FIFO is in error mode: report if OKee again
         LOG_Report(0, "FUN", "RPI-InformFifo():FIFO %s is back.", pcFifo);
         fInError = FALSE;
      }
      //
      // First setup timeout
      // Send inform msg
      //
      GEN_SNPRINTF(pcData, iLen, "%s %d\ninform \"%s\" %s\n", pcInformTimeout, iDuration, pcMsg, pcFrmt);
      safewrite(iFd, pcData, GEN_STRLEN(pcData));
      //LOG_Report(0, "FUN", "RPI-InformFifo():[Msg=%s, Frm=%s]", pcMsg, pcFrmt);
      safeclose(iFd);
      safefree(pcData);
      iCc = EXIT_CC_OKEE;
   }
   else
   {
      if(fInError)
      {
         // FIFO is in error mode already do not report error
      }
      else
      {
         LOG_Report(errno, "FUN", "RPI-InformFifo():ERROR open FIFO %s", pcFifo);
         fInError = TRUE;
      }
   }
   return(iCc);
}

// 
// Function:   RPI_LogTimestamp
// Purpose:    Log motion data timestamped data
//             
// Parameters: Title, Filename, Timestamp
//             
// Returns:    
// Note:       
// 
void RPI_LogTimestamp(const char *pcTitle, char *pcFile, PIKTIM *pstTs)
{
   if(RPI_GetFlag(FLAGS_LOG_EVENT))
   {
      LOG_Report(0, "LOG", "%s:File=%s (Type %s) Date:%04d/%02d/%02d  Time:%02d:%02d:%02d", 
                  pcTitle, pcFile, pstTs->cType,
                  pstTs->iYear, pstTs->iMonth,  pstTs->iDay,
                  pstTs->iHour, pstTs->iMinute, pstTs->iSecond);
   }
}

// 
// Function:   RPI_RemoveFiles
// Purpose:    Remove older timestamped motion files
//             
// Parameters: Dir path, file filter, Number of files to remain (newest)
//             
// Returns:    Total deleted filesize
// Note:       
// 
int64 RPI_RemoveFiles(char *pcDir, char *pcFilter, int iNumStay)
{
   int      iNumRemove;
   int      iNumFiles=0;
   int      iNumDirs=0;
   int64    llSize=0;
   PIKDIR  *pstDir;

   PRINTF("RPI-RemoveFiles():Keep %d files, Dir=%s [filter=%s]" CRLF, iNumStay, pcDir, pcFilter);
   //pwjh pstDir = RPI_GetDirEntries(pcDir, (char *)pcFilter, &iNumFiles, &iNumDirs);
   pstDir = GEN_GetDirEntries(pcDir, pcFilter, &iNumFiles, &iNumDirs, PIK_ALPHA);
   PRINTF("RPI-RemoveFiles():%s has %d Dirs and %d Files" CRLF, pcDir, iNumDirs, iNumFiles);
   if(iNumFiles)
   {
      if(iNumStay < iNumFiles)
      {
         iNumRemove = iNumFiles - iNumStay;
         PRINTF("RPI-RemoveFiles():in [%s]: %d Files need to go !" CRLF, pcDir, iNumRemove);
         //
         // There are files at this node: Delete the oldest files to leave iNumStay
         //
         llSize = rpi_RemoveFiles(pstDir, pcDir, iNumRemove);
         if(llSize > 0)
         {
            //LOG_Report(0, "FUN", "RPI-RemoveFiles():%d files deleted from %s!", iNumRemove, pcDir);
            PRINTF("RPI-RemoveFiles():%d files deleted from %s!" CRLF, iNumRemove, pcDir);
         }
         else
         {
            LOG_Report(0, "FUN", "RPI-RemoveFiles():NO Files were deleted from %s!", pcDir);
            PRINTF("RPI-RemoveFiles():NO Files were deleted from %s!" CRLF, pcDir);
         }
      }
   }
   RPI_FreeDirEntries(pstDir);
   return(llSize);
}

//
// Function:   RPI_SaveMotionToFile
// Purpose:    Save all acquired motion data to file
//
// Parms:      Dest path
// Returns:    
// Note:
//    PIKMOT pstMap->G_stMotion:
//       int      iMotionEvents;                   // Bitfield
//       int      iNumFrames;                      // Total Frames
//       int      iBlocksHor;                      // Hor MBs
//       int      iBlocksVer;                      // Ver MBs
//       int      iBurstCount;                     // Burst count
//       int      iBurstFrames;                    // Burst frames
//       int      iMagLimit;                       // Magnitude limit
//       int      iMagCount;                       // Magnitude count
//       PIKFRM   stFrame                          // Frame
//       char     cMotionFile[MAX_PATH_LENZ];      // Filename
//    
//    PIKFRM *pstFrm
//       double   flSeconds;                       // Frame start in secs
//       //
//       int      iBurst;                          // Burst count
//       int      iAudio;                          // Audio Trigger
//       int      iExternal;                       // External trigger
//       //
//       PIKREG   stCanvas;                        // Whole frame
//       PIKREG   stRegion[MAX_MOTION_REGIONS];    // Regions 0...MAX_MOTION_REGIONS-1
//    
//    PIKREG *pstReg
//       int      iX;                              // MB X
//       int      iY;                              // MB Y
//       int      idX;                             // MB dX
//       int      idY;                             // MB dY
//       int      iMagnitude;                      // Magnitude
//       int      iCount;                          // Count
//
//
void RPI_SaveMotionToFile(char *pcDest)
{
   int      y;
   u_int32  ulSecs;
   FILE    *ptFile;
   PIKFRM  *pstFrm;
   PIKREG  *pstReg;
   char     cDandT[32];
   PIKMOT  *pstMot=&(pstMap->G_stMotion);

   PRINTF("RPI-SaveMotionToFile():%s" CRLF, pcDest);
   ptFile = safefopen(pcDest, "a+");
   if(ptFile)
   {
      RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cDandT, pstMot->ulMotionSecs);
      //
      // Put out full Motion data
      //
      GEN_FPRINTF(ptFile, "Save Motion:%s\n", pstMot->cMotionFile);
      GEN_FPRINTF(ptFile, pcSepLine);
      GEN_FPRINTF(ptFile, "   Motion Time    : %s\n", cDandT);
      GEN_FPRINTF(ptFile, "   Motion Events  : 0x%08X\n", pstMot->iMotionEvents);
      GEN_FPRINTF(ptFile, "   Frames         : %d\n", pstMot->iNumFrames);
      GEN_FPRINTF(ptFile, "   Blocks Hor     : %d\n", pstMot->iBlocksHor);
      GEN_FPRINTF(ptFile, "   Blocks Ver     : %d\n", pstMot->iBlocksVer);
      GEN_FPRINTF(ptFile, "   Burst Count    : %d\n", pstMot->iBurstCount);
      GEN_FPRINTF(ptFile, "   Burst Frames   : %d\n", pstMot->iBurstFrames);
      GEN_FPRINTF(ptFile, "   Magn Limit     : %d\n", pstMot->iMagLimit);
      GEN_FPRINTF(ptFile, "   Magn Count     : %d\n", pstMot->iMagCount);
      //
      pstFrm = &(pstMot->stFrame);
      pstReg = &(pstFrm->stCanvas);
      if(pstFrm->flSeconds)
      {
         ulSecs = pstMot->ulMotionSecs + (u_int32)(pstFrm->flSeconds);
         RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cDandT, ulSecs);
         //
         GEN_FPRINTF(ptFile, "      Time     : %s\n", cDandT);
         GEN_FPRINTF(ptFile, "      Secs     : %5.3f\n", pstFrm->flSeconds);
         GEN_FPRINTF(ptFile, "      Burst    : %4d\n", pstFrm->iBurst);
         GEN_FPRINTF(ptFile, "      Audio    : %4d\n", pstFrm->iAudio);
         GEN_FPRINTF(ptFile, "      External : %4d\n", pstFrm->iExternal);
         GEN_FPRINTF(ptFile, "      Canvas   : X=%4d, Y=%4d, dX=%4d, dY=%4d, Magn=%4d, Cnt=%4d\n",
                        pstReg->iX, pstReg->iY, pstReg->idX, pstReg->idY, pstReg->iMagnitude, pstReg->iCount);
         //
         for(y=0; y<MAX_MOTION_REGIONS;y++)
         {
            pstReg = &(pstFrm->stRegion[y]);
            GEN_FPRINTF(ptFile, "      Region %d : X=%4d, Y=%4d, dX=%4d, dY=%4d, Magn=%4d, Cnt=%4d\n",
                           y, pstReg->iX, pstReg->iY, pstReg->idX, pstReg->idY, pstReg->iMagnitude, pstReg->iCount);
         }
      }
      safefclose(ptFile);
      GEN_ChangeOwner(pcDest, pcNewUser, pcGroupWww, ATTR_RW_RW_R__, TRUE);
   }
   else
   {
      LOG_Report(errno, "FUN", "RPI-SaveMotionToFile():Open ERROR:%s", pcDest);
      PRINTF("RPI-SaveMotionToFile():Open ERROR:%s" CRLF, pcDest);
   }
}

// 
// Function:   RPI_SetTimestampSecs
// Purpose:    Fill the TS struct with the correct data
// 
// Parameters: TIMPIK ptr, secs
// Returns:    
// Note:       
//
void RPI_SetTimestampSecs(PIKTIM *pstTs, u_int32 ulSecs)
{
   GEN_MEMSET(pstTs, 0x00, sizeof(PIKTIM));
   //
   pstTs->iYear   = RTC_GetYear(ulSecs);
   pstTs->iMonth  = RTC_GetMonth(ulSecs);
   pstTs->iDay    = RTC_GetDay(ulSecs);
   pstTs->iHour   = RTC_GetHour(ulSecs);
   pstTs->iMinute = RTC_GetMinute(ulSecs);
   pstTs->iSecond = RTC_GetSecond(ulSecs);
   //
   GEN_SNPRINTF(pstTs->cYear,   PIKTIM_LENZ, "%d",   pstTs->iYear);
   GEN_SNPRINTF(pstTs->cMonth,  PIKTIM_LENZ, "%02d", pstTs->iMonth);
   GEN_SNPRINTF(pstTs->cDay,    PIKTIM_LENZ, "%02d", pstTs->iDay);
   GEN_SNPRINTF(pstTs->cHour,   PIKTIM_LENZ, "%02d", pstTs->iHour);
   GEN_SNPRINTF(pstTs->cMinute, PIKTIM_LENZ, "%02d", pstTs->iMinute);
   GEN_SNPRINTF(pstTs->cSecond, PIKTIM_LENZ, "%02d", pstTs->iSecond);
}


/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   rpi_DeleteFiles
// Purpose:    Delete files on this node
//             
// Parameters: Node struct, Dir path, NumDeleted
//             
// Returns:    Bytes of files deleted
// Note:       
// 
static int64 rpi_DeleteFiles(PIKDIR *pstDir, char *pcDir, int *piNumDeleted)
{
   bool     fHasSubDir=FALSE;
   int64    llSize=0;
   char    *pcDirCur;

   pcDirCur = safemalloc(MAX_PATH_LENZ);
   while(pstDir)
   {
      if(pstDir->fDir)
      {
         PRINTF("rpi-DeleteFiles():Dir=[%s]" CRLF, pstDir->pcName);
         fHasSubDir = TRUE;
      }
      else
      {
         //
         // There are files at this node: Delete them
         //
         GEN_SNPRINTF(pcDirCur, MAX_PATH_LEN, "%s/%s", pcDir, pstDir->pcName);
         if(remove(pcDirCur) == 0) 
         {
            if(piNumDeleted) (*piNumDeleted)++;
            llSize += pstDir->llSize;
            PRINTF("rpi-DeleteFiles():DELETED File=[%s]: Size = %lld" CRLF, pcDirCur, pstDir->llSize);
            //LOG_Report(0, "FUN", "rpi-DeleteFiles():DELETED File=[%s]: Size = %lld", pcDirCur, pstDir->llSize);
         }
         else
         {
            PRINTF("rpi-DeleteFiles():Error deleting file [%s]" CRLF, pcDirCur);
            LOG_Report(errno, "FUN", "rpi-DeleteFiles():ERROR deleting file [%s] ", pcDirCur);
         }
      }
      pstDir = pstDir->pstNext;
   }
   //
   // This node does NOT have any regular files left. 
   //
   if(!fHasSubDir)
   {
      //
      // This node only had files: remove the directory as well, if any
      //
      if(GEN_STRLEN(pcDir))
      {
         if(remove(pcDir))
         {
            PRINTF("rpi-DeleteFiles():Error deleting Dir [%s]" CRLF, pcDir);
            LOG_Report(errno, "FUN", "rpi-DeleteFiles():ERROR deleting Dir [%s] ", pcDir);
         }
         else
         {
            LOG_Report(0, "FUN", "rpi-DeleteFiles():DELETED Dir [%s]", pcDir);
            PRINTF("rpi-DeleteFiles():DELETED Dir [%s]" CRLF, pcDir);
         }
      }
   }
   safefree(pcDirCur);
   return(llSize);
}

// 
// Function:   rpi_ExtractData
// Purpose:    Extract data from the latest pikrellcam video/thumbs file
// 
// Parameters: Char type, file, dest, max_size
// Returns:    Dest actual size
// Note:       Video/Thumb/Motion filename template starts with the delimiter 
//             needed to synchronize template with filename
// 
static int rpi_ExtractData(char cType, char *pcFile, char *pcDest, int iMaxSize)
{
   bool        fFound=FALSE;
   const char *pcMask=pcPiKrellCamMask;
   int         iNr=0;

   if(iMaxSize < 2) return(0);
   else             *pcDest = 0;
   //
   // Synchronize template and filename on delimiter
   //
   while(!fFound && *pcFile)
   {
      if(*pcFile++ == *pcMask)
      {
         // Delimiter found: advance template ptr and continue
         pcMask++;
         fFound = TRUE;
      }
   }
   //
   // Template and filename have been synchronized: find field
   //
   fFound = FALSE;
   while(!fFound && *pcFile && (iNr < iMaxSize))
   {
      if(*pcMask == cType)
      {
         fFound = TRUE;
         pcDest[iNr++] = *pcFile;
         pcDest[iNr]   = 0;
      }
      pcFile++;
      pcMask++;
   }
   //
   while(fFound && *pcFile && (iNr < iMaxSize))
   {
      if(*pcMask == cType)
      {
         pcDest[iNr++] = *pcFile;
         pcDest[iNr]   = 0;
      }
      else fFound = FALSE;
      pcFile++;
      pcMask++;
   }
   return(iNr);
}

// 
// Function:   rpi_RemoveFiles
// Purpose:    Delete the first iNumRemove files on this node
//             
// Parameters: Node struct, Dir name, Number to remove
//             
// Returns:    Bytes of files deleted
// Note:       
// 
static int64 rpi_RemoveFiles(PIKDIR *pstDir, char *pcDir, int iNumRemove)
{
   int64    llSize=0;
   char    *pcFilename;

   pcFilename = safemalloc(MAX_PATH_LENZ);
   while(pstDir)
   {
      if(!pstDir->fDir)
      {
         if(iNumRemove)
         {
            //
            // This is a file: Delete it
            //
            GEN_SNPRINTF(pcFilename, MAX_PATH_LEN, "%s/%s", pcDir, pstDir->pcName);
            PRINTF("rpi-RemoveFiles():DELETE File=[%s]: Size = %lld" CRLF, pcFilename, pstDir->llSize);
            if(remove(pcFilename) == 0) llSize += pstDir->llSize;
            else
            {
               PRINTF("rpi-RemoveFiles():Error deleting [%s]" CRLF, pcFilename);
               LOG_Report(errno, "FUN", "rpi-RemoveFiles():ERROR deleting %s ", pcFilename);
            }
            iNumRemove--;
         }
         else 
         {
            GEN_SNPRINTF(pcFilename, MAX_PATH_LEN, "%s/%s", pcDir, pstDir->pcName);
            PRINTF("rpi-RemoveFiles():NO NEED TO DELETE File=[%s]: Size = %lld" CRLF, pcFilename, pstDir->llSize);
         }
      }
      else
      {
         GEN_SNPRINTF(pcFilename, MAX_PATH_LEN, "%s/%s", pcDir, pstDir->pcName);
         PRINTF("rpi-RemoveFiles():[%s] is a DIR" CRLF, pcFilename);
      }
      pstDir = pstDir->pstNext;
   }
   safefree(pcFilename);
   return(llSize);
}

