/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           hue_cmds.h
 *  Purpose:            Hue commands
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    03 Feb 2022:      New
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if defined (BOARD_RPI12)
//
// Rpi12: Carport
//
EXTRACT_HC(HUE_CMD_IDLE,            "Idle",              hue_CommandIdle            )
EXTRACT_HC(HUE_CMD_UPDATE_MOTION,   "Motion detect",     hue_CommandUpdateMotion    )
EXTRACT_HC(HUE_CMD_UPDATE_LIGHTS,   "Lights",            hue_CommandUpdateLights    )
EXTRACT_HC(HUE_CMD_UPDATE_GROUPS,   "Groups",            NULL                       )
EXTRACT_HC(HUE_CMD_UPDATE_SCENES,   "Scenes",            NULL                       )
EXTRACT_HC(HUE_CMD_LIGHTS_MAX,      "Lights Maximum",    hue_CommandLightsMax       )
EXTRACT_HC(HUE_CMD_LIGHTS_DEF,      "Lights Default",    NULL                       )
EXTRACT_HC(HUE_CMD_LIGHTS_IRD,      "Lights InfraRed",   NULL                       )
EXTRACT_HC(HUE_CMD_WARNING_OFF,     "Warning OFF",       hue_CommandWarningOff      )

#elif defined (BOARD_RPI14)
//
// Rpi14: Garden/Pond
//
EXTRACT_HC(HUE_CMD_IDLE,            "Idle",              hue_CommandIdle            )
EXTRACT_HC(HUE_CMD_UPDATE_MOTION,   "Motion detect",     hue_CommandUpdateMotion    )
EXTRACT_HC(HUE_CMD_UPDATE_LIGHTS,   "Lights",            hue_CommandUpdateLights    )
EXTRACT_HC(HUE_CMD_UPDATE_GROUPS,   "Groups",            hue_CommandUpdateGroups    )
EXTRACT_HC(HUE_CMD_UPDATE_SCENES,   "Scenes",            hue_CommandUpdateScenes    )
EXTRACT_HC(HUE_CMD_LIGHTS_MAX,      "Lights Maximum",    hue_CommandLightsMax       )
EXTRACT_HC(HUE_CMD_LIGHTS_DEF,      "Lights Default",    hue_CommandLightsDef       )
EXTRACT_HC(HUE_CMD_LIGHTS_IRD,      "Lights InfraRed",   hue_CommandLightsIrd       )
EXTRACT_HC(HUE_CMD_WARNING_OFF,     "Warning OFF",       hue_CommandWarningOff      )

#elif defined (BOARD_RPI10)
//
// Rpi10: Development
//
EXTRACT_HC(HUE_CMD_IDLE,            "Idle",              hue_CommandIdle            )
EXTRACT_HC(HUE_CMD_UPDATE_MOTION,   "Motion detect",     hue_CommandUpdateMotion    )
EXTRACT_HC(HUE_CMD_UPDATE_LIGHTS,   "Lights",            hue_CommandUpdateLights    )
EXTRACT_HC(HUE_CMD_UPDATE_GROUPS,   "Groups",            hue_CommandUpdateGroups    )
EXTRACT_HC(HUE_CMD_UPDATE_SCENES,   "Scenes",            hue_CommandUpdateScenes    )
EXTRACT_HC(HUE_CMD_LIGHTS_MAX,      "Lights Maximum",    hue_CommandLightsMax       )
EXTRACT_HC(HUE_CMD_LIGHTS_DEF,      "Lights Default",    hue_CommandLightsDef       )
EXTRACT_HC(HUE_CMD_LIGHTS_IRD,      "Lights InfraRed",   hue_CommandLightsIrd       )
EXTRACT_HC(HUE_CMD_WARNING_OFF,     "Warning OFF",       hue_CommandWarningOff      )

#else
#warning No Application board specified
//
EXTRACT_HC(HUE_CMD_IDLE,            "Idle",              hue_CommandIdle            )
EXTRACT_HC(HUE_CMD_UPDATE_MOTION,   "Motion detect",     hue_CommandUpdateMotion    )
EXTRACT_HC(HUE_CMD_WARNING_OFF,     "Warning OFF",       hue_CommandWarningOff      )

#endif
