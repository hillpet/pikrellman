/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_main.c
 *  Purpose:            Pikrellman Motion/Archiving utility for PiKrellcam
 *                      Entrypoint pikrellman App
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    12 Jun 2021:      Created
 *    10 Jul 2021:      Add maintenance on Archive and Motion storage space
 *    20 Aug 2021:      Add rpi_guard
 *    04 Sep 2021:      Add CL options
 *    08 Nov 2021:      Add Test MMAP
 *    10 Nov 2021:      Add HTTP Server
 *    13 Nov 2021:      Add external msmtp mail client
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    29 Nov 2021:      Add Wifi Ping thread
 *    01 Dec 2021:      Add Button power down
 *    10 Dec 2021:      Add system core temperature
 *    16 Dec 2021:      Delete mail jpg files also
 *    20 Dec 2021:      Use PAR_LOOP_FILE for all motion timestamps
 *    26 Dec 2021:      Add test motion list 
 *    01 Jan 2022:      Add email retries on send error
 *    14 Jan 2022:      Use PAR_REC_FILE (from state-file) for video freeze
 *    18 Jan 2022:      Create csv file as root
 *    23 Jan 2022:      Signal still-archiving only, do NOT react
 *    14 Feb 2022:      LOG Verbose flags at powerup
 *    07 Mar 2022:      Replace atoi() by strtol()
 *    23 Mar 2022:      Test/Fix DST
 *    01 Jun 2022:      Add PidList timestamps
 *    13 Mar 2023:      Add 24 hour WAN status in email
 *    12 Oct 2023:      Flexible WAN retrieval
 *    10 Mar 2024:      Do not save MMAP on test runs
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <pwd.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_func.h"
#include "rpi_motion.h"
#include "rpi_archive.h"
#include "rpi_guard.h"
#include "rpi_hue.h"
#include "rpi_srvr.h"
#include "rpi_mail.h"
#include "rpi_mailx.h"
#include "rpi_ping.h"
//
#include "rpi_main.h"
//
#define USE_PRINTF
#include <printx.h>

//#warning slash-slash-pwjh-Temp-changes 

//
// Global constants
//
const char             *pcLogfile         = RPI_PIKRELL_LOG;
const char             *pcCsvFile         = RPI_CSV_PATH;
const char             *pcMotionLoopPath  = "/mnt/rpipix/motion";
const char             *pcPiKrellEvents   = "/run/pikrellcam/motion-events";
const char             *pcPikrellCam      = "pikrellcam";
const char             *pcNewUser         = "pi";
const char             *pcGroupWww        = "www-data";
//
// Local arguments
//
static bool             fRpiRunning       = TRUE;
static bool             fOnMotEndPending  = FALSE;
static int              iCliSumSecs       = 0;
static u_int32          ulSecsMailTimeout = 0;
static u_int32          ulThreadsPending  = 0;
static u_int32          ulEmailPending    = 0;
static u_int32          ulVideosLoopSecs  = 0;
static u_int32          ulVideosHeronSecs = 0;
static u_int32          ulThumbsLoopSecs  = 0;
static u_int32          ulMailLoopSecs    = 0;
static u_int32          ulMotionLoopSecs  = 0;
static u_int32          ulHeronSecs       = 0; 
static u_int32          ulHeronSecsEnd    = 0; 
static int64            llCacheTotal      = 0;
//
static const char      *pcShellPowerOff   = "sudo shutdown now";
static const char      *pcShellReboot     = "sudo shutdown -r +5";
static const char      *pcCacheDir        = RPI_WORK_DIR;
static const char      *pcEventsFile      = "/run/pikrellcam/motion-events";
static const char      *pcArgsFile        = "/run/pikrellcam/motion-args";
static const char      *pcStateFile       = "/run/pikrellcam/state";
static const char      *pcArchiveFilter   = "*";
static const char      *pcSeplines        = "------------------------------------------------------------------------------------";
//
static const char      *pcHelp            = "PiKrellMan Command line options:\n"
                                            "   -P x  : Wifi pings for preventing connection going OTL\n"
                                            "             0: Do   ping\n"
                                            "             1: Skip ping\n"
                                            "    -R x  : PiKrellCam restart options:\n"
                                            "             0: Verbose none, discard additional stdout/stderr Output (Default)\n"
                                            "             1: Verbose all,  but discard additional stdout/stderr Output\n"
                                            "             2: Verbose none, stdout/stderr Output to AppRestart.log\n"
                                            "             3: Verbose all,  stdout/stderr Output to AppRestart.log\n"
                                            "             4: Verbose none, reroute stdout/stderr Output separately\n"
                                            "             4: Verbose all,  reroute stdout/stderr Output separately\n"
                                            "    -S x  : Create periodically Motion Summaries\n"
                                            "    -T x  : No save MMAP: Test MMAP vars         and EXIT on T 0\n"
                                            "    -d    : No save MMAP: Test DST               and EXIT\n"
                                            "    -h    : Help text                            and EXIT\n"
                                            "    -m    : Create MMAP if bad                   and EXIT\n"
                                            "    -s    : No save MMAP: Print sizeof(mmap)     and EXIT\n"
                                            "    -v x  : Overrule G_iVerbose flag\n"
                                            "\n";
//
// pcArgsFile
//    /run/pikrellcam/motion-args
//
static const PIKVAR stPiArguments[]       =
{
   {  "caller",            PAR_CALLER     },             // Caller
   {  "logfile",           PAR_LOG        },             // Log file
   {  "fifo",              PAR_FIFO       },             // Fifo
   {  "last_video",        PAR_LAST_VID   },             // Last video file
   {  "last_thumb",        PAR_LAST_THB   },             // Last thumb file
   {  "media_dir",         PAR_MEDIA      },             // Media dir
   {  "archive_dir",       PAR_ARCHIVE    },             // Archive dir
   {  "video_dir",         PAR_VIDEO      },             // Video dir
   {  "thumb_dir",         PAR_THUMB      },             // Thumb dir
   {  "loop_dir",          PAR_LOOP       },             // Loop dir
   {  "still_dir",         PAR_STILL      },             // Still dir
   {  "timelapse_dir",     PAR_TIMELAPSE  },             // Timelapse dir
//    Keep persistent verbose !!
// {  "flags",             PAR_VERBOSE    }              // Flags and verbose level
};
static const int iNumPiArguments = sizeof(stPiArguments)/sizeof(PIKVAR);
//
// pcEventsFile
//    /run/pikrellcam/motion-events
//
// If PiKrellCam triggers the ON_MOTION_BEGIN, PAR_LOOP_FILE has the filename of the
// h264 file currently being recorded with the mp4 extension. This file will need to
// be processed during the whole motion detect AND archiving process.
// Depending on the motion settings, the actual filename on the filesystem could be 
//    loop_yyyy-mm-dd_hh.mm.ss_0.mp4
//    loop_yyyy-mm-dd_hh.mm.ss_m.mp4 
//
static const PIKVAR stPiEvents[] =
{
   {  "video",             PAR_LOOP_FILE }               // loop-video file
};
static const int iNumPiEvents = sizeof(stPiEvents)/sizeof(PIKVAR);
//
// pcStateFile
//    /run/pikrellcam/state
//
static const PIKVAR stPiStateVars[] =
{
   {  "video_last",        PAR_REC_FILE   },             // Last recorded and converted mp4 file
   {  "dawn",              PAR_MIN_DAWN   },             // Todays minute dawn 
   {  "sunrise",           PAR_MIN_SRISE  },             //        minute sunrise
   {  "sunset",            PAR_MIN_SSET   },             //        minute sunset
   {  "dusk",              PAR_MIN_DUSK   },             //        minute dusk
   {  "run_state",         PAR_RUN_STATE  }              // RUNST runstate
};
static int iNumPiStateVars = sizeof(stPiStateVars)/sizeof(PIKVAR);
//
// Periodically check directories to clean up max number of files
//
static const PILOOP stLoopRecordings[]  =
{
   // ulCheckSecs    pulNextSecs          iMaxFiles   pcPath          pcFilter
   {  180,           &ulVideosLoopSecs,   5,          "videos",       "loop_*.*"          },
   {  600,           &ulThumbsLoopSecs,   32,         "thumbs",       "loop_*.th.jpg"     },
   {  600,           &ulMailLoopSecs,     32,         "thumbs",       "loop_*.mail.jpg"   },
   {  600,           &ulMotionLoopSecs,   32,         "motion",       "loop_*.txt"        }
};
static const int iNumLoopRecordings = sizeof(stLoopRecordings)/sizeof(PILOOP);
//
// Heron Motion Detection start clean up number of files
//
static const PILOOP stHeronRecordings[] =
{
   // ulCheckSecs    pulNextSecs          iMaxFiles   pcPath          pcFilter
   {  0,             &ulVideosHeronSecs,  3,          "videos",       "loop_*.mp4"        }
};
//
static double      flCpuTemp        = 0.0;
static const SDATA stSystemVars[]   =
{
   { NULL,                                      NULL           },
   { "/sys/class/thermal/thermal_zone0/temp",   &flCpuTemp     },
   //
   { NULL,                                      NULL           }
};

//
// Local functions
//
#ifdef   DEBUG_ASSERT
  #define  FEATURE_DUMP_MOTION_DATA
//#define  FEATURE_TEST_MOTION_LIST
  #ifdef   FEATURE_DUMP_MOTION_DATA
  static void rpi_DumpMotionData();
  #define  RPI_DUMP_MOTION_DATA()         rpi_DumpMotionData()
  #ifdef   FEATURE_TEST_MOTION_LIST
  static void rpi_TestMotionList();
  #endif   // FEATURE_TEST_MOTION_LIST
  #define  RPI_DEAMON()                   LOG_printf("rpi-Deamon():Debug mode !" CRLF);
  #endif   // FEATURE_DUMP_MOTION_DATA

#else    //DEBUG_ASSERT
   static pid_t   rpi_Deamon();
  #define RPI_DEAMON()                    rpi_Deamon()
  #define RPI_DUMP_MOTION_DATA()

#endif   //DEBUG_ASSERT
//
static int     rpi_Execute                (void);
//
static bool    rpi_CheckArchiveUsage      (u_int32);
static void    rpi_CheckButton            (u_int32);
static bool    rpi_CheckGuard             (u_int32);
static int64   rpi_CheckLoopRecordings    (u_int32, const PILOOP *, int);
static bool    rpi_CheckMidnight          (u_int32);
static bool    rpi_CheckMotionSummary     (u_int32);
static void    rpi_CheckTemperature       (u_int32);
static bool    rpi_CheckThreads           (u_int32);
static bool    rpi_CheckVideoFreeze       (u_int32);
static void    rpi_CheckWan               (u_int32);
static bool    rpi_ExtractSystemVars      (SVAR);
static void    rpi_HandleMotionAcquisition(u_int32);
static bool    rpi_HandleMotionCompleted  (MOTCC);
static bool    rpi_RequestReboot          (void);
static void    rpi_ResetSummary           (void);
static void    rpi_SetupGpio              (void);
static bool    rpi_SetupUser              (const char *);
static int     rpi_Startup                (void);
static bool    rpi_WaitStartup            (int);
//
// CLI
//
static void    rpi_DisplayMapfile         ();
static void    rpi_TestMmap               (void);
//
static bool    rpi_SignalRegister         (sigset_t *);
static void    rpi_ReceiveSignalInt       (int);
static void    rpi_ReceiveSignalTerm      (int);
static void    rpi_ReceiveSignalUser1     (int);
static void    rpi_ReceiveSignalUser2     (int);
static void    rpi_ReceiveSignalSegmnt    (int);

//
// Function:   main
// Purpose:    Main entry point for the Raspberry Pi PiKrellman motion/archive utility
//
// Parms:      Commandline options
// Returns:    Exit codes
// Note:       Called by PiKrellCam scripts to handle motion and copy a motion video 
//             file from the loop to the archive dir.
//
//             SIGUSR1: PiKrellCam motion:
//             ===========================
//             on_motion_begin $C/pikrellmotion.sh $G $P $v $A $m $a $V $t $z $S $L
//
//             The pikrellmotion.sh script calls [sudo /usr/bin/pikrellmo "$@" 28] to 
//             run at the beginning of motion-detection.
//
//             This script should be configured in ~/.pikrellcam/pikrellcam.conf and not
//             in the at-command.conf file.
//
//             Motion can be determined further by examining 
//             "/run/pikrellcam/motion-events".
//
//             SIGUSR2: PiKrellCam Archive:
//             ============================
//             on_motion_end $C/pikrellarchive.sh $G $P $v $A $m $a $V $t $z $S $L
//             on_motion_enable $C/pikrellarchive.sh $G $P $v $A $m $a $V $t $z $S $L
//
//             The pikrellarchive.sh script calls [sudo /usr/bin/pikrellar "$@" 28] to 
//             run at the end of motion-detection.
//             If this script is configured to be run as the on_motion_end command, then
//             motion videos can be immediately saved to a backup dir.
//             
//             This script should be configured in ~/.pikrellcam/pikrellcam.conf and not
//             in the at-command.conf file.
//             
//             Argument substitution done by PiKrellCam before running this script:
//               $C - scripts directory so this script is found.
//               $G - log file configured in ~/.pikrellcam/pikrellcam.conf.
//               $P - the command FIFO so we can write commands to PiKrellCam.
//               $v - the full path name of the last motion video file..
//               $A - the full path name of the last motion thumbnail file.
//               $m - the full path name of the Media     directory
//               $a - the full path name of the Archive   directory
//               $V - the full path name of the Videos    directory
//               $t - the full path name of the Thumbs    directory
//               $z - the full path name of the Loop      directory
//               $S - the full path name of the Still     directory
//               $L - the full path name of the Timelapse directory
//               xx - Flags and verbose level
//                      0xFF000000      Verbose level mask
//                      0x00000001      Archiving through PiKrellCam (FIFO)
//                      0x00000002      LOG arguments
//                      0x00000004      Notify PiKrellCam of archiving completed (FIFO)
//                      0x00000008      Log PiKrellCam motion data
//                      0x00000010      Notify PiKrellCam of archive % free (FIFO)
//             
//             Examples of the arguments passed to the pikarch utility are:
//             	0: /usr/bin/pikarch
//                1: /mnt/rpicache/pikrellmo.log
//                2: /home/pi/pikrellcam/www/FIFO
//                3: /mnt/rpipix/videos/loop_2022-06-06_16.36.01_m.mp4
//                4: /mnt/rpipix/thumbs/loop_2022-06-06_16.36.01_m.th.jpg
//                5: /all/public/www/media
//                6: /all/public/www/media/archive
//                7: /all/public/www/media/videos
//                8: /all/public/www/media/thumbs
//                9: /mnt/rpipix/videos
//               10: /all/public/www/media/stills
//               11: /all/public/www/media/timelapse
//               12: 7
//
//               All these arguments are being written to the args-file on /run/pikrellcam
//
int main(int argc, char **argv)
{
   bool     fSaveMap=FALSE;
   int      iOpt, iCc=EXIT_CC_OKEE;
   u_int32  ulSecsNow;
   char    *pcBuffer;
   //
   // At this stage, we should be ROOT.
   // If we use IO, it probably has to init before switching to user mode !
   //
   rpi_SetupGpio();
   //
   if(!rpi_SetupUser(pcNewUser))
   {
      exit(EXIT_CC_GEN_ERROR);
   }
   //
   // Now we are User <pcNewUser> and Group <pcGroupWww>
   // Truncate reports file
   //
   if(GEN_FileTruncate((char *)pcLogfile, MAX_NUM_REPORT_LINES) != 0)
   {
      printf("main():Error truncating reports file %s" CRLF, pcLogfile);
   }
   //
   // Open logfile
   //
   if( LOG_ReportOpenFile((char *)pcLogfile) == FALSE)   LOG_printf("main():Error opening log file %s" CRLF, pcLogfile);
   else 
   {
      //
      // Track the LOG fileposition, we must update the Global G_ variable when we have the MMAP 
      //
      pstMap->G_llLogFilePos = LOG_ReportFilePosition();
      LOG_printf("main():Logfile is %s (at pos %ld)" CRLF, pcLogfile, pstMap->G_llLogFilePos);
   }
   //
   LOG_Report(0, "HST", (char *)pcSeplines);
   LOG_Report(0, "HST", "Main():PiKrellMan Version=%s", VERSION);
   //
   // Check if the MMAP file was properly restored or not
   //
   if(pstMap->G_fInit) 
   {
      LOG_Report(0, "HST", "Main():MMAP file signature or DB version mismatch, created new Mapping:");
      LOG_Report(0, "HST", "Main():DB version now v%d.%d", pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
   }
   else LOG_Report(0, "HST", "Main():MMAP file OKee: DB-v%d.%d", pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
   //
   PRINTF("main():PiKrellMan has %d args" CRLF, argc);
   //
   // PiKrellMan Command line options:
   // -P x  : Wifi pings for preventing connection going OTL
   //          0: Do   ping
   //          1: Skip ping
   // -R x  : PiKrellCam restart options:
   //          0: Verbose none, discard additional stdout/stderr Output (Default)
   //          1: Verbose all,  but discard additional stdout/stderr Output 
   //          2: Verbose none, stdout/stderr Output to AppRestart.log
   //          3: Verbose all,  stdout/stderr Output to AppRestart.log
   //          4: Verbose none, reroute stdout/stderr Output separately
   //          4: Verbose all,  reroute stdout/stderr Output separately
   // -S x  : Create periodically Motion Summaries
   // -T x  : No save MMAP: Test MMAP vars         and EXIT on T 0
   // -d    : No save MMAP: Test DST               and EXIT
   // -h    : Help text                            and EXIT
   // -m    : Create MMAP if bad                   and EXIT
   // -s    : No save MMAP: Print sizeof(mmap)     and EXIT
   // -v x  : Overrule G_iVerbose flag
   //
   while ((iOpt = getopt(argc, argv, "P:R:S:T:dhmsv:")) != -1)
   {
      switch (iOpt)
      {
         case 'P':
            LOG_printf("rpi-main():Option -P %s" CRLF, optarg);
            LOG_Report(0, "HST", "rpi-main():Option -P %s", optarg);
            pstMap->G_iSkipWifiPings = (int)strtol(optarg, NULL, 0);
            break;

         case 'R':
            LOG_printf("rpi-main():Option -R %s" CRLF, optarg);
            LOG_Report(0, "HST", "rpi-main():Option -R %s", optarg);
            pstMap->G_iCamOpts = (int)strtol(optarg, NULL, 0);
            break;

         case 'S':
            LOG_printf("rpi-main():Option -S %s" CRLF, optarg);
            iCliSumSecs = (int)strtol(optarg, NULL, 0);
            LOG_Report(0, "HST", "rpi-main():Option -S %s", optarg);
            break;

         case 'T':
            LOG_printf("rpi-main():Option -T %s" CRLF, optarg);
            fRpiRunning = (bool)strtol(optarg, NULL, 0);
            rpi_TestMmap();
            break;

         case 'd':
            printf("rpi-main():Option -d" CRLF);
            pcBuffer = safemalloc(128);
            for(int iYr=2020; iYr<2025; iYr++)
            {
               ulSecsNow = RTC_GetSecs(iYr, 1, 1);
               LOG_printf("rpi-main():RTC_GetSecs() 1 Jan %4d = %10d Secs" CRLF, iYr, ulSecsNow);
            }
            //
            // Get SystemSecs and show if DST is correctly included
            //
            ulSecsNow = RTC_GetSystemSecs();
            //
            printf("rpi-main():RTC_DaylightSavingTime() = %d"        CRLF, RTC_DaylightSavingTime(ulSecsNow));
            printf("rpi-main():RTC_GetSystemSecs()      = %ld Secs"  CRLF, ulSecsNow);
            printf("rpi-main():RTC_GetMidnight()        = %ld Secs"  CRLF, RTC_GetMidnight(ulSecsNow));
            printf("rpi-main():RTC_GetSecs() Midnight   = %ld Secs"  CRLF, RTC_GetSecs(RTC_GetYear(ulSecsNow), RTC_GetMonth(ulSecsNow), RTC_GetDay(ulSecsNow)));
            //
            printf("rpi-main():RTC_GetYear()            = %d"        CRLF, RTC_GetYear(ulSecsNow));
            printf("rpi-main():RTC_GetMonth()           = %d"        CRLF, RTC_GetMonth(ulSecsNow));
            printf("rpi-main():RTC_GetCurrentQuarter()  = %ld"       CRLF, RTC_GetCurrentQuarter(ulSecsNow));
            printf("rpi-main():RTC_GetDay()             = %d"        CRLF, RTC_GetDay(ulSecsNow));
            printf("rpi-main():RTC_GetHour()            = %d"        CRLF, RTC_GetHour(ulSecsNow));
            printf("rpi-main():RTC_GetMinute()          = %d"        CRLF, RTC_GetMinute(ulSecsNow));
            printf("rpi-main():RTC_GetSecond()          = %d"        CRLF, RTC_GetSecond(ulSecsNow));
            //
            RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcBuffer, ulSecsNow);
            printf("rpi-main():RTC_ConvertDateTime()    = %s" CRLF, pcBuffer);
            //
            RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, pcBuffer, ulSecsNow);
            printf("rpi-main():RTC_ConvertDateTime()    = %s" CRLF, pcBuffer);
            safefree(pcBuffer);
            fRpiRunning = FALSE;
            break;

         case 'h':
            printf("rpi-main():Option -h:" CRLF);
            printf(pcHelp);
            fRpiRunning = FALSE;
            break;

         case 'm':
            printf("rpi-main():Option -m: Create mmap if existing is BAD" CRLF);
            rpi_DisplayMapfile();
            if(pstMap->G_fInit) 
            {
               printf("rpi-main():Existing MMAP was BAD: New default MMAP file was created !!" CRLF);
               fSaveMap = TRUE;
            }
            fRpiRunning = FALSE;
            break;

         case 's':
            printf("rpi-main():Option -s" CRLF);
            rpi_DisplayMapfile();
            fRpiRunning = FALSE;
            break;

         case 'v':
            LOG_printf("rpi-main():Option -v %s" CRLF, optarg);
            LOG_Report(0, "HST", "rpi-main():Option -v %s", optarg);
            pstMap->G_iVerbose = (int)strtol(optarg, NULL, 0);
            break;

         default:
            LOG_printf("rpi-main(): Invalid option %c" CRLF, (char) iOpt);
            LOG_Report(0, "HST", "rpi-main():Invalid option %c", (char) iOpt);
            break;
      }
   }
   //
   // If still enabled to run:
   //
   if(fRpiRunning)
   {
      //
      // Log appropriate verbose flags
      //
      if(RPI_GetFlag(FLAGS_NFY_USAGE))       LOG_Report(0, "HST", "rpi-main():main.c         Notify PiKrellCam of archive % free (FIFO)");
      if(RPI_GetFlag(FLAGS_PIK_ARCHIVE))     LOG_Report(0, "HST", "rpi-main():rpi_archive.c  Archiving through PiKrellCam (FIFO)");
      if(RPI_GetFlag(FLAGS_MOTION_CSV))      LOG_Report(0, "HST", "rpi-main():rpi_archive.c  Runtime update motion CSV file");
      if(RPI_GetFlag(FLAGS_LOG_EVENT))       LOG_Report(0, "HST", "rpi-main():rpi_func.c     Log Motion timestamp");
      if(RPI_GetFlag(FLAGS_LOG_MOTION))      LOG_Report(0, "HST", "rpi-main():rpi_motion.c   Log PiKrellCam motion data");
      if(RPI_GetFlag(FLAGS_LOG_JSON))        LOG_Report(0, "HST", "rpi-main():rpi_hue.c      Log JSON object data");
      if(RPI_GetFlag(FLAGS_LOG_HUECFG))      LOG_Report(0, "HST", "rpi-main():rpi_hue.c      Log actual Hue configuration as string");
      if(RPI_GetFlag(FLAGS_LOG_BINCFG))      LOG_Report(0, "HST", "rpi-main():rpi_hue.c      Add actual Hue configuration as binary ");
      //
      // ---------------------------------------------------------------------------------------------
      // Run threads
      // ---------------------------------------------------------------------------------------------
      // Host     : Main thread: start new threads for:
      // Guard    : Check PiKrellCam progress
      // Hue      : Handle Hue Bridge comms
      // Motion   : Motion detect            (SIGUSR1 trigger from PiKrellCam)
      // Archive  : Archive LOOP recordings  (SIGUSR2 trigger from PiKrellCam)
      // ---------------------------------------------------------------------------------------------
      RPI_DEAMON();
      //
      // If CL debug, the CLI will return here and spin of additional threads to handle motion 
      // and archiving. If run as service, the parent has already exited and the child will 
      // resume here.
      //
      GLOBAL_PidPut(PID_HOST, getpid());
      GLOBAL_SemaphoreInit(PID_HOST);
      //
      pstMap->G_iGuards = rpi_Startup();
      fRpiRunning       = rpi_WaitStartup(pstMap->G_iGuards);
      //
      GLOBAL_PidClearGuards();
      //
      // Display all pids in log file
      //
      GLOBAL_PidsLog();
      PRINTF("rpi-main():Running..." CRLF);
      LOG_Report(0, "HST", (char *)pcSeplines);
      //
      rpi_Execute();
      //
      // If we made it this far, make sure to save all MMAP settings to file
      //
      fSaveMap = TRUE;
      GLOBAL_PidsTerminate(2000);
      GLOBAL_SemaphoreDelete(PID_HOST);
   }
   LOG_printf("Main():SafeMalloc balance=%d (Should be zero)" CRLF, GLOBAL_GetMallocs());
   LOG_Report(0, "HST", "rpi-main():SafeMalloc balance=%d (Should be zero)", GLOBAL_GetMallocs());
   LOG_printf("Main():PiKrellMan Version=%s now EXIT" CRLF, VERSION);
   LOG_Report(0, "HST", "Main():PiKrellMan Version=%s now EXIT", VERSION);
   LOG_ReportCloseFile();
   //==============================================================================================
   // From here on :
   // CANNOT Use functions which use MMAP anymore:
   // CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
   //==============================================================================================
   GLOBAL_Sync();
   GLOBAL_Close(fSaveMap);
   exit(iCc);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/


#ifndef  DEBUG_ASSERT
//
// Function:   rpi_Deamon
// Purpose:    Daemonize the PiLrellCam on_motion_begin
//             
// Parms:       
// Returns:     
// Note:       Do NOT use PRINTF macro's here. Use LOG_printf() instead !!
//
static pid_t rpi_Deamon(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         exit(EXIT_CC_OKEE);
         break;

      case 0:
         // Child:
         // Continue as separeate thread
         break;

      case -1:
         // Error
         LOG_Report(errno, "HST", "rpi-Daemon():ERROR ");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(tPid);
}
#endif   //DEBUG_ASSERT

//
// Function:   rpi_Execute
// Purpose:    Handle motion detect for PiKrellCam:
//             on_motion_begin $C/motion-xmt-rpi5.sh script
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int rpi_Execute()
{
   int      iOpt, iCc=EXIT_CC_GEN_ERROR;
   int     *piState;
   u_int32  ulSecs;
   sigset_t tBlockset;

   if(rpi_SignalRegister(&tBlockset) == FALSE) return(iCc);
   //
   iCc = EXIT_CC_OKEE;
   //
   // Make sure to have initial parms
   //
   RPI_ExtractVariables(pcArgsFile,   stPiArguments, iNumPiArguments);
   RPI_ExtractVariables(pcEventsFile, stPiEvents,    iNumPiEvents);
   RPI_ExtractVariables(pcStateFile,  stPiStateVars, iNumPiStateVars);
   //
   piState = (int *)GLOBAL_GetParameter(PAR_MOT_STATE);

#ifdef FEATURE_TEST_MOTION_LIST
   PRINTF("rpi-Execute():Start Run motion list test" CRLF);
   rpi_TestMotionList();
   RPI_CreateMailBody();
   fRpiRunning = FALSE;
   PRINTF("rpi-Execute():End Run motion list test" CRLF);

#else    //FEATURE_TEST_MOTION_LIST

   PRINTF("rpi-Execute():Run threads" CRLF);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_HST_HUE_CFG);
   GLOBAL_SetSignalNotification(PID_PING, GLOBAL_HST_PNG_MAP);

#endif   //FEATURE_TEST_MOTION_LIST


/*----------------------------------------------------------------------
_______________________RUN_LOOP(){}
------------------------------x----------------------------------------*/
   while(fRpiRunning)
   {
      //
      // Wait until triggered by:
      //    o SIGINT:   Terminate normally
      //    o SIGTERM:  Terminate normally
      //    o SIGSEGV:  Segmentation fault
      //    o SIGUSR1:  Run command
      //    o SIGUSR2:  
      //
      iOpt = GLOBAL_SemaphoreWait(PID_HOST, TIMEOUT_MSECS);
      switch(iOpt)
      {
         case 0:
            ulSecs = RTC_GetSystemSecs();
            GLOBAL_PidTimestamp(PID_HOST, 0);
            //PRINTF("rpi-Execute():--------- Sem unlocked --------- : 0x%x" CRLF, GLOBAL_GetSignal(PID_HOST));
            if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "HST", "rpi-Execute():Signal=0x%x", GLOBAL_GetSignal(PID_HOST));
            //
            // Sem unlocked
            // Check Hue Bridge Config acquisition completed signal
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_HUE_HST_NFY))
            {
               LOG_Report(0, "HST", "rpi-Execute():New HUE Bridge config loaded");
               PRINTF("rpi-Execute():New HUE Bridge config loaded" CRLF);
            }
            //
            // Check Reboot request from one of the threads
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_ALL_HST_RBT))
            {
               LOG_Report(0, "HST", "rpi-Execute():REBOOT Request !!");
               fRpiRunning = rpi_RequestReboot();
            }
            //
            // HTTP server received Heron Motion Detection activation
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SVR_HST_HER))
            {
               LOG_Report(0, "HST", "rpi-Execute():Server Heron MD: Timer=%d Secs", pstMap->G_iHeronSecs);
               PRINTF("rpi-Execute():Server received Heron MD: Timer=%d Secs" CRLF, pstMap->G_iHeronSecs);
               if(*piState != MOT_ACQ_HERON)
               {
                  //
                  // Delete older loop recordings until only the ones remain we want to archive
                  // The total time we will be archiving loop recordings is given by the HTTP URL:
                  //    GET /motion.json?Heron=Xxxx
                  // and has been stored in G_iHeronSecs.
                  // Alternatively, the URL can pass G_iHeronSecs=0 Secs, where the total archiving
                  // time will be NOW + HERON_DEF_ARCHIVE_SECS seconds (be it 10 Mins).
                  //
                  rpi_CheckLoopRecordings(ulSecs, stHeronRecordings, 1);
                  //
                  if(pstMap->G_iHeronSecs) ulHeronSecsEnd = ulSecs + pstMap->G_iHeronSecs; 
                  else                     ulHeronSecsEnd = ulSecs + HERON_DEF_ARCHIVE_SECS;
                  //
                  ulHeronSecs      = ulSecs + HERON_ARCHIVE_SECS; 
                  *piState         = MOT_ACQ_HERON;
                  fOnMotEndPending = TRUE;
               }
               else LOG_Report(0, "HST", "rpi-Execute():Server Heron MD still ongoing for %d Secs: Ignore!", ulHeronSecsEnd-ulSecs);
            }
            //
            // Check on motion acquisition
            //
            rpi_HandleMotionAcquisition(ulSecs);
            break;

         case 1:
            //
            // Timeout: 
            //
            RPI_ExtractVariables(pcArgsFile,  stPiArguments, iNumPiArguments);
            RPI_ExtractVariables(pcStateFile, stPiStateVars, iNumPiStateVars);
            //
            ulSecs = RTC_GetSystemSecs();
            //
            // If the ON_MOTION_END came before the motion acquisition was completed, 
            // handle it now.
            //
            if(fOnMotEndPending) rpi_HandleMotionAcquisition(ulSecs);
            //
            rpi_CheckArchiveUsage(ulSecs);
            rpi_CheckLoopRecordings(ulSecs, stLoopRecordings, iNumLoopRecordings);
            rpi_CheckGuard(ulSecs);
            rpi_CheckMidnight(ulSecs);
            rpi_CheckThreads(ulSecs);
            rpi_CheckMotionSummary(ulSecs);
            rpi_CheckWan(ulSecs);
            //
            if(rpi_CheckVideoFreeze(ulSecs))
            {
               LOG_Report(0, "HST", "rpi-Execute():Video seems FROZEN: Reboot");
               fRpiRunning = rpi_RequestReboot();
            }
            else 
            {
               //
               // if mail has been sent, <ulSecsMailTimeout> has the absolute system secs
               // to check if the email client has completed (default 120 secs).
               // If after this time the email client i still running, terminate it. 
               //
               ulSecsMailTimeout = RPI_WaitMailxCompletion(ulSecsMailTimeout);
               //
               // If we have a general purpose button, use it to power-off the system
               //
               rpi_CheckButton(ulSecs);
            }
            rpi_CheckTemperature(ulSecs);
            break;

         default:
         case -1:
            LOG_Report(errno, "HST", "rpi-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("rpi-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            iCc = EXIT_CC_GEN_ERROR;
            fRpiRunning = FALSE;
            break;
      }
   }
   PRINTF("rpi-Execute():Exit threads" CRLF);
   return(iCc);
}

//
// Function:   rpi_CheckArchiveUsage
// Purpose:    Check the amount of archive free space
//
// Parms:      Current secs timestamp
// Returns:    TRUE if OKee
// Note:       Constantly called on each semaphore timeout
//
static bool rpi_CheckArchiveUsage(u_int32 ulSecs)
{
   static u_int32 ulArchUsageSecs=0;
   static double  flCurrentFree=0.0;
   //
   bool     fCc=TRUE;
   int      iNumDeleted;
   u_int32  ulNextCheck=ARCHIVE_USAGE_SECS;
   char    *pcData;
   char    *pcArchive;
   double   flFree, flFreed;
   int64    llFreed;

   if(ulArchUsageSecs < ulSecs)
   {
      //
      // Time to check on Archive usage
      //
      pcArchive = GLOBAL_GetParameter(PAR_ARCHIVE);
      flFree    = FINFO_GetFreeSpacePercentage(pcArchive);
      if(fabs(flFree-flCurrentFree) > 1.0)
      {
         LOG_Report(0, "HST", "rpi-CheckArchiveUsage():Archive [%s] has %.2f Pct Free space (%.2f Pct Threshold)", pcArchive, flFree, pstMap->G_flFreeArchive);
         PRINTF("rpi-CheckArchiveUsage():Archive [%s] has %.2f%% Free space (%.2f %% Threshold)" CRLF, pcArchive, flFree, pstMap->G_flFreeArchive);
         //
         flCurrentFree = flFree;
      }
      if(flFree < pstMap->G_flFreeArchive)
      {
         PRINTF("rpi-CheckArchiveUsage():Archive [%s] LOW:Cleanup" CRLF, pcArchive);
         //
         // Too little archive space: delete oldest files, and check again soon
         // Keep admin on todays number of deleted files
         //
         iNumDeleted = 0;
         llFreed     = RPI_CleanupFiles(pcArchive, (char *)pcArchiveFilter, &iNumDeleted);
         flFreed     = ((double)llFreed) / ONEMEGABYTE;
         //
         pstMap->G_iArchiveDel += iNumDeleted;
         if(iNumDeleted)
         {
            LOG_Report(0, "HST", "rpi-CheckArchiveUsage():Archive[%s]: Freed %d files (%5.3f MB)", pcArchive, iNumDeleted, flFreed);
            PRINTF("rpi-CheckArchiveUsage():Archive[%s]: Freed %d files (%5.3f MB)" CRLF, pcArchive, iNumDeleted, flFreed);
            ulNextCheck = 10;
         }
      }
      if(RPI_GetFlag(FLAGS_NFY_USAGE))
      {
         //
         // Report free space back to PiKrellCam
         //
         pcData = (char *) safemalloc(MAX_ARG_LENZ);
         //
         GEN_SNPRINTF(pcData, MAX_ARG_LEN, "Archive(%.2f Pct Free)", flFree);
         RPI_InformFifo(NULL, (char *)"0 3 0", pcData, 4);
         safefree(pcData);
      }
      ulArchUsageSecs = ulSecs + ulNextCheck;
   }
   return(fCc);
}

//
//  Function:   rpi_CheckButton
//  Purpose:    Check if the button has been pressed 
//
//  Parms:      Current secs
//  Returns:    
//
static void rpi_CheckButton(u_int32 ulSecs)
{
   static u_int32 ulPowerSecs=0;

   if(ulPowerSecs == 0) 
   {
      ulPowerSecs = ulSecs + POWER_BUTTON_PRESS_SECS;
   }
   else if( IN(IO_BUTTON) )
   {
      if(ulSecs > ulPowerSecs)
      {
         PRINTF("rpi-CheckButton():Pressed:Power Off button" CRLF);
         LOG_Report(0, "HST", "rpi-CheckButton(): Request Reboot() [%s]", pcShellPowerOff);
         system(pcShellPowerOff);
      }
   }
   else
   {
      //PRINTF("rpi-CheckButton():NOT Pressed" CRLF);
      ulPowerSecs = ulSecs + POWER_BUTTON_PRESS_SECS;
   }
}

//
// Function:   rpi_CheckGuard
// Purpose:    Trigger the guard if it's time to check the running dtste of theApp
//
// Parms:      Current secs timestamp
// Returns:    TRUE if OKee
// Note:       Constantly called on each semaphore timeout
//
static bool rpi_CheckGuard(u_int32 ulSecs)
{
   static u_int32 ulGuardSecs=0;
   //
   bool  fCc=TRUE;

   if(ulGuardSecs == 0) 
   {
      ulGuardSecs = ulSecs + GUARD_CHECK_SECS;
   }
   else if(ulGuardSecs < ulSecs)
   {
      GLOBAL_SetSignalNotification(PID_GUARD, GLOBAL_HST_GRD_RUN);
      ulGuardSecs = ulSecs + GUARD_CHECK_SECS;
   }
   return(fCc);
}

//
// Function:   rpi_CheckLoopRecordings
// Purpose:    Check the amount of loop-recording files. Delete oldest if too many
//
// Parms:      Cur secs, loop rec struct ptr
// Returns:   
// Note:       Constantly called on each semaphore timeout
//             Normally the loop-recording files are being moved from the loop recording directory
//             into the archive directory. The files that remain in the loop directory have to be 
//             cleaned up once in a while. This routine mops up those motion files that stay behind.
//
//  u_int32     ulCheckSecs;
//  u_int32    *pulNextSecs;
//  int         iMaxFiles;
//  const char *pcPath;
//  const char *pcFilter;
//
static int64 rpi_CheckLoopRecordings(u_int32 ulSecs, const PILOOP *pstLoop, int iNr)
{
   int      x;
   char    *pcData=NULL;
   int64   llTotal=0, llSize;
   u_int32 ulNextSecs;
   char    *pcLoopDir;

   pcLoopDir = GLOBAL_GetParameter(PAR_LOOP);
   //
   for(x=0; x<iNr; x++)
   {
      ulNextSecs = *pstLoop->pulNextSecs;
      if(ulNextSecs < ulSecs)
      {
         if(pcData == NULL) pcData = (char *) safemalloc(MAX_PATH_LENZ);
         GEN_SNPRINTF(pcData, MAX_PATH_LEN, "%s/%s", pcLoopDir, pstLoop->pcPath);
         PRINTF("rpi-CheckLoopRecordings():[%s]" CRLF, pcData);
         //
         // Check if we have too many motion files: delete OLDEST files
         // Since FINFO sorts dirs/files alphabetically, it will find the
         // loop_YYYY-MM-DD_hh-mm-ss_m.* files OLDEST first !
         //
         llSize = RPI_RemoveFiles(pcData, (char *)pstLoop->pcFilter, pstLoop->iMaxFiles);
         if(llSize > 0)
         {
            llTotal += llSize;
            PRINTF("rpi-CheckLoopRecordings():[%s/%s]: Removed %lld Bytes" CRLF, pstLoop->pcPath, pstLoop->pcFilter, llSize);
         }
         *pstLoop->pulNextSecs = ulSecs + pstLoop->ulCheckSecs;
      }
      pstLoop++;
   }
   if(llTotal && RPI_GetFlag(FLAGS_NFY_USAGE))
   {
      //
      // Report back to PiKrellCam
      //
      GEN_SNPRINTF(pcData, MAX_PATH_LEN, "Removed %lld bytes of loop recording data", llTotal);
      RPI_InformFifo(NULL, (char *)"1 3 0", pcData, 4);
      llCacheTotal += llTotal;
   }
   if(pcData) safefree(pcData);
   return(llCacheTotal);
}

//
// Function:   rpi_CheckMidnight
// Purpose:    Trigger all threads on midnight, called every TIMEOUT_MSECS msecs.
//
// Parms:      Current secs timestamp
// Returns:    TRUE if midnight
// Note:       Constantly called on each semaphore timeout
//
static bool rpi_CheckMidnight(u_int32 ulSecs)
{
   static u_int32 ulMidnightSecs=0;
   //
   bool           fCc=FALSE;
   double         flFreed;

   if(ulMidnightSecs == 0) 
   {
      ulMidnightSecs = RTC_GetMidnight(ulSecs) + ES_SECONDS_PER_DAY;
      PRINTF("rpi-CheckMidnight():SET Midnight" CRLF);
   }
   else if(ulSecs > ulMidnightSecs)
   {
      if(GLOBAL_FlushLog() != EXIT_CC_OKEE) LOG_Report(errno, "HST", "rpi_CheckMidnight(): ERROR saving app files: ");
      //
      // Midnight: Notify the other threads and send summary email
      //
      GLOBAL_SetSignalNotification(PID_PING,    GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_GUARD,   GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_HUE,     GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_ARCHIVE, GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_MOTION,  GLOBAL_HST_ALL_MID);
      //
      // Setup timeout for thread response on Midnight
      //
      ulThreadsPending = ulSecs + THREADS_CHECK_SECS;
      //
      if(RPI_SendMail())
      {
         // Email send OKee: set time to check completion
         LOG_Report(0, "HST", "rpi-CheckMidnight():Email send OKee");
         ulSecsMailTimeout = ulSecs + MAILX_TIMEOUT;
         ulEmailPending    = 0;
      }
      else
      {
         // Email send error: Set retry moment
         LOG_Report(0, "HST", "rpi-CheckMidnight():Email send ERROR");
         ulSecsMailTimeout = 0;
         ulEmailPending    = ulSecs + MAILX_RETRY;
      }
      //
      flFreed = ((double)llCacheTotal) / ONEMEGABYTE;
      LOG_Report(0, "HST", "rpi-CheckMidnight():SafeMalloc balance=%d", GLOBAL_GetMallocs());
      LOG_Report(0, "HST", "rpi-CheckMidnight():Removed %5.3f MB of loop recordings.", flFreed);
      PRINTF("rpi-CheckMidnight():Midnight" CRLF);
      //
      llCacheTotal    = 0;
      ulMidnightSecs += (24 * 3600);
      rpi_HandleMotionCompleted(MOTCC_CLEAR);
      fCc = TRUE;
   }
   else if( ulEmailPending && (ulEmailPending < ulSecs) )
   {
      //
      // Mail did NOT go out last time: retry
      //
      if(RPI_SendMail())
      {
         LOG_Report(0, "HST", "rpi-CheckMidnight():Email send OKee");
         ulSecsMailTimeout = ulSecs + MAILX_TIMEOUT;
         ulEmailPending    = 0;
      }
      else
      {
         LOG_Report(0, "HST", "rpi-CheckMidnight():Email send ERROR");
         ulSecsMailTimeout = 0;
         ulEmailPending   += MAILX_RETRY;
      }
   }
   return(fCc);
}

//
// Function:  rpi_CheckMotionSummary
// Purpose:   Periodically, request motion to build summary
//            Summary will be send to file
//
// Parms:     Current secs
// Returns:   TRUE if OKee
// Note:       Constantly called on each semaphore timeout
//
static bool rpi_CheckMotionSummary(u_int32 ulSecs)
{
   static u_int32 ulSummarySecs=0;
   //
   bool  fCc=TRUE;

   if(iCliSumSecs)
   {
      if(ulSummarySecs == 0) ulSummarySecs = ulSecs + iCliSumSecs;
      else if(ulSummarySecs < ulSecs)
      {
         LOG_Report(0, "HST", "rpi-CheckMotionSummary():Test Create Mail Body");
         PRINTF("rpi-CheckMotionSummary():Test Create Mail Body" CRLF);
         RPI_CreateMailBody();
         ulSummarySecs = 0;
      }
   }
   return(fCc);
}

//
//  Function:  rpi_CheckTemperature
//  Purpose:   Periodically, check the system core temperature
//
//  Parms:     Current secs
//  Returns:   
//
static void rpi_CheckTemperature(u_int32 ulSecs)
{
   static u_int32 ulTempCheckSecs=0;
   static double  flOldTemp=0.0;

   if(ulTempCheckSecs == 0) 
   {
      ulTempCheckSecs = ulSecs + TEMP_CHECK_SECS;
   }
   else if(ulTempCheckSecs < ulSecs)
   {
      if( rpi_ExtractSystemVars(SVAR_TEMPERATURE) )
      {
         //PRINTF("rpi-CheckTemperature():CPU Temp=%3.1f C" CRLF, flCpuTemp);
         pstMap->G_flCpuTemperature = flCpuTemp;
         if( fabs(flCpuTemp - flOldTemp) > MAX_TEMP_DELTA)
         {
            PRINTF("rpi-CheckTemperature():CPU Temp=%3.1f C (was %3.1f C)" CRLF, flCpuTemp, flOldTemp);
            LOG_Report(0, "HST", "rpi-CheckTemperature():CPU Temp=%3.1f C (was %3.1f C)", flCpuTemp, flOldTemp);
            flOldTemp = flCpuTemp;
         }
      }
   }
}

//
// Function:   rpi_CheckThreads
// Purpose:    Check if all applicable threads have responded to the midnight call
//             If not, try to restart the thread
// Parms:      Current secs timestamp
// Returns:    TRUE if OKee
// Note:       Constantly called on each semaphore timeout
//
static bool rpi_CheckThreads(u_int32 ulSecs)
{
   bool  fCc=TRUE;

   if( ulThreadsPending && (ulSecs > ulThreadsPending) )
   {
      fCc = GLOBAL_PidCheckThreads(ulSecs);
      ulThreadsPending = 0;
   }
   return(fCc);
}

//
// Function:   rpi_CheckVideoFreeze
// Purpose:    Periodically, check if video is still running if it should.
//             If not, reboot (untill we find a way to reset the video-freeze)
//
// Parms:      Current secs
// Returns:    TRUE if video is frozen while it should be running
// Note:       Constantly called on each semaphore timeout
//
static bool rpi_CheckVideoFreeze(u_int32 ulSecs)
{
   static u_int32 ulFreezeCheckSecs=0;
   //
   bool     fCc=FALSE;

   if(ulFreezeCheckSecs == 0) 
   {
      ulFreezeCheckSecs = ulSecs + (15*60);
   }
   else if(ulFreezeCheckSecs < ulSecs)
   {
      int      iLastVideoSecs;
      u_int32  ulVideo;
      RUNST    tRunState;
      char    *pcLoopFile;
      PIKTIM  *pstTs;

      ulFreezeCheckSecs = 0;
      //
      // Video freeze check time
      //
      if( (tRunState = (RUNST)pstMap->G_iRunState) == RUN_STATE_RUNNING)
      {
         pstTs = (PIKTIM *) safemalloc(sizeof(PIKTIM));
         //==================================================================================
         // From state file "/run/pikrellcam/state":
         // Get the last recorded video (loop for motion detect) filename and extract the timestamp 
         // info from it. If the MMAP video acquisition is OTL, this timestamp will freeze.
         //==================================================================================
         pcLoopFile = GLOBAL_GetParameter(PAR_REC_FILE);
         PRINTF("rpi-CheckVideoFreeze():VideoLoopfile=[%s]" CRLF, pcLoopFile);
         if( RPI_ExtractTimestamp(pcLoopFile, pstTs) )
         {
            ulVideo = RPI_GetTimestampSecs(pstTs);
            //
            PRINTF("rpi-CheckVideoFreeze():SecsNow   = %d Secs" CRLF, ulSecs);
            PRINTF("rpi-CheckVideoFreeze():VideoLoop = %d Secs" CRLF, ulVideo);
            PRINTF("rpi-CheckVideoFreeze():          = %2d:%02d:%02d" CRLF, pstTs->iHour, pstTs->iMinute, pstTs->iSecond);
            //
            // Calc iLastVideoSecs, the timestamp of the last video sample
            //
            if(ulSecs > ulVideo)
            {
               iLastVideoSecs = (int)(ulSecs - ulVideo);
               //
               if(iLastVideoSecs > MAX_VIDEO_ACQ_SECS)
               {
                  PRINTF("rpi-CheckVideoFreeze():Video frozen for %d secs: REBOOT !!" CRLF, iLastVideoSecs);
                  LOG_Report(0, "HST", "rpi-CheckVideoFreeze():Last Video was [%s]", pcLoopFile);
                  LOG_Report(0, "HST", "rpi-CheckVideoFreeze():Video frozen for %d secs: REBOOT !!" , iLastVideoSecs);
                  fCc = TRUE;
               }
               else PRINTF("rpi-CheckVideoFreeze():Video OKee" CRLF);
            }
            else
            {
               LOG_Report(0, "HST", "rpi-CheckVideoFreeze():BAD:Last Video was [%s]", pcLoopFile);
               LOG_Report(0, "HST", "rpi-CheckVideoFreeze():SecsNow   = %d Secs" CRLF, ulSecs);
               LOG_Report(0, "HST", "rpi-CheckVideoFreeze():VideoLoop = %d Secs" CRLF, ulVideo);
               LOG_Report(0, "HST", "rpi-CheckVideoFreeze():          = %2d:%02d:%02d" CRLF, pstTs->iHour, pstTs->iMinute, pstTs->iSecond);
            }
         }
         else PRINTF("rpi-CheckVideoFreeze():ERROR getting timestamp from [%s]" CRLF, pcLoopFile);
         safefree(pstTs);
      }
   }
   return(fCc);
}

//
// Function:   rpi_CheckWan
// Purpose:    Retrieve the WAN address if it is time to verify it
//
// Parms:      Secs now
// Returns:    
// Note:       This call checks if the WAN address hasn't changed
//             It will update pstMap->G_iWanAddr to:
//             WAN_NOT_FOUND     0: Not yet retrieved
//             WAN_SAME          1: WAN retrieved and no change
//             WAN_CHANGED       2: WAN retrieved and has changed
//             WAN_ERROR        -1: ERROR retrieving
//
//             It will be reset when the latest 24 hour status has been emailed !
//
static void rpi_CheckWan(u_int32 ulSecsNow)
{
   static u_int32 ulSecsWanCheck=0;
   //
   const char    *pcAny="";
   char          *pcIp;

   if(ulSecsWanCheck == 0) 
   {
      ulSecsWanCheck = ulSecsNow + WAN_CHECK_SECS;
      pstMap->G_iWanAddr = WAN_NOT_FOUND;
      GEN_MEMSET(pstMap->G_pcWanAddr, 0x00, MAX_ADDR_LENZ);
   }
   else if(ulSecsNow > ulSecsWanCheck)
   {
      if(GEN_STRLEN(pstMap->G_pcWanUrl))
      {
         //
         // Check if we still have a network connection
         //
         pcIp = safemalloc(256);
         if( HTTP_GetFromNetwork(pstMap->G_pcWanUrl, (char *)pcAny, pcIp, 255, 20) )
         {
            //PRINTF1("rpi-CheckWan():WAN IP-Address found: %s" CRLF, pcIp);
            if(GEN_STRCMP(pcIp, pstMap->G_pcWanAddr) != 0)
            {
               LOG_Report(0, "HST", "rpi-CheckWan():WAN Changed from [%s] to [%s]", pstMap->G_pcWanAddr, pcIp);
               GEN_STRNCPY(pstMap->G_pcWanAddr, pcIp, MAX_ADDR_LEN);
               pstMap->G_iWanAddr |= WAN_CHANGED;
            }
            else 
            {
               //LOG_Report(0, "HST", "rpi-CheckWan():WAN still %s", pcIp);
               pstMap->G_iWanAddr |= WAN_SAME;
            }
         }
         else
         {
            PRINTF("rpi-CheckWan():WAN IP-address not resolved" CRLF);
            LOG_Report(errno, "HST", "rpi-CheckWan():Not able to connect with WAN service: ");
            pstMap->G_iWanAddr |= WAN_ERROR;
         }
         safefree(pcIp);
      }
      else
      {
         pstMap->G_iWanAddr |= WAN_NO_URL;
      }
      ulSecsWanCheck += WAN_CHECK_SECS;
   }
}

// 
// Function:   rpi_ExtractSystemVars
// Purpose:    Extract data from the system
// 
// Parameters: System var SVAR_xxxx
// Returns:    TRUE if OKee
// 
static bool rpi_ExtractSystemVars(SVAR tVar)
{
   bool     fCc=FALSE;
   FILE    *tFp;
   double   flVal;
   double  *pflVal;
   char    *pcFile;
   char     pcBuffer[64];

   if(tVar < NUM_SVARS)
   {
      pcFile = stSystemVars[tVar].pcFile;
      if(pcFile == NULL) return(FALSE);
      //
      if( (tFp = fopen(pcFile, "r")) != NULL)
      {
         switch(tVar)
         {
            default:
            case SVAR_NONE:
               break;   
            
            case SVAR_TEMPERATURE:
               if(fgets(pcBuffer, sizeof(pcBuffer), tFp) != NULL)
               {
                  flVal   = atof(pcBuffer);
                  flVal  /= 1000;
                  pflVal  = (double *)stSystemVars[tVar].pvData;
                  *pflVal = flVal;
                  fCc     = TRUE;
               }
               break;   
         }
         fclose(tFp);
      }
      else
      {
         LOG_Report(errno, "HST", "RPI-ExtractSystemVars():Open ERROR:");
      }
   }
   return(fCc);
}

//
// Function:   rpi_HandleMotionAcquisition
// Purpose:    Handle the motion acquisition
//             MOT_ACQ_BEGIN
//             MOT_ACQ_COMPLETED
//             MOT_ACQ_END
//             MOT_ACQ_ARCHIVED
//             MOT_ACQ_HERON
// Parms:      Current system sec
// Returns:    
// Note:       fOnMotEndPending (ON_MOTION_END pending flag) was set if the ON_MOTION_END signal 
//             came before motion acquisition was completed.
//
//             G_stMotion has the latest motion acquisition data
//             Loop Start 
//                Recording:     12.44.02_0.h264   <-- Motion detection within this file
//                OSD:           12.42.33_0.mp4    <-- Previous file being archived
//                state:         12.42.33_0.mp4    <-- Previous file being archived
//                motion-args:    <some previous recording>
//                motion-events:  <some previous recording>
//              
//              ON_MOTION_BEGIN
//                Recording:     12.44.02_0.h264   <-- Motion detection within this file
//                OSD:           12.42.33_0.mp4       < Previous file >
//                state:         12.42.33_0.mp4       < Previous file >
//                motion-args:   12.42.33_0.mp4       < Previous file >
//                motion-events: 12.44.02_0.mp4    <-- Motion detection within this file
//              
//              ON_MOTION_END
//                Recording:     12.45.32_0.h264      < Already next file being recorded >
//                OSD:           12.44.02_m.mp4
//                state:         12.44.02_m.mp4    
//                motion-args:   12.44.02_m.mp4    <-- Motion detected : *_m.mp4
//                motion-events: 12.44.02_0.mp4
//   
//
static void rpi_HandleMotionAcquisition(u_int32 ulSecs)
{
   char *pcLoopFile;
   int  *piState = (int *)GLOBAL_GetParameter(PAR_MOT_STATE);

   switch(*piState)
   {
      case MOT_ACQ_BEGIN:
         if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CAM_HST_MOT)) 
         {
            PRINTF("rpi-HandleMotionAcquisition():ON_MOTION_BEGIN" CRLF);
            if(RPI_GetFlag(FLAGS_FIF_MOTION)) RPI_InformFifo(NULL, (char *)"1 3 0", "Cam-Hst Motion Begin", 4);
            //
            // PiKrellCam ON_MOTION_BEGIN (SIGUSR1)
            //
            RPI_ExtractVariables(pcArgsFile,   stPiArguments, iNumPiArguments);
            RPI_ExtractVariables(pcEventsFile, stPiEvents,    iNumPiEvents);
            RPI_ExtractVariables(pcStateFile,  stPiStateVars, iNumPiStateVars);
            //====================================================================================
            // Get the currently recorded video filename and extract the timestamp info from it.
            // If that goes well, G_stTs.fConverted signals a valid conversion. 
            //====================================================================================
            pcLoopFile = GLOBAL_GetParameter(PAR_LOOP_FILE);
            RPI_ExtractTimestamp(pcLoopFile, &pstMap->G_stTs);
            //
            PRINTF("rpi-HandleMotionAcquisition():Video=[%s]" CRLF, pcLoopFile);
            //
            if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "HST", "ON_MOTION_BEGIN");
            if(pstMap->G_stTs.fConverted)
            {
               PRINTF("rpi-HandleMotionAcquisition():Motion-Event:Video = %s" CRLF, pcLoopFile);
               GLOBAL_SetSignalNotification(PID_MOTION, GLOBAL_HST_MOT_RUN);
            }
            fOnMotEndPending = FALSE;
            *piState         = MOT_ACQ_COMPLETED;
         }
         break;

      case MOT_ACQ_COMPLETED:
         //
         // Wait till motion acquisition signals the end of motion
         //
         if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_MOT_HST_NFY))
         {
            PRINTF("rpi-HandleMotionAcquisition():ON_MOTION_END" CRLF);
            if(RPI_GetFlag(FLAGS_FIF_MOTION)) RPI_InformFifo(NULL, (char *)"1 3 0", "Mot-Hst Motion Nfy", 4);
            RPI_ExtractVariables(pcArgsFile,   stPiArguments, iNumPiArguments);
            RPI_ExtractVariables(pcEventsFile, stPiEvents,    iNumPiEvents);
            RPI_ExtractVariables(pcStateFile,  stPiStateVars, iNumPiStateVars);
            //
            rpi_HandleMotionCompleted(MOTCC_SET);
            *piState = MOT_ACQ_END;
         }
         if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CAM_HST_ARC))
         {
            if(RPI_GetFlag(FLAGS_FIF_MOTION)) RPI_InformFifo(NULL, (char *)"1 3 0", "Cam-Hst Motion End Pending", 4);
            //
            // we are getting the 
            // PiKrellCam ON_MOTION_END (SIGUSR2)
            // before the motion acquisition has been completed. 
            // Remember the ON_MOTION_END when we get the motion acquisition completed.
            //
            if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "HST", "Pending ON_MOTION_END");
            GLOBAL_SetSignalNotification(PID_MOTION, GLOBAL_HST_MOT_PEN);
            fOnMotEndPending = TRUE;
         }
         break;

      case MOT_ACQ_END:
         if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CAM_HST_ARC) || fOnMotEndPending) 
         {
            PRINTF("rpi-HandleMotionAcquisition():ON_MOTION_END" CRLF);
            if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "HST", "ON_MOTION_END");
            if(RPI_GetFlag(FLAGS_FIF_MOTION)) RPI_InformFifo(NULL, (char *)"1 3 0", "Cam-Hst Motion End", 4);
            //
            // PiKrellCam ON_MOTION_END (SIGUSR2)
            //
            RPI_ExtractVariables(pcArgsFile,   stPiArguments, iNumPiArguments);
            RPI_ExtractVariables(pcEventsFile, stPiEvents,    iNumPiEvents);
            RPI_ExtractVariables(pcStateFile,  stPiStateVars, iNumPiStateVars);
            //
            if(pstMap->G_stTs.fConverted)
            {
               pcLoopFile = GLOBAL_GetParameter(PAR_LOOP_FILE);
               PRINTF("rpi-HandleMotionAcquisition():Start archiving %s" CRLF, pcLoopFile);
               GLOBAL_SetSignalNotification(PID_ARCHIVE, GLOBAL_HST_ARC_RUN);
            }
            fOnMotEndPending = FALSE;
            *piState         = MOT_ACQ_ARCHIVED;
         }
         break;

      case MOT_ACQ_ARCHIVED:
         if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_ARC_HST_NFY))
         {
            RPI_ExtractVariables(pcArgsFile,   stPiArguments, iNumPiArguments);
            RPI_ExtractVariables(pcEventsFile, stPiEvents,    iNumPiEvents);
            RPI_ExtractVariables(pcStateFile,  stPiStateVars, iNumPiStateVars);
            //
            LOG_Report(0, "HST", "rpi-HandleMotionAcquisition():Archiving completed.");
            //
            // Archiving has been completed
            //
            fOnMotEndPending = FALSE;
            *piState         = MOT_ACQ_BEGIN;
            PRINTF("rpi-HandleMotionAcquisition():===========================================================================" CRLF);
         }
         break;

      case MOT_ACQ_HERON:
         if(ulSecs > ulHeronSecsEnd)
         {
            LOG_Report(0, "HST", "rpi-HandleMotionAcquisition(): END Heron archiving");
            PRINTF("rpi-HandleMotionAcquisition(): END Heron archiving" CRLF);
            fOnMotEndPending = FALSE;
            ulHeronSecs      = 0;
            ulHeronSecsEnd   = 0;
            *piState         = MOT_ACQ_BEGIN;
         }
         else
         {
            if(ulSecs > ulHeronSecs)
            {
               PRINTF("rpi-HandleMotionAcquisition(): Request Heron archiving" CRLF);
               GLOBAL_SetSignalNotification(PID_ARCHIVE, GLOBAL_HST_ARC_HER);
               ulHeronSecs = ulSecs + HERON_ARCHIVE_SECS; 
            }
         }
         break;
   }
}

//
// Function:   rpi_HandleMotionCompleted
// Purpose:    Motion acquisition has been completed
//             MOTCC_SET
//             MOTCC_CLEAR
// Parms:      
// Returns:    TRUE if OKee
// Note:       G_stMotion has the latest motion acquisition data
//
static bool rpi_HandleMotionCompleted(MOTCC tMode)
{
   int      iR, iRmin, iCsum, iCmin;

   switch(tMode)
   {
      default:
         break;
   
      case MOTCC_SET:
         PRINTF("rpi-HandleMotionCompleted(SET):Sum counts" CRLF);
         //
         // Save todays largest set of motion timestamp to email later
         //
         for(iCsum=0,iR=0; iR<MAX_MOTION_REGIONS; iR++)
         {
            // Sum Regional Motion counts
            iCsum += pstMap->G_stMotion.stFrame.stRegion[iR].iCount;
         }
         //
         // Save the motion for later reporting if not the smallest
         //
         iCmin = pstMap->G_iMaxMotions[0];
         for(iRmin=0,iR=1; iR<MAX_NUM_MOTIONS; iR++)
         {
            //PRINTF("rpi-HandleMotionCompleted(SET):%2d Cnt=%4d, Ts=%d" CRLF, iR, pstMap->G_iMaxMotions[iR], pstMap->G_ulMaxMotionsTs[iR]);
            if(iCmin > pstMap->G_iMaxMotions[iR])
            {
               // Mark the smallest in the list
               iCmin = pstMap->G_iMaxMotions[iR];
               iRmin = iR;
            }
         }
         if(iCmin < iCsum)
         {
            // Current motion is larger than the smallest in the list: save this one instead
            // LOG_Report(0, "HST", "rpi-HandleMotionCompleted(SET):Replace smallest in list: Count[%d] was %d, now %d", iRmin, iCmin, iCsum);
            PRINTF("rpi-HandleMotionCompleted(SET):Replace smallest in list: Count[%d] was %d, now %d" CRLF, iRmin, iCmin, iCsum);
            pstMap->G_iMaxMotions[iRmin]    = iCsum;
            pstMap->G_ulMaxMotionsTs[iRmin] = RPI_GetTimestampSecs(&pstMap->G_stTs);
         }
         RPI_DUMP_MOTION_DATA();
         break;

      case MOTCC_CLEAR:
         LOG_Report(0, "HST", "rpi-HandleMotionCompleted(CLR):Clear list");
         PRINTF("rpi-HandleMotionCompleted(CLR):Clear list" CRLF);
         rpi_ResetSummary();
         break;
   }
   return(TRUE);
}

// 
// Function:   rpi_RequestReboot
// Purpose:    System request to reboot
// 
// Parameters: 
// Returns:    New Running flag (FALSE if reboot needed)
// Note:       Without an active Watchdog, poweroff will NOT reboot the system, so we need 
//             a dedicated one here
// 
static bool rpi_RequestReboot()
{
   PRINTF("rpi-RequestReboot():[%s]" CRLF, pcShellReboot);
   LOG_Report(0, "HST", "rpi-RequestReboot():[%s]" CRLF, pcShellReboot);
   system(pcShellReboot);
   return(FALSE);
}

//
// Function:   rpi_ResetSummary
// Purpose:    Reset daily Summary
//             
// Parms:      
// Returns:     
// Note:       
// 
static void rpi_ResetSummary()
{
   pstMap->G_iArchiveAdd = 0;
   pstMap->G_iArchiveDel = 0;
   //
   GEN_MEMSET(pstMap->G_iRegionMagnitude, 0x00, sizeof(pstMap->G_iRegionMagnitude));
   GEN_MEMSET(pstMap->G_iRegionCount,     0x00, sizeof(pstMap->G_iRegionCount));
   GEN_MEMSET(pstMap->G_iMaxMotions,      0x00, sizeof(pstMap->G_iMaxMotions));
   GEN_MEMSET(pstMap->G_ulMaxMotionsTs,   0x00, sizeof(pstMap->G_ulMaxMotionsTs));
}

//
//  Function:   rpi_SetupGpio()
//  Purpose:    Setup GPIO for various IO alternatives
//
//  Parms:      
//  Returns:    
//
static void rpi_SetupGpio()
{
   printf("rpi-SetupGpio():" RPI_IO_BOARD CRLF);

#ifdef FEATURE_IO_NONE
   //=============================================================================
   // Use NO I/O board
   //=============================================================================
#endif


#ifdef FEATURE_IO_PATRN
   //=============================================================================
   // Used Patrn.nl IO board (wiringPi API)
   //=============================================================================
   if( (wiringPiSetup() == -1) )
   {
      printf("rpi-SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(LED_Y); OUT_GPIO(LED_Y);
   INP_GPIO(LED_G); OUT_GPIO(LED_G);
#endif

#ifdef FEATURE_IO_IQAUDIO
   //=============================================================================
   // Used IQaudIO-Zero board (wiringPi API)
   //=============================================================================
   if( (wiringPiSetup() == -1) )
   {
      printf("rpi-SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(LED_R); OUT_GPIO(LED_R);
   INP_GPIO(LED_G); OUT_GPIO(LED_G);
   INP_GPIO(IO_BUTTON);
#endif
}

//
//  Function:   rpi_SetupUser
//  Purpose:    Setup the actual User for the app
//
//  Parms:      Username
//  Returns:    TRUE if OKee
//
static bool rpi_SetupUser(const char *pcUser)
{
   bool  fOkee=TRUE;
   uid_t tUid;
   gid_t tGid;

// #define FEATURE_SHOW_USER
   #ifdef FEATURE_SHOW_USER
   struct passwd *pstPwd;

   errno  = 0;
   pstPwd = getpwnam(pcUser);
   if (pstPwd == NULL) 
   {
      printf("rpi_SetupUser(): Error using <%s>:%s"  CRLF, pcUser, strerror(errno));
   }
   else
   {
      printf("rpi_SetupUser(): Name  = %s" CRLF, pstPwd->pw_name);
      printf("rpi_SetupUser(): Psw   = %s" CRLF, pstPwd->pw_passwd);
      printf("rpi_SetupUser(): Gecos = %s" CRLF, pstPwd->pw_gecos);
      printf("rpi_SetupUser(): Dir   = %s" CRLF, pstPwd->pw_dir);
      printf("rpi_SetupUser(): Shell = %s" CRLF, pstPwd->pw_shell);
   }
   #endif   //FEATURE_SHOW_USER


   if(getuid() == 0)
   {
      //
      // We are ROOT
      //
      if(!RPI_DirectoryExists((char *)pcCacheDir))  
      {
         //
         // Cache dir does not yet exist: create it
         //
         if( (fOkee = RPI_DirectoryCreate((char *)pcCacheDir, ATTR_RWXR_XR_X, FALSE)) )
         {
            if( (fOkee = GEN_ChangeOwner(pcCacheDir, pcNewUser, pcGroupWww, ATTR_RWXR_XR_X, TRUE)) )
            {
               int   iFd;
               //
               // Create the CSV file
               //
               if( (iFd = safeopen2((char *)pcCsvFile, O_RDWR|O_CREAT|O_TRUNC, 0640)) < 0) fOkee = FALSE;
               else
               {
                  safeclose(iFd);
                  fOkee = GEN_ChangeOwner(pcCsvFile, pcNewUser, pcGroupWww, ATTR_RW_R__R__, FALSE);
               }
            }
         }
      }
      if(fOkee)
      {
         //
         // Cache Dir exists, attributes changed. 
         // Create/copy mapfile as root, then switch to userspace
         //
         GLOBAL_Init();
         fOkee = GEN_ChangeOwner(pcMapFile, pcNewUser, pcGroupWww, ATTR_RW_R__R__, FALSE);
         //
         // All dirs and files have the correct user/group ID
         // Switch to new user
         //
         tUid = GEN_GetUserIdByName(pcUser);
         tGid = GEN_GetGroupIdByName(pcGroupWww);
         //
         if( (tUid > 0) && (tGid > 0) )
         {
            if( setgid(tGid) == -1)
            {
               printf("rpi-SetupUser():ERROR setting Group %s (%d): %s" CRLF, pcGroupWww, (int)tGid, strerror(errno));
               fOkee = FALSE;
            }
            else 
            {
               printf("rpi-SetupUser():Now Group %s (%d)" CRLF, pcGroupWww, (int)tGid);
            }
            if( setuid(tUid) == -1)
            {
               printf("rpi-SetupUser():ERROR setting User %s (%d): %s" CRLF, pcUser, (int)tUid, strerror(errno));
               fOkee = FALSE;
            }
            else 
            {
               printf("rpi-SetupUser():Now User %s (%d)" CRLF, pcUser, (int)tUid);
            }
         }
         else 
         {
            printf("rpi-SetupUser():Error setting up User %s" CRLF, pcUser);
            fOkee = FALSE;
         }
      }
      else printf("rpi-SetupUser():Error changing attributes for User %s" CRLF, pcUser);
   }
   else
   {
      printf("rpi-SetupUser():Startup as Root !" CRLF);
      fOkee = FALSE;
   }
   return(fOkee);
}

//
//  Function:  rpi_Startup
//  Purpose:   Startup all threads
//
//  Parms:     
//  Returns:   Init markers 
//
static int rpi_Startup(void)
{
   int   iStup=0;

   //
   // Start aux threads:
   //
   iStup |= MOTN_Init();
   iStup |= ARCH_Init();
   iStup |= GRD_Init();
   iStup |= HUE_Init();
   iStup |= SRVR_Init();
   iStup |= PING_Init();
   //
   PRINTF("rpi-Wait():Startup flags =0x%08X" CRLF, iStup);
   return(iStup);
}

//
//  Function:  rpi_WaitStartup
//  Purpose:   Wait until all threads are doing fine
//
//  Parms:     Initial flags of all threads which started
//  Returns:   True if all OK
//
static bool rpi_WaitStartup(int iInit)
{
   bool     fWaiting=TRUE;
   bool     fCc=FALSE;
   int      iStartup=0, iHalfSecs = 20;

   //
   // Mask only INIT threads
   //
   iInit &= GLOBAL_XXX_INI;
   //
   while(fWaiting)
   {
      switch( GLOBAL_SemaphoreWait(PID_HOST, 500) )
      {
         case 0:
            // Thread signalled: verify completion
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_GRD_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Guard OKee");      iStartup |= GLOBAL_GRD_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_MOT_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Motion OKee");     iStartup |= GLOBAL_MOT_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_ARC_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Archiving OKee");  iStartup |= GLOBAL_ARC_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_HUE_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Hue Bridge OKee"); iStartup |= GLOBAL_HUE_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SVR_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Server OKee");     iStartup |= GLOBAL_SVR_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_PNG_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Ping OKee");       iStartup |= GLOBAL_PNG_INI;}
            //
            if(iStartup == iInit)
            {
               PRINTF("rpi-WaitStartup():All inits OKee" CRLF);
               LOG_Report(0, "HST", "rpi-WaitStartup():All inits OKee");
               fWaiting = FALSE;
               fCc      = TRUE;
            }
            else
            {
               PRINTF("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
            }
            break;

         case -1:
            // Error
            fWaiting = FALSE;
            break;

         case 1:
            if(iHalfSecs-- == 0)
            {
               // Timeout: problems
               LOG_Report(0, "HST", "rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X", iInit, iStartup);
               PRINTF("rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X" CRLF, iInit, iStartup);
               fWaiting = FALSE;
            }
            break;
      }
      if(iStartup == iInit)
      {
         LOG_Report(0, "HST", "rpi-WaitStartup():All inits OKee");
         fWaiting = FALSE;
         fCc      = TRUE;
      }
      else
      {
         PRINTF("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
      }
   }
   return(fCc);
}


/* ======   Local Functions separator ===========================================
__PARENT_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &rpi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &rpi_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   PRINTF("rpi-ReceiveSignalTerm()" CRLF);
   LOG_Report(0, "HST", "rpi-ReceiveSignalTerm()");
   //
   fRpiRunning = FALSE;
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   PRINTF("rpi-ReceiveSignalInt()" CRLF);
   LOG_Report(0, "HST", "rpi-ReceiveSignalInt()");
   //
   fRpiRunning = FALSE;
}

//
// Function:   rpi_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void rpi_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

//
// Function:   rpi_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1: PiKrellCam motion:
//             on_motion_begin $C/pikrellmotion.sh $G $P $v $A $m $a $V $t $z $S $L
//
//             Argument substitution done by PiKrellCam before running this script:
//                $C - scripts directory so this script is found.
//                $G - log file configured in ~/.pikrellcam/pikrellcam.conf.
//                $P - the command FIFO so we can write commands to PiKrellCam.
//                $v - the full path name of the last motion video file (or "none").
//                $A - the full path name of the last motion thumbnail file.
//                $m - the full path name of the Media     directory
//                $a - the full path name of the Archive   directory
//                $V - the full path name of the Videos    directory
//                $t - the full path name of the Thumbs    directory
//                $z - the full path name of the Loop      directory
//                $S - the full path name of the Still     directory
//                $L - the full path name of the Timelapse directory
//
//             pikrellmotion.sh on its turn runs:
//                sudo /usr/bin/pikrellmo "$@" 28
//                who writes all arguments to /run/pikrellcam/motion-args and sends
//                SIGUSR1 to PiKrellMan
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_CAM_HST_MOT);
   PRINTF("rpi-ReceiveSignalUser1()" CRLF);
}

//
// Function:   rpi_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2: PiKrellCam Archive:
//             on_motion_end    $C/pikrellarchive.sh $G $P $v $A $m $a $V $t $z $S $L
//             on_motion_enable $C/pikrellarchive.sh $G $P $v $A $m $a $V $t $z $S $L
//
//             Argument substitution done by PiKrellCam before running this script:
//                $C - scripts directory so this script is found.
//                $G - log file configured in ~/.pikrellcam/pikrellcam.conf.
//                $P - the command FIFO so we can write commands to PiKrellCam.
//                $v - the full path name of the last motion video file (or "none").
//                $A - the full path name of the last motion thumbnail file.
//                $m - the full path name of the Media     directory
//                $a - the full path name of the Archive   directory
//                $V - the full path name of the Videos    directory
//                $t - the full path name of the Thumbs    directory
//                $z - the full path name of the Loop      directory
//                $S - the full path name of the Still     directory
//                $L - the full path name of the Timelapse directory
//
//             pikrellarchive.sh on its turn runs:
//                sudo /usr/bin/pikrellar "$@" 28
//                who writes all arguments to /run/pikrellcam/motion-args and sends
//                SIGUSR2 to PiKrellMan
//
static void rpi_ReceiveSignalUser2(int iSignal)
{
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_CAM_HST_ARC);
   PRINTF("rpi-ReceiveSignalUser2()" CRLF);
}

/*------  Local functions separator ------------------------------------
_____________________TEST_DEBUG(){};
------------------------------x----------------------------------------*/

//
// Function:   rpi_DisplayMapfile
// Purpose:    Display MMAP file data
//
// Parms:      
// Returns:    
// Note:       
//
static void rpi_DisplayMapfile()
{
   int   iOffs;
   void *pvMap;
   void *pvTmp1;
   void *pvTmp2;

   printf("Sizeof char       = %d" CRLF, sizeof(char));                       // 1
   printf("Sizeof int        = %d" CRLF, sizeof(int));                        // 4
   printf("Sizeof long       = %d" CRLF, sizeof(long));                       // 4
   printf("Sizeof int64      = %d" CRLF, sizeof(int64));                      // 8
   printf("Sizeof long long  = %d" CRLF, sizeof(long long));                  // 8
   printf("Sizeof char *     = %d" CRLF, sizeof(char *));                     // 4
   printf(CRLF);
   printf("Sizeof Map file   = %d" CRLF, sizeof(RPIMAP)+4);                   // include "EOF"
   printf("Sizeof stPidList  = %d" CRLF, sizeof(pstMap->G_stPidList));        // 120
   printf("Sizeof pcHostname = %d" CRLF, sizeof(pstMap->G_pcHostname));       // 32
   printf("Sizeof stDynPages = %d" CRLF, sizeof(pstMap->G_stDynPages));       // 3584
   printf("Sizeof stLog      = %d" CRLF, sizeof(pstMap->G_stLog));            // 96
   printf("Sizeof PIKTIM     = %d" CRLF, sizeof(pstMap->G_stTs));
   printf("Sizeof PIDMOT     = %d" CRLF, sizeof(pstMap->G_stMotion));
   printf("Sizeof HUECFG     = %d" CRLF, sizeof(pstMap->G_stHueL));
   printf(CRLF);
   //
   pvMap   = pstMap;
   pvTmp1  = &pstMap->G_pcSparePoolZero;
   pvTmp2  = &pstMap->G_iResetEnd;
   //
   iOffs   = (int) (pvTmp1 - pvMap);
   printf("SpareZero Start   = 0x%x (%d)" CRLF, iOffs, iOffs);
   //
   iOffs   = (int) (pvTmp2 - pvMap);
   printf("SpareZero End     = 0x%x (%d)" CRLF, iOffs, iOffs);
   //
   printf("SpareZero Used    = %d" CRLF, EXTRA_POOL_ZERO);
   printf("SpareZero Free    = %d" CRLF, SPARE_POOL_ZERO - EXTRA_POOL_ZERO);
   //
   pvTmp1  = &pstMap->G_pcSparePoolPers;
   pvTmp2  = &pstMap->G_iSignature2;
   //
   iOffs   = (int) (pvTmp1 - pvMap);
   printf("SparePers Start   = 0x%x (%d)" CRLF, iOffs, iOffs);  
   //
   iOffs   = (int) (pvTmp2 - pvMap);
   printf("SparePers End     = 0x%x (%d)" CRLF, iOffs, iOffs);
   //
   printf("SparePers Used    = %d" CRLF, EXTRA_POOL_PERS);
   printf("SparePers Free    = %d" CRLF, SPARE_POOL_PERS - EXTRA_POOL_PERS);
}

//
//  Function:  rpi_TestMmap
//  Purpose:   Test/list all MMAP vars
//
//  Parms:     
//  Returns:   
//
static void rpi_TestMmap()
{
   void    *pvVar;
   int      iSize, iVal;
   GLOPAR   x;
   const GLOBALS *pstGloPar;

   pstGloPar = GLOBAL_GetParameters();
   //
   GEN_Printf(CRLF);
   for(x=0; x<NUM_GLOBAL_DEFS; x++)
   {
      pvVar = GLOBAL_GetParameter(x);
      iSize = GLOBAL_GetParameterSize(x);
      iVal  = pstGloPar[x].iValueOffset;
      //
      GEN_Printf("%2d: Map=%p,  Offset=%4X (%5d)  Addr=%p  Size=%3d   ", x, pstMap, iVal, iVal, pvVar, iSize);
      //
      switch(GLOBAL_GetParameterType(x))
      {
         case PAR_A: 
            // Parm is ASCII
            GEN_Printf("(A):%s" CRLF, (char *)pvVar);
            break;

         case PAR_B: 
            // Parm is BCD
            GEN_Printf("(B):%d" CRLF, *(int *)pvVar);
            break;

         case PAR_F: 
            // Parm is FLOAT
            GEN_Printf("(F):%f" CRLF, *(double *)pvVar);
            break;

         case PAR_H: 
            // Parm is HEX
            GEN_Printf("(H):0x%x" CRLF, *(int *)pvVar);
            break;

         default:
            GEN_Printf("(-)" CRLF);
            break;
      }
   }
   GEN_Printf(CRLF);
}

#ifdef   FEATURE_DUMP_MOTION_DATA

#ifdef   FEATURE_TEST_MOTION_LIST
typedef struct DLST
{
   u_int32 ulSecs;
   int     iCount; 
}  DLST;

static const DLST stDummyList[] =
{
   {  1640429450,  322  },
   {  1640431777,  106  },
   {  1640449496,  794  },
   {  1640450480,  565  },
   {  1640430256,  118  },
   {  1640441174,  791  },
   {  0,           0    },    //   {  1640430435,  113  },
   {  0,           0    },    //   {  1640430524,  101  },
   {  0,           0    },    //   {  1640455938,  514  },
   {  0,           0    },    //   {  1640430792,  105  },
   {  0,           0    },    //   {  1640463992,  505  },
   {  0,           0    },    //   {  1640449764,  463  },
   {  0,           0    },    //   {  1640433388,  339  },
   {  0,           0    },    //   {  1640447886,  99   },
   {  0,           0    },    //   {  1640433120,  108  },
   {  0,           0    },    //   {  1640463903,  302  },
   {  0,           0    },    //   {  1640436610,  194  },
   {  0,           0    },    //   {  1640463456,  528  },
   {  0,           0    },    //   {  1640440279,  504  },
   {  0,           0    }     //   {  1640450659,  195  }
};

static void rpi_TestMotionList()
{
   int   iX;

   for(iX=0; iX<MAX_NUM_MOTIONS; iX++)
   {
      pstMap->G_ulMaxMotionsTs[iX]  = stDummyList[iX].ulSecs;
      pstMap->G_iMaxMotions[iX]     = stDummyList[iX].iCount;
   }
}
#endif      //FEATURE_TEST_MOTION_LIST

// 
// Function:   rpi_DumpMotionData
// Purpose:    Dump totals containing the summarize of todays events
// 
// Parameters: 
// Returns:     
// 
static void rpi_DumpMotionData()
{
   static bool fOnce=TRUE;
   static int  iNumMotions=0;
   //
   int   iReg;

   iNumMotions++;
   //
   // Summarize motion results from today:
   //
   // G_iArchiveAdd;                               // Added   to   archived today
   // G_iArchiveDel;                               // Removed from archived today
   // G_iRegionMagnitude   [MAX_MOTION_REGIONS];   // Region Sum Magnitude
   // G_iRegionCount       [MAX_MOTION_REGIONS];   // Region Sum Count
   //
   GEN_Printf("rpi-DumpMotionData():SafeMalloc balance = %d" CRLF, GLOBAL_GetMallocs());
   GEN_Printf("rpi-DumpMotionData():Archives" CRLF);
   GEN_Printf("rpi-DumpMotionData():  Added:     %d files" CRLF, pstMap->G_iArchiveAdd);
   GEN_Printf("rpi-DumpMotionData():  Removed:   %d files" CRLF, pstMap->G_iArchiveDel);
   //
   GEN_Printf("rpi-DumpMotionData():" CRLF);
   GEN_Printf("rpi-DumpMotionData():Sum Motions" CRLF);
   GEN_Printf("rpi-DumpMotionData():  Motions:   %d" CRLF, iNumMotions);
   //
   for(iReg=0; iReg<MAX_MOTION_REGIONS; iReg++)
   {
      if(pstMap->G_iRegionCount[iReg])
      {
         GEN_Printf("rpi-DumpMotionData():  Region %d" CRLF, iReg);
         GEN_Printf("rpi-DumpMotionData():    Magnitude: %d" CRLF, pstMap->G_iRegionMagnitude[iReg]);
         GEN_Printf("rpi-DumpMotionData():    Count:     %d" CRLF, pstMap->G_iRegionCount[iReg]);
      }
   }
   //
   // Put out email once when enough motions detected 
   //
   if(fOnce)   
   {
      if(iNumMotions > 5)
      {
         fOnce = FALSE;
         if(RPI_SendMail()) ulSecsMailTimeout = RTC_GetSystemSecs() + MAILX_TIMEOUT;
         GEN_Printf("rpi-DumpMotionData():Mail motion results." CRLF);
         rpi_HandleMotionCompleted(MOTCC_CLEAR);
      }
      else RPI_CreateMailBody();
   }
}

#endif   //FEATURE_DUMP_MOTION_DATA

