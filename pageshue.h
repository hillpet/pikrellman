/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           pageshue.h
 *  Purpose:            Hue server specific dynamic HTML snd JSON pages
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Nov 2021:      Ported from rpihue
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//          tUrl,                   tType,      iTimeout,   iFlags,           pcUrl,                  pfDynCb;
EXTRACT_DYN(DYN_HUE_NONE_ALL,       HTTP_HTML,  0,          DYN_FLAG_PORT,    "all",                  srvr_DynPageHueCommand              )
EXTRACT_DYN(DYN_HUE_NONE_BRIDGE,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "bridge",               srvr_DynPageHueCommand              )
EXTRACT_DYN(DYN_HUE_HTML_BRIDGE,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "bridge.html",          srvr_DynPageHueCommand              )
EXTRACT_DYN(DYN_HUE_NONE_LIGHTS,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "lights",               srvr_DynPageHueLights               )
EXTRACT_DYN(DYN_HUE_HTML_LIGHTS,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "lights.html",          srvr_DynPageHueLights               )
EXTRACT_DYN(DYN_HUE_JSON_LIGHTS,    HTTP_JSON,  0,          DYN_FLAG_PORT,    "lights.json",          srvr_DynPageHueLights               )
EXTRACT_DYN(DYN_HUE_NONE_GROUPS,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "groups",               srvr_DynPageHueGroups               )
EXTRACT_DYN(DYN_HUE_HTML_GROUPS,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "groups.html",          srvr_DynPageHueGroups               )
EXTRACT_DYN(DYN_HUE_JSON_GROUPS,    HTTP_JSON,  0,          DYN_FLAG_PORT,    "groups.json",          srvr_DynPageHueGroups               )
EXTRACT_DYN(DYN_HUE_NONE_SCENES,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "scenes",               srvr_DynPageHueScenes               )
EXTRACT_DYN(DYN_HUE_HTML_SCENES,    HTTP_HTML,  0,          DYN_FLAG_PORT,    "scenes.html",          srvr_DynPageHueScenes               )
EXTRACT_DYN(DYN_HUE_JSON_SCENES,    HTTP_JSON,  0,          DYN_FLAG_PORT,    "scenes.json",          srvr_DynPageHueScenes               )
//
EXTRACT_DYN(DYN_HUE_NONE_CMD,       HTTP_JSON,  0,          DYN_FLAG_PORT,    "hue",                  srvr_DynPageHueCommand              )
EXTRACT_DYN(DYN_HUE_JSON_CMD,       HTTP_JSON,  0,          DYN_FLAG_PORT,    "hue.json",             srvr_DynPageHueCommand              )
EXTRACT_DYN(DYN_HUE_HTML_CMD,       HTTP_HTML,  0,          DYN_FLAG_PORT,    "hue.html",             srvr_DynPageHueCommand              )
