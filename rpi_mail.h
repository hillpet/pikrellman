/*  (c) Copyright:  2021  Patrn, Confidential Data
 *
 *  Workfile:           rpi_mail.h
 *  Purpose:            Headerfile for pikrellman mail
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    18 Nov 2021:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIL_H_
#define _RPI_MAIL_H_

//
// Global functions
//
bool     RPI_CreateMailBody         (void);
u_int32  RPI_WaitMailxCompletion    (u_int32);
bool     RPI_SendMail               (void);

#endif /* _RPI_MAIL_H_ */
