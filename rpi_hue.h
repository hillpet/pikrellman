/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_hue.h
 *  Purpose:            Header file for HUE specific Dynamic HTTP pages for Raspberry pi webserver
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    06 Nov 2021:      Ported from rpihue
 *    20 Nov 2021:      Add Hue commands for Garden/Carport lightning
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_HUE_H_
#define _RPI_HUE_H_

#define DEVICE_ON                TRUE
#define DEVICE_OFF               FALSE
//
// HS Colors
//
#define  HUE_HUE_COLOR_WHITE     0
#define  HUE_SAT_COLOR_WHITE     0
//
#define  HUE_HUE_COLOR_RED       65535
#define  HUE_SAT_COLOR_RED       254
//
#define  HUE_HUE_COLOR_GREEN     22000
#define  HUE_SAT_COLOR_GREEN     254
//
#define  HUE_HUE_COLOR_BLUE      44000
#define  HUE_SAT_COLOR_BLUE      254
//
#define  HUE_HUE_COLOR_YELLOW    10000
#define  HUE_SAT_COLOR_YELLOW    254
//
// Set semaphore timeouts
//
#define  HUE_STATE_HANDLER       10000       // Hue Bridge State Handler mSecs
#define  HUE_TIMEOUT_IDLE        10000       // Motion Handler mSecs if No ongoing Motion detection
#define  HUE_TIMEOUT_ACTIVE      1000        // Motion Handler mSecs if Motion detected
//
#define  HUE_RANDOM_SECS         300         // Random secs time-span
#define  MAX_HUE_HTTP_REPLY      16384       //
#define  NUM_HUE_RETRIES         3           // Number of HUE Bridge set retries

//
// HUE Bridge JSON translator enums
//
typedef enum HUEBRI
{
#define  EXTRACT_HUE(a,b,c,d,e,f,g,h)        a,
#include "par_hue.h"   
#undef   EXTRACT_HUE
//
   NUM_BRIDGE_VARS
}  HUEBRI;

//
// HUE Bridge light/group/scene names
//
typedef enum HUENAME
{
#define  EXTRACT_NAME(a,b,c,d)               a,
#include "par_names.h"   
#undef   EXTRACT_NAME
//
   NUM_HUE_NAMES
}  HUENAME;

//
// HUE Bridge commands
//
typedef enum HUECMD
{
#define  EXTRACT_HC(a,b,c)                   a,
#include "hue_cmds.h"   
#undef   EXTRACT_HC
//
   NUM_HUE_CMDS
}  HUECMD;

//
// HUE states
//
typedef enum HUEST
{
#define  EXTRACT_HS(a,b,c,d)                 a,
#include "hue_states.h"   
#undef   EXTRACT_HS
   //
   NUM_HUE_STATES
}  HUEST;

//
// HUE Device states
//
typedef enum HUEDEV
{
   HUEDEV_NONE = 0,
   HUEDEV_ALL_OFF,
   HUEDEV_ALL_ON,
   HUEDEV_ANY_ON,
   HUEDEV_UNREACH,
   //
   NUM_HUEDEVS,
}  HUEDEV;

//
// HUE State functions
//
typedef struct huefunctions
{
   HUEST       tNextState;
   const char *pcHelp;
   bool      (*pfHueFun)(const struct huefunctions *);
}  HUEFUN;
//
typedef bool (*HUEFPTR)(const struct huefunctions *);

//
// HUE Command functions
//
typedef bool (*HUECPTR)();
typedef struct _hue_commands_
{
   const char *pcHelp;
   HUECPTR     pfHueFun;
}  HUECOM;

//
// HUE global functions
//
int         HUE_Init                (void);
HUECFG     *HUE_GetDeviceConfig     (char);
HUECFG     *HUE_GetDeviceStructure  (char *);
bool        HUE_StoreDeviceName     (char, int, char *, int);

#endif /* _RPI_HUE_H_ */
