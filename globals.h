/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           globals.h
 *  Purpose:            Define the mmap variables, common over all threads. If the structure 
 *                      needs to change, roll the revision number to make sure a new mmap 
 *                      file is created !
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    12 Jun 2021:      DB-1.0: Created
 *    05 Jul 2021:      Add Sem main
 *    20 Aug 2021:      Add Sem guard
 *    04 Sep 2021:      Add CL options
 *    07 Nov 2021:      Add Semaphore to PID list
 *    12 Nov 2021:      Remove mail client
 *    13 Nov 2021:      Add external msmtp mail client
 *    16 Nov 2021:      Add Motion summaries
 *    18 Nov 2021:      Sum all motion frame data
 *    29 Nov 2021:      Add Reboot 
 *    10 Dec 2021:      Add system core temperature
 *    01 Jan 2022:      Add Ping Camera
 *    06 Fev 2022:      Add Region HUE devices
 *    12 Feb 2022:      DB-2.0: Cleanup global struct data; Add fInit
 *    01 Jun 2022:      Add PID timestamps
 *    12 Oct 2023:      Flexible WAN retrieval
 *    09 Dec 2024:      Add Xmas Outlet 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <netinet/in.h>

#ifdef DEFINE_GLOBALS
   #define EXTERN
#else
   #define EXTERN extern
#endif
//
// We MUST define these sizes here as well (if no pre-processor)
//
#define  SIZEOF_INT                 4           // int (i)
#define  SIZEOF_LONG                4           // long (l, ul32)
#define  SIZEOF_DOUBLE              8           // double (fl)
//
#define  TIMEOUT_MSECS              5000        // Host Timeout in msecs
//
#define  POWER_BUTTON_PRESS_SECS    3           // Button press time before poweroff
#define  MAX_VIDEO_ACQ_SECS         600         // Max freeze video acquisition secs 
#define  ARCHIVE_USAGE_SECS         120         // Check Archiving necessity in delta secs
#define  VIDEOS_LOOP_REC_SECS       30          // Check Videos Loop recordings file usage in delta secs
#define  THUMBS_LOOP_REC_SECS       30          // Check Thumbs Loop recordings file usage in delta secs
#define  MOTION_LOOP_REC_SECS       120         // Check Motion Loop recordings file usage in delta secs
#define  GUARD_CHECK_SECS           120         // Monitor interval
#define  THREADS_CHECK_SECS         300         // Thread min respond secs
#define  TEMP_CHECK_SECS            300         // Check core temperature interval
#define  HERON_ARCHIVE_SECS         30          // Secs to check Heron archiving
#define  HERON_DEF_ARCHIVE_SECS     (10*60)     // Default 10 Mins archive recording
//
#define  LOOP_RECORDINGS_KEEP_FILES 16          // Keep this number of loop recordings files (video/thumb/motion)
#define  MAX_TEMP_DELTA              3.0        // Max temperature rise/fall for Log
//
//
// HTTP Functions (callback through HTML or JSON)
//
#define  PAR____                          0x00000000     // No command
#define  PAR_PIX                          0x00000001     // List of pictures
#define  PAR_EML                          0x00000002     // List of Mailx
#define  PAR_MOT                          0x00000004     // List of Motion Parameters
#define  PAR_HUE                          0x00000008     // List of Hue Parameters
#define  PAR_DAT                          0x00000010     // List of Date time Parameters
#define  PAR_ERR                          0x00008000     // Error replies
#define  PAR_ALL                          0x0000FFFF     // All HTTP functions
//
// JSON markers for text-style-vars ("value" = "123456")
//             or number-style-vars ("value" =  123456 )
//
#define  JSN_TXT                          0x10000000     // JSON Variable is text
#define  JSN_INT                          0x20000000     // JSON Variable is integer
#define  WB                               0x80000000     // Warm boot var
#define  PAR_A                            0x08000000     // Parm is ASCII
#define  PAR_B                            0x04000000     // Parm is BCD
#define  PAR_F                            0x02000000     // Parm is FLOAT
#define  PAR_H                            0x01000000     // Parm is HEX
#define  PAR_TYPE_MASK                    0x0F000000     // Parm is HEX
//
#define  ALWAYS_CHANGED                   -1
#define  NEVER_CHANGED                    -2
//
#define  ONEMEGABYTE                      (1024*1024)
#define  ONEGIGABYTE                      (1024*ONEMEGABYTE)
#define  ONEMILLION                       1000000L
#define  ONEBILLION                       1000000000L
//
#define  MAX_CMD_LEN                      256
#define  MAX_CMDLINE_LEN                  (MAX_CMD_LEN + MAX_ARG_LEN)
//
#define  MAX_NUM_REPORT_LINES             10000
#define  MAX_DYN_PAGES                    64
//
// Global area array sizes
//
#define  MAX_DIRS_LEN                     2047
#define  MAX_DIRS_LENZ                    MAX_DIRS_LEN+1
#define  MAX_PATH_LEN                     511
#define  MAX_PATH_LENZ                    MAX_PATH_LEN+1
#define  MAX_URL_LEN                      31
#define  MAX_URL_LENZ                     MAX_URL_LEN+1
#define  MAX_MAC_LEN                      31
#define  MAX_MAC_LENZ                     MAX_MAC_LEN+1
#define  MAX_ADDR_LEN                     INET_ADDRSTRLEN
#define  MAX_ADDR_LENZ                    INET_ADDRSTRLEN+1
#define  MAX_PARM_LEN                     15
#define  MAX_PARM_LENZ                    MAX_PARM_LEN+1
#define  MAX_MAIL_LEN                     127
#define  MAX_MAIL_LENZ                    MAX_MAIL_LEN+1
#define  MAX_PASS_LEN                     127
#define  MAX_PASS_LENZ                    MAX_PASS_LEN+1
#define  MAX_ARG_LEN                      255
#define  MAX_ARG_LENZ                     MAX_ARG_LEN+1
#define  MAX_HUEB_LEN                     31
#define  MAX_HUEB_LENZ                    MAX_HUEB_LEN+1
#define  MAX_HUER_LEN                     3
#define  MAX_HUER_LENZ                    MAX_HUER_LEN+1
#define  MAX_USER_LEN                     63
#define  MAX_USER_LENZ                    MAX_USER_LEN+1
//
#define  MAX_MOTION_REGIONS               10 // Max regions in one frame
#define  MAX_NUM_MOTIONS                  20 // Number of motion files remembered this day
//
// Global notification flags
// Activations:
//
#define  GLOBAL_GRD_INI       0x01000000     // Ini: PiKrellCam Guard
#define  GLOBAL_MOT_INI       0x02000000     // Ini: Motion Detection
#define  GLOBAL_ARC_INI       0x04000000     // Ini: Motion Archiving
#define  GLOBAL_HUE_INI       0x08000000     // Ini: HUE Bridge
#define  GLOBAL_SVR_INI       0x10000000     // Ini: HTTP Server
#define  GLOBAL_PNG_INI       0x20000000     // Ini: HTTP Server
#define  GLOBAL_XXX_INI       0xFF000000     // Init Mask
//
// These flags are thread specific
// HOST:
#define  GLOBAL_ALL_HST_FLT   0x00000001     // Segmentation fault host nfy (exit)
#define  GLOBAL_ALL_HST_END   0x00000002     // Host Exit
#define  GLOBAL_ALL_HST_OFF   0x00000004     // Power Off button
#define  GLOBAL_ALL_HST_RST   0x00000008     // Restart theApp
#define  GLOBAL_ALL_HST_RBT   0x00000010     // Reboot request
#define  GLOBAL_HUE_HST_NFY   0x00000020     // Hue Config was read
#define  GLOBAL_GRD_HST_NFY   0x00000040     // Guard Nfy
#define  GLOBAL_CAM_HST_MOT   0x00000080     // PiKrellCam Motion detected
#define  GLOBAL_CAM_HST_ARC   0x00000100     // PiKrellCam Archive
#define  GLOBAL_MOT_HST_NFY   0x00000200     // Motion acquisition completed
#define  GLOBAL_ARC_HST_NFY   0x00000400     // Notification archive ready
#define  GLOBAL_SVR_HST_HER   0x00000800     // Server has Heron activation
#define  GLOBAL_XXX_HST_MSK   0x0000FFFF     // Mask
// GUARD:
#define  GLOBAL_HST_GRD_RUN   0x00000001     // Run
#define  GLOBAL_XXX_GRD_MSK   0x0000FFFF     // Mask
// MOTION:
#define  GLOBAL_HST_MOT_RUN   0x00000001     // Run
#define  GLOBAL_HUE_MOT_NFY   0x00000002     // Notification from Hue Bridge
#define  GLOBAL_HST_MOT_SUM   0x00000004     // Host motion summary request
#define  GLOBAL_HST_MOT_PEN   0x00000008     // Pending ON-MOTION-END
#define  GLOBAL_XXX_MOT_MSK   0x0000FFFF     // Mask
// ARCHIVE:
#define  GLOBAL_HST_ARC_RUN   0x00000001     // Run
#define  GLOBAL_HST_ARC_HER   0x00000002     // Host enabled Heron activation
#define  GLOBAL_XXX_ARC_MSK   0x0000FFFF     // Mask
// HUE:
#define  GLOBAL_HST_HUE_CFG   0x00000001     // Read Bridge device config
#define  GLOBAL_MOT_HUE_RUN   0x00000002     // Motion Detect to Update Bridge
#define  GLOBAL_SVR_HUE_RUN   0x00000004     // HTTP Server   to Update Bridge
#define  GLOBAL_HUE_HUE_CFG   0x00000008     // Read Bridge device config
#define  GLOBAL_SVR_HUE_DAT   0x00000010     // Update HUE on/off time
#define  GLOBAL_XXX_HUE_MSK   0x0000FFFF     // Mask
// SRVR:
#define  GLOBAL_GRD_SVR_NFY   0x00000001     // Guard Nfy
// PING:
#define  GLOBAL_HST_PNG_ARP   0x00000001     // Ping retrieve ARP table
#define  GLOBAL_HST_PNG_MAP   0x00000002     // Ping retrieve NMAP data
// ALL:
#define  GLOBAL_GRD_ALL_RUN   0x00010000     // Guard Run
#define  GLOBAL_HST_ALL_MID   0x00020000     // Midnight signal from Host
#define  GLOBAL_SVR_ALL_NFY   0x00040000     // Http Server notify to threads
#define  GLOBAL_XXX_ALL_MSK   0x00FF0000     // Mask
//
typedef enum _global_parameters_
{
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    a,
#include "par_defs.h"
#undef EXTRACT_PAR
   NUM_GLOBAL_DEFS
}  GLOPAR;
//
// Generic status strings/enums
//
typedef enum _genstat_
{
#define  EXTRACT_ST(a,b)   a,
#include "gen_stats.h"
#undef   EXTRACT_ST
   //
   NUM_GEN_STATUS,
   GEN_STATUS_ASK                   // Return current status
}  GENSTAT;
//
typedef struct _rpidata_
{
   char    *pcObject;
   FTYPE    tType;
   int      iIdx;
   int      iSize;
   int      iFree;
   int      iLastComma;
}  RPIDATA;
//
//
// Enums PIDs
//
typedef enum _proc_pids_
{
   #define  EXTRACT_PID(a,b,c)   a,
   #include "par_pids.h"
   #undef EXTRACT_PID
   //
   NUM_PIDT
}  PIDT;
//
typedef int (*PFUNIN)();
//
typedef struct _proc_pidlist_
{
   pid_t       tPid;
   sem_t      *ptSem;
   int         iFlag;
   int         iNotify;
   u_int32     ulTs;
   int         iCount;
   PFUNIN      pfnInit;
   const char *pcName;
   const char *pcHelp;
}  PIDL;
//
typedef struct _globals_
{
   int         iFunction;
   const char *pcOption;
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iValueSize;
}  GLOBALS;
//
#define  PIKTIM_LEN          7
#define  PIKTIM_LENZ         PIKTIM_LEN+1
//
typedef struct _pik_timestamp_
{
   bool        fConverted;
   //
   char        cYear[PIKTIM_LENZ];
   char        cMonth[PIKTIM_LENZ];
   char        cDay[PIKTIM_LENZ];
   char        cHour[PIKTIM_LENZ];
   char        cMinute[PIKTIM_LENZ];
   char        cSecond[PIKTIM_LENZ];
   //
   char        cType[PIKTIM_LENZ];
   //
   int         iYear;
   int         iMonth;
   int         iDay;
   int         iHour;
   int         iMinute;
   int         iSecond;
}  PIKTIM;
//
//pwjh Now GEN_GetDirEntries()  typedef struct PIKDIR
//pwjh Now GEN_GetDirEntries()  {
//pwjh Now GEN_GetDirEntries()     bool           fDir;                      // IsDir
//pwjh Now GEN_GetDirEntries()     int64          llSize;                    // Size in bytes
//pwjh Now GEN_GetDirEntries()     char          *pcName;                    // Node name
//pwjh Now GEN_GetDirEntries()     struct PIKDIR *pstNext;                   // Next entry
//pwjh Now GEN_GetDirEntries()  }  PIKDIR;
//
typedef struct _pik_variabless_
{
   const char *pcVar;
   GLOPAR      tVar;
}  PIKVAR;
//
typedef struct _pik_region_
{
   int      iX;                              // MB X
   int      iY;                              // MB Y
   int      idX;                             // MB dX
   int      idY;                             // MB dY
   int      iMagnitude;                      // Total Magnitude
   int      iCount;                          // Total Count
}  PIKREG;
//
typedef struct _pik_frame_
{
   double   flSeconds;                       // Frame start in secs
   //
   int      iBurst;                          // Burst count
   int      iAudio;                          // Audio Triggers
   int      iExternal;                       // External triggers
   //
   PIKREG   stCanvas;                        // Whole frame (also outside any region)
   PIKREG   stRegion[MAX_MOTION_REGIONS];    // Regions 0...?
}  PIKFRM;
//
typedef struct _pik_motion_
{
   u_int32  ulMotionSecs;                    // T&D stamp of motion
   int      iMotionEvents;                   // Bitfield
   int      iNumFrames;                      // Number of captured frames
   int      iBlocksHor;                      // Hor MBs
   int      iBlocksVer;                      // Ver MBs
   int      iBurstCount;                     // Burst count
   int      iBurstFrames;                    // Burst frames
   int      iMagLimit;                       // Magnitude limit
   int      iMagCount;                       // Magnitude count
   char     cMotionFile[MAX_PATH_LENZ];      // Filename
   //
   PIKFRM   stFrame;                         // Canvas/Regions with motion
}  PIKMOT;
//
typedef  int (*PFEVT)(PIKMOT *, char *, int);
typedef struct PIKEVT
{
   const char    *pcEvent;
   PFEVT          pfEvtCb;
}  PIKEVT;
//
typedef enum _varreset_
{
   VAR_COLD    = 0,                 // Cold reset
   VAR_WARM,                        // Warm reset
   VAR_UPDATE,                      // Update

   NUM_VARRESETS
}  VARRESET;
//
typedef enum _rpi_mapstate_
{
   MAP_CLEAR    = 0,                 // Clear all the mapped memory
   MAP_SYNCHR,                       // Synchronize the map
   MAP_RESTART,                      // Restart the counters using the last stored data

   NUM_MAPSTATES
}  MAPSTATE;
//
typedef enum _rpi_content_type_
{
   CT_MOTION = 0,
   CT_VIDEO,
   CT_AUDIO,
   CT_THUMB,
   CT_EMAIL,
}  CTTYPE;
//
// WAN changes
//
#define WAN_CHECK_SECS  (4*3600)       // 4 hour checks
//
#define WAN_NOT_FOUND   0x0000         // Not yet retrieved
#define WAN_SAME        0x0001         // WAN retrieved and no change
#define WAN_CHANGED     0x0002         // WAN retrieved and has changed
#define WAN_NO_URL      0x0004         // WAN URL not set in persistent memory
#define WAN_ERROR       0x8000         // ERROR retrieving
//
// Hue data structure
//
#define  MAX_HUE_DEVICES               32
#define  HUE_MAX_LEVELS                5
//
#define  HUE_DATA_LEN                  31
#define  HUE_DATA_LENZ                 HUE_DATA_LEN+1
//
#define  HUE_DEVICE_LIGHT              'l'
#define  HUE_DEVICE_GROUP              'g'
#define  HUE_DEVICE_SCENE              's'
//
// Note the usage of the colormode parameter: There are 3 ways of setting the light 
// color: xy, color temperature (ct) or hue and saturation (hs). A light may contain 
// different settings for xy, ct and hs, but only the mode indicated by the colormode 
// parameter will be certain to give the active light color.
//
typedef enum _Hue_Colormode_
{
   HUE_CMODE_NONE = 0,                 // None
   HUE_CMODE_HS,                       // Hue, Sat
   HUE_CMODE_XY,                       // X, Y
   HUE_CMODE_CT,                       // Color temperature
   HUE_CMODE_OTHER,                    // Other (Wcd, On/Off
   //
   NUM_HUE_CMODES
}  HUECM;
//
typedef struct HUECFG
{
   char       *pcObject;               // JSON Object ptr     ->   {"state":{....}
   const char *pcDevType;              // JSON devicetype ptr ->    "none", "lights", "groups" or "scenes"
   int         iChecksum;              // Bytewise checksum to identify changes
   //
   bool     fReach;                       // Device is reachable  0/1
   bool     fOn;                          //        on state      0/1
   bool     fChanged;                     //        has changed   0/1
   HUECM    tColorMode;                   //        Colormode     HUE_CMODE_...
   int      iBri;                         //        brightness    0..255
   int      iHue;                         //        hue           0..32767
   int      iSat;                         //        saturation    0..255
   double   flX;                          //        X             0..1
   double   flY;                          //        Y             0..1
   int      iCt;                          //        Ct            0..65536
   int      iCtMin;                       //        Cap Ct Min    0..?
   int      iCtMax;                       //        Cap Ct Max    0..?
   char     cDevId   [HUE_DATA_LENZ];     //        DeviceId      "1"
   char     cName    [HUE_DATA_LENZ];     //        name          "Garden-West"
   char     cUniqId  [HUE_DATA_LENZ];     //        unique Id     "00:15:...-0b"
   char     cModel   [HUE_DATA_LENZ];     //        model id      "ZBT-ExtendedColor"
   char     cEffect  [HUE_DATA_LENZ];     //        effect        "none"
   char     cMode    [HUE_DATA_LENZ];     //        mode          "homeautomation"
   char     cColMode [HUE_DATA_LENZ];     //        color mode    "xy"
   char     cType    [HUE_DATA_LENZ];     //        type          "Extended color light"
   char     cGroup   [HUE_DATA_LENZ];     //        group         "1"
   char     cLights  [HUE_DATA_LENZ];     //        lights        ["1","3","6", ...]
}  HUECFG;
//
typedef struct HUEJS
{
   const char *pcElm[HUE_MAX_LEVELS];  // JSON Element Level 0..MAX
   int         iOffset;                // HUECFG Structure offset
   int         iSize;                  // HUECFG size
}  HUEJS;
//
typedef struct HUEID
{
   char        cDevType;               // Device type
   const char *pcName;                 // Bridge name
   const char *pcHelp;                 // Help text
}  HUEID;

//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
typedef struct _dynr_
{
   int         tUrl;                   // DYN_HTML_... (from EXTRACT_DYN pages.h, ...)
   FTYPE       tType;                  // HTTP_JSON, HTTP_HTML
   int         ePid;                   // Owner pid enum PID_xxxx
   int         iTimeout;               // Estimated call duration 
   int         iPort;                  // Port
   char        pcUrl[MAX_URL_LENZ];    // URL
   PFVOIDPI    pfCallback;             // Callback function
}  DYNR;
//
typedef enum _system_vars_
{
   SVAR_NONE         = 0,
   SVAR_TEMPERATURE, 
   //
   NUM_SVARS
}  SVAR;
//
typedef struct _system_data_
{
   char    *pcFile;
   void    *pvData;
}  SDATA;
//
typedef enum _motion_cc_
{
   MOTCC_NONE     = 0,
   MOTCC_SET,
   MOTCC_CLEAR,
}  MOTCC;
//

//=============================================================================
// Global parameters mapped to a shared MMAP file 
//=============================================================================
typedef struct _rpimap_
{
   int               G_iSignature1;
   int               G_iVersionMajor;
   int               G_iVersionMinor;
   bool              G_fInit;
   u_int32           G_ulStartTimestamp;
   pthread_mutex_t   G_tMutex;
   //
   //=============== Start WB RESET ZERO area =================================
   int               G_iResetStart;
   //
   double            G_flCpuTemperature;
   int64             G_llLogFilePos;
   u_int32           G_ulDebugMask;
   u_int32           G_ulSecondsCounter;
   //
   int               G_iHttpStatus;
   int               G_iHttpArgs;
   int               G_iMallocs;
   int               G_iSegFaultLine;
   int               G_iSegFaultPid;
   int               G_iGuards;
   int               G_iTraceCode;
   int               G_iRunState;
   int               G_iNumDynPages;
   int               G_iCurDynPage;
   int               G_iHeronSecs;
   //
   // Misc semaphores
   //
   pid_t             G_tPidHost;
   pid_t             G_tPidGuard;
   pid_t             G_tPidArchive;
   pid_t             G_tPidMotion;
   pid_t             G_tPidHue;
   //
   char              G_pcCommand          [MAX_PARM_LENZ];
   char              G_pcStatus           [MAX_PARM_LENZ];
   char              G_pcDelete           [MAX_PARM_LENZ];
   char              G_pcSegFaultFile     [MAX_PATH_LENZ];
   char              G_pcLastFile         [MAX_PATH_LENZ];
   char              G_pcLastFileSize     [MAX_PARM_LENZ];
   char              G_pcDefault          [MAX_PATH_LENZ];
   //
   GLOG              G_stLog;
   PIDL              G_stPidList          [NUM_PIDT];             // 8*9*4=288 bytes
   char              G_pcPidList          [256];                  // Original placeholder
   sem_t             G_tSemList           [NUM_PIDT];
   PIKTIM            G_stTs;
   PIKMOT            G_stMotion;
   DYNR              G_stDynPages         [MAX_DYN_PAGES];
   //
   HUECFG            G_stHueL             [MAX_HUE_DEVICES];      // Lights
   HUECFG            G_stHueG             [MAX_HUE_DEVICES];      // Groups
   HUECFG            G_stHueS             [MAX_HUE_DEVICES];      // Scenes
   //
   char              G_pcHueDevice        [MAX_PARM_LENZ];        // Hue Device L,G,S
   int               G_iHueCmd;                                   // Hue Bridge Command
   int               G_iRegionMagnitude   [MAX_MOTION_REGIONS];   // Region Sum Magnitude
   int               G_iRegionCount       [MAX_MOTION_REGIONS];   // Region Sum Count
   //
   int               G_iArchiveAdd;                               // Added   to   archived today
   int               G_iArchiveDel;                               // Removed from archived today
   int               G_iMaxMotions        [MAX_NUM_MOTIONS];
   u_int32           G_ulMaxMotionsTs     [MAX_NUM_MOTIONS];
   //
   //
   int               G_iSpareData         [MAX_MOTION_REGIONS];   // ----------------SPARE -----------------
   //
   // Changed markers
   //
   u_int8            G_ubCommandChanged;

   //--------------------------------------------------------------------------
   // Take additional power-on reset parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_ZERO 1000
   #define EXTRA_POOL_ZERO (1*SIZEOF_INT)
   //
   #if     EXTRA_POOL_ZERO > SPARE_POOL_ZERO
   #error  Spare zero-init pool too small
   #endif
   //
   // Extra from zero-init pool
   //
   int               G_iMotionState;
   char              G_pcSparePoolZero    [SPARE_POOL_ZERO-EXTRA_POOL_ZERO];
   //
   //--------------------------------------------------------------------------
   int               G_iResetEnd;
   //=============== End WB reset area ========================================


   //=============== Start Persistent storage==================================
   char              G_pcHostname         [MAX_URL_LENZ];
   char              G_pcVersionSw        [MAX_PARM_LENZ];
   char              G_pcWwwDir           [MAX_PATH_LENZ];
   char              G_pcHostIp           [MAX_ADDR_LENZ];
   char              G_pcWanAddr          [MAX_ADDR_LENZ];
   //
   char              G_pcCaller           [MAX_PATH_LENZ];
   char              G_pcLogFile          [MAX_PATH_LENZ];
   char              G_pcFifo             [MAX_PATH_LENZ];
   char              G_pcMediaDir         [MAX_PATH_LENZ];
   char              G_pcArchiveDir       [MAX_PATH_LENZ];
   char              G_pcVideoDir         [MAX_PATH_LENZ];
   char              G_pcThumbDir         [MAX_PATH_LENZ];
   char              G_pcLoopDir          [MAX_PATH_LENZ];
   char              G_pcStillDir         [MAX_PATH_LENZ];
   char              G_pcTimelapseDir     [MAX_PATH_LENZ];
   char              G_pcRecordingFile    [MAX_PATH_LENZ];
   char              G_pcLastVideoFile    [MAX_PATH_LENZ];
   char              G_pcLastThumbFile    [MAX_PATH_LENZ];
   char              G_pcLoopMotionFile   [MAX_PATH_LENZ];
   char              G_pcLastStillFile    [MAX_PATH_LENZ];
   char              G_xxMotionState      [MAX_PARM_LENZ]; //Not Used
   char              G_pcDebugMask        [MAX_PARM_LENZ];
   //
   char              G_pcHostPort         [MAX_PARM_LENZ];
   char              G_pcHueIp            [MAX_ADDR_LENZ];
   char              G_pcHuePort          [MAX_PARM_LENZ];
   char              G_pcHueUser          [MAX_USER_LENZ];
   char              G_pcHueTimeDark      [MAX_USER_LENZ];
   //
   char              G_pcPingIp           [MAX_ADDR_LENZ];
   char              G_pcPingMac          [MAX_MAC_LENZ];
   char              G_pcPingCam          [MAX_ADDR_LENZ];
   //
   char              G_pcMailxSubj        [MAX_MAIL_LENZ];
   char              G_pcMailxBody        [MAX_MAIL_LENZ];
   char              G_pcMailxAttm        [MAX_MAIL_LENZ];
   char              G_pcMailxTo          [MAX_MAIL_LENZ];
   char              G_pcMailxCc          [MAX_MAIL_LENZ];
   //
   u_int32           G_ulMailSecsNow;
   u_int32           G_ulMailSecsMail;
   double            G_flFreeArchive;
   int               G_iSkipWifiPings;
   int               G_iArchiveCount;
   int               G_iVerbose;
   int               G_iCamOpts;
   //
   int               G_iMinuteDawn;          // State vars from PiKrellCam
   int               G_iMinuteSunrise;       // 
   int               G_iMinuteSunset;        // 
   int               G_iMinuteDusk;          // 
   //
   int               G_iHueBri;
   int               G_iHueHue;
   int               G_iHueSat;
   int               G_iHueCt;
   //
   int               G_iTholdMag;
   int               G_iTholdCnt;
   int               G_iTholdMot;
   //
   int               G_iSecsDeltaDawn;          // Delta seconds sunrise/dawn
   int               G_iSecsDeltaDusk;          // Delta seconds sunset /dusk
   int               G_iSecsTimerDawn;          // Timer seconds sunrise/dawn
   int               G_iSecsTimerDusk;          // Timer seconds sunset /dusk
   int               G_iSecsWarnDay;            // Duration seconds warnings Day
   int               G_iSecsWarnNight;          // Duration seconds warnings Night
   int               G_iWanAddr;                // WAN retrieval
   //
   //--------------------------------------------------------------------------
   // Take additional persistent parms from this pool:
   //--------------------------------------------------------------------------
   // Add:  
   //
   #define SPARE_POOL_PERS 1000
   #define EXTRA_POOL_PERS ((1*MAX_URL_LENZ) + (2*SIZEOF_INT))
   //
   #if     EXTRA_POOL_PERS > SPARE_POOL_PERS
   #error  Spare persistent pool too small
   #endif
   //
   // Extra from non-volatile pool
   //    G_pcWanUrl[MAX_URL_LENZ]                  URL for WAN retrieval
   //    G_iSecsOutletOn                           XMas Outlet secs on
   //    G_iSecsOutletOff                          XMas Outlet secs off
   //
   char              G_pcWanUrl[MAX_URL_LENZ];
   int               G_iSecsOutletOn;
   int               G_iSecsOutletOff;
   char              G_pcSparePoolPers       [SPARE_POOL_PERS-EXTRA_POOL_PERS];
   //
   //=============== End Persistent storage ===================================
   //
   //--------------------------------------------------------------------------
   int               G_iSignature2;          // Signature bottom
}  RPIMAP;

//=============================================================================
// GLOBAL MMAP structure pointer
//=============================================================================
EXTERN RPIMAP *pstMap;

//
// Global functions
//
bool           GLOBAL_CheckDelete            (char *);
bool           GLOBAL_Close                  (bool);
u_int32        GLOBAL_ConvertDebugMask       (bool);
void           GLOBAL_ExpandMap              (int, int);
bool           GLOBAL_Init                   (void);
u_int32        GLOBAL_GetDebugMask           (void);
int            GLOBAL_FlushLog               (void);
char          *GLOBAL_GetHostName            (void);
GLOG          *GLOBAL_GetLog                 (void);
int            GLOBAL_GetMallocs             (void);
const GLOBALS *GLOBAL_GetParameters          (void);
void          *GLOBAL_GetParameter           (GLOPAR);
int            GLOBAL_GetParameterSize       (GLOPAR);
int            GLOBAL_GetParameterType       (GLOPAR);
int            GLOBAL_GetSignal              (int);
bool           GLOBAL_GetSignalNotification  (int, int);
int            GLOBAL_GetTraceCode           (void);
int            GLOBAL_HostNotification       (int);
int            GLOBAL_Lock                   (void);
int            GLOBAL_Notify                 (int, int, int);
u_int32        GLOBAL_ReadSecs               (void);
void           GLOBAL_SegmentationFault      (const char *, int);
void           GLOBAL_SetDebugMask           (u_int32);
int            GLOBAL_SetSignalNotification  (int, int);
bool           GLOBAL_Share                  (void);
int            GLOBAL_Status                 (int);
bool           GLOBAL_Sync                   (void);
int            GLOBAL_Unlock                 (void);
bool           GLOBAL_UpdateParameter        (GLOPAR, char *);
void           GLOBAL_UpdateSecs             (void);
//
// Global PID functions
//
bool           GLOBAL_PidCheckGuards         (void);
bool           GLOBAL_PidCheckThreads        (u_int32);
bool           GLOBAL_PidGetGuard            (PIDT);
bool           GLOBAL_PidSetGuard            (PIDT);
void           GLOBAL_PidClearGuards         (void);
void           GLOBAL_PidSaveGuard           (PIDT, int);
pid_t          GLOBAL_PidGet                 (PIDT);
const char    *GLOBAL_PidGetName             (PIDT);
const char    *GLOBAL_PidGetHelp             (PIDT);
int            GLOBAL_PidInit                (PIDT);
bool           GLOBAL_PidPut                 (PIDT, pid_t);
int            GLOBAL_PidsLog                (void);
void           GLOBAL_PidSetInit             (PIDT, PFUNIN);
int            GLOBAL_PidTerminate           (int, int);
int            GLOBAL_PidsTerminate          (int);
void           GLOBAL_PidTimestamp           (PIDT, u_int32);
void           GLOBAL_PutMallocs             (int);
void           GLOBAL_Signal                 (PIDT, int);
void           GLOBAL_SignalPid              (pid_t, int);
//
bool           GLOBAL_SemaphoreDelete        (int);
bool           GLOBAL_SemaphoreInit          (int);
bool           GLOBAL_SemaphorePost          (int);
int            GLOBAL_SemaphoreWait          (int, int);

#endif /* _GLOBALS_H_ */
