/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_mailx.h
 *  Purpose:            Headerfile for the external mail client (for msmtp)
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect, msmtp, mpack
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    13 Nov 2021:      Ported from spicam
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAILX_
#define _RPI_MAILX_

//
// MAILX completion timeout in Secs
//
#define MAILX_TIMEOUT                  120
#define MAILX_RETRY                    300
//
// Define argument structure to mail app
//
typedef struct sarg
{
   GLOPAR      tArgs;
   const char *pcArg;
   char       *(*pfGetArg)(const struct sarg *);
}  SARG;

bool  MAILX_SendMail       (const SARG *, int);

#endif /* _RPI_MAILX_ */
