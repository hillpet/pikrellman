/*  (c) Copyright:  2021 Patrn, Confidential Data
 *
 *  Workfile:           rpi_guard.c
 *  Purpose:            This utility is executed to monitor the PiKrellCam running state
 *                      
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    20 Aug 2021:      Created
 *    04 Sep 2021:      Add CL option R x
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    01 Jun 2022:      Add PidList timestamps
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_main.h"
#include "rpi_func.h"
#include "rpi_guard.h"
//
//#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static void    grd_CheckRunningState      (PSAUX *);
static int     grd_Execute                (void);
static int     grd_RestartTheApp          (void);
//
static bool    grd_SignalRegister         (sigset_t *);
static void    grd_ReceiveSignalSegmnt    (int);
static void    grd_ReceiveSignalInt       (int);
static void    grd_ReceiveSignalTerm      (int);
static void    grd_ReceiveSignalUser1     (int);
static void    grd_ReceiveSignalUser2     (int);

//
// pikrellcam stores run-time variables in the filesystem as run/pikrellcam/state :
//
//             motion_enable off
//             show_preset off
//             show_vectors off
//             preset 1 3
//             magnitude_limit 5
//             magnitude_count 5
//             burst_count 350
//             burst_frames 3
//             video_record_state stop
//             video_last /mnt/rpipix/videos/loop_2018-10-15_19.10.59_0.mp4
//             video_last_frame_count 22
//             video_last_time 0.92
//             video_last_fps 24.01
//             audio_last_frame_count 43986
//             audio_last_rate 47999
//             still_last none
//             show_timelapse off
//             timelapse_period 60
//             timelapse_active off
//             timelapse_hold off
//             timelapse_jpeg_last none
//             timelapse_converting none
//             timelapse_video_last none
//             current_minute 1180
//             dawn 445
//             sunrise 479
//             sunset 1117
//             dusk 1151
//             multicast_group_IP 225.0.0.55
//             multicast_group_port 22555
//             running_state 1
//
// Local arguments
//
static int  fThreadRunning = TRUE;

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   GRD_Init
// Purpose:    Handle running state monitor for PiKrellCam
//
// Parms:      
// Returns:    Exit codes
// Note:       Calld from main() to split off thread and let it handle the 
//             Runnning state detection.
//             If PiKrellCam should be running (running_state = 1) but the thread is
//             nowhere to be found, it probably has crashed. We might find the cause of
//             it before restarting theApp.
//
int GRD_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_GUARD, tPid);
         GLOBAL_PidSetInit(PID_GUARD, GRD_Init);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = grd_Execute();
         GLOBAL_PidPut(PID_GUARD, 0);
         LOG_Report(0, "GRD", "GRD-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("GRD-Init(): Error!" CRLF);
         LOG_Report(errno, "GRD", "GRD-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_GRD_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   grd_Execute
// Purpose:    Handle PiKrellCam running state monitor
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int grd_Execute()
{
   int      iOpt, iCc=EXIT_CC_OKEE;
   sigset_t tBlockset;
   PSAUX   *pstInfo;

   pstInfo = (PSAUX *)safemalloc(sizeof(PSAUX));
   //
   if(grd_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "GRD", "grd-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_GUARD);
   //
   // Init ready: await further actions
   //
   GLOBAL_PidTimestamp(PID_GUARD, 0);
   GLOBAL_PidSaveGuard(PID_GUARD, GLOBAL_HUE_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_GRD_INI);
   //
   // Main loop of the Guard: wait for the semaphore posted by the parent 
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_GUARD, 5000);
      switch(iOpt)
      {
         case 0:
            GLOBAL_PidTimestamp(PID_GUARD, 0);
            if(GLOBAL_GetSignalNotification(PID_GUARD, GLOBAL_HST_ALL_MID)) 
            {
               // Midnight
               PRINTF("grd-Execute():Midnight" CRLF);
               LOG_Report(0, "GRD", "grd-Execute():Midnight");
            }
            if(GLOBAL_GetSignalNotification(PID_GUARD, GLOBAL_HST_GRD_RUN)) 
            {
               // Signal from host: check PiKrellCam running state
               grd_CheckRunningState(pstInfo);
            }
            break;

         case 1:
            // Timeout: 
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "GRD", "grd-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("grd-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   safefree(pstInfo);
   GLOBAL_SemaphoreDelete(PID_GUARD);
   return(iCc);
}

// 
// Function:   grd_CheckRunningState
// Purpose:    Check if theApp PiKrellCam is still running
// 
// Parameters:  Info struct
// Returns:
// 
static void grd_CheckRunningState(PSAUX *pstInfo)
{
   static pid_t   tLastErrorPid=0;
   static bool    fRunStateRunning=FALSE;
   //
   RUNST tRunState = (RUNST)pstMap->G_iRunState;
   
   //
   // Determine if pikrellcam is running and wether it actually should be, according to the running state.
   // If so, exit for now, else restart theApp, after checking why theApp crashed.
   //
   GEN_GetThreadInfo(pcPikrellCam, pstInfo);
   switch(pstInfo->tPid)
   {
      case -1:
         PRINTF("grd-CheckRunningState():GEN-GetThreadInfo ERROR" CRLF);
         if(tLastErrorPid != pstInfo->tPid)
         {
            LOG_Report(errno, "GRD", "grd-CheckRunningState():GEN-GetThreadInfo ERROR");
            tLastErrorPid = pstInfo->tPid;
         }
         break;

      case 0:
         //=========================================================================================================
         // pikrellcam PID NOT found, probably NOT running
         //=========================================================================================================
         PRINTF("grd-CheckRunningState():pikrellcam Pid not found" CRLF);
         if(tLastErrorPid != pstInfo->tPid)
         {
            LOG_Report(0, "GRD", "grd-CheckRunningState():PiKrellCam PID Not found");
            tLastErrorPid = pstInfo->tPid;
         }
         //
         // Guard will ONLY restart theApp if pikrellcam
         // has been switched OFF controlled through the web interface. 
         //
         switch(tRunState)
         {
            default:
            case RUN_STATE_ERROR_LOOP_SPACE:
            case RUN_STATE_STOPPED:
               //
               // PiKrellCam was running, but has been terminated through the WEB interface
               // Guard will do nothing
               //
               if(fRunStateRunning)
               {
                  // Log it ONCE.
                  PRINTF("grd-CheckRunningState():OKEE-->PiKrellCam STOPPED through WEB interface!)" CRLF);
                  LOG_Report(0, "GRD", "grd-CheckRunningState():OKEE-->PiKrellCam STOPPED through WEB interface!)");
                  fRunStateRunning = FALSE;
               }
               break;

            case RUN_STATE_RUNNING:
               //
               // The state-file thinks PiKrellCam is still running but has NOT been found
               // in the task list: PiKrellCam probably crashed: Restart it if all seems well
               //
               PRINTF("grd-CheckRunningState():BAD-->PiKrellCam RunState says RUNNING, but it's not!" CRLF);
               LOG_Report(0, "GRD", "grd-CheckRunningState():BAD-->PiKrellCam RunState says RUNNING but it's not: Restart theApp");
               grd_RestartTheApp();
               fRunStateRunning = TRUE;
               break;
         }
         break;

      default:
         //=========================================================================================================
         // pikrellcam PID found so theApp is probably running
         //=========================================================================================================
         PRINTF("grd-CheckRunningState():PiKrellCam Pid=%d" CRLF, pstInfo->tPid);
         if(tLastErrorPid != pstInfo->tPid)
         {
            LOG_Report(0, "GRD", "grd-CheckRunningState():PiKrellCam PID=%d", pstInfo->tPid);
            tLastErrorPid = pstInfo->tPid;
         }
         switch(tRunState)
         {
            default:
            case RUN_STATE_STOPPED:
               //
               // PiKrellCam was not supposed to be running
               //
               if(fRunStateRunning)
               {
                  PRINTF("grd-CheckRunningState():BAD-->PiKrellCam PID found, but run_state=STOPPED" CRLF);
                  LOG_Report(0, "GRD", "grd-CheckRunningState():BAD-->PiKrellCam PID found, but run_state=STOPPED");
                  //
                  fRunStateRunning = FALSE;
               }
               break;

            case RUN_STATE_RUNNING:
               //
               // PiKrellCam seems to be running OKee
               //
               if(!fRunStateRunning)
               {
                  PRINTF("grd-CheckRunningState():OKEE --> PiKrellCam PID found, run_state=RUNNING" CRLF);
                  LOG_Report(0, "GRD", "grd-CheckRunningState():OKEE --> PiKrellCam PID found, run_state=RUNNING");
                  //
                  fRunStateRunning = TRUE;
               }
               break;

            case RUN_STATE_ERROR_LOOP_SPACE:
               //
               // PiKrellCam is running because it has detected a temp-drive FULL issue.
               //
               if(fRunStateRunning)
               {
                  // Log it ONCE.
                  PRINTF("grd-CheckRunningState():BAD-->PiKrellCam run_state=LOOP Disk FULL" CRLF);
                  LOG_Report(0, "GRD", "grd-CheckRunningState():BAD-->PiKrellCam run_state=LOOP Disk FULL");
                  //
                  fRunStateRunning = FALSE;
               }
               break;
         }
         break;
   }
}

// 
// Function:   grd_RestartTheApp
// Purpose:    System restart theApp PiKrellcam
// 
// Parameters: 
// Returns:    Cc
// Note:       Since in problems before: opt for start in full verbose mode
// 
static int grd_RestartTheApp()
{
   int   iCc;
   char *pcShell;

   pcShell = (char *)safemalloc(MAX_PATH_LENZ);
	// 
   // Restart PiKrellcam
   //          0: Verbose none, discard additional stdout/stderr Output 
   //          1: Verbose all,  but discard additional stdout/stderr Output 
   //          2: Verbose none, stdout/stderr Output to AppRestart.log
   //          3: Verbose all,  stdout/stderr Output to AppRestart.log
   //          4: Verbose none, reroute stdout/stderr Output separately
   //          4: Verbose all,  reroute stdout/stderr Output separately
   //
   switch(pstMap->G_iCamOpts)
   {
      case 0:
      default:
         GEN_SNPRINTF(pcShell, MAX_PATH_LEN, "sudo -u pi /home/pi/pikrellcam/pikrellcam  >/dev/null 2>&1 &");
         break;

      case 1:
         GEN_SNPRINTF(pcShell, MAX_PATH_LEN, "sudo -u pi /home/pi/pikrellcam/pikrellcam -vall >/dev/null 2>&1 &");
         break;

      case 2:
         GEN_SNPRINTF(pcShell, MAX_PATH_LEN, "sudo -u pi /home/pi/pikrellcam/pikrellcam >/mnt/rpicache/pikrellman/AppRestart.log 2>&1 &");
         break;

      case 3:
         GEN_SNPRINTF(pcShell, MAX_PATH_LEN, "sudo -u pi /home/pi/pikrellcam/pikrellcam -vall >/mnt/rpicache/pikrellman/AppRestart.log 2>&1 &");
         break;

      case 4:
         GEN_SNPRINTF(pcShell, MAX_PATH_LEN, "sudo -u pi /home/pi/pikrellcam/pikrellcam 1>/mnt/rpicache/pikrellman/AppOutput.log 2>/mnt/rpicache/pikrellman/AppError.log &");
         break;

      case 5:
         GEN_SNPRINTF(pcShell, MAX_PATH_LEN, "sudo -u pi /home/pi/pikrellcam/pikrellcam -vall 1>/mnt/rpicache/pikrellman/AppOutput.log 2>/mnt/rpicache/pikrellman/AppError.log &");
         break;
   }
   iCc = system(pcShell);
   PRINTF("grd-RestartTheApp():[%s]:cc=%d" CRLF, pcShell, iCc);
   LOG_Report(0, "GRD", "grd-RestartTheApp():Restart theApp:[%s]:cc=%d", pcShell, iCc);
   safefree(pcShell);
   return(iCc);
}


/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   grd_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool grd_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &grd_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &grd_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &grd_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &grd_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &grd_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   grd_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void grd_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   grd_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void grd_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "GRD", "grd-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_GUARD);
}

//
// Function:   grd_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void grd_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "GRD", "grd-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_GUARD);
}

//
// Function:   grd_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void grd_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   grd_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void grd_ReceiveSignalUser2(int iSignal)
{
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT

#endif   //COMMENT
