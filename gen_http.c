/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           gen_http.c
 *  Purpose:            Generic HTTP functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Nov 2021:      Ported from rpihue
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_page.h"
//
#include "gen_http.h"

//#define USE_PRINTF
#include <printx.h>


/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
// Function:   GEN_FindDynamicPageName
// Purpose:    Find a registered dynamic HTTP webpage by it's tUrl
//
// Parameters: tUrl (DYN_SME_HTML_FILES, ...)
// Returns:
// Note:
//
char *GEN_FindDynamicPageName(int tUrl)
{
   int      iIdx;
   char    *pcUrl=NULL;
   
   PRINTF("GEN_FindDynamicPageName():tUrl=%d, Total pages=%d" CRLF, tUrl, pstMap->G_iNumDynPages);

   for(iIdx=0; iIdx<pstMap->G_iNumDynPages; iIdx++)
   {
      if(tUrl == pstMap->G_stDynPages[iIdx].tUrl)
      {
         pcUrl = pstMap->G_stDynPages[iIdx].pcUrl;
         break;
      }
   }
   return(pcUrl);
}


//  
// Function:   GEN_RegisterDynamicPage
// Purpose:    Register a dynamic HTTP webpage
//  
// Parameters: Owner Url, Pid enum, Page type, timeout, port, Page name, callback()
// Returns:    New index or -1 if full
// Note:       Dynamic pages are port dependent:
//                iPort has the portnumber (or zero for all ports)
//             Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//             
int GEN_RegisterDynamicPage(int tUrl, int ePid, FTYPE tType, int iTimeout, int iPort, const char *pcUrl, PFVOIDPI pfCb)
{
   int      iIdx=-1;
   DYNR    *pstDyn;
   
   GLOBAL_Lock();
   if(pstMap->G_iNumDynPages < MAX_DYN_PAGES)
   {
      iIdx   =   pstMap->G_iNumDynPages++;
      pstDyn = &(pstMap->G_stDynPages[iIdx]);
      //
      // Store new enrty
      //
      pstDyn->tUrl         = tUrl;
      pstDyn->tType        = tType;
      pstDyn->iTimeout     = iTimeout;
      pstDyn->iPort        = iPort;
      pstDyn->ePid         = ePid;
      pstDyn->pfCallback   = pfCb;   
      GEN_STRNCPY(pstDyn->pcUrl, pcUrl, MAX_URL_LEN);
      GLOBAL_Unlock();
      PRINTF("GEN_RegisterDynamicPage():%d=[%s]" CRLF, iIdx, pcUrl);
   }
   else
   {
      GLOBAL_Unlock();
      PRINTF("GEN_RegisterDynamicPage():Register Dynamic Page failed Pages=%d URL=[%s]" CRLF, pstMap->G_iNumDynPages, pcUrl);
      LOG_Report(0, "GEN", "Register Dynamic Page failed !");
   }
   return(iIdx);
}

//  
// Function:   GEN_GetDynamicPageCallback
// Purpose:    Get the dynamic Page callback()
//  
// Parameters: Index
// Returns:    Callback func or NULL
// Note:       
//             
PFVOIDPI GEN_GetDynamicPageCallback(int iIdx)
{
   if(iIdx < 0)                      return(NULL);
   if(iIdx < pstMap->G_iNumDynPages) return(pstMap->G_stDynPages[iIdx].pfCallback);
   else                              return(NULL);
}

//  
// Function:   GEN_GetDynamicPageUrl
// Purpose:    Get the dynamic Page URL type 
//             (DYN_HTML_... from EXTRACT_DYN pages.h, ...)
//  
// Parameters: Index
// Returns:    Owner CMD or -1
// Note:       
//             
int GEN_GetDynamicPageUrl(int iIdx)
{
   if(iIdx < 0)                      return(-1);
   if(iIdx < pstMap->G_iNumDynPages) return(pstMap->G_stDynPages[iIdx].tUrl);
   else                              return(-1);
}

//
// Function:   GEN_GetDynamicPageName
// Purpose:    Get a registered dynamic HTTP webpage
//  
// Parameters: Index 0..max
// Returns:    
// Note:       
//             
char *GEN_GetDynamicPageName(int iIdx)
{
   PRINTF("GEN_GetDynamicPageName():Idx=%d, Total pages=%d" CRLF, iIdx, pstMap->G_iNumDynPages);

   if(iIdx < 0)                      return(NULL);
   if(iIdx < pstMap->G_iNumDynPages) return(pstMap->G_stDynPages[iIdx].pcUrl);
   else                              return(NULL);
}

//  
// Function:   GEN_GetDynamicPagePid
// Purpose:    Get the dynamic Page PID
//  
// Parameters: Index
// Returns:    Pid enum of the thread handling the request or -1
// Note:       
//             
int GEN_GetDynamicPagePid(int iIdx)
{
   if(iIdx < 0)                      return(-1);
   if(iIdx < pstMap->G_iNumDynPages) return(pstMap->G_stDynPages[iIdx].ePid);
   else                              return(-1);
}

//  
// Function:   GEN_GetDynamicPagePort
// Purpose:    Get the dynamic Page Port
//  
// Parameters: Index
// Returns:    Port (0=all ports, -1=error)
// Note:       
//             
int GEN_GetDynamicPagePort(int iIdx)
{
   if(iIdx < 0)                      return(-1);
   if(iIdx < pstMap->G_iNumDynPages) return(pstMap->G_stDynPages[iIdx].iPort);
   else                              return(-1);
}

//  
// Function:   GEN_GetDynamicPageTimeout
// Purpose:    Get the dynamic Page timeout
//  
// Parameters: Index
// Returns:    Timeout 0...xx in msec or -1
// Note:       
//             
int GEN_GetDynamicPageTimeout(int iIdx)
{
   if(iIdx < 0)                      return(-1);
   if(iIdx < pstMap->G_iNumDynPages) return(pstMap->G_stDynPages[iIdx].iTimeout);
   else                              return(-1);
}

//  
//  Function    :  GEN_GetDynamicPageType
//  Description :  Get the dynamic Page
//  
//  Parameters  :  Index
//  Returns     :  The HTTP tpye (HTTP_HTML, HTTP_JSON, ...) or -1
//  
//  
FTYPE GEN_GetDynamicPageType(int iIdx)
{
   if(iIdx < 0)                      return(-1);
   if(iIdx < pstMap->G_iNumDynPages) return(pstMap->G_stDynPages[iIdx].tType);
   else                              return(-1);
}


/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//  
// Function:   
// Purpose:    
//  
// Parameters: 
// Returns:    
// Note:       
//             
