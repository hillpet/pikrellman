/*  (c) Copyright:  2021  Patrn, Confidential Data
 *
 *  Workfile:           rpi_func.h
 *  Purpose:            Headerfile for pikrellman functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Jun 2021:      Created
 *    14 Dec 2021:      Add RPI_AllocateArchivePath(), RPI_GetTimestampSecs()
 *    15 Dec 2021:      Add RPI_FileExists()
 *    02 Oct 2024:      RPI_GetDirEntries() --> GEN_GetDirEntries()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_FUNC_H_
#define _RPI_FUNC_H_

//
// =========================================================================================
// FIFO Commands
//    example: 
//    echo "motion_enable [on|off|toggle]" > ~/pikrellcam/www/FIFO
// or
//    pcFifo = GLOBAL_GetParameter(PAR_FIFO);
//    GEN_SNPRINTF(pcShell, MAX_CMDLINE_LEN, "echo \"archive_video %s %s-%s-%s\" >%s", pcFile, cYear, cMonth, cDay, pcFifo);
//    iCc = system(pcShell);
//
// =========================================================================================
//    audio mic_open
//    audio mic_close
//    audio mic_toggle
//    audio gain [up|down|N]		# N: 0 - 30
//    audio stream_open
//    audio stream_close
//    audio_trigger_video [on|off]
//    audio_trigger_level N		# N: 2 - 100
//    box_MP3_only [on|off]
//    record on
//    record on pre_capture_time
//    record on pre_capture_time time_limit
//    record pause
//    record off
//    loop [on|off|toggle]
//    still
//    tl_start period
//    tl_end
//    tl_hold [on|off|toggle]
//    tl_show_status [on|off|toggle]
//    motion_enable [on|off|toggle]
//    motion limits magnitude count
//    motion burst count frames
//    motion trigger code    # code is digit N or N:ID    N is 0 or 1 and ID is a string (see Examples)
//    motion trigger code pre_capture time_limit
//    motion load_regions name
//    motion save_regions name
//    motion list_regions
//    motion show_regions [on|off|toggle]
//    motion show_vectors [on|off|toggle]
//    motion [command] - other commands sent by the web page to edit motion regions not
//    	intented for script or command line use.
//    
//    preset prev_position
//    preset next_position
//    preset prev_settings
//    preset next_settings
//    preset goto position settings
//    
//    display [command] - commands sent by the web page to display OSD menus. Not intended for
//    	script or command line use.
//    
//    tl_inform_convert
//    video_fps fps
//    video_bitrate bitrate
//    still_quality quality
//    video_mp4box_fps fps
//    inform "some string" row justify font xs ys
//    	echo inform \"Have a nice day.\" 3 3 1 > FIFO
//    	echo inform timeout 3
//    archive_video [day|day_loop|video.mp4] [today|yesterday|yyyy-mm-dd]
//    archive_still [day|video.mp4] [today|yesterday]yyyy-mm-dd]
//    annotate_text_background_color [none|rrggbb]   # rrggbb is hex color value 000000 - ffffff
//    annotate_text_brightness value   # value is integer 0 - 255, 255 default
//    annotate_text_size  value        # value is integer 6 - 160, 32 default
//    annotate_string [prepend|append] id string
//    annotate_string remove id
//    annotate_string spacechar c
//    fix_thumbs [fix|test]
//    delete_log
//    upgrade
//    quit
//    
// PiKrellCam file mask:
//
// No motion
// ===================================
// loop_2018-07-27_15.34.55_0.mp4
// loop_2018-07-27_15.34.55_0.th.jpg
//
// Motion
// ===================================
// loop_2018-07-27_15.34.55_m.mp4
// loop_2018-07-27_15.34.55_m.th.jpg
//
//    C:    Caption     (loop, manual, ..)
//    Y:    Year        (2018,..)
//    M:    Month       (01...12)
//    D:    Day         (01...31)
//    h:    Hour        (00...23)
//    m:    Minute      (00...59)
//    s:    Second      (00...59)
//    T:    Type        (0, m, ...)
//    x:    Extension   (.mp4, .th.jpg, ...)
//    _:    Separator
//
typedef enum _pikrellcam_mask_
{
   MASK_DELIM = 0,         // 1st position of template must be the delimiter
   MASK_YEAR,
   MASK_MONTH,
   MASK_DAY,
   MASK_HOUR,
   MASK_MINUTE,
   MASK_SECOND,
   MASK_TYPE,
   MASK_EXT,
   //
   NUM_MASK
}  PIKMSK;

#define  CMD_BUFFER_SIZE      32768
//
// Directory/File Attributes
//
#define  ATTR_RW_R__R__       (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)
#define  ATTR_RW_RW_R__       (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH)
#define  ATTR_RWXR_XR_X       (S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH)
#define  ATTR_RWXRWXR_X       (S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH)
//
// Global functions
//
char    *RPI_AllocateArchivePath          (PIKTIM *, CTTYPE);
int64    RPI_CleanupFiles                 (char *, char *, int *);
bool     RPI_DirectoryCreate              (char *, mode_t, bool);
bool     RPI_DirectoryExists              (char *);
int      RPI_ExtractArguments             (void);
int      RPI_ExtractParameter             (PIKMSK, char *, char *, int);
bool     RPI_ExtractTimestamp             (char *, PIKTIM *);
int      RPI_ExtractVariables             (const char *, const PIKVAR *, int);
bool     RPI_FileExists                   (char *);
void     RPI_FreeDirEntries               (PIKDIR *);
char    *RPI_FindFilename                 (char *);
//pwjh PIKDIR  *RPI_GetDirEntries                (char *, char *, int *, int *);
bool     RPI_GetFlag                      (int);
int      RPI_GetFlags                     (void);
u_int32  RPI_GetTimestampSecs             (PIKTIM *);
int      RPI_GetVerboseLevel              (void);
int      RPI_InformFifo                   (const char *, char *, char *, int);
void     RPI_LogTimestamp                 (const char *, char *, PIKTIM *);
int64    RPI_RemoveFiles                  (char *, char *, int);
void     RPI_SaveMotionToFile             (char *);
void     RPI_SetFlag                      (int, bool);
void     RPI_SetTimestampSecs             (PIKTIM *, u_int32);
//
bool     RPI_SemaphoreDelete              (sem_t *);
bool     RPI_SemaphoreInit                (sem_t *);
bool     RPI_SemaphorePost                (sem_t *);

#endif /* _RPI_FUNC_H_ */
