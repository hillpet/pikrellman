/*  (c) Copyright:  2020 Patrn, Confidential Data 
 *
 *  Workfile:           gen_stats.h
 *  Purpose:            Status defines   
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Jun 2021:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
 
EXTRACT_ST(GEN_STATUS_IDLE,         "Idle"            )
EXTRACT_ST(GEN_STATUS_ERROR,        "Error"           )
EXTRACT_ST(GEN_STATUS_TIMEOUT,      "HTTP Timeout"    )
EXTRACT_ST(GEN_STATUS_REJECTED,     "HTTP Rejected"   )
EXTRACT_ST(GEN_STATUS_REDIRECT,     "HTTP Redirected" )
