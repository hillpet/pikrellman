/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           hue_states.h
 *  Purpose:            Hue bridge states
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Mar 2022:      New
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if defined (BOARD_RPI12)
//
// Rpi12: Carport
//
EXTRACT_HS(HUE_STATE_IDLE,       HUE_STATE_IDLE,      "HUE_STATE_IDLE       Idle",              hue_StateIdle              )
EXTRACT_HS(HUE_STATE_STARTUP,    HUE_STATE_DAWN_WAIT, "HUE_STATE_STARTUP    Start up",          hue_StateStartup           )
EXTRACT_HS(HUE_STATE_DAWN_WAIT,  HUE_STATE_DAWN_SET,  "HUE_STATE_DAWN_WAIT  Wait for dawn",     hue_StateDawnWait          )
EXTRACT_HS(HUE_STATE_DAWN_SET,   HUE_STATE_VERIFY_0,  "HUE_STATE_DAWN_SET   Set dawn lights",   hue_StateDawnSet           )
EXTRACT_HS(HUE_STATE_VERIFY_0,   HUE_STATE_DAWN_TMR,  "HUE_STATE_VERIFY_0   Verify Set Dawn",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DAWN_TMR,   HUE_STATE_DAWN_OFF,  "HUE_STATE_DAWN_TMR   Timer dawn lights", hue_StateDawnTimer         )
EXTRACT_HS(HUE_STATE_DAWN_OFF,   HUE_STATE_VERIFY_1,  "HUE_STATE_DAWN_OFF   Reset dawn lights", hue_StateDawnOff           )
EXTRACT_HS(HUE_STATE_VERIFY_1,   HUE_STATE_DUSK_WAIT, "HUE_STATE_VERIFY_1   Verify Set Dawn",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DUSK_WAIT,  HUE_STATE_DUSK_SET,  "HUE_STATE_DUSK_WAIT  Wait for dusk",     hue_StateDuskWait          )
EXTRACT_HS(HUE_STATE_DUSK_SET,   HUE_STATE_VERIFY_2,  "HUE_STATE_DUSK_SET   Set dusk lights",   hue_StateDuskSet           )
EXTRACT_HS(HUE_STATE_VERIFY_2,   HUE_STATE_DUSK_TMR,  "HUE_STATE_VERIFY_2   Verify Set Dusk",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DUSK_TMR,   HUE_STATE_EVNG_SET,  "HUE_STATE_DUSK_TMR   Timer dusk lights", hue_StateDuskTimer         )
EXTRACT_HS(HUE_STATE_EVNG_SET,   HUE_STATE_VERIFY_3,  "HUE_STATE_EVNG_SET   Evening lights",    hue_StateEveningSet        )
EXTRACT_HS(HUE_STATE_VERIFY_3,   HUE_STATE_DARK_WAIT, "HUE_STATE_VERIFY_3   Verify Set Eve",    hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DARK_WAIT,  HUE_STATE_DARK_SET,  "HUE_STATE_DARK_WAIT  Wait for night",    hue_StateDarkWait          )
EXTRACT_HS(HUE_STATE_DARK_SET,   HUE_STATE_VERIFY_4,  "HUE_STATE_DARK_SET   Turn lights OFF",   hue_StateDarkSet           )
EXTRACT_HS(HUE_STATE_VERIFY_4,   HUE_STATE_NEW_DAY,   "HUE_STATE_VERIFY4    Verify Set Dark",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_NEW_DAY,    HUE_STATE_DAWN_WAIT, "HUE_STATE_NEW_DAY    Wait for next day", hue_StateNewDay            )
//
EXTRACT_HS(HUE_STATE_WARN_SET,   HUE_STATE_WARN_WAIT, "HUE_STATE_WARN_SET   Warning ON",        hue_StateWarnSet           )
EXTRACT_HS(HUE_STATE_WARN_WAIT,  HUE_STATE_WARN_WAIT, "HUE_STATE_WARN_WAIT  Warning Wait",      hue_StateWarnWait          )
                                      
#elif defined (BOARD_RPI14)
//
// Rpi14: Garden/Pond
//
EXTRACT_HS(HUE_STATE_IDLE,       HUE_STATE_IDLE,      "HUE_STATE_IDLE       Idle",              hue_StateIdle              )
EXTRACT_HS(HUE_STATE_STARTUP,    HUE_STATE_DAWN_WAIT, "HUE_STATE_STARTUP    Start up",          hue_StateStartup           )
EXTRACT_HS(HUE_STATE_DAWN_WAIT,  HUE_STATE_DAWN_SET,  "HUE_STATE_DAWN_WAIT  Wait for dawn",     hue_StateDawnWait          )
EXTRACT_HS(HUE_STATE_DAWN_SET,   HUE_STATE_VERIFY_0,  "HUE_STATE_DAWN_SET   Set dawn lights",   hue_StateDawnSet           )
EXTRACT_HS(HUE_STATE_VERIFY_0,   HUE_STATE_DAWN_TMR,  "HUE_STATE_VERIFY_0   Verify Set Dawn",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DAWN_TMR,   HUE_STATE_DAWN_OFF,  "HUE_STATE_DAWN_TMR   Timer dawn lights", hue_StateDawnTimer         )
EXTRACT_HS(HUE_STATE_DAWN_OFF,   HUE_STATE_VERIFY_1,  "HUE_STATE_DAWN_OFF   Reset dawn lights", hue_StateDawnOff           )
EXTRACT_HS(HUE_STATE_VERIFY_1,   HUE_STATE_DUSK_WAIT, "HUE_STATE_VERIFY_1   Verify Set Dawn",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DUSK_WAIT,  HUE_STATE_DUSK_SET,  "HUE_STATE_DUSK_WAIT  Wait for dusk",     hue_StateDuskWait          )
EXTRACT_HS(HUE_STATE_DUSK_SET,   HUE_STATE_VERIFY_2,  "HUE_STATE_DUSK_SET   Set dusk lights",   hue_StateDuskSet           )
EXTRACT_HS(HUE_STATE_VERIFY_2,   HUE_STATE_DUSK_TMR,  "HUE_STATE_VERIFY_2   Verify Set Dusk",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DUSK_TMR,   HUE_STATE_EVNG_SET,  "HUE_STATE_DUSK_TMR   Timer dusk lights", hue_StateDuskTimer         )
EXTRACT_HS(HUE_STATE_EVNG_SET,   HUE_STATE_VERIFY_3,  "HUE_STATE_EVNG_SET   Evening lights",    hue_StateEveningSet        )
EXTRACT_HS(HUE_STATE_VERIFY_3,   HUE_STATE_DARK_WAIT, "HUE_STATE_VERIFY_3   Verify Set Eve",    hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DARK_WAIT,  HUE_STATE_DARK_SET,  "HUE_STATE_DARK_WAIT  Wait for night",    hue_StateDarkWait          )
EXTRACT_HS(HUE_STATE_DARK_SET,   HUE_STATE_VERIFY_4,  "HUE_STATE_DARK_SET   Turn lights OFF",   hue_StateDarkSet           )
EXTRACT_HS(HUE_STATE_VERIFY_4,   HUE_STATE_NEW_DAY,   "HUE_STATE_VERIFY4    Verify Set Dark",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_NEW_DAY,    HUE_STATE_DAWN_WAIT, "HUE_STATE_NEW_DAY    Wait for next day", hue_StateNewDay            )
//
EXTRACT_HS(HUE_STATE_WARN_SET,   HUE_STATE_WARN_WAIT, "HUE_STATE_WARN_SET   Warning ON",        hue_StateWarnSet           )
EXTRACT_HS(HUE_STATE_WARN_WAIT,  HUE_STATE_WARN_WAIT, "HUE_STATE_WARN_WAIT  Warning Wait",      hue_StateWarnWait          )

#elif defined (BOARD_RPI10)
//
// Rpi10: Development
//
EXTRACT_HS(HUE_STATE_IDLE,       HUE_STATE_IDLE,      "HUE_STATE_IDLE       Idle",              hue_StateIdle              )
EXTRACT_HS(HUE_STATE_STARTUP,    HUE_STATE_DAWN_WAIT, "HUE_STATE_STARTUP    Start up",          hue_StateStartup           )
EXTRACT_HS(HUE_STATE_DAWN_WAIT,  HUE_STATE_DAWN_SET,  "HUE_STATE_DAWN_WAIT  Wait for dawn",     hue_StateDawnWait          )
EXTRACT_HS(HUE_STATE_DAWN_SET,   HUE_STATE_VERIFY_0,  "HUE_STATE_DAWN_SET   Set dawn lights",   hue_StateDawnSet           )
EXTRACT_HS(HUE_STATE_VERIFY_0,   HUE_STATE_DAWN_TMR,  "HUE_STATE_VERIFY_0   Verify Set Dawn",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DAWN_TMR,   HUE_STATE_DAWN_OFF,  "HUE_STATE_DAWN_TMR   Timer dawn lights", hue_StateDawnTimer         )
EXTRACT_HS(HUE_STATE_DAWN_OFF,   HUE_STATE_VERIFY_1,  "HUE_STATE_DAWN_OFF   Reset dawn lights", hue_StateDawnOff           )
EXTRACT_HS(HUE_STATE_VERIFY_1,   HUE_STATE_DUSK_WAIT, "HUE_STATE_VERIFY_1   Verify Set Dawn",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DUSK_WAIT,  HUE_STATE_DUSK_SET,  "HUE_STATE_DUSK_WAIT  Wait for dusk",     hue_StateDuskWait          )
EXTRACT_HS(HUE_STATE_DUSK_SET,   HUE_STATE_VERIFY_2,  "HUE_STATE_DUSK_SET   Set dusk lights",   hue_StateDuskSet           )
EXTRACT_HS(HUE_STATE_VERIFY_2,   HUE_STATE_DUSK_TMR,  "HUE_STATE_VERIFY_2   Verify Set Dusk",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DUSK_TMR,   HUE_STATE_EVNG_SET,  "HUE_STATE_DUSK_TMR   Timer dusk lights", hue_StateDuskTimer         )
EXTRACT_HS(HUE_STATE_EVNG_SET,   HUE_STATE_VERIFY_3,  "HUE_STATE_EVNG_SET   Evening lights",    hue_StateEveningSet        )
EXTRACT_HS(HUE_STATE_VERIFY_3,   HUE_STATE_DARK_WAIT, "HUE_STATE_VERIFY_3   Verify Set Eve",    hue_StateVerify            )
EXTRACT_HS(HUE_STATE_DARK_WAIT,  HUE_STATE_DARK_SET,  "HUE_STATE_DARK_WAIT  Wait for night",    hue_StateDarkWait          )
EXTRACT_HS(HUE_STATE_DARK_SET,   HUE_STATE_VERIFY_4,  "HUE_STATE_DARK_SET   Turn lights OFF",   hue_StateDarkSet           )
EXTRACT_HS(HUE_STATE_VERIFY_4,   HUE_STATE_NEW_DAY,   "HUE_STATE_VERIFY4    Verify Set Dark",   hue_StateVerify            )
EXTRACT_HS(HUE_STATE_NEW_DAY,    HUE_STATE_DAWN_WAIT, "HUE_STATE_NEW_DAY    Wait for next day", hue_StateNewDay            )
//
EXTRACT_HS(HUE_STATE_WARN_SET,   HUE_STATE_WARN_WAIT, "HUE_STATE_WARN_SET   Warning ON",        hue_StateWarnSet           )
EXTRACT_HS(HUE_STATE_WARN_WAIT,  HUE_STATE_WARN_WAIT, "HUE_STATE_WARN_WAIT  Warning Wait",      hue_StateWarnWait          )

#else
#warning No Application board specified
//
EXTRACT_HS(HUE_STATE_IDLE,       HUE_STATE_IDLE,      "HUE_STATE_IDLE       Idle",              hue_StateIdle              )
EXTRACT_HS(HUE_STATE_STARTUP,    HUE_STATE_DAWN_WAIT, "HUE_STATE_STARTUP    Start up",          hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DAWN_WAIT,  HUE_STATE_DAWN_SET,  "HUE_STATE_DAWN_WAIT  Wait for dawn",     hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DAWN_SET,   HUE_STATE_DAWN_TMR,  "HUE_STATE_DAWN_SET   Set dawn lights",   hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DAWN_TMR,   HUE_STATE_DAWN_OFF,  "HUE_STATE_DAWN_TMR   Timer dawn lights", hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DAWN_OFF,   HUE_STATE_DUSK_WAIT, "HUE_STATE_DAWN_OFF   Reset dawn lights", hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DUSK_WAIT,  HUE_STATE_DUSK_SET,  "HUE_STATE_DUSK_WAIT  Wait for dusk",     hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DUSK_SET,   HUE_STATE_DUSK_TMR,  "HUE_STATE_DUSK_SET   Set dusk lights",   hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DUSK_TMR,   HUE_STATE_EVNG_SET,  "HUE_STATE_DUSK_TMR   Timer dusk lights", hue_StateIdle              )
EXTRACT_HS(HUE_STATE_EVNG_SET,   HUE_STATE_DARK_WAIT, "HUE_STATE_EVNG_SET   Evening lights",    hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DARK_WAIT,  HUE_STATE_DARK_SET,  "HUE_STATE_DARK_WAIT  Wait for night",    hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DARK_SET,   HUE_STATE_DARK_VFY,  "HUE_STATE_DARK_SET   Turn lights OFF",   hue_StateIdle              )
EXTRACT_HS(HUE_STATE_DARK_VFY,   HUE_STATE_DAWN_WAIT, "HUE_STATE_DARK_VFY   Verify lights OFF", hue_StateIdle              )
//
EXTRACT_HS(HUE_STATE_WARN_SET,   HUE_STATE_WARN_WAIT, "HUE_STATE_WARN_SET   Warning ON",        hue_StateIdle              )
EXTRACT_HS(HUE_STATE_WARN_WAIT,  HUE_STATE_WARN_BACK, "HUE_STATE_WARN_WAIT  Warning Wait",      hue_StateIdle              )
EXTRACT_HS(HUE_STATE_WARN_BACK,  HUE_STATE_WARN_BACK, "HUE_STATE_WARN_BACK  Warning Return",    hue_StateIdle              )

#endif
