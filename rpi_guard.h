/*  (c) Copyright:  2021  Patrn, Confidential Data
 *
 *  Workfile:           rpi_guard.h
 *  Purpose:            Headerfile for pikrellman running state monitor thread
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    20 Aug 2021:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_GUARD_H_
#define _RPI_GUARD_H_

//
// PikrellCamRun_states
//
typedef enum _runst_
{
   RUN_STATE_STOPPED = 0,
   RUN_STATE_RUNNING,
   RUN_STATE_ERROR_LOOP_SPACE,
   //
   NUM_RUNST
}  RUNST;

//
// Global functions
//
int GRD_Init               (void);

#endif /* _RPI_GUARD_H_ */
