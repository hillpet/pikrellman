/*  (c) Copyright:  2021  Patrn, Confidential Data
 *
 *  Workfile:           rpi_motion.h
 *  Purpose:            Headerfile for pikrellman motion detection thread
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Jun 2021:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MOTN_H_
#define _RPI_MOTN_H_

//
// Poll and parse motion-events file at:
//
#define  EVENT_POLL_ACTIVE    1000
#define  EVENT_POLL_IDLE      30000
//
#define  EVENT_BUFFER_LEN     1023
#define  EVENT_BUFFER_LENZ    EVENT_BUFFER_LEN+1

enum
{                                            // Motion acquisition
   MOT_STATE_IDLE = 0,                       //  - not started
   MOT_STATE_BUSY,                           //  - busy
   MOT_STATE_DONE,                           //  - finished
   MOT_STATE_ERROR                           //  - Error completion
};

//
// PiKrellCam motion data
//
#define  MOT_HEADER_START     0x00000001     // <header>
#define  MOT_HEADER_END       0x00000002     // </header>
#define  MOT_MOTION_START     0x00000004     // <motion 12.897>
#define  MOT_MOTION_END       0x00000008     // </motion>
#define  MOT_VIDEO            0x00000010     // video
#define  MOT_FRAME            0x00000020     // frame 121 68
#define  MOT_BURST_COUNT      0x00000040     // burst_count 400
#define  MOT_BURST_FRAMES     0x00000080     // burst_frames 3
#define  MOT_PRESET           0x00000100     // preset 1 2
#define  MOT_MAG_LIMIT        0x00000200     // magnitude_limit 6
#define  MOT_MAG_COUNT        0x00000400     // magnitude_count 6
#define  MOT_REGIONS          0x00000800     // f 21 14 9 -4 10 21
#define  MOT_END              0x40000000     // <end>
#define  MOT_ERROR            0x80000000     // Frames ERROR
//
#define  VALID_ACQUISITION    (MOT_HEADER_START|MOT_HEADER_END|MOT_MOTION_START)
//
#define  ACQUSITION_TIMEOUT   (3*60)         // Max Motion Acquisition in Secs
//
// Global functions
//
int MOTN_Init              (void);

#endif /* _RPI_MOTN_H_ */
