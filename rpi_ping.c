/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_ping.c
 *  Purpose:            Ping thread, to keep Wifi network alive
 *                      Ping address @PAR_PING_IP            
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    29 Nov 2021:      Ported from pikdog
 *    01 Jan 2022:      Add Ping Camera
 *    01 Jun 2022:      Add PidList timestamps
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_main.h"
//
#include "rpi_ping.h"
//
//#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static int     ping_Execute               (void);
static bool    ping_GetIpFromArpTable     (char *, char *, int);
static bool    ping_GetIpFromNmap         (char *, char *, int);
static bool    ping_GetPingIpAddressArp   (void);
static bool    ping_GetPingIpAddressNmap  (void);
static void    ping_IsMidnight            (void);
static void    ping_RequestReboot         (void);
static void    ping_ReportBehavior        (PSAUX *);
static bool    ping_PingNetwork           (PNET *, GLOPAR);
//
static bool    ping_SignalRegister        (sigset_t *);
static void    ping_ReceiveSignalInt      (int);
static void    ping_ReceiveSignalTerm     (int);
static void    ping_ReceiveSignalUser1    (int);
static void    ping_ReceiveSignalUser2    (int);
static void    ping_ReceiveSignalSegmnt   (int);
//
static volatile bool fPingRunning         = TRUE;
static PNET          stPingNet            = { FALSE, 0, PING_FREQ_MINUTES, 3, 0, 0, 0, 0 };
static PNET          stPingCam            = { FALSE, 0, PING_FREQ_MINUTES, 3, 0, 0, 0, 0 };
//
//
// Local arguments
//
static const char   *pcPingLocalHost      = "ping -4 -c1 localhost >/dev/null";
static const char   *pcPingNull           = "ping -4 -c1 %s >/dev/null";
static const char   *pcPingLog            = "ping -4 -c1 %s >>/mnt/rpicache/ping.log";
static const char   *pcCmdArp             = "arp -n";
static const char   *pcCmdNmap            = "sudo nmap -sn -T4 192.168.178.1/24";
static const char   *pcNmapReport         = "Nmap scan report for ";
static const char   *pcNmapMacAddr        = "MAC Address";
static const char   *pcDefaultIp          = "192.168.178.1";
static const char   *pcDefaultMac         = "5c:fa:25:a2:90:41";

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//  
// Function:   PING_Init
// Purpose:    Init the PING thread
// 
// Parameters: 
// Returns:    Startup code GLOBAL_XXX_INI
// Note:       
//  
int PING_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_PING, tPid);
         GLOBAL_PidSetInit(PID_PING, PING_Init);
         break;

      case 0:
         // Child: RPI Ping service
         GEN_Sleep(500);
         iCc = ping_Execute();
         GLOBAL_PidPut(PID_PING, 0);
         LOG_Report(0, "PNG", "PING-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("PING-Init(): Error!" CRLF);
         LOG_Report(errno, "PNG", "PING-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_PNG_INI);
}

/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
//  Function:   ping_Execute
//  Purpose:    Run the ping thread as a daemon
//
//  Parms:
//  Returns:    CC
//
static int ping_Execute(void)
{
   sigset_t    tBlockset;
   int         iOpt;

   GLOBAL_SemaphoreInit(PID_PING);
   if(ping_SignalRegister(&tBlockset) == FALSE) 
   {
      LOG_Report(errno, "PNG", "ping-Execute():Exit SignalRegister ERROR:");
      exit(EXIT_CC_GEN_ERROR);
   }
   if(pstMap->G_iSkipWifiPings) LOG_Report(0, "PNG", "ping-Execute():No Wifi pings required!");
   //
   // Init ready: await further actions
   //
   GLOBAL_PidSaveGuard(PID_PING, GLOBAL_PNG_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_PNG_INI);
   //
   // Wait until triggered by:
   //    o SIGINT:   Terminate normally
   //    o SIGTERM:  Terminate normally
   //    o SIGSEGV:  Segmentation fault
   //    o SIGUSR1:  Run command
   //    o SIGUSR2:  
   //
   while(fPingRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_PING, ONEMINUTE);
      switch(iOpt)
      {
         case 0:
            GLOBAL_PidTimestamp(PID_PING, 0);
            //
            // Check if HOST reports Midnight
            //
            if(GLOBAL_GetSignalNotification(PID_PING, GLOBAL_HST_ALL_MID)) 
            {
               PRINTF("ping-Execute():Received MID from host" CRLF);
               if(!pstMap->G_iSkipWifiPings) ping_IsMidnight();
            }
            //
            // Check if we need to retrieve the ARP table
            //
            if(GLOBAL_GetSignalNotification(PID_PING, GLOBAL_HST_PNG_ARP)) 
            {
               PRINTF("ping-Execute():Received ARP request from host" CRLF);
               if(!pstMap->G_iSkipWifiPings) ping_GetPingIpAddressArp();
            }
            //
            // Check if we need to retrieve the Nmap data
            //
            if(GLOBAL_GetSignalNotification(PID_PING, GLOBAL_HST_PNG_MAP)) 
            {
               PRINTF("ping-Execute():Received NMAP request from host" CRLF);
               if(!pstMap->G_iSkipWifiPings) ping_GetPingIpAddressNmap();
            }
            break;

         case 1:
            //
            // Timeout: ONEMINUTE 
            // Check Network pings
            //
            if(stPingNet.iPingMinutes-- == 0)
            {
               if(!pstMap->G_iSkipWifiPings) 
               {
                  ping_PingNetwork(&stPingNet, PAR_PING_IP);
               }
               else PRINTF("ping-Execute():Skip Wifi Net Pings" CRLF);
               stPingNet.iPingMinutes = PING_FREQ_MINUTES;
            }
            //
            // Check Camera pings
            //
            if(stPingCam.iPingMinutes-- == 0)
            {
               if(!pstMap->G_iSkipWifiPings) 
               {
                  ping_PingNetwork(&stPingCam, PAR_PING_CAM);
               }
               else PRINTF("ping-Execute():Skip Wifi Cam Pings" CRLF);
               stPingCam.iPingMinutes = PING_FREQ_MINUTES;
            }
            break;

         default:
         case -1:
            LOG_Report(errno, "PNG", "ping-daemon():ERROR GLOBAL_SemaphoreWait");
            PRINTF("ping-daemon():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   GLOBAL_SemaphoreDelete(PID_PING);
   return(EXIT_CC_OKEE);
}

//
// Function:   ping_GetIpFromArpTable
// Purpose:    Retrieve the IP address from the MAC by exploring the ARP table
//
// Parms:      MAC Address, IP Address, IP length
// Returns:    TRUE if found
// Note:       $ arp
//             Address           HWtypw   HWaddress            Flags Mask  Iface
//             192.....
//             192.168.178.18    ether    00:0b:3b:9b:7e:77    C           wlan0
//             192.....
//
static bool ping_GetIpFromArpTable(char *pcMac, char *pcIp, int iIpLen)
{
   bool     fCc=FALSE;
   bool     fEof=FALSE;
   char    *pcRec;
   char    *pcRes;
   FILE    *ptFp;

   pcRec = safemalloc(128);
   ptFp  = popen(pcCmdArp, "r");
   //
   if(ptFp)
   {
      PRINTF("ping-GetIpFromArpTable():Pipe open:%s" CRLF, pcCmdArp);
      //
      while(!fEof)
      {
         pcRes = fgets(pcRec, 127, ptFp);
         if(pcRes)
         {
            PRINTF("ping-GetIpFromArpTable():[%s]", pcRes);
            GEN_RemoveChar(pcRes, 0x0a);
            GEN_RemoveChar(pcRes, 0x0d);
            GEN_TrimRight(pcRes);
            PRINTF("ping-GetIpFromArpTable():[%s]" CRLF, pcRes);
            if(GEN_STRSTRI(pcRes, pcMac))
            {
               //
               // MAC found: extract IP
               //
               PRINTF("ping-GetIpFromArpTable():Found MAC=[%s]" CRLF, pcRes);
               GEN_ReplaceChar(pcRec, 0x20, 0x00);
               GEN_STRNCPY(pcIp, pcRec, iIpLen);
               fCc  = TRUE;
               fEof = TRUE;
            }
         }
         else 
         {
            PRINTF("ping-GetIpFromArpTable():EOF" CRLF);
            fEof = TRUE;
         }
      }
      pclose(ptFp);
   }
   else
   {
      PRINTF("ping-GetIpFromArpTable():ERROR Pipe open:%s" CRLF, pcCmdArp);
      LOG_Report(errno, "PNG", "ping-GetIpFromArpTable():ERROR Pipe open:%s", pcCmdArp);
   }
   safefree(pcRec);
   return(fCc);
}

//
// Function:   ping_GetIpFromNmap
// Purpose:    Retrieve the IP address from the MAC by exploring the nmap data
//
// Parms:      MAC Address, IP Address, IP length
// Returns:    TRUE if found
// Note:       $ sudo nmap -sn -T4 192.168.178.1/24
//             "...
//             "Nmap scan report for 192.168.178.18\n"
//             "Host is up (0.059s latency)\n"
//             "MAC Address: 00:0B:3B:9B:7E:77 (devolo AG\n"
//             "...
//
static bool ping_GetIpFromNmap(char *pcMac, char *pcIp, int iIpLen)
{
   bool     fCc=FALSE;
   bool     fEof=FALSE;
   char    *pcRec;
   char    *pcRes;
   char    *pcTmp;
   char    *pcNmapIp=NULL;
   char    *pcNmapMac;
   FILE    *ptFp;

   pcTmp = safemalloc(128);
   pcRec = safemalloc(128);
   ptFp  = popen(pcCmdNmap, "r");
   //
   if(ptFp)
   {
      PRINTF("ping-GetIpFromNmap():Pipe open:%s" CRLF, pcCmdNmap);
      //
      while(!fEof)
      {
         pcRes = fgets(pcRec, 127, ptFp);
         if(pcRes)
         {
            GEN_RemoveChar(pcRes, 0x0a);
            GEN_RemoveChar(pcRes, 0x0d);
            GEN_TrimRight(pcRes);
            PRINTF("ping-GetIpFromNmap():[%s]" CRLF, pcRes);
            if(GEN_STRSTRI(pcRes, pcNmapReport))
            {
               // "Nmap scan report for 192.168.178.18"
               pcNmapIp = GEN_FindDelimiter(pcRes, DELIM_NUMERIC);
               GEN_STRNCPY(pcTmp, pcNmapIp, 127);

            }
            if(GEN_STRSTRI(pcRes, pcNmapMacAddr))
            {
               // "MAC Address: 00:0B:3B:9B:7E:77 (devolo AG"
               if( (pcNmapMac = GEN_STRSTRI(pcRes, pcMac)) )
               {
                  //
                  // MAC found: Copy last IP, if any
                  //
                  if(pcNmapIp)
                  {
                     PRINTF("ping-GetIpFromNmap():Found IP=[%s], MAC=[%s]" CRLF, pcTmp, pcNmapMac);
                     GEN_STRNCPY(pcIp, pcTmp, iIpLen);
                     fCc  = TRUE;
                     fEof = TRUE;
                  }
               }
               else 
               {
                  //
                  // Wrong MAC: discard IP also
                  //
                  pcNmapIp = NULL;
               }
            }
         }
         else 
         {
            PRINTF("ping-GetIpFromNmap():EOF" CRLF);
            fEof = TRUE;
         }
      }
      pclose(ptFp);
   }
   else
   {
      PRINTF("ping-GetIpFromNmap():ERROR Pipe open:%s" CRLF, pcCmdNmap);
      LOG_Report(errno, "PNG", "ping-GetIpFromNmap():ERROR Pipe open:%s", pcCmdNmap);
   }
   safefree(pcRec);
   safefree(pcTmp);
   return(fCc);
}

//
//  Function:  ping_GetPingIpAddressArp
//  Purpose:   Get the IP address for this MAC from the network ARP table
//
//  Parms:     TRUE if IP found for this MAC
//  Returns:   
//  Note:      
//
static bool ping_GetPingIpAddressArp()
{
   bool     fCc=FALSE;
   int      iLen;
   char    *pcMac;
   char    *pcIp;

   PRINTF("ping-GetPingIpAddressArp()" CRLF);
   //
   // Let's seen if we have a MAC address to lookup. If not, take the default one
   //
   if( (pcMac = GLOBAL_GetParameter(PAR_PING_MAC)) )
   {
      if( (iLen = GEN_STRLEN(pcMac)) == 0)
      {
         iLen = GLOBAL_GetParameterSize(PAR_PING_MAC);
         GEN_STRNCPY(pcMac, pcDefaultMac, iLen);
      }
   }
   pcIp  = GLOBAL_GetParameter(PAR_PING_IP);
   iLen  = GLOBAL_GetParameterSize(PAR_PING_IP);
   //
   if(pcMac && pcIp)
   {
      PRINTF("ping-GetPingIpAddressArp():Find IP for Mac=[%s]" CRLF, pcMac);
      //
      // Pick up IP address of ping client
      //
      if( (fCc = ping_GetIpFromArpTable(pcMac, pcIp, iLen)) )
      {
         PRINTF("ping-GetPingIpAddressArp():Ping Mac=[%s], IP=[%s]" CRLF, pcMac, pcIp);
         LOG_Report(0, "PNG", "ping-GetPingIpAddressArp():Ping Mac=[%s], IP=[%s]", pcMac, pcIp);
      }
      else
      {
         PRINTF("ping-GetPingIpAddressArp():MAC address [%s] not found in ARP" CRLF, pcMac);
         LOG_Report(0, "PNG", "ping-GetPingIpAddressArp():MAC address [%s] not found in ARP", pcMac);
      }
   }
   else
   {
      PRINTF("ping-GetPingIpAddressArp():No MAC or IP in persistent storage" CRLF);
      if(!pcMac)  LOG_Report(0, "PNG", "ping-GetPingIpAddressArp():No Mac found in persistent storage");
      if(!pcIp)   LOG_Report(0, "PNG", "ping-GetPingIpAddressArp():No IP found in persistent storage");
   }
   return(fCc);
}

//
//  Function:  ping_GetPingIpAddressNmap
//  Purpose:   Get the IP address for this MAC from the network Nmap
//
//  Parms:     TRUE if IP found for this MAC
//  Returns:   
//  Note:      
//
static bool ping_GetPingIpAddressNmap()
{
   bool     fCc=FALSE;
   int      iLen;
   char    *pcMac;
   char    *pcIp;

   PRINTF("ping-GetPingIpAddressNmap()" CRLF);
   //
   // Let's seen if we have a MAC address to lookup. If not, take the default one
   //
   if( (pcMac = GLOBAL_GetParameter(PAR_PING_MAC)) )
   {
      if( (iLen = GEN_STRLEN(pcMac)) == 0)
      {
         iLen = GLOBAL_GetParameterSize(PAR_PING_MAC);
         GEN_STRNCPY(pcMac, pcDefaultMac, iLen);
      }
   }
   pcIp  = GLOBAL_GetParameter(PAR_PING_IP);
   iLen  = GLOBAL_GetParameterSize(PAR_PING_IP);
   //
   if(pcMac && pcIp)
   {
      PRINTF("ping-GetPingIpAddressNmap():Find IP for Mac=[%s]" CRLF, pcMac);
      //
      // Pick up IP address of ping client
      //
      if( (fCc = ping_GetIpFromNmap(pcMac, pcIp, iLen)) )
      {
         PRINTF("ping-GetPingIpAddressNmap():Ping Mac=[%s], IP=[%s]" CRLF, pcMac, pcIp);
         LOG_Report(0, "PNG", "ping-GetPingIpAddressNmap():Ping Mac=[%s], IP=[%s]", pcMac, pcIp);
      }
      else
      {
         PRINTF("ping-GetPingIpAddressNmap():MAC address [%s] not found in ARP" CRLF, pcMac);
         LOG_Report(0, "PNG", "ping-GetPingIpAddressNmap():MAC address [%s] not found in ARP", pcMac);
      }
   }
   else
   {
      PRINTF("ping-GetPingIpAddressNmap():No MAC or IP in persistent storage" CRLF);
      if(!pcMac)  LOG_Report(0, "PNG", "ping-GetPingIpAddressNmap():No Mac found in persistent storage");
      if(!pcIp)   LOG_Report(0, "PNG", "ping-GetPingIpAddressNmap():No IP found in persistent storage");
   }
   return(fCc);
}

//
//  Function:  ping_IsMidnight
//  Purpose:   It's passed midnight
//
//  Parms:     
//  Returns:   
//  Note:      
//
static void ping_IsMidnight()
{
   int      iLen;
   char    *pcIp;
   PSAUX    stInfo;

   //
   // Get the desired IP address (from the global MAC address) to ping the
   // wifi network on a regular basis to prevent it going OTL.
   //
   if( !ping_GetPingIpAddressArp() ) 
   {
      if( !ping_GetPingIpAddressNmap() )
      {
         //
         // Neither arp or nmap has the desired MAC address
         // Take the Router IP instead
         //
         pcIp = GLOBAL_GetParameter(PAR_PING_IP);
         iLen = GLOBAL_GetParameterSize(PAR_PING_IP);
         if(pcIp) GEN_STRNCPY(pcIp, pcDefaultIp, iLen);
      }
   }
   //
   // Report todays results
   //
   ping_ReportBehavior(&stInfo);
   if( (stPingNet.iNoWifi > PING_REBOOT_THRESHOLD) || (stPingCam.iNoWifi > PING_REBOOT_THRESHOLD))
   {
      //
      // Reboot if last PING_REBOOT_THRESHOLD pings went missing
      //
      ping_RequestReboot();
   }
   stPingNet.iNoWifi  = 0;
   stPingNet.iCountOk = 0;
   stPingNet.iCountNo = 0;
   //
   stPingCam.iNoWifi  = 0;
   stPingCam.iCountOk = 0;
   stPingCam.iCountNo = 0;
}

// 
// Function:   ping_RequestReboot
// Purpose:    System request to reboot
// 
// Parameters: 
// Returns:    
// Note:       
// 
static void ping_RequestReboot()
{
   PRINTF("ping-RequestReboot()" CRLF);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ALL_HST_RBT);
}

// 
// Function:   ping_ReportBehavior
// Purpose:    Show thread behavior/status
// 
// Parameters: 
// Returns:    
// Note:       
// 
static void ping_ReportBehavior(PSAUX *pstInfo)
{
   char  cDandT[32];
   char *pcParm;

   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cDandT, stPingNet.ulLastWifi);
   pcParm = GLOBAL_GetParameter(PAR_PING_IP);
   LOG_Report(0, "RPI", "ping-ReportBehavior():IP=%s, Wifi OK/NO = %d, %d (last seen at %s)", pcParm, stPingNet.iCountOk, stPingNet.iCountNo, cDandT);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cDandT, stPingCam.ulLastWifi);
   pcParm = GLOBAL_GetParameter(PAR_PING_CAM);
   LOG_Report(0, "RPI", "ping-ReportBehavior():IP=%s, Wifi OK/NO = %d, %d (last seen at %s)", pcParm, stPingCam.iCountOk, stPingCam.iCountNo, cDandT);
   //
   if(GEN_GetThreadInfo(pcPikrellCam, pstInfo))
   {
      LOG_Report(0, "RPI", "ping-ReportBehavior():Running %d-%d:%d:%d", pstInfo->iDays, pstInfo->iHours, pstInfo->iMins, pstInfo->iSecs);
      LOG_Report(0, "RPI", "ping-ReportBehavior():USER=%s PID=%d CPU=%3.1f MEM=%3.1f VSZ=%d RSS=%d STAT=%s TIME=%s CL=[%s]",
                              pstInfo->cUser,
                              pstInfo->tPid,
                              pstInfo->flCpu,
                              pstInfo->flMem,
                              pstInfo->iVsz,
                              pstInfo->iRss,
                              pstInfo->cStat,
                              pstInfo->cTime,
                              pstInfo->cCommand);
   }
   else 
   {
      LOG_Report(0, "RPI", "ping-ReportBehavior():GEN-GetThreadInfo ERROR");
   }
}

// 
// Function:   ping_PingNetwork
// Purpose:    Check if the gateway is still there
// 
// Parameters: Ping struct, Global Parm
// Returns:    TRUE if Gateway is OKee
// Note:       pstP->iPingLogged
//              0: Nothing logged
//             -1: Offline logged
//             +1: Online logged
//             
//             pstP->iCountNo = Counter missed Gateway pings
//
static bool ping_PingNetwork(PNET *pstP, GLOPAR eParm)
{
   bool        fCc=FALSE;
   int         iGateway=0;
   int         iRetries;
   char       *pcShell;
   char       *pcParms;
   const char *pcPing;

   //
   // First check if local network is awake
   //
   if( (iGateway = system(pcPingLocalHost)) != 0)
   {
         PRINTF("ping-PingNetwork():Local Network not responding!" CRLF);
         LOG_Report(0, "RPI", "ping-PingNetwork():Local Network not responding!");
         return(FALSE);
   }
   //
   if(pstP->fLog) pcPing = pcPingLog;
   else           pcPing = pcPingNull;
   //
   pcParms = GLOBAL_GetParameter(eParm);
   if(pcParms && GEN_STRLEN(pcParms) )
   {
      pcShell = (char *)safemalloc(128);
      GEN_SNPRINTF(pcShell, 127, pcPing, pcParms);
      //
      iRetries = pstP->iRetries;
      //
      do
      {
         if( (iGateway = system(pcShell)) == 0)
         {
            //
            // OKee completion: Wifi seems to be alive
            //
            pstP->ulLastWifi = RTC_GetSystemSecs();
            PRINTF("ping-PingNetwork():[%s] OKee" CRLF, pcShell);
            if(pstP->iPingLogged != 1)
            {
               LOG_Report(0, "RPI", "ping-PingNetwork():[%s] OKee", pcShell);
               pstP->iPingLogged = 1;
            }
            pstP->iCountOk++;
            pstP->iNoWifi = 0;
            iRetries      = 0;
            fCc           = TRUE;
         }
         else
         {
            if(iRetries == 0)
            {
               //
               // Error completion: Wifi seems to be OTL
               // Count and manage at midnight
               //
               pstP->iNoWifi++;
               pstP->iCountNo++;
               PRINTF("ping-PingNetwork():[%s] not found" CRLF, pcShell);
               if(pstP->iPingLogged != -1)
               {
                  LOG_Report(0, "RPI", "ping-PingNetwork():[%s] not found", pcShell);
                  pstP->iPingLogged = -1;
               }
            }
            else iRetries--;
         }
      }
      while(iRetries);
      safefree(pcShell);
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
________________SIGNAL_FUNTIONS(){};
------------------------------x----------------------------------------*/

//
//  Function:   ping_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void ping_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "PNG", "ping-ReceiveSignalTerm()");
   fPingRunning = FALSE;
   GLOBAL_SemaphorePost(PID_PING);
}

//
//  Function:   ping_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void ping_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "PNG", "ping-ReceiveSignalInt()");
   fPingRunning = FALSE;
   GLOBAL_SemaphorePost(PID_PING);
}

//
//  Function:   ping_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void ping_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

//
//  Function:   ping_ReceiveSignalPipe
//  Purpose:    Add the SIGPIPE command in the buffer (Broken pipe)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void ping_ReceiveSignalPipe(int iSignal)
{
   LOG_Report(errno, "PNG", "ping-ReceiveSignalPipe()");
}

//
// Function:   ping_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void ping_ReceiveSignalUser1(int iSignal)
{
   GLOBAL_SemaphorePost(PID_PING);
}

//
// Function:   ping_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void ping_ReceiveSignalUser2(int iSignal)
{
}

//
//  Function:   ping_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool ping_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &ping_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "PNG", "ping-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &ping_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "PNG", "ping-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &ping_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "PNG", "ping-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &ping_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "PNG", "ping-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &ping_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "PNG", "ping-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // Pipe error
  if( signal(SIGPIPE, &ping_ReceiveSignalPipe) == SIG_ERR)
  {
     LOG_Report(errno, "PNG", "ping-ReceiveSignalPipe(): SIGPIPE ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGPIPE);
  }
  return(fCC);
}


