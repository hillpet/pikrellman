/*  (c) Copyright:  2022  Patrn, Confidential Data
 *
 *  Workfile:           par_names.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Extract headerfile for the Hue bridge light/group/scene names
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    11 Jan 2022:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//           eName               cDevType             pcName                  pcHelp
EXTRACT_NAME(HUE_GARDEN_WEST,    HUE_DEVICE_LIGHT,    "Garden-West",          "Garden Light West")
EXTRACT_NAME(HUE_GARDEN_EAST,    HUE_DEVICE_LIGHT,    "Garden-East",          "Garden Light East")
EXTRACT_NAME(HUE_GARDEN_SPOT,    HUE_DEVICE_LIGHT,    "Garden-Spot",          "Garden Watercourse Spotlight")
EXTRACT_NAME(HUE_GARDEN_IR,      HUE_DEVICE_LIGHT,    "Garden-IR",            "Garden Light Infrared")
EXTRACT_NAME(HUE_GARDEN_NORTH,   HUE_DEVICE_LIGHT,    "Garden-North",         "Garden Light North")
EXTRACT_NAME(HUE_GARDEN_DOOR,    HUE_DEVICE_LIGHT,    "Garden-Door",          "Garden Doors Light")
//
EXTRACT_NAME(HUE_SPARE_DIMMER,   HUE_DEVICE_LIGHT,    "DIM 1 (Robb)",         "Spare Dimmer")

#if defined (BOARD_RPI12)
//
// Rpi12: Carport
//
EXTRACT_NAME(HUE_OUTLET_1,       HUE_DEVICE_LIGHT,    "Carport-Wcd",          "Carport Outlet")
EXTRACT_NAME(HUE_DAWN,           HUE_DEVICE_SCENE,    "Carport-Dawn",         "Carport Dawn lights")
EXTRACT_NAME(HUE_DUSK,           HUE_DEVICE_SCENE,    "Carport-Dusk",         "Carport Dusk lights")
EXTRACT_NAME(HUE_EVENING,        HUE_DEVICE_SCENE,    "Carport-Evening",      "Carport Evening lights")
EXTRACT_NAME(HUE_WARN_DAY,       HUE_DEVICE_SCENE,    "Carport-Warn-Day",     "Carport Daytime Warning")
EXTRACT_NAME(HUE_WARN_NIGHT,     HUE_DEVICE_SCENE,    "Carport-Warn-Night",   "Carport Nighttime Warning")
EXTRACT_NAME(HUE_DARK,           HUE_DEVICE_SCENE,    "Carport-Off",          "Carport NO lights")

#elif defined (BOARD_RPI14)
//
// Rpi14: Garden/Pond
//
EXTRACT_NAME(HUE_OUTLET_1,       HUE_DEVICE_LIGHT,    "Garden-Wcd",           "Garden Shed Outlet")
EXTRACT_NAME(HUE_DAWN,           HUE_DEVICE_SCENE,    "Garden-Dawn",          "Garden Dawn lights")
EXTRACT_NAME(HUE_DUSK,           HUE_DEVICE_SCENE,    "Garden-Dusk",          "Garden Dusk lights")
EXTRACT_NAME(HUE_EVENING,        HUE_DEVICE_SCENE,    "Garden-Evening",       "Garden Evening lights")
EXTRACT_NAME(HUE_WARN_DAY,       HUE_DEVICE_SCENE,    "Garden-Warn-Day",      "Garden Daytime Warning")
EXTRACT_NAME(HUE_WARN_NIGHT,     HUE_DEVICE_SCENE,    "Garden-Warn-Night",    "Garden Nighttime Warning")
EXTRACT_NAME(HUE_DARK,           HUE_DEVICE_SCENE,    "Garden-Off",           "Garden NO lights")

#elif defined (BOARD_RPI10)
//
// Rpi10: Development
//
EXTRACT_NAME(HUE_OUTLET_1,       HUE_DEVICE_LIGHT,    "Garden-Wcd",           "Garden Shed Outlet")
EXTRACT_NAME(HUE_DAWN,           HUE_DEVICE_SCENE,    "Garden-Dawn",          "Garden Dawn lights")
EXTRACT_NAME(HUE_DUSK,           HUE_DEVICE_SCENE,    "Garden-Dusk",          "Garden Dusk lights")
EXTRACT_NAME(HUE_EVENING,        HUE_DEVICE_SCENE,    "Garden-Evening",       "Garden Evening lights")
EXTRACT_NAME(HUE_WARN_DAY,       HUE_DEVICE_SCENE,    "Garden-Warn-Day",      "Garden Daytime Warning")
EXTRACT_NAME(HUE_WARN_NIGHT,     HUE_DEVICE_SCENE,    "Garden-Warn-Night",    "Garden Nighttime Warning")
EXTRACT_NAME(HUE_DARK,           HUE_DEVICE_SCENE,    "Garden-Off",           "Garden NO lights")

#else
#warning No Application board specified
//
EXTRACT_NAME(HUE_OUTLET_1,       HUE_DEVICE_LIGHT,    "DIM 1 (Robb)",         "Spare Dimmer")
EXTRACT_NAME(HUE_DAWN,           HUE_DEVICE_LIGHT,    "DIM 1 (Robb)",         "Spare Dimmer")
EXTRACT_NAME(HUE_DUSK,           HUE_DEVICE_LIGHT,    "DIM 1 (Robb)",         "Spare Dimmer")
EXTRACT_NAME(HUE_EVENING,        HUE_DEVICE_LIGHT,    "DIM 1 (Robb)",         "Spare Dimmer")
EXTRACT_NAME(HUE_WARN_DAY,       HUE_DEVICE_LIGHT,    "DIM 1 (Robb)",         "Spare Dimmer")
EXTRACT_NAME(HUE_WARN_NIGHT,     HUE_DEVICE_LIGHT,    "DIM 1 (Robb)",         "Spare Dimmer")
EXTRACT_NAME(HUE_DARK,           HUE_DEVICE_SCENE,    "DIM 1 (Robb)",         "Spare Dimmer")

#endif   //BOARD_RPI14
