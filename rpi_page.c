/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_page.c
 *  Purpose:            Dynamic HTTP page for Raspberry pi webserver
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Nov 2021:      Ported from rpihue
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    01 Jun 2022:      Add pids.html
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "http_func.h"
#include "rpi_main.h"
#include "rpi_func.h"
#include "rpi_json.h"
//
#include "rpi_page.h"

//#define USE_PRINTF
#include <printx.h>

//
// Generic HTTP data from common lib
//
extern const char *pcHttpResponseHeader;
extern const char *pcHttpResponseHeaderBad;
extern const char *pcHttpResponseHeaderJson;
extern const char *pcWebPageLineBreak;
extern const char *pcWebPageCenter;
extern const char *pcWebPageStart;
extern const char *pcWebPageStartRefresh;
extern const char *pcWebPagePreStart;
extern const char *pcWebPagePreEnd;
extern const char *pcWebPageTitle;
extern const char *pcWebPageDefault;
extern const char *pcWebPageEnd;

//
// Local prototypes
//
static bool    http_DynPageDefault           (NETCL *, int);
static bool    http_DynPageEmpty             (NETCL *, int);
static bool    http_DynPageExitHue           (NETCL *, int);
static bool    http_DynPageExit              (NETCL *, int);
static bool    http_DynPageLog               (NETCL *, int);
static bool    http_DynPageMail              (NETCL *, int);
static bool    http_DynPageMotion            (NETCL *, int);
static bool    http_DynPageParms             (NETCL *, int);
static bool    http_DynPagePids              (NETCL *, int);
static bool    http_DynPageCsvFiles          (NETCL *, int);
static bool    http_DynPageDateTime          (NETCL *, int);
//
// Local prototypes
//
static void    http_BuildPidRow              (NETCL *, int, int, int, char *, char *, char *);
static char   *http_ExtractPageName          (NETCL *);
static bool    http_ShowFiles                (NETCL *, const char *, const char *, const char *);

//
//
// Enums and dynamic LUTs
//
enum
{
   #define  EXTRACT_DYN(a,b,c,d,e,f)   a,
   #include "pages.h"
   #undef EXTRACT_DYN
   NUM_PAGES_DYNS
};
//
static const NETDYN stDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
   #include "pages.h"
   #undef EXTRACT_DYN
   {  -1,      0,       0,          0,       NULL,    NULL  }
};

//
// Misc global http header strings 
// HTTP Response:
//=============================
//    HTTP Response header
//    NEWLINE
//    Optional Message body
//    NEWLINE NEWLINE
//
const char *pcHttpResponseLength        = "HTTP/1.0 200 OK" NEWLINE
                                          "Content-Type: text/html" NEWLINE
                                          "Content-Length: %d" NEWLINE NEWLINE;
const char *pcHttpResponseLengthJson    = "HTTP/1.0 200 OK" NEWLINE
                                          "Content-Type: application/json" NEWLINE
                                          "Content-Length: %d" NEWLINE NEWLINE;
const char *pcWebPageContentBad         = "<p>No content found !</p>" NEWLINE;
const char *pcWebPageFontStart          = "<p style=\"font-family:'%s'\">" NEWLINE;
const char *pcWebPageFontEnd            = "</p>" NEWLINE;
const char *pcWebPageExitWarning        = "<hr/>" NEWLINE
                                         "<h2>PATRN ESS</h2>" NEWLINE
                                         "<h3>Cannot exit Smart Sensor HTTP server !!</h3>" NEWLINE
                                         "<hr/>" NEWLINE;
const char *pcWebPageNotImplemented     = "<hr/>" NEWLINE
                                         "<h2>PATRN ESS</h2>" NEWLINE
                                         "<h3>Page not implemented</h3>" NEWLINE
                                         "<hr/>" NEWLINE;
//
// HTTP table start: Needs border, width(%), colspan, headerstring
//
const char *pcWebPageTableStart         = "<table align=center border=%d width=\"%d%%\">" NEWLINE
                                         "<body style=\"text-align:center\">" NEWLINE
                                         "<thead>" NEWLINE
                                         "<th colspan=%d>%s</th>" NEWLINE;
const char *pcWebPageTableEnd           = "</tbody></table>" NEWLINE;
//
// HTTP table row: Needs row height
//
const char *pcWebPageTableRowStart      = "<tr height=%d>" NEWLINE;
const char *pcWebPageTableRowEnd        = "</tr>" NEWLINE;
const char *pcWebPageStopBad            = "<p>No data</p>" NEWLINE;

//=================================================================================================
//
// Misc local http header strings 
//
//=================================================================================================
static const char *pcWebPageEmpty       = "<br/>" NEWLINE
                                         "Welcome to another PATRN mini HTTP server." NEWLINE
                                         "<a href=\"http://www.patrn.nl\" target=\"_blank\">Patrn.nl</a>" NEWLINE
                                         "<br/>" NEWLINE;
static const char *pcWebExitPage        = "<br/>" NEWLINE
                                         "<br/>" NEWLINE
                                         "Exiting PATRN HTTP server. Good bye !" NEWLINE
                                         "<br/>" NEWLINE
                                         "<br/>" NEWLINE;

//
// HTTP table cel data: Needs [cell width,] data
//
static const char *pcWebPageCellData[] = 
{
   "<td style=\"text-align:center\">%s</td>" NEWLINE,
   "<td style=\"text-align:center\" width=\"%d%%\">%s</td>" NEWLINE NEWLINE
};

//
// HTTP table cel link data : Needs [cell width,] link, data
//
static const char *pcWebPageCellDataLink[] =
{
   "<td><a href=\"%s\">%s</td>" NEWLINE,
   "<td width=\"%d%%\"><a href=\"%s\">%s</td>" NEWLINE
};

//
// HTTP table cel numeric data : Needs [cell width,] data
//
static const char * pcWebPageCellDataNumber[] =
{
   "<td style=\"text-align:center\">%d</td>" NEWLINE,
   "<td style=\"text-align:center\" width=\"%d%%\">%d</td>" NEWLINE
};
//
// Misc http header strings 
//
static const char *pcEmpty                   = "";
//
#ifdef   BOARD_RPI12
static const char *pcWebRefreshPids          = "6;url=http://192.168.178.232:8080/pidsref.html";
#else    //BOARD_RPI14)
static const char *pcWebRefreshPids          = "6;url=http://192.168.178.214:8080/pidsref.html";
#endif
//
//
// CSV data
//
static const char *pcCsvValuesPath     = RPI_WORK_DIR;
static const char *pcCsvFilter         = "*.csv";
static const char *pcCsvHeader         = "HUE Bridge CSV Files";

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

// 
// Function:   HTTP_DynamicPageHandler
// Purpose:    Execute the dynamic webpage callback
// 
// Parameters: Socket description, DynPage index
// Returns:    TRUE if there is data to be send back
// Note:       
//
bool HTTP_DynamicPageHandler(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   PFVOIDPI pfCb;

   PRINTF("HTTP_DynamicPageHandler():Idx=%d" CRLF, iIdx);
   //
   pfCb = GEN_GetDynamicPageCallback(iIdx);
   if(pfCb)
   {
      fCc = pfCb(pstCl, iIdx);
   }
   return(fCc);
}

// 
// Function:   HTTP_InitDynamicPages
// Purpose:    Register dynamic webpages
// 
// Parameters: Port
// Returns:    Nr registered
// Note:       
//
int HTTP_InitDynamicPages(int iPort)
{
   int            iNr=0; 
   const NETDYN  *pstDyn=stDynamicPages;

   PRINTF("HTTP_InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      //PRINTF("HTTP_InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      iNr++;
      pstDyn++;
   }
   return(iNr);
}

//
// Function:   HTTP_PageIsDynamic
// Purpose:    Check if the requested page is dynamic or (file)static
// 
// Parameters: Net client struct
// Returns:    >=0 if requested page is dynamic (and needs construction)
// Note:       
//
int HTTP_PageIsDynamic(NETCL *pstCl)
{
   int   iIdx=0, iPort;
   char *pcDynName;
   char *pcPagename;

   pcPagename = http_ExtractPageName(pstCl);

   if ( pcPagename == NULL)   return(-1);
   if (*pcPagename == '/')    pcPagename++;
   if (*pcPagename == 0)      return(0);

   do
   {
      if( (pcDynName = GEN_GetDynamicPageName(iIdx)) )
      {
         PRINTF("HTTP_PageIsDynamic(): URL=%s:%04d, DynPage=%s" CRLF, pcPagename, pstCl->iPort, pcDynName);
         if(GEN_STRCMP(pcDynName, pcPagename) == 0)
         {
            //
            // This request is a dynamic HTTP page: check if port matches !
            //
            iPort = GEN_GetDynamicPagePort(iIdx);
            if( (iPort == 0) || (iPort == pstCl->iPort) ) return(iIdx);
         }
         //
         // No match: keep looking
         //
         iIdx++;
      }
   }
   while(pcDynName);
   return(-1);
}

/*------  Local functions separator ------------------------------------
________________BUILD_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
// Function:   HTTP_BuildStart
// Purpose:    Put out HTTP page start 
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildStart(NETCL *pstCl)
{
   //
   // Put out the HTML header, start tag, title etc
   //
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageDefault);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildRespondHeader
// Purpose:    Put out HTTP response header
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeader(NETCL *pstCl)
{
   return( HTTP_BuildGeneric(pstCl, pcHttpResponseHeader) );
}

//
// Function:   HTTP_BuildRespondHeaderBad
// Purpose:    Put out HTTP response header Bad Request
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeaderBad(NETCL *pstCl)
{
   return( HTTP_BuildGeneric(pstCl, pcHttpResponseHeaderBad) );
}

//
// Function:   HTTP_BuildRespondHeaderJson
// Purpose:    Put out HTTP page start Json
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeaderJson(NETCL *pstCl)
{
   return( HTTP_BuildGeneric(pstCl, pcHttpResponseHeaderJson) );
}

//
// Function:   HTTP_BuildLineBreaks
// Purpose:    Put out HTTP linebreaks
// 
// Parameters: Client, number of newlines
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildLineBreaks(NETCL *pstCl, int iNr)
{
   //
   // Put out the HTML header, start tag, title etc
   //
   while(iNr--) HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildDivStart
// Purpose:    Put out HTTP page div/section start 
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildDivStart(NETCL *pstCl)
{
   //
   // Put out the HTML div start tag
   //
   HTTP_BuildGeneric(pstCl, pcWebPagePreStart);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildDivEnd
// Purpose:    Put out HTTP page div/section end
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildDivEnd(NETCL *pstCl)
{
   //
   // Put out the HTML div end tag
   //
   HTTP_BuildGeneric(pstCl, pcWebPagePreEnd);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildEnd
// Purpose:    Put out HTTP page end
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildEnd(NETCL *pstCl)
{
   //
   // Put out the HTML end
   //
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
________________TABLE_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
// Function:   HTTP_BuildTableStart
// Purpose:    Start a http web page table
// 
// Parameters: Client, page header, border with, table width, number of columns
// Returns:    
// Note:       
//
bool  HTTP_BuildTableStart(NETCL *pstCl, const char *pcPage, u_int16 usBorder, u_int16 usWidth, u_int16 usCols)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableStart, usBorder, usWidth, usCols, pcPage);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableRowStart
// Purpose:    Starts a row in a table
// 
// Parameters: Client, row height
// Returns:    
// Note:       
//
bool  HTTP_BuildTableRowStart(NETCL *pstCl,  u_int16 usHeight)
{
   bool  fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableRowStart, usHeight);
   
   return(fOKee);
}

//
// Function:   HTTP_BuildTableRowEnd
// Purpose:    Ends a row
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool  HTTP_BuildTableRowEnd(NETCL *pstCl)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableRowEnd);
   
   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnText
// Purpose:    Start a generic column
// 
// Parameters: Client, page struct, column text, column width
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnText(NETCL *pstCl, char *pcText, u_int16 usWidth)
{
   bool     fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellData[1], usWidth, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellData[0], pcText);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnLink
// Purpose:    Start a link with a hyperlink
// 
// Parameters: Client, page struct, column text, width, link
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnLink(NETCL *pstCl, char *pcText, u_int16 usWidth, char *pcLink)
{
   bool  fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataLink[1], usWidth, pcLink, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataLink[0], pcLink, pcText);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnNumber
// Purpose:    Start a column with a number
// 
// Parameters: Client, page struct, number, width
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnNumber(NETCL *pstCl, u_int16 usNr, u_int16 usWidth)
{
   bool     fOKee;
   
   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataNumber[1], usWidth, usNr);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataNumber[0], usNr);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableEnd
// Purpose:    End a http web page table
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool  HTTP_BuildTableEnd(NETCL *pstCl)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableEnd);

   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__________________PAGE_HANDLERS(){};
------------------------------x----------------------------------------*/

//
// Function:   http_DynPageDefault
// Purpose:    Dynamically created default page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageDefault(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageEmpty);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "info");
         fCc = TRUE;
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageEmpty
// Purpose:    Dynamically created empty page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageEmpty(NETCL *pstCl, int iIdx)
{
   int      iSize;
   char    *pcFile;

   //
   // Build default file pathname
   //
   iSize  = (2 * MAX_PATH_LEN) + 1;
   pcFile = safemalloc(iSize);
   //
   GEN_SPRINTF(pcFile, "%s%s", pstMap->G_pcWwwDir, pstMap->G_pcDefault);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   //
   // Try to send out the default HTML file. If none exists, revert to default lines
   //
   if(!HTTP_SendTextFile(pstCl, pcFile) )
   {
      //
      // Put out a default reply: 
      //    
      HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
      HTTP_BuildGeneric(pstCl, pcWebPageDefault);
      HTTP_BuildGeneric(pstCl, pcWebPageEmpty);
      HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   }
   HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   safefree(pcFile);
   return(TRUE);
}

//
// Function:   http_DynPageExit
// Purpose:    Dynamically created system page to capture illegal exits from the server
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageExit(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageExitWarning);
         HTTP_BuildGeneric(pstCl, pcWebExitPage);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         //
         PRINTF("http_DynPageExit():Warning: no Smart Sensor HTTP server exit here !" CRLF);
         LOG_Report(0, "DYN", "Smart Sensor HTTP server exit request from browser: IGNORED");
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "exit");
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageExitHue
// Purpose:    Dynamically created system page to exit the HUE server
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       This call acts as a gracefull exit from the server to the Linux command shell
//
static bool http_DynPageExitHue(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebExitPage);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         //
         //PRINTF("http_DynPageExitHue():OKee" CRLF);
         LOG_Report(0, "DYN", "Exit HUE server request from browser");
         //
         // Request HOST exit
         //
         kill(GLOBAL_PidGet(PID_HOST), SIGINT);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "exithue");
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageLog
// Purpose:    Dynamically created system page to show the log file
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageLog(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         //
         HTTP_BuildGeneric(pstCl, pcWebPagePreStart);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_SendTextFile(pstCl, (char *)pcLogfile);
         HTTP_BuildGeneric(pstCl, pcWebPagePreEnd);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         //
         fCc = TRUE;
         //PRINTF("http-DynPageLog():OKee" CRLF);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "log");
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageMail
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageMail(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   PRINTF("http-DynPageMail()" CRLF);
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http-DynPageMail():No type !" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http-DynPageMail():Html" CRLF);
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("http-DynPageMail():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "mail");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_EML);
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageMotion
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageMotion(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   PRINTF("http-DynPageMotion()" CRLF);
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http-DynPageMotion():No type !" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http-DynPageMotion():Html" CRLF);
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("http-DynPageMotion():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "motion");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_MOT);
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageParms
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageParms(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   PRINTF("http_DynPageParms()" CRLF);
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http_DynPageParms():No type !" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http_DynPageParms():Html" CRLF);
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("http_DynPageParms():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "parms");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_ALL);
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageCsvFiles
// Purpose:    Dynamically created system page to send back all cvs files
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageCsvFiles(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   PRINTF("http-DynPageCsvFiles():" CRLF);
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         fCc = http_ShowFiles(pstCl, pcCsvValuesPath, pcCsvFilter, pcCsvHeader);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "csv");
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPagePids
// Purpose:    Dynamically created system page to show all threads
// 
// Params:     Client, DynPage index
// Returns:    TRUE
// Note:       Call as:
//             <url>/pids.html
//             <url>/pidsdef.html
//             <url>/pids.html?<thread-name>&term
//             <url>/pids.html?<thread-name>&kill
//
static bool http_DynPagePids(NETCL *pstCl, int iIdx)
{
   int      iCc=0;
   PIDT     ePid;
   bool     fFriendly=TRUE, fDoKill=FALSE;
   pid_t   *ptPid=NULL;
   char     cBuffer[32];
   char    *pcParm1;
   char    *pcParm2;

   // 
   // Put out the HTML header, start tag and the rest
   //         
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   //
   switch(GEN_GetDynamicPageUrl(iIdx))
   {
      default:
      case DYN_NONE_PIDS:
      case DYN_HTML_PIDS:
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         break;

      case DYN_HTML_PIDSR:
         HTTP_BuildGeneric(pstCl, pcWebPageStartRefresh, pcWebPageTitle, pcWebRefreshPids);
         break;
   }
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   HTTP_BuildGeneric(pstCl, pcWebPageDefault);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   HTTP_BuildTableStart(pstCl, "PiKrellMan Thread PIDs", 1, 70, 6);
   HTTP_BuildTableRowStart(pstCl, 25);
   HTTP_BuildTableColumnText(pstCl, "Thread Name", 10);
   HTTP_BuildTableColumnText(pstCl, "Thread Pid", 10);
   HTTP_BuildTableColumnText(pstCl, "Notify", 10);
   HTTP_BuildTableColumnText(pstCl, "Description", 10);
   HTTP_BuildTableColumnText(pstCl, "Guard Count", 10);
   HTTP_BuildTableColumnText(pstCl, "Time and Date", 20);
   HTTP_BuildTableRowEnd(pstCl);
   //
   for(ePid=0; ePid<NUM_PIDT; ePid++)
   {
      RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cBuffer, pstMap->G_stPidList[ePid].ulTs);
      http_BuildPidRow(pstCl, (int)pstMap->G_stPidList[ePid].tPid, 
                              pstMap->G_stPidList[ePid].iCount,
                              pstMap->G_stPidList[ePid].iNotify,
                              (char *)pstMap->G_stPidList[ePid].pcName, 
                              (char *)pstMap->G_stPidList[ePid].pcHelp, 
                              cBuffer);
   }
   pcParm1 = HTTP_GetParameter(pstCl, 1, cBuffer, 32);   //Pid-Name
   pcParm2 = HTTP_GetParameter(pstCl, 2, NULL, 0);       //Action
   //
   if(pcParm1 && pcParm2) 
   {
      PRINTF("http-DynPagePids(): Parm1=[%s] Parm2=[%s]" CRLF, pcParm1, pcParm2);
      //
      if(GEN_STRNCMPI(pcParm2, "kill", 4) == 0) { fDoKill=TRUE; fFriendly=FALSE; }
      if(GEN_STRNCMPI(pcParm2, "term", 4) == 0) { fDoKill=TRUE; fFriendly=TRUE;  }
      //
      if(fDoKill)
      {
         for(ePid=0; ePid<NUM_PIDT; ePid++)
         {
            if(GEN_STRNCMPI(pcParm1, pstMap->G_stPidList[ePid].pcName, GEN_STRLEN(pstMap->G_stPidList[ePid].pcName)) == 0)
            {
               ptPid = &(pstMap->G_stPidList[ePid].tPid);
               LOG_Report(0, "DYN", "http-DynPagePids():%s %s (%d)", pcParm2, pstMap->G_stPidList[ePid].pcName, *ptPid);
               break;
            }
         }
         if( ptPid && (*ptPid > 0))
         {
            iCc = GEN_KillProcess(*ptPid, fFriendly, 2000);
            if(iCc > 0) 
            {
               // Thread still running
               http_BuildPidRow(pstCl, *ptPid, 0, 0, cBuffer, "Thread exit ERROR", (char *)pcEmpty);
               LOG_Report(0, "DYN", "http-DynPagePids(): Friendly ask pid=%d to exit ERROR ", *ptPid);
               PRINTF("http-DynPagePids():%s:%s NOT OKee" CRLF, cBuffer, pcParm2);
            }
            else
            {
               // Thread gone or zombie
               LOG_Report(0, "DYN", "http-DynPagePids(): Friendly ask pid=%d to exit", *ptPid);
               http_BuildPidRow(pstCl, *ptPid, 0, 0, cBuffer, pcParm2, (char *)pcEmpty);
               *ptPid = 0;
               PRINTF("http-DynPagePids():%s:%s OKee" CRLF, cBuffer, pcParm2);
            }
         }
      }
   }
   //
   HTTP_BuildTableEnd(pstCl);
   HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   return(TRUE);
}

// 
// Function:   http_DynPageDateTime
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageDateTime(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   PRINTF("http-DynPageDateTime()" CRLF);
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http-DynPageDateTime():No type !" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http-DynPageDateTime():Html" CRLF);
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("http-DynPageDateTime():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "Date");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_DAT);
         break;
   }
   return(fCc);
}



/*------  Local functions separator ------------------------------------
_________________HTTP_FUNCTIONS(){};
------------------------------x----------------------------------------*/

// 
// Function    : http_BuildPidRow
// Description : Build a singe row with text
// 
// Parameters  : Client, pid, Count, Notify, Name, help, timestamp
// Returns     : 
// Note        :
//
static void http_BuildPidRow(NETCL *pstCl, int iPid, int iVal, int iNfy, char *pcText1, char *pcText2, char *pcText3)
{
   char pcValue[16];

   HTTP_BuildTableRowStart(pstCl, 15);
   // Text1=Name string
   HTTP_BuildTableColumnText(pstCl, pcText1, 10);
   // Pid
   GEN_SNPRINTF(pcValue, 15, "%d", iPid);
   HTTP_BuildTableColumnText(pstCl, pcValue, 10);
   // Notification
   GEN_SNPRINTF(pcValue, 15, "0x%X", iNfy);
   HTTP_BuildTableColumnText(pstCl, pcValue, 10);
   // Text2=Help string
   HTTP_BuildTableColumnText(pstCl, pcText2, 10);
   // Counter value
   GEN_SNPRINTF(pcValue, 15, "%d", iVal);
   HTTP_BuildTableColumnText(pstCl, pcValue, 10);
   // Text3=Timestamp 
   HTTP_BuildTableColumnText(pstCl, pcText3, 20);
   // 
   HTTP_BuildTableRowEnd(pstCl);
}

// 
// Function:   http_ExtractPageName
// Purpose:    Remove HTTP specific chars 
// 
// Parameters: Net client struct
// Returns:    Filename ptr
// Note:       
//
static char *http_ExtractPageName(NETCL *pstCl)
{
   char *pcPageName = NULL;

   //
   //   "GET /index.html HTTP/1.1"
   //   "Host: 10.0.0.231
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5
   //   "
   //   "Accept-Encoding: gzip,deflate
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
   //   "Keep-Alive: 115
   //   "Connection: keep-alive
   //   CR,CR,LF,CR,CR,LF
   //
   //    pcUrl
   //       iFileIdx : index to filename
   //       iExtnIdx : index to extension
   //
   PRINTF("http_ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pstCl->pcUrl);
   if(pstCl->iFileIdx != -1)
   {
      pcPageName = &pstCl->pcUrl[pstCl->iFileIdx];
      PRINTF("http_ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pcPageName);
   }
   return(pcPageName);
}

// 
// Function:   http_ShowFiles
// Purpose:    Show files in directory from disk
// 
// Parameters: Client, www dir name, filter, header
// Returns:    TRUE if OKee
// Note:       FINFO_GetDirEntries() are sorted on alphabetic order!
//
static bool http_ShowFiles(NETCL *pstCl, const char *pcDir, const char *pcFilter, const char *pcHeader)
{
   bool     fCc=FALSE;
   int      x, iNum=0;
   char     cForD;
   char    *pcFileName;
   char    *pcFilePath;
   char    *pcTemp;
   void    *pvData;

   pcFileName = (char *) safemalloc(MAX_PATH_LEN);
   pcFilePath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   PRINTF("http-ShowFiles(): Filter[%s]-Path=[%s]" CRLF, pcFilter, pcDir);
   //
   pvData = FINFO_GetDirEntries((char *)pcDir, (char *)pcFilter, &iNum);
   if(iNum > 0)
   {
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      // Build table
      HTTP_BuildTableStart(pstCl, pcHeader, 1, 70, 3);
      //
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         GEN_SPRINTF(pcFilePath, "%s%s", pcDir, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         PRINTF("http-ShowFiles(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFileName);
         if( cForD == 'F')
         {
            GEN_SPRINTF(pcTemp, "%s%s", pcDir, pcFileName);
            HTTP_BuildTableRowStart(pstCl, 6);
            HTTP_BuildTableColumnLink(pstCl, pcFileName, 25, pcTemp);
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 15);
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 30);
            HTTP_BuildTableRowEnd(pstCl);
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
      HTTP_BuildTableEnd(pstCl);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
      fCc = TRUE;
   }
   else
   {
      PRINTF("http-ShowFiles(): No file/dir entries" CRLF);
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      HTTP_BuildGeneric(pstCl, pcWebPageContentBad);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
   }
   safefree(pcFileName);
   safefree(pcFilePath);
   safefree(pcTemp);
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__________________COMMENTED_OUT(){};
------------------------------x----------------------------------------*/

#ifdef COMMENT

static bool    http_SendTextFile             (NETCL *, char *);

// 
// Function:   http_SendTextFile
// Purpose:    Send a textfile from fs to socket
// 
// Parameters: Client, filename
// Returns:    TRUE if OKee
// Note:       
//
static bool http_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = safemalloc(RCV_BUFFER_SIZE);
      //
      PRINTF("http-SendTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, RCV_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            //PRINTF("http-SendTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            //PRINTF("http-SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      //PRINTF("http-SendTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      safefree(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF("http-SendTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(fCc);
}

// 
// Function:   HTTP_SendTextFile
// Purpose:    Send a textfile from fs to socket
// 
// Parameters: Client, filename
// Returns:    TRUE if OKee
// Note:       
//
bool HTTP_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = safemalloc(RCV_BUFFER_SIZE);
      //
      PRINTF("HTTP-SendTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, RCV_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            //PRINTF("HTTP-SendTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            //PRINTF("HTTP-SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      //PRINTF("HTTP-SendTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      safefree(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF("HTTP-SendTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(fCc);
}


#endif   //COMMENT

