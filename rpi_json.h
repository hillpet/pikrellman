/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_json.h
 *  Purpose:            JSON functions header file
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    06 Nov 2021:      Ported from rpihue
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_JSON_H_
#define _RPI_JSON_H_

//
// Define JSON Element type
//
#define  JE_NONE        0x0000         // No specials
#define  JE_CURLB       0x0001         // { for multi-element begin
#define  JE_CURLE       0x0002         // }                   end
#define  JE_COMMA       0x0004         // , for multi=element Separator
#define  JE_COLN        0x0008         // : for multi=element Separator
#define  JE_TEXT        0x0010         // "Text"
#define  JE_SQRB        0x0020         // [ for multi-element begin
#define  JE_SQRE        0x0040         // ]                   end
#define  JE_CRLF        0x0080         // CRLF

//
// Global prototypes
//
bool     JSON_RespondObject         (NETCL *, RPIDATA *);
RPIDATA *JSON_TerminateObject       (RPIDATA *);
RPIDATA *JSON_InsertBool            (RPIDATA *, const char *, int, int);
RPIDATA *JSON_InsertFile            (RPIDATA *, const char *, char *, int);
RPIDATA *JSON_InsertInteger         (RPIDATA *, const char *, int, int);
RPIDATA *JSON_InsertParameter       (RPIDATA *, const char *, char *, int);
RPIDATA *JSON_InsertValue           (RPIDATA *, char *, int);
RPIDATA *JSON_TerminateArray        (RPIDATA *);
int      JSON_GetObjectSize         (RPIDATA *);
void     JSON_ReleaseObject         (RPIDATA *);

#endif   //_RPI_JSON_H_
