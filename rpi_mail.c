/*  (c) Copyright:  2021 Patrn, Confidential Data
 *
 *  Workfile:           rpi_mail.c
 *  Purpose:            Send mail functions
 *                      
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    18 Nov 2021:      Split off from rpi_motion.c
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    03 Dec 2021:      Append new logs to email body
 *    15 Dec 2021:      Add largest motion thumbnail attachment
 *    13 Mar 2023:      Add 24 hour WAN status in email
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_func.h"
#include "rpi_main.h"
#include "rpi_mailx.h"
#include "rpi_mail.h"
//
//#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static char   *rpi_GetArgGlobal           (const SARG *);
static char   *rpi_GetArgOption           (const SARG *);
static int     rpi_SortMotionList         (void);
static int     rpi_GetLargestMotion       (void);
//
// Mail Summary stuff
//
#ifdef   DEBUG_OUTPUT
static const char   *pcMailDebug       = "/mnt/rpicache/pikrellman/mail.out";
#endif   //DEBUG_OUTPUT
//
static const char   *pcMailBodyFile    = RPI_WORK_DIR "motionsummary.txt";
static const char   *pcNotFoundAttment = "/home/pi/proj/pikrellman/notfound.jpg";
static const char   *pcNoMotionAttment = "/home/pi/proj/pikrellman/nomotion.jpg";
static const char   *pcMailSubject     = "Summary Motion Detection from ";
static const char   *pcMailSepLine50   = "==================================================\n";
static const char   *pcMailTo          = "peter@patrn.nl";
static const char   *pcMailCc          = "peter.hillen@ziggo.nl";
static const char   *pcSubjWan         = " ** WAN CHANGED! **";
//
// msmtp mailx parameters
//
static const SARG stMailArguments[]    =
{
//    tArgs             pcArg                   pfGetArg
   {  0,                "mpack",                rpi_GetArgOption    },    // 
   {  0,                "-s",                   rpi_GetArgOption    },    // 
   {  PAR_MAILX_SUBJ,   NULL,                   rpi_GetArgGlobal    },    // 
   {  0,                "-d",                   rpi_GetArgOption    },    // 
   {  PAR_MAILX_BODY,   NULL,                   rpi_GetArgGlobal    },    // 
   {  PAR_MAILX_ATTM,   NULL,                   rpi_GetArgGlobal    },    // 

// #define  DEBUG_OUTPUT
   #ifdef   DEBUG_OUTPUT
   {  0,                "-o",                   rpi_GetArgOption    },    // 
   {  0,                pcMailDebug,            rpi_GetArgOption    },    // 
   #else    //DEBUG_OUTPUT
   {  PAR_MAILX_TO,     NULL,                   rpi_GetArgGlobal    },    // 
   {  PAR_MAILX_CC,     NULL,                   rpi_GetArgGlobal    },    //
   #endif   //DEBUG_OUTPUT
};
static const int iNumMailArguments = (sizeof(stMailArguments) / sizeof(SARG)) - 1;

//=============================================================================
// Global Data 
//=============================================================================
//
//    Motion data capture
//
//    PIKMOT pstMap->G_stMotion:
//    ---------------------------------------------
//    cMotionFile[MAX_PATH_LENZ]    : motion filename
//    iMotionEvents                 : bitfields
//    iFrame                        : frame index 0..?
//    iBlocksHor                    : frame hor ver
//    iBlocksVer                    : frame hor ver
//    iMagLimit                     : magnitude limit
//    iMagCount                     : magnitude count
//    iBurstCount                   : burst count
//    iBurstFrames                  : burst frames
//    stFrame                       : motion frame data regions
//
//    PIKFRM stFrame:
//    ---------------------------------------------
//    flSeconds                     : motion offset in secs
//    iBurst                        : Burst trigger
//    iAudio                        : Audio trigger
//    iExternal                     : External trigger
//    stCanvas                      ; Whole canvas frame vector data
//    stRegion[10]                  : Single frame region 0..9 vector data
//
//    PIKREG stRegion:
//    ---------------------------------------------
//    iX                            : X vector 
//    iY                            : Y vector
//    idX                           : dX
//    idY                           : dY
//    iMagnitude                    : Magnitude Sum
//    iCount                        : Count     Sum
//

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   RPI_CreateMailBody
// Purpose:    Create the Mailbody containing the summarize of todays events
// 
// Parameters: 
// Returns:     
// 
bool RPI_CreateMailBody()
{
   int      iX, iCsum, iXnum;
   u_int32  ulSec;
   char     cDandT[32];
   int64    llPos;
   FILE    *ptFile;

   //
   // Write new Logfile entries to mail body
   //
   llPos = pstMap->G_llLogFilePos;
   if( GEN_FileCopyRecords((char *)pcMailBodyFile, (char *)pcLogfile, FALSE, &llPos) )
   {
      pstMap->G_llLogFilePos = llPos;
   }
   //
   // Mail: Add summarize motion results from today:
   //
   // G_iArchiveAdd;                               // Added   to   archived today
   // G_iArchiveDel;                               // Removed from archived today
   // G_iRegionMagnitude   [MAX_MOTION_REGIONS];   // Region Sum Magnitude
   // G_iRegionCount       [MAX_MOTION_REGIONS];   // Region Sum Count
   //
   ptFile = safefopen((char *)pcMailBodyFile, "a+");
   if(ptFile)
   {
      RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cDandT, RTC_GetSystemSecs());
      //
      // Put out Motion summary data
      //
      GEN_FPRINTF(ptFile, "\n");
      GEN_FPRINTF(ptFile, pcMailSepLine50);
      //
      // Check if WAN changed since the last email
      //    WAN_NOT_FOUND     Not yet retrieved
      //    WAN_SAME          WAN retrieved and no change
      //    WAN_CHANGED       WAN retrieved and has changed
      //    WAN_ERROR         ERROR retrieving
      //
      if(pstMap->G_iWanAddr == WAN_NOT_FOUND)
      {
         GEN_FPRINTF(ptFile, "WAN Address Not yet detected!\n\n");
      }
      else 
      {
         if(pstMap->G_iWanAddr & WAN_SAME)
         {
            GEN_FPRINTF(ptFile, "WAN Address still %s\n\n", pstMap->G_pcWanAddr);
            pstMap->G_iWanAddr &= ~WAN_SAME;
         }
         if(pstMap->G_iWanAddr & WAN_CHANGED)
         {
            GEN_FPRINTF(ptFile, "WAN Address has changed to %s\n\n", pstMap->G_pcWanAddr);
            pstMap->G_iWanAddr &= ~WAN_CHANGED;
         }
         if(pstMap->G_iWanAddr & WAN_ERROR)
         {
            GEN_FPRINTF(ptFile, "WAN Address ERROR in detection, last IP was %s\n\n", pstMap->G_pcWanAddr);
            pstMap->G_iWanAddr &= ~WAN_ERROR;
         }
         if(pstMap->G_iWanAddr & WAN_NO_URL)
         {
            GEN_FPRINTF(ptFile, "NO WAN URL was set in persistent memory\n\n");
            pstMap->G_iWanAddr &= ~WAN_NO_URL;
         }
      }
      GEN_FPRINTF(ptFile, "SafeMalloc balance = %d\n", GLOBAL_GetMallocs());
      GEN_FPRINTF(ptFile, "Motion Summary on %s\n\n", cDandT);
      GEN_FPRINTF(ptFile, "Archives\n");
      GEN_FPRINTF(ptFile, "Added:\t\t%d files\n", pstMap->G_iArchiveAdd);
      GEN_FPRINTF(ptFile, "Removed:\t%d files\n", pstMap->G_iArchiveDel);
      GEN_FPRINTF(ptFile, "\n");
      //
      GEN_FPRINTF(ptFile, "Motionlist :\n");
      if( (iXnum = rpi_SortMotionList()) )
      {
         for(iX=0; iX<iXnum; iX++)
         {
            iCsum = pstMap->G_iMaxMotions[iX];
            ulSec = pstMap->G_ulMaxMotionsTs[iX];
            RTC_ConvertDateTime(TIME_FORMAT_HH_MM_SS, cDandT, ulSec);
            GEN_FPRINTF(ptFile, "\t%s\tCounts = %d\n", cDandT, iCsum);
         }
      }
      else 
      {
         GEN_FPRINTF(ptFile, "   Empty.\n");
      }
      GEN_FPRINTF(ptFile, pcMailSepLine50);
      safefclose(ptFile);
   }
   else
   {
      LOG_Report(errno, "EML", "RPI-CreateMailBody():Open ERROR:%s", pcMailBodyFile);
      PRINTF("RPI-CreateMailBody():Open ERROR:%s" CRLF, pcMailBodyFile);
   }
   return(TRUE);
}

// 
// Function:   RPI_SendMail
// Purpose:    Mail a summarize of todays events
// 
// Parameters:  
// Returns:     
// 
bool RPI_SendMail()
{
   bool     fCc=TRUE;
   int      iL;
   int      iX;
   u_int32  ulSec;
   char    *pcPar;
   char    *pcPix;
   char    *pcRpi;
   PIKTIM  *pstTs;

   //
   // Send mail: Summarize motion results from today:
   //
   RPI_CreateMailBody();
   //
   // PAR_MAILX_SUBJ
   // PAR_MAILX_BODY
   // PAR_MAILX_ATTM
   // PAR_MAILX_TO
   // PAR_MAILX_CC
   //
   // pcPar size is MAX_MAIL_LEN bytes. Make sure the subjectline fits 
   //
   pcPar = GLOBAL_GetParameter(PAR_MAILX_SUBJ);
   iL    = GLOBAL_GetParameterSize(PAR_MAILX_SUBJ);
   GEN_STRNCPY(pcPar, pcMailSubject, iL);
   iL   -= GEN_STRLEN(pcMailSubject);
   //
   // Calc remaining max space for FULL Hostname
   // pcRpi = "PatrnRpi12(Carport)"
   //
   pcRpi = GLOBAL_GetParameter(PAR_HOSTNAME);
   iL   -= GEN_STRLEN(pcRpi);
   GEN_STRNCAT(pcPar, pcRpi, iL);
   //
   // Check if WAN changed, if so add it to the subject line
   //
   if(pstMap->G_iWanAddr == WAN_CHANGED)
   {
      //
      // WAN_ERROR        -1: ERROR retrieving
      // WAN_NOT_FOUND     0: Not yet retrieved
      // WAN_SAME          1: WAN retrieved and no change
      // WAN_CHANGED       2: WAN retrieved and has changed
      //
      iL -= GEN_STRLEN(pcSubjWan);
      GEN_STRNCAT(pcPar, pcSubjWan, iL);
   }
   //
   // Attach thumb with largest motion of today, if any
   //
   pcPar = GLOBAL_GetParameter(PAR_MAILX_ATTM);
   iL    = GLOBAL_GetParameterSize(PAR_MAILX_ATTM);
   iX    = rpi_GetLargestMotion();
   ulSec = pstMap->G_ulMaxMotionsTs[iX];
   if(ulSec)
   {
      pstTs = (PIKTIM *)safemalloc(sizeof(PIKTIM));
      RPI_SetTimestampSecs(pstTs, ulSec);
      pcPix = RPI_AllocateArchivePath(pstTs, CT_EMAIL);
      if(pcPix)
      {
         if(RPI_FileExists(pcPix))
         {
            PRINTF("RPI-SendMail():Largest motion today is [%s]" CRLF, pcPix);
            LOG_Report(0, "EML", "RPI-SendMail():Largest motion today is [%s]", pcPix);
            GEN_STRNCPY(pcPar, pcPix, iL);
         }
         else
         {
            PRINTF("RPI-SendMail():Largest motion thumb [%s] does not exist" CRLF, pcPix);
            LOG_Report(0, "EML", "RPI-SendMail():Largest motion thumb [%s] does not exist", pcPix);
            GEN_STRCPY(pcPar, pcNotFoundAttment);
         }
         safefree(pcPix);
      }
      else
      {
         PRINTF("RPI-SendMail():Cannot allocate motion thumbpath" CRLF);
         LOG_Report(errno, "EML", "RPI-SendMail():Cannot allocate motion thumbpath ");
         GEN_STRCPY(pcPar, pcNoMotionAttment);
      }
      safefree(pstTs);
   }
   else
   {
      GEN_STRCPY(pcPar, pcNoMotionAttment);
   }
   if( (pcPar = GLOBAL_GetParameter(PAR_MAILX_BODY)) )   GEN_STRCPY(pcPar, pcMailBodyFile);
   if( (pcPar = GLOBAL_GetParameter(PAR_MAILX_TO))   )   GEN_STRCPY(pcPar, pcMailTo);
   if( (pcPar = GLOBAL_GetParameter(PAR_MAILX_CC))   )   GEN_STRCPY(pcPar, pcMailCc);
   //
   fCc = MAILX_SendMail(stMailArguments, iNumMailArguments);
   return(fCc);
}

//
// Function:   RPI_WaitMailxCompletion
// Purpose:    Wait for motion mailx completion
//             
// Parms:      Timeout secs
// Returns:    New absolute timeout secs or 0 if done.
// Note:       
// 
u_int32 RPI_WaitMailxCompletion(u_int32 ulSecsTimeout)
{
   int      iCc;
   u_int32  ulSecsNow;
   pid_t    tPid;

   if(ulSecsTimeout)
   {
      //
      // We are waiting for mailx completion
      //
      ulSecsNow = RTC_GetSystemSecs();
      if(ulSecsNow > ulSecsTimeout)
      {
         //
         // Mailx should be completed by now. If not, something went
         // wrong and we will terminate Mailx and report back.
         //
         tPid = GLOBAL_PidGet(PID_MAILX);
         if(tPid)
         {
            //
            // Check if the Mailx Helper thread is still running:
            //     0:   OKee, PID not found
            //    +1:   Hmmm, PID Still running
            //    -1:   OKee, PID Zombie or error
            //
            iCc = GEN_WaitProcessTimeout(tPid, 200);
            switch(iCc)
            {
               case 0:
                  GLOBAL_PidPut(PID_MAILX, 0);
                  break;

               case 1:
                  LOG_Report(0, "EML", "RPI-WaitMailxCompletion():Mailx still running: force exit");
                  PRINTF("RPI-WaitMailxCompletion():Mailx still running: force exit" CRLF);
                  //
                  GEN_KillProcess(tPid, DO_FRIENDLY, KILL_TIMEOUT_FORCED);
                  GEN_WaitProcess(tPid);
                  GLOBAL_PidPut(PID_MAILX, 0);
                  break;

               default:
                  PRINTF("RPI-WaitMailxCompletion():Mailx is zombie" CRLF);
                  GEN_WaitProcess(tPid);
                  break;
            }
         }
         ulSecsTimeout = 0;
      }
   }
   return(ulSecsTimeout);
}


/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/


/*------  Local functions separator ------------------------------------
__ARGS_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   rpi_GetArgGlobal
// Purpose:    Get Argument from the global list, insert value from Global parameter
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
//             pstArg-> PAR_xxxx
//                      "-opt"
//
static char *rpi_GetArgGlobal(const SARG *pstArg)
{
   int         iLenArg;
   const char *pcPar;
   char       *pcArg=NULL;

   pcPar = (char *)GLOBAL_GetParameter(pstArg->tArgs);
   if(pcPar)
   {
      iLenArg = GEN_STRLEN(pcPar);
      if(iLenArg)
      {
         pcArg = (char *) malloc(iLenArg+8);
         //
         // Copy args and vars to destination
         //
         GEN_STRCPY(pcArg, pcPar);
         PRINTF("rpi-GetArgGlobal():Arg=%s" CRLF, pcArg);
      }
   }
   return(pcArg);
}

//
// Function:   rpi_GetArgOption
// Purpose:    Get Argument: Option value
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char *rpi_GetArgOption(const SARG *pstArg)
{
   int         iLenArg;
   const char *pcOpt;
   char       *pcArg=NULL;

   pcOpt = pstArg->pcArg;
   if(pcOpt)
   {
      iLenArg = GEN_STRLEN(pcOpt);
      if(iLenArg)
      {
         pcArg = (char *) malloc(iLenArg+8);
         //
         // Copy args and vars to destination
         //
         GEN_STRCPY(pcArg, pcOpt);
         PRINTF("rpi-GetArgNull():Arg=%s" CRLF, pcArg);
      }
   }
   return(pcArg);
}

//
// Function:   rpi_GetLargestMotion
// Purpose:    Get the largest motion from the list
//             
// Parms:      
// Returns:    Index to largest
// Note:       
//
static int rpi_GetLargestMotion()
{
   int      iX, iXmax, iCmax;

   iXmax = 0;
   iCmax = pstMap->G_iMaxMotions[0];
   //
   PRINTF("rpi-GetLargestMotion()" CRLF);
   for(iX=1; iX<MAX_NUM_MOTIONS; iX++)
   {
      if( pstMap->G_iMaxMotions[iX] > iCmax)
      {
         iXmax = iX;
         iCmax = pstMap->G_iMaxMotions[iX];
      }
   }
   PRINTF("rpi-GetLargestMotion():Idx=%d, Cnt=%d" CRLF, iXmax, iCmax);
   return(iXmax);
}

//
// Function:   rpi_SortMotionList
// Purpose:    Sort the motion list on time
//             
// Parms:      
// Returns:    Numb in list
// Note:       
//
static int rpi_SortMotionList()
{
   bool     fSorting;
   int      iNum=0;
   int      iX, iY, iCnt;
   u_int32  ulSec;

   PRINTF("rpi-SortMotionList()" CRLF);
   for(iX=0; iX<MAX_NUM_MOTIONS; iX++)
   {
      PRINTF("rpi-SortMotionList():Unsorted Ts=%d  Cnt=%2d" CRLF, pstMap->G_ulMaxMotionsTs[iX], pstMap->G_iMaxMotions[iX]);
      //LOG_Report(0, "EML", "rpi-SortMotionList():Unsorted Ts=%d  Cnt=%2d", pstMap->G_ulMaxMotionsTs[iX], pstMap->G_iMaxMotions[iX]);
   }
   if(pstMap->G_ulMaxMotionsTs[0])
   {
      do
      {
         PRINTF(CRLF);
         PRINTF("rpi-SortMotionList():Sort H-->L ..." CRLF);
         fSorting = FALSE;
         for(iX=0, iY=1; iY<MAX_NUM_MOTIONS; iX++, iY++)
         {
            if(pstMap->G_ulMaxMotionsTs[iY])
            {
               if( pstMap->G_ulMaxMotionsTs[iX] > pstMap->G_ulMaxMotionsTs[iY])
               {
                  // Swap
                  ulSec = pstMap->G_ulMaxMotionsTs[iX];
                  pstMap->G_ulMaxMotionsTs[iX] = pstMap->G_ulMaxMotionsTs[iY];
                  pstMap->G_ulMaxMotionsTs[iY] = ulSec;
                  //
                  iCnt = pstMap->G_iMaxMotions[iX];
                  pstMap->G_iMaxMotions[iX] = pstMap->G_iMaxMotions[iY];
                  pstMap->G_iMaxMotions[iY] = iCnt;
                  //
                  PRINTF("rpi-SortMotionList():Swapped Ts %d and %d" CRLF, pstMap->G_ulMaxMotionsTs[iX], pstMap->G_ulMaxMotionsTs[iY]);
                  //LOG_Report(0, "EML", "rpi-SortMotionList():Swapped Ts %d and %d", pstMap->G_ulMaxMotionsTs[iX], pstMap->G_ulMaxMotionsTs[iY]);
                  fSorting = TRUE;
               }
            }
            else
            {
               // Rest of list is empty: if nothing was sorted, we are done. Else look again.
               PRINTF("rpi-SortMotionList():Restart: Sorted=%d" CRLF, fSorting);
               break;
            }
         }
      }
      while(fSorting == TRUE);
      iNum = iY;
   }
   //
   // Sorting is finished
   //
   //
   for(iX=0; iX<MAX_NUM_MOTIONS; iX++)
   {
      PRINTF("rpi-SortMotionList():Sorted Ts=%d  Cnt=%2d" CRLF, pstMap->G_ulMaxMotionsTs[iX], pstMap->G_iMaxMotions[iX]);
      //LOG_Report(0, "EML", "rpi-SortMotionList():Sorted Ts=%d  Cnt=%2d", pstMap->G_ulMaxMotionsTs[iX], pstMap->G_iMaxMotions[iX]);
   }
   LOG_Report(0, "EML", "rpi-SortMotionList():%d items", iNum);
   PRINTF("rpi-SortMotionList():%d items" CRLF, iNum);
   return(iNum);
}

/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT

#endif   //COMMENT
