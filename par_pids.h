/*  (c) Copyright:  2021  Patrn, Confidential Data
 *
 *  Workfile:           par_pids.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Extract headerfile for the RPI thread PIDs
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *    20 Aug 2021:      Add PID guard
 *    06 Nov 2021:      Ported from rpihue
 *    07 Nov 2021:      Add Semaphore
 *    10 Nov 2021:      Add HTTP Server
 *    13 Nov 2021:      Add Mailx client
 *    29 Nov 2021:      Add Ping
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          ePid           pcPid          pcHelp
EXTRACT_PID(PID_HOST,      "Host   ",     "Host")
EXTRACT_PID(PID_MOTION,    "Motion ",     "Motion detection")
EXTRACT_PID(PID_ARCHIVE,   "Archive",     "Archive loop recordings")
EXTRACT_PID(PID_GUARD,     "Guard  ",     "Guard")
EXTRACT_PID(PID_HUE,       "Hue    ",     "Hue Brigde")
EXTRACT_PID(PID_SRVR,      "Server ",     "HTTP Server")
EXTRACT_PID(PID_MAILX,     "Mailx  ",     "Mailx client")
EXTRACT_PID(PID_PING,      "Ping   ",     "Ping Service")

