/*  (c) Copyright:  2021..2024  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_hue.c
 *  Purpose:            HUE Bridge support
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    06 Nov 2021:      Ported from rpihue
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    20 Nov 2021:      Add Hue commands for Garden/Carport lightning
 *    18 Jan 2022:      Add Server Cmd handling
 *    20 Jan 2022:      Add bridge device driver code
 *    02 Feb 2022:      Add Hue motion trigger
 *    03 Feb 2022:      Add board specific functions
 *    28 Feb 2022:      Control Hue bridge for dawn/dusk
 *    08 Mar 2022:      Add Motion Warning
 *    01 Jun 2022:      Add PidList timestamps
 *    23 Nov 2022:      Check if all lights are OFF on DARKness
 *    11 Jan 2023:      LOG new state on verify OKee
 *    09 Dec 2024:      Add Xmas Outlet 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *       Connect to the Philips Hue Bridge:
 *       Get http://192.168.178.107/api/<Hue_User_ID>/lights (groups, scenes)
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_json.h"
#include "rpi_func.h"
//
#include "rpi_hue.h"

//#define USE_PRINTF
#include <printx.h>

//
// Prototypes Hue Command Callbacks
//
//
static bool hue_CommandIdle            (void);
static bool hue_CommandUpdateMotion    (void);
static bool hue_CommandUpdateLights    (void);
static bool hue_CommandUpdateGroups    (void);
static bool hue_CommandUpdateScenes    (void);
static bool hue_CommandLightsMax       (void);
static bool hue_CommandLightsDef       (void);
static bool hue_CommandLightsIrd       (void);
static bool hue_CommandWarningOff      (void);
//
static const HUECOM stHueCommandFunctions[] =
{
#define  EXTRACT_HC(a,b,c)           {b, c},
#include "hue_cmds.h"
#undef   EXTRACT_HC
   {     NULL, NULL  }
};

//
// Prototypes Hue State Callbacks
//
//
static bool hue_StateIdle                    (const HUEFUN *);
static bool hue_StateStartup                 (const HUEFUN *);
static bool hue_StateDawnWait                (const HUEFUN *);
static bool hue_StateDawnSet                 (const HUEFUN *);
static bool hue_StateDawnTimer               (const HUEFUN *);
static bool hue_StateDawnOff                 (const HUEFUN *);
static bool hue_StateDuskWait                (const HUEFUN *);
static bool hue_StateDuskSet                 (const HUEFUN *);
static bool hue_StateDuskTimer               (const HUEFUN *);
static bool hue_StateEveningSet              (const HUEFUN *);
static bool hue_StateDarkWait                (const HUEFUN *);
static bool hue_StateDarkSet                 (const HUEFUN *);
static bool hue_StateNewDay                  (const HUEFUN *);
static bool hue_StateVerify                  (const HUEFUN *);
static bool hue_StateWarnSet                 (const HUEFUN *);
static bool hue_StateWarnWait                (const HUEFUN *);
//
static const HUEFUN stHueStateFunctions[] =
{
#define  EXTRACT_HS(a,b,c,d)                 {b,c,d},
#include "hue_states.h"
#undef   EXTRACT_HS
   {     0, NULL, NULL  }
};
//
// Local prototypes
//
static int        hue_Execute                (void);
static bool       hue_HandleBridgeStates     (void);
static bool       hue_HandleMotionDetection  (void);
static bool       hue_HandleOutlet           (u_int32);
static void       hue_LoadSettings           (HUECFG *);
static bool       hue_ReadBridgeConfig       (char);
static void       hue_ReportNewState         (const HUEFUN *, u_int32);
static void       hue_RestoreOutlet          (bool);
static bool       hue_RunBridgeUpdate        (HUECFG *);
static bool       hue_RunCommand             (HUECMD);
static void       hue_SetDefaults            (void);
static void       hue_SetupNewDay            (void);
//
static bool       hue_Activation             (HUENAME, bool);
static int        hue_ArrayGetElementIndex   (char *, int);
static int        hue_ArrayNumElements       (char *);
static bool       hue_BridgeReadData         (char, char *, int);
static bool       hue_BridgeWriteData        (HUECFG *, RPIDATA *, char *, int);
static HUEDEV     hue_CheckDeviceStates      (void);
static int        hue_ExtractJsonAllElements (HUECFG *, char, char *);
static int        hue_ExtractJsonElement     (HUECFG *, char *, const HUEJS *, int, int);
static HUECFG    *hue_GetDeviceByName        (HUECFG *, char *);
static HUECFG    *hue_GetDeviceByDeviceId    (HUECFG *, char *);
static HUECFG    *hue_GetDeviceByIdt         (HUENAME);
static const char*hue_GetDeviceTypeString    (char);
static void      *hue_GetPersistentStorage   (HUECFG *, const HUEJS *, int);
static int        hue_SetupWarningTimer      (void);
static bool       hue_TriggerOutlet          (bool);
static bool       hue_UpdateBridgeDevices    (char);
static bool       hue_UpdateBridgeDevice     (HUECFG *);
static void       hue_UpdateConfigSettings   (HUECFG *);
//
static RPIDATA   *hue_DeviceDriverLights     (HUECFG *);
static RPIDATA   *hue_DeviceDriverGroups     (HUECFG *);
static RPIDATA   *hue_DeviceDriverScenes     (HUECFG *);
static int        hue_GenerateDeviceUrl      (HUECFG *, char *, int, RPIDATA *);
//
static int        hue_LogJsonObjects         (char *);
static void       hue_LogConfiguration       (char);
static void       hue_ListModConfigurations  (char);
static void       hue_ListAllConfigurations  (char);
static void       hue_ListDeviceConfig       (HUECFG *);
static bool       hue_SumConfiguration       (HUECFG *);
//
static void       hue_ReceiveSignalInt       (int);
static void       hue_ReceiveSignalPipe      (int);
static void       hue_ReceiveSignalSegmnt    (int);
static void       hue_ReceiveSignalTerm      (int);
static void       hue_ReceiveSignalUser1     (int);
static void       hue_ReceiveSignalUser2     (int);
static bool       hue_SignalRegister         (sigset_t *);

//
// Static data
//
static   bool        fHueRunning          = TRUE;
static   bool        fOutletOn            = FALSE;
static   bool        fOutletOff           = FALSE;
static   int         iRunMotionDetection  = 0;
static   int         iDarkDay             = 0;
static   int         iCurrDay             = 0;
//
static   int         iHueStateRetries     = 0;
static   int         iHueStateVerify      = 0;
static   HUENAME     tHueOldName          = HUE_DARK;
static   HUEST       tHueOldState         = HUE_STATE_STARTUP;
static   HUEST       tHueNewState         = HUE_STATE_STARTUP;
//
static   u_int32     ulAbsSecsMidnite     = 0;
static   u_int32     ulAbsSecsDawnSta     = 0;
static   u_int32     ulAbsSecsDawnSto     = 0;
static   u_int32     ulAbsSecsDuskSta     = 0;    
static   u_int32     ulAbsSecsDuskSto     = 0;
static   u_int32     ulAbsSecsDarkSta     = 0;    
static   u_int32     ulAbsSecsWarning     = 0;    
static   u_int32     ulAbsSecsOutletOn    = 0;
static   u_int32     ulAbsSecsOutletOff   = 0;
//
// Setup correct lights group name
//
#if   defined (BOARD_RPI12)
static const char *pcGroupName   = "CvB-Carport";
static const int   iSecsOffset   = 0;
#elif defined (BOARD_RPI14)
static const char *pcGroupName   = "CvB-Garden";
static const int   iSecsOffset   = 30;
#elif defined (BOARD_RPI10)
static const char *pcGroupName   = "CvB-Garden";
static const int   iSecsOffset   = 60;
#else
#error No Application board specified
static const char *pcGroupName   = "Peter-Office";
static const int   iSecsOffset   = 0;
#endif
//
// Default HTTP strings
// pcHttpReply is the main HTTP data buffer to send/receive HTTP GET/PUT/POST data
//
static char         *pcHttpReply          = NULL;
//
//    Default IP and user ID (if not available from persistent storage)
static const char   *pcHueDefaultIpAddr   = "192.168.178.107";
static const char   *pcHueDefaultIpPort   = "80";
static const char   *pcHueDefaultUserId   = "9nMma-HL3UVkMMnJKziCrEwthT2f39PdBS4iLjxb";
//
//    HTTP Bridge commands
static const char   *pcBridgeLights       = "lights";
static const char   *pcBridgeGroups       = "groups";
static const char   *pcBridgeScenes       = "scenes";
//
static const char   *pcError              = "error";
static const char   *pcDescription        = "description";
//
//    HTTP GET Commands (lights, groups, scenes)
//    GET /api/xx...xxxxx/<device>
//
//    Parms: <user-id>, <device>
//
static const char   *pcHttpGet            = "GET /api/%s/%s HTTP/1.1" CRLF
                                            "Host: cvb.patrn.nl" CRLF
                                            "Keep-Alive: 115" CRLF
                                            "Connection: keep-alive" CRLF CRLF;
//
//    HTTP PUT Commands Lights
//    PUT /api/xx...xxxxx/lights/<id>/state
//    
//		{"on":true,"hue":20000}
//
//    Parms: <user-id>, <dev-id>, content length, json body
//
static const char   *pcHttpPutLights      = "PUT /api/%s/lights/%s/state HTTP/1.1" CRLF
                                            "Host: cvb.patrn.nl" CRLF
//                                          "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0" CRLF
                                            "Accept: */*" CRLF
                                            "Accept-Language: en-US,en;q=0.5" CRLF
                                            "Accept-Encoding: gzip, deflate" CRLF
                                            "Content-Type: text/plain;charset=UTF-8" CRLF
                                            "Content-Length: %d" CRLF
//                                          "Origin: http://192.168.178.110" CRLF
                                            "Connection: keep-alive" CRLF CRLF
                                            "%s" CRLF;
//
//    HTTP PUT Commands Groups
//    PUT /api/xx...xxxxx/groups/<id>/action
//    
//		{"on":true,"hue":20000}
//
//    Parms: <user-id>, <dev-id>, content length, json body
//
static const char   *pcHttpPutGroups      = "PUT /api/%s/groups/%s/action HTTP/1.1" CRLF
                                            "Host: cvb.patrn.nl" CRLF
//                                          "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0" CRLF
                                            "Accept: */*" CRLF
                                            "Accept-Language: en-US,en;q=0.5" CRLF
                                            "Accept-Encoding: gzip, deflate" CRLF
                                            "Content-Type: text/plain;charset=UTF-8" CRLF
                                            "Content-Length: %d" CRLF
//                                          "Origin: http://192.168.178.107" CRLF
                                            "Connection: keep-alive" CRLF CRLF
                                            "%s" CRLF;
//
//    HTTP PUT Commands Scenes
//    PUT /api/xx...xxxxx/groups/0/action
//    
//		{"scene":"xx...xxxxx"}
//
//    Parms: <user-id>, content length, json body
//
static const char   *pcHttpPutScenes      = "PUT /api/%s/groups/0/action HTTP/1.1" CRLF
                                            "Host: cvb.patrn.nl" CRLF
//                                          "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0" CRLF
                                            "Accept: */*" CRLF
                                            "Accept-Language: en-US,en;q=0.5" CRLF
                                            "Accept-Encoding: gzip, deflate" CRLF
                                            "Content-Type: text/plain;charset=UTF-8" CRLF
                                            "Content-Length: %d" CRLF
//                                          "Origin: http://192.168.178.107" CRLF
                                            "Connection: keep-alive" CRLF CRLF
                                            "%s" CRLF;

//
// HUE Bridge JSON translator
//
static const HUEJS stHueJsonId            = { {NULL, NULL, NULL, NULL, NULL}, offsetof(HUECFG, cDevId), HUE_DATA_LENZ};
static const HUEJS stHueJsonParameters[]  =
{
#define  EXTRACT_HUE(a,b,c,d,e,f,g,h)    {{b,c,d,e,f},g,h},
#include "par_hue.h"   
#undef   EXTRACT_HUE
};
static const int iNumHueJsonParameters = sizeof(stHueJsonParameters)/sizeof(HUEJS);

//
// Bridge light/group/scene names
//
static const HUEID stHueBridgeNames[] =
{
#define  EXTRACT_NAME(a,b,c,d)            {b,c,d},
#include "par_names.h"   
#undef   EXTRACT_NAME
};

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//  
// Function:   HUE_Init
// Purpose:    Init the HUE specific data
// 
// Parameters: 
// Returns:    Startup code GLOBAL_XXX_INI
// Note:       
//  
int HUE_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_HUE, tPid);
         GLOBAL_PidSetInit(PID_HUE, HUE_Init);
         break;

      case 0:
         // Child: RPI HUE-server
         GEN_Sleep(500);
         iCc = hue_Execute();
         GLOBAL_PidPut(PID_HUE, 0);
         LOG_Report(0, "HUE", "HUE-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("hue-Init(): Error!" CRLF);
         LOG_Report(errno, "HUE", "HUE-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_HUE_INI);
}

//
// Function:   HUE_GetDeviceConfig
// Purpose:    Determine which devicetype we are dealing with (Light, Group, Scene)
//
// Parms:      Hue device type (l,g,s)
// Returns:    HUECFG ptr
// Note:       
//
HUECFG *HUE_GetDeviceConfig(char cDevType)
{
   HUECFG  *pstHueCfg;
   
   switch(cDevType)
   {
      default:
      case HUE_DEVICE_LIGHT:
         pstHueCfg = pstMap->G_stHueL;
         break;

      case HUE_DEVICE_GROUP:
         pstHueCfg = pstMap->G_stHueG;
         break;

      case HUE_DEVICE_SCENE:
         pstHueCfg = pstMap->G_stHueS;
         break;
   }
   return(pstHueCfg);
}

//
//  Function:   HUE_GetDeviceStructure
//  Purpose:    Get the correct Light/Group or Scene struct
//
//  Parms:     Hue device string (f.i. "G12")
//  Returns:   Struct or NULL
//
HUECFG *HUE_GetDeviceStructure(char *pcDev)
{
   char     cDevType;
   int      iDevice;
   HUECFG  *pstHue=NULL;
   HUECFG  *pstHueCfg;

   PRINTF("hue-GetDeviceStructure():Device [%s]" CRLF, pcDev);
   //
   if(GEN_STRLEN(pcDev) == 0) return(NULL);
   //
   cDevType = *pcDev++;
   iDevice  = (int)strtol(pcDev, NULL, 0);
   //
   if(iDevice > 0)
   {
      iDevice--;
      if(iDevice < MAX_HUE_DEVICES)
      {
         //
         // OK: Store Hue device 
         //
         pstHueCfg = HUE_GetDeviceConfig(cDevType);
         pstHue    = &pstHueCfg[iDevice];
      }
   }
   return(pstHue);
}

//
//  Function:   HUE_StoreDeviceName
//  Purpose:    Copy the correct name into the appropriate Light/Group or Scene struct
//
//  Parms:     Hue device (n,l,g,s), Hue Device number (1..?), Storage location, Storage length
//  Returns:   CC
//  Note:      
//
bool HUE_StoreDeviceName(char cDevType, int iDevice, char *pcName, int iLen)
{
   bool     fCc=FALSE;
   HUECFG  *pstHue;
   HUECFG  *pstHueCfg;

   PRINTF("hue-StoreDeviceName():Device %c%d..." CRLF, cDevType, iDevice);
   if(iDevice > 0)
   {
      iDevice--;
      if(iDevice < MAX_HUE_DEVICES)
      {
         //
         // OK: Store Hue device 
         //
         pstHueCfg = HUE_GetDeviceConfig(cDevType);
         pstHue    = &pstHueCfg[iDevice];
         //
         GEN_STRNCPY(pcName, pstHue->cName, iLen);
         PRINTF("hue-StoreDeviceName():.... is [%s]" CRLF, pcName);
         fCc = TRUE;
      }
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
//  Function:   hue_Execute
//  Purpose:    Run the HUE Bridge thread as a daemon
//
//  Parms:
//  Returns:    CC
//
static int hue_Execute(void)
{
   sigset_t tBlockset;
   int      iOpt, iTimeout=HUE_TIMEOUT_IDLE;
   u_int32  ulSecs;

   GLOBAL_SemaphoreInit(PID_HUE);
   if(hue_SignalRegister(&tBlockset) == FALSE) 
   {
      LOG_Report(errno, "HUE", "hue-Execute():Exit fork ERROR:");
      exit(EXIT_CC_GEN_ERROR);
   }
   pcHttpReply = safemalloc(MAX_HUE_HTTP_REPLY);
   //
   // Setup default Hue Bridge values (if not in persistent memory)
   //
   hue_SetDefaults();
   //
   // Init random generator (for Hue Bridge collisions)
   //
   // srandom(RTC_GetSystemSecs());
   //
   // Init ready: await further actions
   //
   GLOBAL_PidTimestamp(PID_HUE, 0);
   GLOBAL_PidSaveGuard(PID_HUE, GLOBAL_HUE_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_HUE_INI);
   //
   // Wait until triggered by:
   //    o SIGINT:   Terminate normally
   //    o SIGTERM:  Terminate normally
   //    o SIGSEGV:  Segmentation fault
   //    o SIGUSR1:  Run command
   //    o SIGUSR2:  
   //
   while(fHueRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_HUE, iTimeout);
      switch(iOpt)
      {
         case 0:
            GLOBAL_PidTimestamp(PID_HUE, 0);
            //
            // Check if HOST requires Hue Bridge configuration acquisition
            //
            if(GLOBAL_GetSignalNotification(PID_HUE, GLOBAL_HST_HUE_CFG)) 
            {
               PRINTF("hue-Execute():HOST request to load config" CRLF);
               LOG_Report(0, "HUE", "hue-Execute():HOST request to load config");
               hue_ReadBridgeConfig(HUE_DEVICE_LIGHT);
               hue_ReadBridgeConfig(HUE_DEVICE_GROUP);
               hue_ReadBridgeConfig(HUE_DEVICE_SCENE);
               GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_HUE_HST_NFY);
               LOG_Report(0, "HUE", "hue-Execute():Config reloaded");
            }
            //
            // Check if we need Hue Bridge configuration acquisition
            //
            if(GLOBAL_GetSignalNotification(PID_HUE, GLOBAL_HUE_HUE_CFG)) 
            {
               PRINTF("hue-Execute():Local reload config..." CRLF);
               LOG_Report(0, "HUE", "hue-Execute():Reload config...");
               hue_ReadBridgeConfig(HUE_DEVICE_LIGHT);
               hue_ReadBridgeConfig(HUE_DEVICE_GROUP);
               hue_ReadBridgeConfig(HUE_DEVICE_SCENE);
               GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_HUE_HST_NFY);
               PRINTF("hue-Execute():Local config reloaded" CRLF);
               LOG_Report(0, "HUE", "hue-Execute():Config reloaded");
            }
            //
            // Check if Motion Detection requires action
            //
            if(GLOBAL_GetSignalNotification(PID_HUE, GLOBAL_MOT_HUE_RUN)) 
            {
               if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-Execute():Received RUN from Motion thread" CRLF);
               PRINTF("hue-Execute():Received RUN from Motion thread" CRLF);
               //
               hue_RunCommand((HUECMD)pstMap->G_iHueCmd);
               //
               // Notify Motion thread
               //
               GLOBAL_SetSignalNotification(PID_MOTION, GLOBAL_HUE_MOT_NFY);
            }
            //
            // Check if HTTP Web Server requires action
            //
            if(GLOBAL_GetSignalNotification(PID_HUE, GLOBAL_SVR_HUE_RUN)) 
            {
               HUECFG  *pstHue;

               PRINTF("hue-Execute():Received RUN from HTTP Web Server" CRLF);
               //
               pstHue = HUE_GetDeviceStructure(pstMap->G_pcHueDevice);
               //
               hue_LoadSettings(pstHue);
               hue_UpdateBridgeDevice(pstHue);
            }
            //
            // Check if HTTP Web Server requires D&T action
            //
            if(GLOBAL_GetSignalNotification(PID_HUE, GLOBAL_SVR_HUE_DAT)) 
            {
               PRINTF("hue-Execute():Received TMR update from HTTP Web Server" CRLF);
               hue_SetupNewDay();
               hue_StateStartup(NULL);

            }
            //
            // Check if HOST reports Midnight
            //
            if(GLOBAL_GetSignalNotification(PID_HUE, GLOBAL_HST_ALL_MID)) 
            {
               ulSecs   = RTC_GetSystemSecs();
               iCurrDay = RTC_GetDay(ulSecs);
               ulAbsSecsMidnite = RTC_GetMidnight(ulSecs);
               LOG_Report(0, "HUE", "hue-Execute():Midnight: New day=%d", iCurrDay);
               PRINTF("hue-Execute():Received MID from host thread" CRLF);
            }
            break;

         case 1:
            //
            // Timeout: 
            // - Handle Hue Motion detection
            // - Handle Hue Bridge control state handler 
            //
            if(hue_HandleMotionDetection()) iTimeout = HUE_TIMEOUT_ACTIVE;
            else                            iTimeout = HUE_TIMEOUT_IDLE;
            //
            // Run the normal Hue Bridge control state handler
            //
            if(!hue_HandleBridgeStates()) 
            {
               LOG_Report(0, "HUE", "hue-Execute():Handle Bridge ERROR");
               PRINTF("hue-Execute():Handle Bridge ERROR" CRLF);
            }
            break;

         default:
         case -1:
            LOG_Report(errno, "HUE", "hue-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("hue-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   GLOBAL_SemaphoreDelete(PID_HUE);
   safefree(pcHttpReply);
   return(EXIT_CC_OKEE);
}

//
// Function:   hue_HandleBridgeStates
// Purpose:    Handle the Hue bridge to activate scenes
//             
// Parms:      
// Returns:    TRUE if Bridge comms OKee
// Note:       Called every 10 seconds
//
static bool hue_HandleBridgeStates()
{
   bool           fCc=FALSE;
   const HUEFUN  *pstState;
   HUEFPTR        pfHueState;

   if(tHueNewState < NUM_HUE_STATES)
   {
      pstState   = &stHueStateFunctions[tHueNewState];
      pfHueState = pstState->pfHueFun;
      if(pfHueState)
      {
         //
         // We have a valid Hue state handler: run it
         //
         PRINTF("hue-HandleBridgeStates():%s" CRLF, pstState->pcHelp);
         fCc = pfHueState(pstState);
      }
   }
   return(fCc);
}

//
// Function:   hue_HandleOutlet
// Purpose:    Handle the Hue bridge on (XMas) outlet control 
//             
// Parms:      Secs now
// Returns:    TRUE if OK
// Note:       Control outlet as light HUE_OUTLET_1
//             G_iSecsOutletOn  : Turn Outlet ON  xx secs after midnight. 0=Disabled.
//             G_iSecsOutletOff : Turn Outlet OFF xx secs after midnight. 0=Turn OFF at DARK_OFF.
//
static bool hue_HandleOutlet(u_int32 ulSecs)
{
   bool  fCc=TRUE;

   if(ulAbsSecsOutletOn)  
   {
      //
      // We have Outlet setup active
      //                                                                                      DARK  
      //                          DAWN      DAWN                  DUSK    EVENING               | 
      //   00:00                   ON       OFF                    ON       ON                  |  24:00
      // <---+-----X---------------+----x----+---------------------+---------+----------x-------+----+------------>
      //           |                    |                                               |
      //        ulSecs--->              |<---------------Outlet On--------------------->|
      //                       ulAbsSecsOutletOn                               ulAbsSecsOutletOff
      //
      if(ulSecs > ulAbsSecsOutletOn)
      {
         //
         // Time to activate outlet
         //
         if(fOutletOn)
         {
            //
            // Already ON: check OFF
            // 
            if(ulSecs > ulAbsSecsOutletOff)
            {
               if(!fOutletOff)
               {
                  //
                  // Not yet OFF: Trigger OFF
                  //
                  if(RPI_GetFlag(FLAGS_HUE_OUTLET)) LOG_Report(0, "HUE", "hue-HandleOutlet():Outlet OFF");
                  hue_TriggerOutlet(FALSE);
                  fOutletOff = TRUE;
               }
            }
         }
         else
         {
            //
            // Not yet ON: trigger Outlet ON
            //
            if(RPI_GetFlag(FLAGS_HUE_OUTLET)) LOG_Report(0, "HUE", "hue-HandleOutlet():Outlet ON");
            hue_TriggerOutlet(TRUE);
            fOutletOn = TRUE;
         }
      }
   }
   return(fCc); 
}

//
// Function:   hue_HandleMotionDetection
// Purpose:    Handle the Hue bridge on reported motion detection
//             Called every second if motion detection is active, else every 10 Secs
// Parms:      
// Returns:    TRUE if there is still motion ongoing
// Note:       <iRunMotionDetection> has been incremented through a signal from the MOTION thread.
//             If all Hue bridge action has completed here, <iRunMotionDetection> will be 
//             cleared here to signal the end of Hue motion detection.
//
static bool hue_HandleMotionDetection()
{
   bool  fCc=TRUE;

   switch(iRunMotionDetection)
   {
      case 0:
         // No motion detection is ongoing
         fCc = FALSE;
         break;

      case 1:
         //
         // Initial motion detected: set Hue State Handler to activate warning lights
         // Check if warning lightning is turned OFF for this part of the day
         //
         if(hue_SetupWarningTimer())
         {
            if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-HandleMotionDetection():%d" CRLF, iRunMotionDetection);
            iRunMotionDetection++;
            tHueNewState = HUE_STATE_WARN_SET;
         }
         break;

      case 2:
         //
         // Motion detected: Hue State Handler has been set to activate warning lights
         // Check if warning lightning is turned OFF for this part of the day
         //
         if(hue_SetupWarningTimer())
         {
            if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-HandleMotionDetection():%d" CRLF, iRunMotionDetection);
         }
         break;

      default:
         //
         // Multiple motion detections: retrigger warning lights
         // Check if warning lightning is turned OFF for this part of the day
         //
         if(hue_SetupWarningTimer())
         {
            if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-HandleMotionDetection():%d" CRLF, iRunMotionDetection);
         }
         break;
   }
   return(fCc);
}

//
// Function:   hue_LoadSettings
// Purpose:    Load the Bri/Hue/Sat settings from persistent memory
//
// Parms:      HUECFG struct
// Returns:   
// Note:       Called to handle the bridge through the HTTP socket
//
static void hue_LoadSettings(HUECFG *pstHue)
{
   if(pstHue)
   {
      if(pstMap->G_iHueBri > 0)
      {
         pstHue->fOn = 1;
         pstHue->iBri = pstMap->G_iHueBri;
         pstHue->iHue = pstMap->G_iHueHue;
         pstHue->iSat = pstMap->G_iHueSat;
         pstHue->iCt  = pstMap->G_iHueCt;
         PRINTF("hue-LoadSettings():ON:B=%d, H=%d, S=%d, C=%d" CRLF, pstHue->iBri, pstHue->iHue, pstHue->iCt);
      }
      else
      {
         pstHue->fOn = 0;
         PRINTF("hue-LoadSettings():OFF" CRLF);
      }
      pstHue->fChanged = 1;
   }
   else PRINTF("hue-LoadSettings():Device not found" CRLF);
}

//
// Function:   hue_ReadBridgeConfig
// Purpose:    Read the HUE Bridge data through HTTP GET
//
// Parms:      Device type (Lights, Groups, ...)
// Returns:    TRUE if OKee
//  Note:      
//
static bool hue_ReadBridgeConfig(char cDevType)
{
   bool        fCc=FALSE;
   int         iMaxLevel, iNr;
   char       *pcHueJson;
   HUECFG     *pstHueCfg;

   PRINTF("hue-ReadBridgeConfig() %c" CRLF, cDevType);
   pstHueCfg = HUE_GetDeviceConfig(cDevType);
   GEN_MEMSET(pstHueCfg, 0x00, MAX_HUE_DEVICES * sizeof(HUECFG));
   //
   // Read Current HUE Bridge configuration
   //
   if( hue_BridgeReadData(cDevType, pcHttpReply, MAX_HUE_HTTP_REPLY) )
   {
      //PRINTF("%s" CRLF, pcHttpReply);
      if( (pcHueJson = GEN_JsonFindObject(pcHttpReply)) )
      {
         //
         // pcHueJson-> { 
         //                "1":
         //                {
         //                   "state":
         //                   {
         //                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
         //                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
         //                      "mode":"homeautomation","reachable":true
         //                   },
         //                   "swupdate":
         //                   ......
         //                }
         //             }
         //  
         //PRINTF("%s" CRLF, pcHueJson);
         if(GEN_JsonCheckObject(pcHueJson, &iMaxLevel))
         {
            //PRINTF("hue-ReadBridgeConfig(%c):GEN_JsonCheckObject():Object max level=%d" CRLF, cDevType, iMaxLevel);
            if(RPI_GetFlag(FLAGS_LOG_JSON)) hue_LogJsonObjects(pcHueJson);
            //
            // Store specific elements in persistent storage
            //
            iNr = hue_ExtractJsonAllElements(pstHueCfg, cDevType, pcHueJson);
            hue_UpdateConfigSettings(pstHueCfg);
            PRINTF("hue-ReadBridgeConfig(%c):%d elements in persistent storage" CRLF, cDevType, iNr);
            if(RPI_GetFlag(FLAGS_PRN_HUEALL)) hue_ListAllConfigurations(cDevType);
            if(RPI_GetFlag(FLAGS_PRN_HUEMOD)) hue_ListModConfigurations(cDevType);
            if(RPI_GetFlag(FLAGS_LOG_HUECFG)) hue_LogConfiguration(cDevType);
            fCc = TRUE;
         }
         else
         {
            PRINTF("hue-ReadBridgeConfig(%c):ERRORs in JSON object!" CRLF, cDevType);
            LOG_Report(0, "HUE", "hue-ReadBridgeConfig(%c):ERRORs in JSON object!", cDevType);
         }
      }
      else  
      {
         PRINTF("hue-ReadBridgeConfig(%c):ERROR, No JSON object found !" CRLF, cDevType);
         LOG_Report(0, "HUE", "hue-ReadBridgeConfig(%c):ERROR, No JSON object found !", cDevType);
      }
   }
   else
   {
      //
      // No Bridge config read: exit
      //
      LOG_Report(errno, "HUE", "hue-ReadBridgeConfig(%c):ERROR reading data", cDevType);
   }
   return(fCc);
}

//
// Function:   hue_ReportNewState
// Purpose:    Log the new state and D&T
//
// Parms:      Hue state, T&D Secs next action
// Returns:    
// Note:       
//             
//
static void hue_ReportNewState(const HUEFUN *pstState, u_int32 ulSecs)
{
   char  cDateTime[32];

   //
   // Next state:
   // Report state and T&D
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulSecs);
   LOG_Report(0, "HUE", "hue-ReportNewState:Hue Bridge state %s: await %s", pstState->pcHelp, cDateTime);
   iHueStateVerify = 0;
}

//
// Function:   hue_RestoreOutlet
// Purpose:    Restore the Hue bridge on (XMas) outlet control 
//             
// Parms:      Group/Scene new state (On or Off)
// Returns:    
// Note:       Verify correct state after global Group/Scene updates)
//
static void hue_RestoreOutlet(bool fOn)
{
   if(RPI_GetFlag(FLAGS_HUE_OUTLET)) LOG_Report(0, "HUE", "hue-RestoreOutlet():Was: ON=%d, OFF=%d, Scene=%d", fOutletOn, fOutletOff, fOn);
   //
   // We have Outlet setup active
   //                                                                                  
   //                          DAWN      DAWN                  DUSK     EVENING            DARK       
   //   00:00                   ON       OFF                    ON        ON                OFF   24:00
   // <---+-----X---------------+----x----+---------------------+---------+----------x-------+------+------------>
   //           |                    |                                               |
   //        ulSecs--->              |<---------------Outlet On--------------------->|
   //                       ulAbsSecsOutletOn                               ulAbsSecsOutletOff
   //
   // If the Outlet setup has been activated, put the outlet back in its correct state
   //
   //    fOutletOn   fOutletOff  fOn   Action         Comment
   //    ----------------------------------------------------------------------
   //       0            0        0     ---
   //       0            0        1     ---
   //       1            0        0    Outlet-->ON    Trigger OutletOn again
   //       1            0        1     ---
   //       1            1        0     ---
   //       1            1        1    Outlet-->OFF   Trigger OutletOff again
   //
   if(     fOutletOn && !fOutletOff && !fOn)  fOutletOn  = FALSE;
   else if(fOutletOn &&  fOutletOff &&  fOn)  fOutletOff = FALSE;
   //
   if(RPI_GetFlag(FLAGS_HUE_OUTLET)) LOG_Report(0, "HUE", "hue-RestoreOutlet():Now: ON=%d, OFF=%d, Scene=%d", fOutletOn, fOutletOff, fOn);
}

//
// Function:   hue_RunBridgeUpdate
// Purpose:    Update the HUE Bridge data through HTTP PUT/POST
//
// Parms:      Local device Hue Config
// Returns:    TRUE if OKee
// Note:       Hue device data are -1 if not to be updated
//             
//
static bool hue_RunBridgeUpdate(HUECFG *pstHue)
{
   bool        fCc=FALSE;
   RPIDATA    *pstObj=NULL;

   //
   // Build the JSON object which will instruct the Hue Bridge what to do
   //
   switch(*pstHue->pcDevType)
   {
      default:
      case HUE_DEVICE_LIGHT:
         pstObj = hue_DeviceDriverLights(pstHue);
         break;

      case HUE_DEVICE_GROUP:
         pstObj = hue_DeviceDriverGroups(pstHue);
         break;

      case HUE_DEVICE_SCENE:
         pstObj = hue_DeviceDriverScenes(pstHue);
         break;
   }
   if(pstObj)
   {
      //
      // We have payload data for the Hue Bridge
      //
      LOG_Report(0, "HUE", "hue-RunBridgeUpdate():Update Device[%s]=[%s]", pstHue->cDevId, pstHue->cName);
      if( hue_BridgeWriteData(pstHue, pstObj, pcHttpReply, MAX_HUE_HTTP_REPLY) ) 
      {
         char *pcHueJson;
         char *pcErr;

         //
         // OKee: check reply 
         // Reply should be like:
         //    {"success":{"/lights/3/state/bri":123}},{"success":{"/lights/3/state/on":false}}
         //    {"error": {"type": 2,"address": "/lights/3/state","description": "body contains invalid json"}}
         //
         if( (pcHueJson = GEN_JsonFindObject(pcHttpReply)) )
         {
            fCc = TRUE;
            //PRINTF("hue-RunBridgeUpdate():Reply [%s]" CRLF, pcHueJson);
            if( (pcErr = GEN_STRSTRI(pcHueJson, pcError)))
            {
               char  cBuffer[32];

               //
               // Some of the replies report error, but maybe the device is incapable of 
               // supporting the Bri etc commands.
               //
               if(GEN_JsonGetStringCopy(pcHueJson, pcDescription, cBuffer, 31))
               {
                  LOG_Report(0, "HUE", "hue-RunBridgeUpdate():ERROR:%s", cBuffer);
               }
            }
         }
         else
         {
            LOG_Report(0, "HUE", "hue-RunBridgeUpdate():ERROR: NO JSON data found in reply:%s", pcHttpReply);
         }
      }
      else
      {
         LOG_Report(errno, "HUE", "hue-RunBridgeUpdate():ERROR writing data.");
      }
      //
      // Release JSON Object data
      //
      JSON_ReleaseObject(pstObj);
   }
   return(fCc);
}

//
//  Function:   hue_RunCommand
//  Purpose:    Run a HUE Bridge command
//
//  Parms:      HUECMD 
//  Returns:    TRUE if okee
//
static bool hue_RunCommand(HUECMD tCmd)
{
   bool     fCc=FALSE;
   HUECPTR  pfHueCommand;

   if(tCmd < NUM_HUE_CMDS)
   {
      pfHueCommand = stHueCommandFunctions[tCmd].pfHueFun;
      if(pfHueCommand)
      {
         //
         // We have a valid Hue command: runit
         //
         fCc = pfHueCommand();
      }
   }
   return(fCc);
}

//
//  Function:   hue_SetDefaults
//  Purpose:    Set/Check HUE Bridge parameters
//              Only called once at startup of theApp.
//  Parms:
//  Returns:    
//
static void hue_SetDefaults(void)
{
   char    *pcVar;
   int      iMax;
   u_int32  ulSecs;

   //
   // Setup proper Dawn/Dusk settings
   //    Dawn : 07:00
   //    Dusk : 19:00
   //
   if(pstMap->G_iMinuteDawn == 0) pstMap->G_iMinuteDawn = ( 7 * 60);
   if(pstMap->G_iMinuteDusk == 0) pstMap->G_iMinuteDusk = (19 * 60);
   //
   // Setup all system-secs-timers for dawn, dusk and dark
   //
   ulSecs           = RTC_GetSystemSecs();
   ulAbsSecsMidnite = RTC_GetMidnight(ulSecs);
   iCurrDay         = RTC_GetDay(ulSecs);
   iDarkDay         = iCurrDay;
   //
   hue_SetupNewDay();
   //
   // Setup default Hue Bridge IP Address, if not available yet
   //
   pcVar = GLOBAL_GetParameter(PAR_HUE_IP);
   if(GEN_STRLEN(pcVar) == 0)
   {
      iMax = GLOBAL_GetParameterSize(PAR_HUE_IP);
      PRINTF("hue-SetDefaults():No Bridge IP, take default (%d bytes max)" CRLF, iMax);
      GEN_STRNCPY(pcVar, pcHueDefaultIpAddr, (size_t)iMax);
   }
   PRINTF("hue-SetDefaults():Bridge IP=%s" CRLF, pcVar);
   //
   // Setup default Hue Bridge IP Port, if not available yet 
   //
   pcVar = GLOBAL_GetParameter(PAR_HUE_PORT);
   if(GEN_STRLEN(pcVar) == 0)
   {
      iMax = GLOBAL_GetParameterSize(PAR_HUE_PORT);
      PRINTF("hue-SetDefaults():No Bridge Port, take default (%d bytes max)" CRLF, iMax);
      GEN_STRNCPY(pcVar, pcHueDefaultIpPort, (size_t)iMax);
   }
   PRINTF("hue-SetDefaults():Bridge Port=%s" CRLF, pcVar);
   //
   // Setup default Hue Bridge ID, if not available yet
   //
   pcVar = GLOBAL_GetParameter(PAR_HUE_USER);
   if(GEN_STRLEN(pcVar) == 0)
   {
      iMax = GLOBAL_GetParameterSize(PAR_HUE_USER);
      PRINTF("hue-SetDefaults():No Bridge User ID, take default (%d bytes max)" CRLF, iMax);
      GEN_STRNCPY(pcVar, pcHueDefaultUserId, (size_t)iMax);
   }
   PRINTF("hue-SetDefaults():Hue UserID=[%s]" CRLF, pcVar);
}

//
// Function:   hue_SetupNewDay
// Purpose:    Setup all secs data for the coming day
//
// Parms:      
// Returns:    
// Note:       We will call this before midnight (after a restart), or just after a new midnight.
//
static void hue_SetupNewDay()
{
   int      iNr, iHrs, iMins;
   u_int32  ulSecs, ulRelSecsDark;
   char     cDateTime[32];

   //
   // Obtain the Hrs/Min when we turn off the lights from persistent memory:
   // TimeDark = xx:xx
   //
   iNr = GEN_SSCANF(pstMap->G_pcHueTimeDark, "%d:%d", &iHrs, &iMins);
   if(iNr != 2)
   {
      iHrs  = 23;
      iMins = 45;
      PRINTF("hue-SetupNewDay():Hue-to-Dark time not found [%s] (%d items)" CRLF, pstMap->G_pcHueTimeDark, iNr);
      LOG_Report(0, "HUE", "hue-SetupNewDay():Hue-to-Dark time not found [%s]", pstMap->G_pcHueTimeDark);
   }
   ulRelSecsDark = (iHrs * 3600) + (iMins * 60);
   //
   // Calculate secs for Dawn and Dusk (including the delta's)
   // These are CLOCK times, so DST is included through RTC_GetMidnight()
   //
   //             XXXX=Outlet On/Off checks
   //     |XXXXXXXXXXXXXXXXXXXXX|XXXXXXXXX|XXXXXXXXXXXXXXXXXXXXX|XXXXXXXXX|XXXXXXXXXXXXXXXXXX|    |
   //     |                     |         |                     |         |                  |    |
   //     |                     DAWN     DAWN                  DUSK    EVENING             DARK   |
   //   00:00                   ON       OFF                    ON        ON                OFF  24:00
   // <---+---------------------+---------+---------------------+---------+------------------+----+------------>
   // <-------lights off------->|         |<---lights off------>|         |                  |<---lights off--->
   // <---+---------------------+---------+---------------------+---------+------------------+----+------------>
   //     |                     |<------->|                     |<------->|                  |
   //     | ulAbsSecsMidnite    | G_iSecsTimerDawn                G_iSecsTimerDusk           |
   //     |                     |                                                            |
   //     |-------------------->|                                                            |<---------------->
   //     |_pcHueTimeDark       |                                                            |G_pcHueTimeDark
   //     |lAbsSecsDarkSta      |                                                            |ulAbsSecsDarkSta
   //     |                     |                                                            |
   // <------------------------>|                                                            |<---------------->
   //      Possible HUE_DARK                                                                   Possible HUE_DARK
   //
   //   Keep EVENING < DARK < DAWN
   //                  ----   
   //                  TimeDark = 23:30 in Persistent memory
   //
   ulSecs = RTC_GetSystemSecs();
   if((ulSecs - ulAbsSecsMidnite) > (24 * 3600))
   {
      // Sanity check: Midnight was more than 24 hours ago
      RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulSecs);
      LOG_Report(0, "HUE", "hue-SetupNewDay(): %s", cDateTime);
      RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulAbsSecsMidnite);
      LOG_Report(0, "HUE", "hue-SetupNewDay(): Assumed Midnight (%s) >> 24h ago!", cDateTime);
      ulAbsSecsMidnite += 24*3600;
   }
   ulAbsSecsDawnSta = ulAbsSecsMidnite + (pstMap->G_iMinuteDawn * 60) + pstMap->G_iSecsDeltaDawn;
   ulAbsSecsDawnSto = ulAbsSecsDawnSta +  pstMap->G_iSecsTimerDawn;
   ulAbsSecsDuskSta = ulAbsSecsMidnite + (pstMap->G_iMinuteDusk * 60) + pstMap->G_iSecsDeltaDusk;
   ulAbsSecsDuskSto = ulAbsSecsDuskSta +  pstMap->G_iSecsTimerDusk;
   ulAbsSecsDarkSta = ulAbsSecsMidnite + ulRelSecsDark;
   //
   // Introduce a small offset prevent Hue bridge collisions between the RPi's
   //
   ulAbsSecsDawnSta += iSecsOffset;
   ulAbsSecsDawnSto += iSecsOffset;
   ulAbsSecsDuskSta += iSecsOffset;
   ulAbsSecsDuskSto += iSecsOffset;
   ulAbsSecsDarkSta += iSecsOffset;
   //
   // Setup Outlet control for (XMas) lights HUE_OUTLET_1
   //
   if(pstMap->G_iSecsOutletOn)
   {
      ulAbsSecsOutletOn  = ulAbsSecsMidnite + (u_int32) pstMap->G_iSecsOutletOn;
      if(pstMap->G_iSecsOutletOff)
      {
         ulAbsSecsOutletOff = ulAbsSecsMidnite + (u_int32)pstMap->G_iSecsOutletOff;
         if(RPI_GetFlag(FLAGS_HUE_OUTLET)) LOG_Report(0, "HUE", "hue-SetupNewDay():Outlet ON at %d, OFF at %d", pstMap->G_iSecsOutletOn, pstMap->G_iSecsOutletOff);
      }
      else 
      {
         //
         // No end-time: take end-of-the-day
         //
         ulAbsSecsOutletOff = ulAbsSecsMidnite + (24 * 3600);
         if(RPI_GetFlag(FLAGS_HUE_OUTLET)) LOG_Report(0, "HUE", "hue-SetupNewDay():Outlet ON at %d, OFF at DARK", pstMap->G_iSecsOutletOn);
      }
   }
   else
   {
      if(RPI_GetFlag(FLAGS_HUE_OUTLET)) LOG_Report(0, "HUE", "hue-SetupNewDay():Outlet disabled");
      ulAbsSecsOutletOn  = 0;
      ulAbsSecsOutletOff = 0;
   }
   fOutletOn  = FALSE;
   fOutletOff = FALSE;
   //
   // Do some sanity checks:
   // Dawn < Dusk < Dark
   //
   if(ulAbsSecsDawnSto > ulAbsSecsDuskSta) 
   {
      // Ignore Dawn
      LOG_Report(0, "HUE", "hue-SetupNewDay():Dawn End(%d) >> Dusk Start(%d)", ulAbsSecsDawnSto, ulAbsSecsDuskSta);
      ulAbsSecsDuskSta = ulAbsSecsDawnSto;
   }
   if(ulAbsSecsDuskSto > ulAbsSecsDarkSta)
   {
      // Ignore Dusk
      LOG_Report(0, "HUE", "hue-SetupNewDay():Dusk End(%d) >> Dark(%d)", ulAbsSecsDuskSto, ulAbsSecsDarkSta);
      ulAbsSecsDarkSta = ulAbsSecsDuskSto;
   }
   //
   // Report todays HUE timers
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulAbsSecsDawnSta);
   LOG_Report(0, "HUE", "hue-SetupNewDay(): Dawn Start %s",  cDateTime);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulAbsSecsDawnSto);
   LOG_Report(0, "HUE", "hue-SetupNewDay(): Dawn End   %s",  cDateTime);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulAbsSecsDuskSta);
   LOG_Report(0, "HUE", "hue-SetupNewDay(): Dusk Start %s",  cDateTime);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulAbsSecsDuskSto);
   LOG_Report(0, "HUE", "hue-SetupNewDay(): Dusk End   %s",  cDateTime);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, cDateTime, ulAbsSecsDarkSta);
   LOG_Report(0, "HUE", "hue-SetupNewDay(): Dark Start %s",  cDateTime);
}

/*------  Local functions separator ------------------------------------
________________HUE_BRIDGE_DATA(){};
------------------------------x----------------------------------------*/

//
//    The HUE Bridge will return following data is the configuration is being read:
//    {
//        "1": {
//            "state": {
//                "on": false,
//                "bri": 252,
//                "hue": 0,
//                "sat": 0,
//                "effect": "none",
//                "xy": [
//                    0.3144,
//                    0.1644
//                ],
//                "ct": 156,
//                "alert": "select",
//                "colormode": "xy",
//                "mode": "homeautomation",
//                "reachable": false
//            },
//            "swupdate": {
//                "state": "notupdatable",
//                "lastinstall": "2021-10-07T18:42:30"
//            },
//            "type": "Extended color light",
//            "name": "Tint nr 2",
//            "modelid": "ZBT-ExtendedColor",
//            "manufacturername": "MLI",
//            "productname": "Extended color light",
//            "capabilities": {
//                "certified": false,
//                "control": {
//                    "colorgamuttype": "other",
//                    "colorgamut": [
//                        [
//                            0.68,
//                            0.31
//                        ],
//                        [
//                            0.11,
//                            0.82
//                        ],
//                        [
//                            0.13,
//                            0.04
//                        ]
//                    ],
//                    "ct": {
//                        "min": 153,
//                        "max": 556
//                    }
//                },
//                "streaming": {
//                    "renderer": false,
//                    "proxy": false
//                }
//            },
//            "config": {
//                "archetype": "classicbulb",
//                "function": "mixed",
//                "direction": "omnidirectional"
//            },
//            "uniqueid": "00:15:8d:00:02:cb:c1:ac-01",
//            "swversion": "2.0"
//        },
//        "2": {
//            "state": {
//                "on": false,
//                "bri": 45,
//                "hue": 0,
//                "sat": 0,
//                "effect": "none",
//                "xy": [
//                    0.3517,
//                    0.1668
//                ],
//                "ct": 207,
//                "alert": "select",
//                "colormode": "xy",
//                "mode": "homeautomation",
//                "reachable": true
//            },
//            "swupdate": {
//                "state": "notupdatable",
//                "lastinstall": "2021-10-07T18:46:00"
//            },
//            "type": "Extended color light",
//            "name": "Tint nr 1",
//            "modelid": "ZBT-ExtendedColor",
//            "manufacturername": "MLI",
//            "productname": "Extended color light",
//            "capabilities": {
//                "certified": false,
//                "control": {
//                    "colorgamuttype": "other",
//                    "colorgamut": [
//                        [
//                            0.68,
//                            0.31
//                        ],
//                        [
//                            0.11,
//                            0.82
//                        ],
//                        [
//                            0.13,
//                            0.04
//                        ]
//                    ],
//                    "ct": {
//                        "min": 153,
//                        "max": 556
//                    }
//                },
//                "streaming": {
//                    "renderer": false,
//                    "proxy": false
//                }
//            },
//            "config": {
//                "archetype": "classicbulb",
//                "function": "mixed",
//                "direction": "omnidirectional"
//            },
//            "uniqueid": "00:15:8d:00:02:cb:97:c9-01",
//            "swversion": "2.0"
//        },
//        "3": {
//            "state": {
//                "on": false,
//                "bri": 38,
//                "ct": 370,
//                "alert": "select",
//                "colormode": "ct",
//                "mode": "homeautomation",
//                "reachable": true
//            },
//            "swupdate": {
//                "state": "notupdatable",
//                "lastinstall": "2021-10-07T18:47:26"
//            },
//            "type": "Color temperature light",
//            "name": "Tint Spot nr 1",
//            "modelid": "ZBT-ColorTemperature",
//            "manufacturername": "MLI",
//            "productname": "Color temperature light",
//            "capabilities": {
//                "certified": false,
//                "control": {
//                    "ct": {
//                        "min": 153,
//                        "max": 370
//                    }
//                },
//                "streaming": {
//                    "renderer": false,
//                    "proxy": false
//                }
//            },
//            "config": {
//                "archetype": "classicbulb",
//                "function": "functional",
//                "direction": "omnidirectional"
//            },
//            "uniqueid": "00:15:8d:00:03:27:9c:49-01",
//            "swversion": "2.0"
//        },
//        "4": {
//            "state": {
//                "on": false,
//                "alert": "select",
//                "mode": "homeautomation",
//                "reachable": true
//            },
//            "swupdate": {
//                "state": "noupdates",
//                "lastinstall": "2021-10-09T12:13:53"
//            },
//            "type": "On/Off plug-in unit",
//            "name": "Hue Smart plug 1",
//            "modelid": "LOM001",
//            "manufacturername": "Signify Netherlands B.V.",
//            "productname": "Hue Smart plug",
//            "capabilities": {
//                "certified": true,
//                "control": {},
//                "streaming": {
//                    "renderer": false,
//                    "proxy": false
//                }
//            },
//            "config": {
//                "archetype": "plug",
//                "function": "functional",
//                "direction": "omnidirectional",
//                "startup": {
//                    "mode": "safety",
//                    "configured": true
//                }
//            },
//            "uniqueid": "00:17:88:01:09:d3:5b:49-0b",
//            "swversion": "1.76.10",
//            "swconfigid": "5C3EFAF7",
//            "productid": "SmartPlug_OnOff_v01-00_01"
//        },
//        "5": {
//            "state": {
//                "on": false,
//                "alert": "select",
//                "mode": "homeautomation",
//                "reachable": true
//            },
//            "swupdate": {
//                "state": "noupdates",
//                "lastinstall": "2021-10-09T12:13:50"
//            },
//            "type": "On/Off plug-in unit",
//            "name": "Hue Smart plug 2",
//            "modelid": "LOM001",
//            "manufacturername": "Signify Netherlands B.V.",
//            "productname": "Hue Smart plug",
//            "capabilities": {
//                "certified": true,
//                "control": {},
//                "streaming": {
//                    "renderer": false,
//                    "proxy": false
//                }
//            },
//            "config": {
//                "archetype": "plug",
//                "function": "functional",
//                "direction": "omnidirectional",
//                "startup": {
//                    "mode": "safety",
//                    "configured": true
//                }
//            },
//            "uniqueid": "00:17:88:01:09:d3:5b:4f-0b",
//            "swversion": "1.76.10",
//            "swconfigid": "5C3EFAF7",
//            "productid": "SmartPlug_OnOff_v01-00_01"
//        }
//    }
//

//
// Function:   hue_Activation
// Purpose:    (De)activate a single light/group/scene
//
// Parms:      Light/Group/Scene name IDT, ON/OFF
// Returns:    TRUE if OKee
// Note:       Name ID = HUE_DAWN, HUE_DUSK, ...
//
static bool hue_Activation(HUENAME tName, bool fOn)
{
   bool     fCc=FALSE;
   HUECFG  *pstHueCfg;
   HUECFG  *pstHue;
   
   pstHue = hue_GetDeviceByIdt(tName);
   if(pstHue)
   {
      if(fOn)
      {
         PRINTF("hue-Activation(): Turn %s ON" CRLF, pstHue->cName);
         //
         pstHue->fOn      = 1;
         pstHue->fChanged = TRUE;
         fCc = hue_UpdateBridgeDevice(pstHue);
      }
      else
      {
         PRINTF("hue-Activation(): Turn %s OFF" CRLF, pstHue->cName);
         //
         // Lights and Groups: handle as determined by the "On"-clause
         // Scenes MUST be handled by turning OFF the corresponding group !
         //
         if(RPI_GetFlag(FLAGS_PRN_HUEURL)) 
         {
            LOG_printf("hue-Activation():Turn OFF:[%s]" CRLF, pstHue->cName);
            hue_ListDeviceConfig(pstHue);
         }
         if(GEN_STRLEN(pstHue->cGroup))
         {
            // We have a Scene to turn OFF
            pstHueCfg = HUE_GetDeviceConfig(HUE_DEVICE_GROUP);
            pstHue    = hue_GetDeviceByDeviceId(pstHueCfg, pstHue->cGroup);
         }
         if(pstHue)
         {
            if(RPI_GetFlag(FLAGS_PRN_HUEURL)) 
            {
               LOG_printf("hue-Activation():Turn OFF: Scene has Group:" CRLF);
               hue_ListDeviceConfig(pstHue);
            }
            pstHue->fOn      = 0;
            pstHue->fChanged = TRUE;
            fCc = hue_UpdateBridgeDevice(pstHue);
         }
         else 
         {
            LOG_Report(0, "HUE", "hue-Activation(): Idt=%d, No Hue config to turn OFF!", tName);
         }
      }
      tHueOldName = tName;
   }
   else LOG_Report(0, "HUE", "hue-Activation(): Name Idt=%d not found!", tName);
   return(fCc);
}

//
// Function:   hue_ArrayGetElementIndex
// Purpose:    Get this element from the array
//
// Parms:      Array ptr, index 0..?
//
// Returns:    Element index 0...?
// Note:       HUE light IDTs are like:
//             "lights":["4","2","7","6","3","1","8"]
//                 where ^pcElement
//
static int hue_ArrayGetElementIndex(char *pcElement, int iIdx)
{
   int   iVal=0, x=0;

   iIdx *= 2;
   //
   while(*pcElement)
   {
      if(*pcElement++ == '"')
      {
         if(iIdx == x)
         {
            //
            // Correct index found: convert the IDt to int INDEX
            //
            iVal = strtol(pcElement, NULL, 10);
            if(iVal > 0) iVal--;    // OKee: return INDEX to IDT
            else         iVal = 0;  // Problems: return index to 1st element
            break;
         }
         x++;
      }
   }
   return(iVal);
}

//
// Function:   hue_ArrayNumElements
// Purpose:    Count the array size
//
// Parms:      Hue cfg
//
//
// Returns:    Nr of entries in array
// Note:       JSON Array = "1","2","3","4",....
//
static int hue_ArrayNumElements(char *pcElement)
{
   int   iNr=0;

   while(*pcElement)
   {
      if(*pcElement++ == '"') iNr++;
   }
   return(iNr/2);
}

//
// Function:   hue_BridgeReadData (HTTP_GET)
// Purpose:    Read Hue Bridge JSON data
//
// Parms:      Dev Type: lights, groups, scenes
//             Reply buffer, buffer size
// Returns:    TRUE if OKee
// Note:       The HUE Bridge can be polled for data by reading the appropriate URL:
//             HTTP: Get <HueBridge_IP>
//             
//
static bool hue_BridgeReadData(char cDevType, char *pcBuffer, int iMax)
{
   bool        fCc=FALSE;
   int         iSend, iLen, iRead, iIdx;
   int         iFdIp, iPort;
   const char *pcDevType; 
   char       *pcUserId;
   char       *pcIp;
   char       *pcPort;
   char       *pcUrl;
   
   pcDevType = hue_GetDeviceTypeString(cDevType);
   pcIp      = GLOBAL_GetParameter(PAR_HUE_IP);
   pcPort    = GLOBAL_GetParameter(PAR_HUE_PORT);
   iPort     = (int)strtol(pcPort, NULL, 0);
   iFdIp     = NET_ClientConnect(pcIp, iPort, "tcp");
   //
   if(iFdIp < 0)
   {
      LOG_Report(errno, "HUE", "hue-BridgeReadData():Connect ERROR:%s:%d", pcIp, iPort);
      PRINTF("hue-BridgeReadData(): Not able to connect to server %s:d=%s" CRLF, pcIp, iPort, strerror(errno));
   }
   else
   {
      //PRINTF("hue-BridgeReadData():Connect to server {%s:%s] OK" CRLF, pcIp, pcPort);
      //
      // Go get the Hue config
      //
      pcUrl    = safemalloc(MAX_PATH_LEN);
      pcUserId = GLOBAL_GetParameter(PAR_HUE_USER);
      //
      //    HTTP GET Commands (lights, groups, scenes)
      //    GET /api/xx...xxxxx/<device>
      //
      //    Parms: <user-id>, <device>
      //
      GEN_SNPRINTF(pcUrl, MAX_PATH_LEN, pcHttpGet, pcUserId, pcDevType);
      iLen = GEN_STRLEN(pcUrl);
      //PRINTF("hue-BridgeReadData():Going to send %d bytes" CRLF, iLen);
      iSend = NET_Write(iFdIp, (char *) pcUrl, iLen);
      if(iSend == iLen)
      {
         iIdx = 0;
         //PRINTF("hue-BridgeReadData():Have send %d bytes:" CRLF, iSend);
         //PRINTF("hue-BridgeReadData():[%s]" CRLF, pcUrl);
         fCc = TRUE;
         do
         {
            iRead = NET_Read(iFdIp, &pcBuffer[iIdx], iMax);
            if(iRead < 0)
            {
               PRINTF("hue-BridgeReadData():ERROR %d reading socket" CRLF, iRead);
               fCc = FALSE;
            }
            else
            {
               iMax -= iRead;
               iIdx += iRead;
            }
         }
         while(iRead > 0);
         //PRINTF("hue-BridgeReadData():Have read %d bytes:" CRLF, iIdx);
         pcBuffer[iIdx] = 0;
      }
      else
      {
         LOG_Report(errno, "HUE", "hue-BridgeReadData():Write ERROR:%s:%d", pcIp, iPort);
         PRINTF("hue-BridgeReadData():ERROR: %d bytes send" CRLF, iSend, strerror(errno));
      }
      NET_ClientDisconnect(iFdIp);
      safefree(pcUrl);
   }
   return(fCc);
}   

//
// Function:   hue_BridgeWriteData
// Purpose:    Write Hue Bridge JSON data (HTTP PUT)
//
// Parms:      Hue cfg
//             JSON object
//             Reply buffer, buffer size
// Returns:    TRUE if OKee
// Note:       The HUE Bridge can be polled for data by writing the appropriate URL:
//                HTTP: PUT <HueBridge_IP>...
//
static bool hue_BridgeWriteData(HUECFG *pstHue, RPIDATA *pstObj, char *pcBuffer, int iMax)
{
   bool        fCc=FALSE;
   int         iSend, iLen, iRead, iIdx;
   int         iFdIp, iPort;
   char       *pcIp;
   char       *pcPort;
   char       *pcUrl;
   
   pcIp      = GLOBAL_GetParameter(PAR_HUE_IP);
   pcPort    = GLOBAL_GetParameter(PAR_HUE_PORT);
   iPort     = (int)strtol(pcPort, NULL, 0);
   iFdIp     = NET_ClientConnect(pcIp, iPort, "tcp");
   //
   if(iFdIp < 0)
   {
      LOG_Report(errno, "HUE", "hue-BridgeWriteData():Connect ERROR:%s:%d", pcIp, iPort);
      PRINTF("hue-BridgeWriteData(): Not able to connect to server %s:d=%s" CRLF, pcIp, iPort, strerror(errno));
   }
   else
   {
      PRINTF("hue-BridgeWriteData():Connect to server {%s:%s] OK" CRLF, pcIp, pcPort);
      //
      // Go write the Hue data
      //
      pcUrl = safemalloc(MAX_PATH_LEN);
      //
      // HTTP PUT:
      //    "... PUT /api/<HueUserId>/<Request>.. <Header>..<Payload>"
      //
      iLen = hue_GenerateDeviceUrl(pstHue, pcUrl, MAX_PATH_LEN, pstObj);
      if(RPI_GetFlag(FLAGS_LOG_HUEURL)) 
      {
         //
         // HTTP PUT:
         //    "... PUT /api/<HueUserId>/<Request>.. <Header>..<Payload>"
         //
         hue_LogConfiguration(*pstHue->pcDevType);
         LOG_Report(0, "HUE", "hue-BridgeWriteData():\n%s\n\n", pcUrl);
      }
      PRINTF("hue-BridgeWriteData():Going to send %d bytes" CRLF, iLen);
      iSend = NET_Write(iFdIp, (char *) pcUrl, iLen);
      if(iSend == iLen)
      {
         PRINTF("hue-BridgeWriteData():Have send %d bytes:" CRLF, iSend);
         PRINTF(CRLF);
         PRINTF("%s" CRLF, pcUrl);
         PRINTF(CRLF);
         //
         iIdx = 0;
         fCc  = TRUE;
         do
         {
            iRead = NET_Read(iFdIp, &pcBuffer[iIdx], iMax);
            if(iRead < 0)
            {
               PRINTF("hue-BridgeWriteData():ERROR %d reading socket" CRLF, iRead);
               fCc = FALSE;
            }
            else
            {
               iMax -= iRead;
               iIdx += iRead;
            }
         }
         while(iRead > 0);
         //PRINTF("hue-BridgeWriteData():Have read %d bytes:" CRLF, iIdx);
         pcBuffer[iIdx] = 0;
      }
      else
      {
         LOG_Report(errno, "HUE", "hue-BridgeWriteData():Write ERROR:%s:%d", pcIp, iPort);
         PRINTF("hue-BridgeWriteData():ERROR: %d bytes send" CRLF, iSend, strerror(errno));
      }
      NET_ClientDisconnect(iFdIp);
      safefree(pcUrl);
   }
   return(fCc);
}   

//
// Function:   hue_CheckDeviceStates
// Purpose:    Check the state of all devices (lights, switches,..
//
// Parms:      
// Returns:    Device state
// Note:       
//
static HUEDEV hue_CheckDeviceStates()
{
   bool     fSearch=TRUE;
   bool     fGroup=FALSE;
   HUEDEV   tState;
   int      x=0, iNr, iLight, y;
   int      iOn=0, iNoReach=0;
   HUECFG  *pstHueCfg;
   HUECFG  *pstHue;
   HUECFG  *pstCfg;

   //
   // Check all entries of this group
   //
   pstHueCfg = HUE_GetDeviceConfig(HUE_DEVICE_GROUP);
   do
   {
      if(pstHueCfg->pcObject)
      {
         if(GEN_STRCMP(pstHueCfg->cName, pcGroupName) == 0)
         {
            fGroup = TRUE;
            PRINTF("hue-CheckDeviceStates():Group=%s" CRLF, pstHueCfg->cName);
            //
            // This is the group with all lights
            // If one is still on (and reachable), try to turn it off again
            //
            iNr = hue_ArrayNumElements(pstHueCfg->cLights);
            for(y=0; y<iNr; y++)
            {
               pstHue = HUE_GetDeviceConfig(HUE_DEVICE_LIGHT);
               iLight = hue_ArrayGetElementIndex(pstHueCfg->cLights, y);
               pstCfg = &pstHue[iLight];
               PRINTF("hue-CheckDeviceStates():Light=%s On=%d (Reach=%d)" CRLF, pstCfg->cName, pstCfg->fOn, pstCfg->fReach);
               if(pstCfg->fReach)
               {
                  if(pstCfg->fOn) iOn++;
               }
               else iNoReach++;
            }
            fSearch = FALSE;
         }
      }
      pstHueCfg++;
      x++;
   }
   while(fSearch && (x < MAX_HUE_DEVICES));
   //
   // Check states
   //
   if(fGroup)
   {
      if(iOn)
      {
         if(iNr == iOn)       tState = HUEDEV_ALL_ON;
         else                 tState = HUEDEV_ANY_ON;
      }
      else 
      {
         if(iNr == iNoReach)  tState = HUEDEV_UNREACH;
         else                 tState = HUEDEV_ALL_OFF;
      }
   }
   else tState = HUEDEV_NONE;
   PRINTF("hue-CheckDeviceStates():State=%d" CRLF, tState);
   return(tState);
}

//
//  Function:  hue_ExtractJsonAllElements
//  Purpose:   Extract all JSON objects from the bridge reply
//
//  Parms:     Hue Cfg ptr, device type (l,g,s), JSON Object
//  Returns:   Nr of elements found
//  Note:      pstHueCfg -> 
//
static int hue_ExtractJsonAllElements(HUECFG *pstHueCfg, char cDevType, char *pcRootObj)
{
   int      x, iNr=0;
   int      iRootIdx, iRootNr;
   char    *pcObj;
   char    *pcDevId;

   iRootNr = GEN_JsonGetNumberOfElements(pcRootObj);
   //
   if(iRootNr < MAX_HUE_DEVICES)
   {
      PRINTF("hue-ExtractJsonAllElements():Root has %d devices" CRLF, iRootNr);
   }
   else
   {
      PRINTF("hue-ExtractJsonAllElements():Root has too many devices (%d of max %d)" CRLF, iRootNr, MAX_HUE_DEVICES);
      iRootNr = MAX_HUE_DEVICES;
   }
   //
   // Find all elements in the list for each root object and
   // store each element in the persistent storage G_stHueL[RootIndex]
   //
   for(iRootIdx=0; iRootIdx<iRootNr; iRootIdx++)
   {
      //
      // Store the devicetype ID
      // For Lights and Groups this is the sequence number "1"..."?",
      // but for Scenes a random ID: "S8ShDad33BY3XnM"
      //
      pcDevId = (char *)hue_GetPersistentStorage(pstHueCfg, &stHueJsonId, iRootIdx);
      if(pcDevId) 
      {
         GEN_JsonGetElementName(pcRootObj, iRootIdx, pcDevId, stHueJsonId.iSize, EXCLUDE_DQ);
         PRINTF("hue-ExtractJsonAllElements():%2d Element=[%s]" CRLF, iRootIdx, pcDevId);
      }
      else
      {
         PRINTF("hue-ExtractJsonAllElements():%2d NO Id found!" CRLF, iRootIdx);
      }
      if( (pcObj = GEN_JsonGetElementObject(pcRootObj, iRootIdx, 0)) )
      {
         PRINTF("hue-ExtractJsonAllElements():Store device #%d" CRLF, iRootIdx);
         PRINTF( "hue-ExtractJsonAllElements():=========================================" CRLF);
         PRINTF(CRLF);
         //
         //       {"1":
         // pcObj->  {"state":{"on":true,"bri":100,....},"type":"Extended",...}.
         //       {"2":....
         //
         pstHueCfg[iRootIdx].pcObject  = pcObj;
         pstHueCfg[iRootIdx].pcDevType = hue_GetDeviceTypeString(cDevType);
         //
         // Search each element for this HUE device in the persistent storage list
         //
         for(x=0; x<iNumHueJsonParameters; x++)
         {
            //#define  FEATURE_HUE_LIST_TODOS
            #ifdef   FEATURE_HUE_LIST_TODOS
            {
            int   i;

               PRINTF("hue-ExtractJsonAllElements():To-Do-List[%2d]=", x);
               for(i=0; i<HUE_MAX_LEVELS; i++)
               {
                  if(stHueJsonParameters[x].pcElm[i]) printf("[%s] ", stHueJsonParameters[x].pcElm[i]);
                  else                                printf("[----] ");
               }
               printf(CRLF);
            }
            #endif   //FEATURE_HUE_LIST_TODOS

            iNr += hue_ExtractJsonElement(pstHueCfg, pcObj, &stHueJsonParameters[x], iRootIdx, 0);
         }
      }
      else
      {
         PRINTF("hue-ExtractJsonAllElements():Device %d not found!" CRLF, iRootIdx);
      }
   }
   return(iNr);
}

//
//  Function:  hue_ExtractJsonElement
//  Purpose:   Lookup this list entry and extract a single JSON element from the JSON object
//             into global storage if it is on the list
//
//  Parms:     Hue cfg ptr, JSON Object, Parms ptr, Device Index, Object Level
//  Returns:   Number of values found
//  Note:      
//
static int hue_ExtractJsonElement(HUECFG *pstHueCfg, char *pcObj, const HUEJS *pstParm, int iDevIdx, int iLevIdx)
{
   bool     fFound=FALSE;
   bool     fChanged=FALSE;
   int      x, iSize, iNumElms, iNr=0;
   char    *pcBuffer;
   char    *pcNewObj;
   bool     fValue, *pfValue;
   int      iValue, *piValue;
   double   flValue, *pflValue;
   char    *pcTemp, *pcValue;

   if(iLevIdx < HUE_MAX_LEVELS)
   {
      pcBuffer = safemalloc(MAX_ARG_LENZ);
      pcTemp   = safemalloc(MAX_ARG_LENZ);
      iNumElms = GEN_JsonGetNumberOfElements(pcObj);
      //PRINTF("hue-ExtractJsonElement(%2d):This JSON level has %d elements" CRLF, iLevIdx, iNumElms);
      //
      for(x=0; x<iNumElms; x++)
      {
         if(GEN_JsonGetElementName(pcObj, x, pcBuffer, MAX_ARG_LEN, INCLUDE_DQ))
         {
            iSize = GEN_STRLEN(pstParm->pcElm[iLevIdx]);
            //PRINTF("hue-ExtractJsonElement(%2d): Check Element %2d [%s] Len=%d" CRLF, iLevIdx, x, pcBuffer, iSize);
            if(GEN_STRNCMPI(pstParm->pcElm[iLevIdx], &pcBuffer[1], iSize) == 0)
            {
               switch(GEN_JsonGetObjectType(pcObj, x))
               {
                  default:
                  case JSON_TYPE_NONE:
                     break;

                  case JSON_TYPE_OBJECT:
                     PRINTF("hue-ExtractJsonElement(%2d):Element [%s] is Object" CRLF, iLevIdx, pcBuffer);
                     iLevIdx++;
                     if( (pcNewObj = GEN_JsonGetElementObject(pcObj, x, 0)) )
                     {
                        iNr += hue_ExtractJsonElement(pstHueCfg, pcNewObj, pstParm, iDevIdx, iLevIdx);
                     }
                     else
                     {
                        PRINTF("hue-ExtractJsonElement(%2d):Object[%s] NOT FOUND!" CRLF, iLevIdx, pcBuffer);
                     }
                     iLevIdx--;
                     fFound = TRUE;
                     break;

                  case JSON_TYPE_ARRAY:
                     PRINTF("hue-ExtractJsonElement(%2d):Element [%s] is Array:" CRLF, iLevIdx, pcBuffer);
                     pcValue = GEN_JsonGetArrayEntry(pcObj, 0);
                     if(pcValue) 
                     {
                        PRINTF("hue-ExtractJsonElement(%2d):Original Array=%s" CRLF, iLevIdx, pcValue);
                        if( GEN_JsonGetArrayCopy(pcValue, pcTemp, MAX_ARG_LEN))
                        {
                           PRINTF("hue-ExtractJsonElement(%2d):Copied Array=%s" CRLF, iLevIdx, pcTemp);
                           pcValue = (char *)hue_GetPersistentStorage(pstHueCfg, pstParm, iDevIdx);
                           if(pcValue)
                           {
                              GEN_STRNCPY(pcValue, pcTemp, (size_t)pstParm->iSize);
                              PRINTF("hue-ExtractJsonElement(%2d):Element [%s] is Array=[%s]" CRLF, iLevIdx, pcBuffer, pcValue);
                              fChanged = TRUE;
                              iNr++;
                           }
                           else PRINTF("hue-ExtractJsonElement(%2d):No persistent storage found" CRLF, iLevIdx);
                           fFound = TRUE;
                        }
                     }
                     break;

                  case JSON_TYPE_BOOL:
                     if( GEN_JsonGetBoolean(pcObj, pcBuffer, &fValue))
                     {
                        pfValue = (bool *)hue_GetPersistentStorage(pstHueCfg, pstParm, iDevIdx);
                        if(pfValue)
                        {
                           *pfValue = fValue;
                           fChanged = TRUE;
                           iNr++;
                        }
                        else PRINTF("hue-ExtractJsonElement(%2d):No persistent storage found" CRLF, iLevIdx);
                        fFound = TRUE;
                        PRINTF("hue-ExtractJsonElement(%2d):Element [%s] is Bool=%d" CRLF, iLevIdx, pcBuffer, fValue);
                     }
                     else
                     {
                        PRINTF("hue-ExtractJsonElement(%2d):Bool[%s] has no value" CRLF, iLevIdx, pcBuffer);
                     }
                     break;

                  case JSON_TYPE_STRING:
                     if( GEN_JsonGetStringCopy(pcObj, pcBuffer, pcTemp, MAX_ARG_LEN))
                     {
                        pcValue = (char *)hue_GetPersistentStorage(pstHueCfg, pstParm, iDevIdx);
                        if(pcValue)
                        {
                           GEN_STRNCPY(pcValue, pcTemp, (size_t)pstParm->iSize);
                           PRINTF("hue-ExtractJsonElement(%2d):Element [%s] is String=[%s]" CRLF, iLevIdx, pcBuffer, pcValue);
                           fChanged = TRUE;
                           iNr++;
                        }
                        else PRINTF("hue-ExtractJsonElement(%2d):No persistent storage found" CRLF, iLevIdx);
                        fFound = TRUE;
                     }
                     else
                     {
                        PRINTF("hue-ExtractJsonElement(%2d):String[%s] has no value" CRLF, iLevIdx, pcBuffer);
                     }
                     break;

                  case JSON_TYPE_INTEGER:
                     if( GEN_JsonGetInteger(pcObj, pcBuffer, &iValue))
                     {
                        piValue  = (int *)hue_GetPersistentStorage(pstHueCfg, pstParm, iDevIdx);
                        if(piValue)
                        {
                           *piValue = iValue;
                           PRINTF("hue-ExtractJsonElement(%2d):Element [%s] is Integer=%d" CRLF, iLevIdx, pcBuffer, iValue);
                           fChanged = TRUE;
                           iNr++;
                        }
                        fFound = TRUE;
                     }
                     else
                     {
                        PRINTF("hue-ExtractJsonElement(%2d):Integer[%s] has no value" CRLF, iLevIdx, pcBuffer);
                     }
                     break;

                  case JSON_TYPE_FLOAT:
                     if( GEN_JsonGetDouble(pcObj, pcBuffer, &flValue))
                     {
                        pflValue = (double *)hue_GetPersistentStorage(pstHueCfg, pstParm, iDevIdx);
                        if(pflValue)
                        {
                           *pflValue = flValue;
                           PRINTF("hue-ExtractJsonElement(%2d):Element [%s] is Float=%.3f" CRLF, iLevIdx, pcBuffer, flValue);
                           fChanged = TRUE;
                           iNr++;
                        }
                        else PRINTF("hue-ExtractJsonElement(%2d):No persistent storage found" CRLF, iLevIdx);
                        fFound = TRUE;
                     }
                     else
                     {
                        PRINTF("hue-ExtractJsonElement(%2d):Float[%s] has no value" CRLF, iLevIdx, pcBuffer);
                     }
                     break;
               }     // End switch(Type)
            }
            else
            {
               //PRINTF("hue-ExtractJsonElement(%2d):Ignore Element %2d [%s]" CRLF, iLevIdx, x, pcBuffer);
            }
            if(fFound) 
            {
               PRINTF("hue-ExtractJsonElement(%02d):Element value found, skip search in to-do-list!" CRLF, iLevIdx);
               pstHueCfg[iDevIdx].fChanged = fChanged;
               break;
            }
         }
         else
         {
            PRINTF("hue-ExtractJsonElement(%2d):Element %d name not found [%s]" CRLF, iLevIdx, x, pstParm->pcElm[iLevIdx]);
         }
      }
      safefree(pcTemp);
      safefree(pcBuffer);
   }
   else
   {
      PRINTF("hue-ExtractJsonElement(%2d):Too many JSON objects!" CRLF, iLevIdx);
   }
   return(iNr);
}

//
//  Function:   hue_GetDeviceByName
//  Purpose:    Find the name in one of the HUE Bridge Config structures
//
//  Parms:      HueCfg ptr, Name
//  Returns:    Hue Config ptr
//
static HUECFG *hue_GetDeviceByName(HUECFG *pstHue, char *pcName)
{
   int   x, iLen;

   for(x=0; x<MAX_HUE_DEVICES; x++)
   {
      iLen = GEN_STRLEN(pstHue->cName);
      if(iLen)
      {
         //PRINTF("hue-GetDeviceByName():Idt=%d [%s]" CRLF, x, pstHue->cName);
         if(GEN_STRNCMP(pcName, pstHue->cName, iLen) == 0) return(pstHue);
      }
      pstHue++;
   }
   PRINTF("hue-GetDeviceByName():[%s] NOT FOUND" CRLF, pcName);
   return(NULL);
}

//
//  Function:   hue_GetDeviceByDeviceId
//  Purpose:    Find the Device Id in one of the HUE Bridge Config structures
//
//  Parms:      HueCfg ptr, Device Id
//  Returns:    Hue Config ptr
//
static HUECFG *hue_GetDeviceByDeviceId(HUECFG *pstHue, char *pcDevId)
{
   int   x, iLen;

   for(x=0; x<MAX_HUE_DEVICES; x++)
   {
      iLen = GEN_STRLEN(pstHue->cDevId);
      if(iLen)
      {
         if(GEN_STRNCMP(pcDevId, pstHue->cDevId, iLen) == 0) return(pstHue);
      }
      pstHue++;
   }
   PRINTF("hue-GetDeviceByDeviceId():[%s] NOT FOUND" CRLF, pcDevId);
   return(NULL);
}

//
// Function:   hue_GetDeviceByIdt
// Purpose:    Get the device struct from the name Enum Idt
//
// Parms:      Hue Name ID (HUE_DUSK, HUE_DAWN, ..
// Returns:    Hue Struct ptr
// Note:       Name ID mostly refers to a HUE Scene
//
static HUECFG *hue_GetDeviceByIdt(HUENAME tName)
{
   char        cDevType;
   const char *pcName;
   HUECFG     *pstHueCfg;

   cDevType  = stHueBridgeNames[tName].cDevType;      // HUE_DEVICE_SCENE 
   pcName    = stHueBridgeNames[tName].pcName;        // "Garden-Dawn"
   pstHueCfg = HUE_GetDeviceConfig(cDevType);         // pstMap->G_stHueS
   //
   // Find the struct ptr 
   //
   PRINTF("hue-GetDeviceByIdt():Find Idt=%d (%c)=[%s]" CRLF, tName, cDevType, pcName);
   return(hue_GetDeviceByName(pstHueCfg, (char *)pcName));
}

//
// Function:   hue_GetDeviceTypeString
// Purpose:    Determine which devicetype we are dealing with (light, group, scene)
//
// Parms:      Hue type (l,g,s)
// Returns:    "lights, ..."
// Note:       
//
static const char *hue_GetDeviceTypeString(char cDevType)
{
   const char *pcDevType;
   
   switch(cDevType)
   {
      default:
      case HUE_DEVICE_LIGHT:
         pcDevType = pcBridgeLights;
         break;

      case HUE_DEVICE_GROUP:
         pcDevType = pcBridgeGroups;
         break;

      case HUE_DEVICE_SCENE:
         pcDevType = pcBridgeScenes;
         break;
   }
   return(pcDevType);
}

//
//  Function:  hue_GetPersistentStorage
//  Purpose:   Retrieve the persistent storage pointer for this variable
//
//  Parms:     Hue cfg ptr, Hue JS variables list ptr, Current device index
//  Returns:   Pointer to variable
//
static void *hue_GetPersistentStorage(HUECFG *pstHueCfg, const HUEJS *pstParm, int iDevIdx)
{
   HUECFG  *pstTmp;
   void    *pvValue=NULL;
   void    *pvEnd;

   pvEnd   = (void *)&(pstHueCfg[MAX_HUE_DEVICES]);
   pstTmp  = &(pstHueCfg[iDevIdx]);
   pvValue = (void *) pstTmp + pstParm->iOffset;
   if(pvValue >= pvEnd)
   {
      LOG_Report(0, "HUE", "hue-GetPersistentstorage():Variable for Device %d exceeds end of Hue Config data:Var=%p, End=%p", iDevIdx, pvValue, pvEnd);
      PRINTF("hue-GetPersistentstorage():Variable for Device %d exceeds end of Hue Config data:Var=%p, End=%p" CRLF, iDevIdx, pvValue, pvEnd);
      pvValue = NULL;
   }
   return(pvValue);
}

//
// Function:   hue_SetupWarningTimer
// Purpose:    Setup abs system secs warning timer (SecsNow plus the timer value)
//
// Parms:      
// Returns:    >0: Daytime   warning 
//             <0: Nighttime warning
//              0: No warnings to be given
// Note:       Set the global ulAbsSecsWarning
//
//             Motion at:    Hue Name           returns
//             ------------------------------------------
//              <Dawn      : HUE_WARN_NIGHT     -1
//               Dawn-Dusk : HUE_WARN_DAY       +1
//               Dusk-Dark : None                0
//              >Dark      : HUE_WARN_          -1
//
static int hue_SetupWarningTimer()
{
   int      iCc=0;
   u_int32  ulSecs;

   //
   // Check if we are in daytime or nighttime
   // Calculate seconds after midnight of this day
   //
   ulSecs = RTC_GetSystemSecs();
   if( (ulSecs < ulAbsSecsDawnSta) || (ulSecs > ulAbsSecsDarkSta) )
   {
      //
      // Now is Night time (Before Dawn or after Dark)
      // Setup warning time using the nightly duration
      //
      if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-SetupWarningTimer():Nighttime %ld secs" CRLF, pstMap->G_iSecsWarnNight);
      if(pstMap->G_iSecsWarnNight)
      {
         iCc = -1;
         ulAbsSecsWarning = ulSecs + pstMap->G_iSecsWarnNight;
      }
      else ulAbsSecsWarning = 0;
   }
   else
   {
      //
      // Now is Day time (Between Dawn and Dusk)
      // Setup warning time using the dayly duration
      //
      if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-SetupWarningTimer():Daytime %ld secs" CRLF, pstMap->G_iSecsWarnDay);
      if(pstMap->G_iSecsWarnDay)
      {
         iCc = +1;
         ulAbsSecsWarning = ulSecs + pstMap->G_iSecsWarnDay;
      }
      else ulAbsSecsWarning = 0;
   }
   return(iCc);
}

//
// Function:   hue_TriggerOutlet
// Purpose:    Trigger the outlet control 
//             
// Parms:      New On-Off state
// Returns:    TRUE if OK
// Note:       Control outlet as light HUE_OUTLET_1
//
static bool hue_TriggerOutlet(bool fOnOff)
{
   bool     fCc=FALSE;
   HUECFG  *pstHue;
   
   pstHue = hue_GetDeviceByIdt(HUE_OUTLET_1);
   if(pstHue)
   {
      PRINTF("hue-TriggerOutlet():ON" CRLF);
      pstHue->fChanged = TRUE;
      pstHue->fOn      = fOnOff;
      hue_ListDeviceConfig(pstHue);
      fCc = hue_UpdateBridgeDevice(pstHue);
   }
   return(fCc);
}

//
//  Function:  hue_UpdateBridgeDevices
//  Purpose:   Send updated data to all bridge devices of this type (if changed)
//
//  Parms:     Device type (l,g,s)
//  Returns:   TRUE if OKee
//
static bool hue_UpdateBridgeDevices(char cDevType)
{
   bool     fCc=FALSE;
   int      x;
   HUECFG  *pstHueCfg;
   HUECFG  *pstHue;

   pstHueCfg = HUE_GetDeviceConfig(cDevType);
   for(x=0; x<MAX_HUE_DEVICES; x++)
   {
      pstHue = &(pstHueCfg[x]);
      if(pstHue->fChanged)
      {
         fCc = hue_UpdateBridgeDevice(pstHue);
      }
   }
   return(fCc);
}

//
//  Function:  hue_UpdateBridgeDevice
//  Purpose:   Send updated data to this bridge device
//
//  Parms:     Hue struct
//  Returns:   TRUE if OKee
//  Note:      To update the HUE lights, 3 different pathways exist:
//                1. Update individual lights
//                2. Update whole groups
//                3. Update scenes
//
//             Each pathway has its own API driver to implement the update:
//             Lights:
//                PUT /api/<user-id>/lights/<id>/state
//                JSON: {"on":true, "bri":123, "hue":12345, "sat":123}
//                where: <user-id> is the 40 chars bridge user ID
//                       <id>      is the Light ID "1", "2",...
//             Groups:
//                PUT /api/<user-id>/groups/<id>/action
//                JSON: {"on":true, "bri":123, "hue":12345, "sat":123}
//                where: <user-id> is the 40 chars bridge user ID
//                       <id>      is the Group ID "1", "2",...
//
//             Scenes:
//                PUT /api/<user-id>/groups/0/action
//                JSON: {"scene":"<id>"}
//                where: <user-id> is the 40 chars bridge user ID
//                       <id>      is the 15 char Scene ID "xxxxxxxxxxxxxxxxxxxxx"
//
static bool hue_UpdateBridgeDevice(HUECFG *pstHue)
{
   bool  fCc=FALSE;

   if(pstHue)
   {
      PRINTF("hue-UpdateBridgeDevice():On=%d B=%3d H=%5d S=%3d, C=%5d %s" CRLF, 
                  pstHue->fOn, pstHue->iBri, pstHue->iHue, pstHue->iSat, pstHue->iCt, pstHue->cName);
      LOG_Report(0, "HUE", "hue-UpdateBridgeDevice():On=%d B=%3d H=%5d S=%3d C=%5d %s", 
                  pstHue->fOn, pstHue->iBri, pstHue->iHue, pstHue->iSat, pstHue->iCt, pstHue->cName);
      //
      fCc = hue_RunBridgeUpdate(pstHue);
      pstHue->fChanged = FALSE;
   }
   return(fCc);
}

//
// Function:   hue_UpdateConfigSettings
// Purpose:    Update various Hue config internal setting 
//
// Parms:      Top Hue config ptr
// Returns:    
// Note:       Internal settings:
//                - tColorMode
//
static void hue_UpdateConfigSettings(HUECFG *pstHue)
{
   int      x;

   for(x=0; x<MAX_HUE_DEVICES; x++, pstHue++)
   {
      if(GEN_STRNCMPI(     pstHue->cColMode, "hs", 2) == 0) pstHue->tColorMode = HUE_CMODE_HS;
      else if(GEN_STRNCMPI(pstHue->cColMode, "xy", 2) == 0) pstHue->tColorMode = HUE_CMODE_XY;
      else if(GEN_STRNCMPI(pstHue->cColMode, "ct", 2) == 0) pstHue->tColorMode = HUE_CMODE_CT;
      else                                                  pstHue->tColorMode = HUE_CMODE_OTHER;
   }
}

/*------  Local functions separator ------------------------------------
________________DRIVER_FUNTIONS(){};
------------------------------x----------------------------------------*/

//
// Function:   hue_DeviceDriverLights
// Purpose:    Build device dependent json object
//
// Parms:      Hue Config
// Returns:    JSON object struct or NULL if none needed
// Note:       
//
static RPIDATA *hue_DeviceDriverLights(HUECFG *pstHue)
{
   RPIDATA *pstObj;

   //
   // Build JSON object
   //
   pstObj = JSON_InsertParameter(NULL, NULL, NULL, JE_CURLB);
   //
   // Lights:
   //    PUT /api/<user-id>/lights/<id>/state
   //    JSON: {"on":true, "bri":123, "hue":12345, "sat":123}
   //    where: <user-id> is the 40 chars bridge user ID
   //           <id>      is the Light ID "1", "2",...
   //
   //PRINTF("hue-DeviceDriverLights():Devicename [%s] update:" CRLF, pstHue->cDevId);
   if(pstHue->fOn) 
   {
      //PRINTF("hue-DeviceDriverLights(): On :%d" CRLF, pstHue->fOn);
      pstObj = JSON_InsertBool(pstObj, "on", pstHue->fOn, JE_COMMA);
      if(pstHue->iBri >= 0) 
      {
         //PRINTF("hue-DeviceDriverLights():Bri:%d" CRLF, pstHue->iBri);
         pstObj = JSON_InsertInteger(pstObj, "bri", pstHue->iBri, JE_COMMA);
      }
      if(pstHue->iHue >= 0) 
      {
         //PRINTF("hue-DeviceDriverLights():Hue:%d" CRLF, pstHue->iHue);
         pstObj = JSON_InsertInteger(pstObj, "hue", pstHue->iHue, JE_COMMA);
      }
      if(pstHue->iSat >= 0) 
      {
         //PRINTF("hue-DeviceDriverLights():Sat:%d" CRLF, pstHue->iSat);
         pstObj = JSON_InsertInteger(pstObj, "sat", pstHue->iSat, JE_COMMA);
      }
      if(pstHue->iCt >= 0) 
      {
         //PRINTF("hue-DeviceDriverLights():Ct:%d" CRLF, pstHue->iCt);
         pstObj = JSON_InsertInteger(pstObj, "ct", pstHue->iCt, JE_COMMA);
      }
   }
   else
   {
      //PRINTF("hue-DeviceDriverLights(): On :%d" CRLF, pstHue->fOn);
      pstObj = JSON_InsertBool(pstObj, "on", pstHue->fOn, JE_COMMA);
   }
   pstObj = JSON_TerminateObject(pstObj);
   //
   //PRINTF("hue-DeviceDriverLights():JSON Object=%s" CRLF, pstObj->pcObject);
   return(pstObj);
}

//
// Function:   hue_DeviceDriverGroups
// Purpose:    Build device dependent json object
//
// Parms:      Hue Config
// Returns:    JSON object struct or NULL if none needed
// Note:       
//
static RPIDATA *hue_DeviceDriverGroups(HUECFG *pstHue)
{
   RPIDATA *pstObj;

   //
   // Build JSON object
   //
   pstObj = JSON_InsertParameter(NULL, NULL, NULL, JE_CURLB);
   //
   // Groups:
   //    PUT /api/<user-id>/groups/<id>/action
   //    JSON: {"on":true, "bri":123, "hue":12345, "sat":123}
   //    where: <user-id> is the 40 chars bridge user ID
   //           <id>      is the Group ID "1", "2",...
   //
   //PRINTF("hue-DeviceDriverGroups():Devicename [%s] update:" CRLF, pstHue->cDevId);
   if(pstHue->fOn) 
   {
      //PRINTF("hue-DeviceDriverGroups(): On :%d" CRLF, pstHue->fOn);
      pstObj = JSON_InsertBool(pstObj, "on", pstHue->fOn, JE_COMMA);
      if(pstHue->iBri >= 0) 
      {
         //PRINTF("hue-DeviceDriverGroups():Bri:%d" CRLF, pstHue->iBri);
         pstObj = JSON_InsertInteger(pstObj, "bri", pstHue->iBri, JE_COMMA);
      }
      if(pstHue->iHue >= 0) 
      {
         //PRINTF("hue-DeviceDriverGroups():Hue:%d" CRLF, pstHue->iHue);
         pstObj = JSON_InsertInteger(pstObj, "hue", pstHue->iHue, JE_COMMA);
      }
      if(pstHue->iSat >= 0) 
      {
         //PRINTF("hue-DeviceDriverGroups():Sat:%d" CRLF, pstHue->iSat);
         pstObj = JSON_InsertInteger(pstObj, "sat", pstHue->iSat, JE_COMMA);
      }
      if(pstHue->iCt >= 0) 
      {
         //PRINTF("hue-DeviceDriverGroups():Ct:%d" CRLF, pstHue->iCt);
         pstObj = JSON_InsertInteger(pstObj, "ct", pstHue->iCt, JE_COMMA);
      }
   }
   else
   {
      //PRINTF("hue-DeviceDriverGroups(): On :%d" CRLF, pstHue->fOn);
      pstObj = JSON_InsertBool(pstObj, "on", pstHue->fOn, JE_COMMA);
   }
   pstObj = JSON_TerminateObject(pstObj);
   //
   //PRINTF("hue-DeviceDriverGroups():JSON Object=%s" CRLF, pstObj->pcObject);
   return(pstObj);
}

//
// Function:   hue_DeviceDriverScenes
// Purpose:    Build device dependent json object
//
// Parms:      Hue Config
// Returns:    JSON object struct or NULL if none needed
// Note:       
//
static RPIDATA *hue_DeviceDriverScenes(HUECFG *pstHue)
{
   RPIDATA *pstObj;

   //
   // Build JSON object
   //
   pstObj = JSON_InsertParameter(NULL, NULL, NULL, JE_CURLB);
   //
   // Scenes:
   //    PUT /api/<user-id>/groups/0/action
   //    JSON: {"scene":"<id>"}
   //    where: <user-id> is the 40 chars bridge user ID
   //           <id>      is the 15 char Scene ID "xxxxxxxxxxxxxxxxxxxxx"
   //
   PRINTF("hue-DeviceDriverScenes():Scene:%s" CRLF, pstHue->cDevId);
   pstObj = JSON_InsertParameter(pstObj, "scene", pstHue->cDevId, JE_COMMA|JE_TEXT);
   pstObj = JSON_TerminateObject(pstObj);
   PRINTF("hue-DeviceDriverScenes():JSON Object=%s" CRLF, pstObj->pcObject);
   return(pstObj);
}

//
// Function:   hue_DeviceDriverScenes
// Purpose:    Build device dependent URL data
//
// Parms:      Hue Cfg, Device type, Dest, Dezt size, JSON Object
// Returns:    JSON object struct or NULL if none needed
// Note:       
//
static int hue_GenerateDeviceUrl(HUECFG *pstHue, char *pcUrl, int iMaxlen, RPIDATA *pstObj)
{
   int   iLen, iPaySize;
   char *pcUserId;
      
   pcUserId = GLOBAL_GetParameter(PAR_HUE_USER);
   iPaySize = JSON_GetObjectSize(pstObj);
   //
   switch(*pstHue->pcDevType)
   {
      default:
      case HUE_DEVICE_LIGHT:
         //
         //    HTTP PUT Commands Lights:
         //    PUT /api/xx...xxxxx/lights/<id>/state
         //
         //		{"on":true,"hue":20000}
         //
         //    Parms: <user-id>, <dev-id>, content length, json body
         //
         GEN_SNPRINTF(pcUrl, iMaxlen, pcHttpPutLights, pcUserId, pstHue->cDevId, iPaySize, pstObj->pcObject);
         break;

      case HUE_DEVICE_GROUP:
         //
         //    HTTP PUT Commands Groups
         //    PUT /api/xx...xxxxx/groups/<id>/action
         //    
         //		{"on":true,"hue":20000}
         //
         //    Parms: <user-id>, <dev-id>, content length, json body
         //
         GEN_SNPRINTF(pcUrl, iMaxlen, pcHttpPutGroups, pcUserId, pstHue->cDevId, iPaySize, pstObj->pcObject);
         break;

      case HUE_DEVICE_SCENE:
         //
         //    HTTP PUT Commands Scenes
         //    PUT /api/xx...xxxxx/groups/0/action
         //    
         //		{"scene":"xx...xxxxx"}
         //
         //    Parms: <user-id>, content length, json body
         //
         GEN_SNPRINTF(pcUrl, iMaxlen, pcHttpPutScenes, pcUserId, iPaySize, pstObj->pcObject);
         break;
   }
   iLen = GEN_STRLEN(pcUrl);
   return(iLen);
}

/*------  Local functions separator ------------------------------------
___________________HUE_COMMANDS(){};
------------------------------x----------------------------------------*/

//
//  Function:   hue_CommandIdle
//  Purpose:    
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandIdle()
{
   return(TRUE);
}

//
// Function:   hue_CommandUpdateMotion
// Purpose:    Motion detected
//             Turn on the appropriate lights for a period of secs
//
// Parms:
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_CommandUpdateMotion()
{
   switch(iRunMotionDetection)
   {
      case 0:
         // No motion ongoing: start
         iRunMotionDetection = 1;
         break;

      case 1:
         // Motion in progress, but not initiated yet, ignore updates
         break;

      default:
         // Motion in progress and already initiated
         iRunMotionDetection++;
         break;
   }
   if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-CommandUpdateMotion():Count=%d" CRLF, iRunMotionDetection);
   return(TRUE);
}

//
//  Function:   hue_CommandUpdateLights
//  Purpose:    
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandUpdateLights()
{
   bool  fCc;

   fCc = hue_UpdateBridgeDevices(HUE_DEVICE_LIGHT);
   return(fCc);
}

//
//  Function:   hue_CommandUpdateGroups
//  Purpose:    
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandUpdateGroups()
{
   bool  fCc;

   fCc = hue_UpdateBridgeDevices(HUE_DEVICE_GROUP);
   return(fCc);
}

//
//  Function:   hue_CommandUpdateScenes
//  Purpose:    
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandUpdateScenes()
{
   bool  fCc;

   fCc = hue_UpdateBridgeDevices(HUE_DEVICE_SCENE);
   return(fCc);
}

//
//  Function:   hue_CommandLightsMax
//  Purpose:    Turn on all lights on full strength
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandLightsMax()
{
   bool  fCc=FALSE;
   HUECFG  *pstHue;
   
   pstHue = hue_GetDeviceByIdt(HUE_WARN_NIGHT);
   if(pstHue)
   {
      PRINTF("hue-MotionTrigger():ON" CRLF);
      pstHue->fOn      = TRUE;
      pstHue->fChanged = TRUE;
      pstHue->iBri     = 254;
      pstHue->iHue     = HUE_HUE_COLOR_WHITE;
      pstHue->iSat     = HUE_SAT_COLOR_WHITE;
      pstHue->iCt      = 254;
      fCc = hue_UpdateBridgeDevice(pstHue);
   }
   else LOG_Report(0, "HUE", "hue-CommandLightsMax(): Warning group not found!");
   return(fCc);
}

//
//  Function:   hue_CommandLightsDef
//  Purpose:    Turn on all lights on default strength
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandLightsDef()
{
   return(FALSE);
}

//
//  Function:   hue_CommandLightsIrd
//  Purpose:    Turn on IR lights 
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandLightsIrd()
{
   return(FALSE);
}

//
//  Function:   hue_CommandWarningOff
//  Purpose:    Reset the warning lights
//
//  Parms:
//  Returns:    TRUE if OKee
//
static bool hue_CommandWarningOff()
{
   if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-CommandWarningOff()" CRLF);
   iRunMotionDetection = 0;
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
_____________________HUE_STATES(){};
------------------------------x----------------------------------------*/

//
//    A: HUE_STATE_STARTUP
//    B: HUE_STATE_DAWN_WAIT  (Lights are OFF)
//    C: HUE_STATE_DAWN_SET
//    c: HUE_STATE_DAWN_TMR
//    D: HUE_STATE_DAWN_OFF
//    E: HUE_STATE_DUSK_WAIT  (Lights are OFF)
//    F: HUE_STATE_DUSK_SET
//    G: HUE_STATE_DUSK_TMR
//    H: HUE_STATE_EVNG_SET
//    I: HUE_STATE_DARK_WAIT
//    O: HUE_STATE_DARK_SET   (Lights are OFF)
//
//                           DAWN DAWN                  DUSK     EVENING            DARK
//   00:00                   ON   OFF                    ON        ON                OFF    24:00
// ----+---------------------+-----+---------------------+---------+------------------+-------+----------->
// AB<---lights off--------->Cc    DE<--lights off------>FG        H                  IO<----lights off--->
// ----+---------------------+-----+---------------------+---------+------------------+-------+----------->
//                           |<--->|                     |<------->|                XX:XX
//                             G_iSecsTimerDawn            G_iSecsTimerDusk         G_pcHueTimeDark
//

//
// Function:   hue_StateIdle
// Purpose:    Hue state handler Idle state
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateIdle(const HUEFUN *pstState)
{
   return(TRUE);
}

//
// Function:   hue_StateStartup
// Purpose:    Hue state handler Startup
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateStartup(const HUEFUN *pstState)
{
   u_int32  ulSecs;

   ulSecs = RTC_GetSystemSecs();
   //
   // Set up correct bridge state
   //
   if(ulSecs < ulAbsSecsDawnSta)
   {
      // Before dawn
      tHueNewState = HUE_STATE_DARK_SET;
   }
   else if(ulSecs < ulAbsSecsDawnSto)
   {
      // During dawn
      tHueNewState = HUE_STATE_DAWN_SET;
   }
   else if(ulSecs < ulAbsSecsDuskSta)
   {
      // Daytime, between dawn and dusk
      tHueNewState = HUE_STATE_DAWN_OFF;
   }
   else if(ulSecs < ulAbsSecsDuskSto)
   {
      // During dusk
      tHueNewState = HUE_STATE_DUSK_SET;
   }
   else if(ulSecs < ulAbsSecsDarkSta)
   {
      // Rest of the evening
      tHueNewState = HUE_STATE_EVNG_SET;
   }
   else
   {
      // Reset of the night
      tHueNewState = HUE_STATE_DARK_SET;
   }
   LOG_Report(0, "HUE", "hue-StateStartup():Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   return(TRUE);
}

//
// Function:   hue_StateDawnWait
// Purpose:    Hue state handler waiting for DAWN
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateDawnWait(const HUEFUN *pstState)
{
   u_int32  ulSecs;

   if(iHueStateVerify) hue_ReportNewState(pstState, ulAbsSecsDawnSta);
   //
   ulSecs = RTC_GetSystemSecs();
   if(ulSecs >= ulAbsSecsDawnSta)  
   {
      tHueNewState = pstState->tNextState;
      LOG_Report(0, "HUE", "hue-StateDawnWait:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   }
   else hue_HandleOutlet(ulSecs);
   return(TRUE);
}

//
// Function:   hue_StateDawnSet
// Purpose:    Hue state handler Set DAWN lightning
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateDawnSet(const HUEFUN *pstState)
{
   bool  fCc;

   fCc = hue_Activation(HUE_DAWN, DEVICE_ON);
   hue_RestoreOutlet(TRUE);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_HUE_HUE_CFG);
   //
   // Dawn timer has been set already at midnight
   //
   iHueStateRetries = NUM_HUE_RETRIES;
   tHueOldState     = tHueNewState;
   tHueNewState     = pstState->tNextState;
   LOG_Report(0, "HUE", "hue-StateDawnSet:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   return(fCc);
}

//
// Function:   hue_StateDawnTimer
// Purpose:    Hue state handler DAWN timer
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       Dawn lights duration is already setup at midnight
//
static bool hue_StateDawnTimer(const HUEFUN *pstState)
{
   u_int32  ulSecs;

   if(iHueStateVerify) hue_ReportNewState(pstState, ulAbsSecsDawnSto);
   //
   ulSecs = RTC_GetSystemSecs();
   if(ulSecs >= ulAbsSecsDawnSto)  
   {
      tHueNewState = pstState->tNextState;
      LOG_Report(0, "HUE", "hue-StateDawnTimer:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   }
   else hue_HandleOutlet(ulSecs);
   return(TRUE);
}

//
// Function:   hue_StateDawnOff
// Purpose:    Hue state handler Set OFF DAWN lightning
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateDawnOff(const HUEFUN *pstState)
{
   bool  fCc;

   fCc = hue_Activation(HUE_DARK, DEVICE_OFF);
   hue_RestoreOutlet(FALSE);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_HUE_HUE_CFG);
   //
   iHueStateRetries = NUM_HUE_RETRIES;
   tHueOldState     = tHueNewState;
   tHueNewState     = pstState->tNextState;
   //
   LOG_Report(0, "HUE", "hue-StateDawnOff:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   return(fCc);
}

//
// Function:   hue_StateDuskWait
// Purpose:    Hue state handler waiting for DUSK
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateDuskWait(const HUEFUN *pstState)
{
   u_int32  ulSecs;

   if(iHueStateVerify) hue_ReportNewState(pstState, ulAbsSecsDuskSta);
   //
   ulSecs = RTC_GetSystemSecs();
   if(ulSecs >= ulAbsSecsDuskSta)
   {
      tHueNewState = pstState->tNextState;
      LOG_Report(0, "HUE", "hue-StateDuskWait:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   }
   else hue_HandleOutlet(ulSecs);
   return(TRUE);
}

//
// Function:   hue_StateDuskSet
// Purpose:    Hue state handler Set DUSK lightning
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateDuskSet(const HUEFUN *pstState)
{
   bool  fCc;

   fCc = hue_Activation(HUE_DUSK, DEVICE_ON);
   hue_RestoreOutlet(TRUE);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_HUE_HUE_CFG);
   //
   iHueStateRetries = NUM_HUE_RETRIES;
   tHueOldState     = tHueNewState;
   tHueNewState     = pstState->tNextState;
   LOG_Report(0, "HUE", "hue-StateDuskSet:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   return(fCc);
}

//
// Function:   hue_StateDuskTimer
// Purpose:    Hue state handler DUSK timer
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       Dusk lights duration is already setup at midnight
//
static bool hue_StateDuskTimer(const HUEFUN *pstState)
{
   u_int32  ulSecs;

   if(iHueStateVerify) hue_ReportNewState(pstState, ulAbsSecsDuskSto);
   //
   ulSecs = RTC_GetSystemSecs();
   if(ulSecs >= ulAbsSecsDuskSto)
   {
      tHueNewState = pstState->tNextState;
      LOG_Report(0, "HUE", "hue-StateDuskTimer:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   }
   else hue_HandleOutlet(ulSecs);
   return(TRUE);
}

//
// Function:   hue_StateEveningSet
// Purpose:    Hue state handler Turn Evening lightning
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateEveningSet(const HUEFUN *pstState)
{
   bool  fCc;

   fCc = hue_Activation(HUE_EVENING, DEVICE_ON);
   hue_RestoreOutlet(TRUE);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_HUE_HUE_CFG);
   //
   tHueOldState = tHueNewState;
   tHueNewState = pstState->tNextState;
   LOG_Report(0, "HUE", "hue-StateEveningSet:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   return(fCc);
}

//
// Function:   hue_StateDarkWait
// Purpose:    Hue state handler waiting for the moment to turn all lights OFF
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       HUE_DARK can be just before midnight OR just after midnight !
//                                                                                       
//                          DAWN     DAWN                   DUSK    EVENING             DARK 
//   00:00                   ON       OFF                    ON        ON                OFF    24:00
// <---+---------------------+---------+---------------------+---------+------------------+-------+------------>
// <-------lights off------->|         |<---lights off------>|         |                  |<-----lights off---->
// <---+---------------------+---------+---------------------+---------+------------------+-------+------------>
//     |                     |<------->|                     |<------->|                  |                 
//     |ulAbsSecsMidnite     |G_iSecsTimerDawn               |G_iSecsTimerDusk            | 
//     |                     |                                                            |
//     |<------------------->|                                                            |<---------------->
//     |G_pcHueTimeDark      |                                                            |G_pcHueTimeDark
//     |ulAbsSecsDarkSta     |                                                            |ulAbsSecsDarkSta
//                           |                                                            |
// <------------------------>|                                                            |<---------------->
//      Possible HUE_DARK                                                                  Possible HUE_DARK
//
static bool hue_StateDarkWait(const HUEFUN *pstState)
{
   u_int32  ulSecs;

   if(iHueStateVerify) hue_ReportNewState(pstState, ulAbsSecsDarkSta);
   //
   ulSecs = RTC_GetSystemSecs();
   if(ulSecs >= ulAbsSecsDarkSta)
   {
      tHueNewState = pstState->tNextState;
      LOG_Report(0, "HUE", "hue-StateDarkWait:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   }
   else hue_HandleOutlet(ulSecs);
   return(TRUE);
}

//
// Function:   hue_StateDarkSet
// Purpose:    Hue state handler to turn all lights OFF
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateDarkSet(const HUEFUN *pstState)
{
   bool  fCc;

   fCc = hue_Activation(HUE_DARK, DEVICE_OFF);
   hue_RestoreOutlet(FALSE);
   GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_HUE_HUE_CFG);
   //
   iHueStateRetries = NUM_HUE_RETRIES;
   tHueOldState     = tHueNewState;
   tHueNewState     = pstState->tNextState;
   LOG_Report(0, "HUE", "hue-StateDarkSet:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   return(fCc);
}

//
// Function:   hue_StateNewDay
// Purpose:    Hue state handler to set up for a new day
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       Make sure to setup all secs timers for the next day !
//             The STH has processed the HUE_DARK state just now, and we now are
//             between EVENING and DAWN.
//             
//             XXXX=Outlet On/Off checks
//     |XXXXXXXXXXXXXXXXXXXXX|XXXXXXXXX|XXXXXXXXXXXXXXXXXXXXX|XXXXXXXXX|XXXXXXXXXXXXXXXXXX|       |
//     |                     |         |                     |         |                  |       |
//     |                    DAWN      DAWN                  DUSK    EVENING              DARK     |
//   00:00                   ON       OFF                    ON        ON                 ON     24:00
// <---+---------------------+---------+---------------------+---------+------------------+-------+------------>
// <-------lights off------->|         |<---lights off------>|         |                  |<-----lights off---->
// <---+---------------------+---------+---------------------+---------+------------------+-------+------------>
//     |                     |<------->|                     |<------->|                  |                 
//     |ulAbsSecsMidnite     |G_iSecsTimerDawn               |G_iSecsTimerDusk            | 
//     |                     |                                                            |
//     |<------------------->|                                                            |<---------------->
//     |G_pcHueTimeDark      |                                                            |G_pcHueTimeDark
//     |ulAbsSecsDarkSta     |                                                            |ulAbsSecsDarkSta
//                           |                                                            |
// <------------------------>|                                                            |<---------------->
//      Possible HUE_DARK                                                                  Possible HUE_DARK
//
static bool hue_StateNewDay(const HUEFUN *pstState)
{
   if(iHueStateVerify) hue_ReportNewState(pstState, RTC_GetSystemSecs());
   //
   // We have already switched to HUE_DARK.
   // Setup the new day after we have received midnight notification (iCurrDay++)
   //
   if(iDarkDay != iCurrDay)
   {
      LOG_Report(0, "HUE", "hue-StateNewDay:From Day%d to %d", iDarkDay, iCurrDay);
      //
      // It is after midnight AND HUE_DARK has already been set
      // Day is over: set up new timers
      //
      hue_SetupNewDay();
      //
      iDarkDay     = iCurrDay;
      tHueNewState = pstState->tNextState;
      LOG_Report(0, "HUE", "hue-StateNewDay:Hue Bridge state is now %s", stHueStateFunctions[tHueNewState].pcHelp);
   }
   return(TRUE);
}

//
// Function:   hue_StateVerify
// Purpose:    Hue state handler to verify last selected state
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       A SET state has been executed just now, verify that the expected
//             action has been done properly. 
//             If (some) devices have been turned ON, verify that the config shows that. 
//             If all lights have been turn OFF, no devices should have stayed ON.
//             fDevice = TRUE means the device is in the proper state.
//
static bool hue_StateVerify(const HUEFUN *pstState)
{
   bool     fCc=TRUE;
   bool     fDevice=FALSE;
   bool     fGroup=DEVICE_ON;
   HUEDEV   tDev;

   tDev = hue_CheckDeviceStates();
   //
   switch(tHueOldName)
   {
      case HUE_DAWN:
      case HUE_DUSK:
      case HUE_EVENING:
         //
         // Some lights within the group should be ON
         //
         if( (tDev == HUEDEV_ALL_ON) || (tDev == HUEDEV_ANY_ON) ) 
         {
            PRINTF("hue-StateVerify:Some are ON" CRLF);
            fDevice = TRUE;
         }
         else PRINTF("hue-StateVerify:Some should be ON" CRLF);
         break;

      case HUE_DARK:
         //
         // All lights within the group MUST be OFF
         //
         if(tDev == HUEDEV_ALL_OFF) 
         {
            PRINTF("hue-StateVerify:All are OFF" CRLF);
            fDevice = TRUE;
            fGroup  = DEVICE_OFF;
         }
         else PRINTF("hue-StateVerify:All must be OFF" CRLF);
         break;

      default:
         //
         // Group not found
         //
         PRINTF("hue-StateVerify:Group not found" CRLF);
         if( (tDev == HUEDEV_NONE) || (tDev == HUEDEV_UNREACH) ) 
         {
            LOG_Report(0, "HUE", "hue-StateVerify:Group not found");
            fCc = FALSE;
         }
         break;
   }
   //
   // Check all entries of this group and turn each OFF if not already
   //
   if(fDevice)
   {
      //
      // All group devices are in the correct state
      //
      tHueNewState    = pstState->tNextState;
      iHueStateVerify = 1;
      PRINTF("hue-StateVerify:OKee" CRLF);
      LOG_Report(0, "HUE", "hue-StateVerify:OKee");
   }
   else
   {
      if(iHueStateRetries)
      {
         PRINTF("hue-StateVerify():Group still has lights ON: Retry %d" CRLF, iHueStateRetries);
         LOG_Report(0, "HUE", "hue-StateVerify():Group still has lights ON: Retry %d", iHueStateRetries);
         fCc = hue_Activation(tHueOldName, fGroup);
         GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_HUE_HUE_CFG);
         iHueStateRetries--;
         iHueStateVerify = -1;
      }
      else
      {
         tHueNewState    = pstState->tNextState;
         iHueStateVerify = 1;
         PRINTF("hue-StateVerify():ERROR: Group still has lights ON" CRLF);
         LOG_Report(0, "HUE", "hue-StateVerify():ERROR: Group still has lights ON");
         fCc = FALSE;
      }
   }
   return(fCc);
}

//
// Function:   hue_StateWarnSet
// Purpose:    Hue state handler to turn warning lights ON
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateWarnSet(const HUEFUN *pstState)
{
   bool     fCc=TRUE;

   switch(hue_SetupWarningTimer())
   {
      case -1:
         //
         // Night time (Before Dawn or after Dusk)
         //
         if(RPI_GetFlag(FLAGS_LOG_HUEURL)) LOG_Report(0, "HUE", "hue-StateWarnSet():Night");
         if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-StateWarnSet():Night" CRLF);
         //
         fCc = hue_Activation(HUE_WARN_NIGHT, DEVICE_ON);
         iHueStateRetries = NUM_HUE_RETRIES;
         tHueNewState = pstState->tNextState;
         break;

      case +1:
         //
         // Day time (Between Dawn and Dusk)
         //
         if(RPI_GetFlag(FLAGS_LOG_HUEURL)) LOG_Report(0, "HUE", "hue-StateWarnSet():Day");
         if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-StateWarnSet():Day" CRLF);
         //
         fCc = hue_Activation(HUE_WARN_DAY, DEVICE_ON);
         iHueStateRetries = NUM_HUE_RETRIES;
         tHueNewState = pstState->tNextState;
         break;

      default:
         if(RPI_GetFlag(FLAGS_LOG_HUEURL)) LOG_Report(0, "HUE", "hue-StateWarnSet():Warnings are OFF");
         if(RPI_GetFlag(FLAGS_PRN_HUEURL)) LOG_printf("hue-StateWarnSet():Warnings are OFF" CRLF);
         //
         iHueStateRetries    = NUM_HUE_RETRIES;
         iRunMotionDetection = 0;
         tHueNewState        = tHueOldState;
         break;
   }
   LOG_Report(0, "HUE", "hue-StateWarnSet:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   return(fCc);
}

//
// Function:   hue_StateWarnWait
// Purpose:    Hue state handler to time the warning lights ON
//
// Parms:      HUEFUN *
// Returns:    TRUE if OKee
// Note:       
//
static bool hue_StateWarnWait(const HUEFUN *pstState)
{
   u_int32  ulSecs;

   ulSecs = RTC_GetSystemSecs();
   if(ulSecs >= ulAbsSecsWarning)
   {
      //
      // Warning done: restore previous lights
      //
      iRunMotionDetection = 0;
      tHueNewState        = tHueOldState;
      //
      LOG_Report(0, "HUE", "hue-StateWarnWait:Turn OFF warning lights");
      LOG_Report(0, "HUE", "hue-StateWarnWait:Hue Bridge state is %s", stHueStateFunctions[tHueNewState].pcHelp);
   }
   return(TRUE);
}


/*------  Local functions separator ------------------------------------
_______________SIGNAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
//  Function:   hue_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void hue_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "HUE", "hue-ReceiveSignalTerm()");
   fHueRunning = FALSE;
   GLOBAL_SemaphorePost(PID_HUE);
}

//
//  Function:   hue_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void hue_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "HUE", "hue-ReceiveSignalInt()");
   fHueRunning = FALSE;
   GLOBAL_SemaphorePost(PID_HUE);
}

//
//  Function:   hue_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void hue_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

//
//  Function:   hue_ReceiveSignalPipe
//  Purpose:    Add the SIGPIPE command in the buffer (Broken pipe)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void hue_ReceiveSignalPipe(int iSignal)
{
   LOG_Report(errno, "HUE", "hue-ReceiveSignalPipe()");
}

//
// Function:   hue_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void hue_ReceiveSignalUser1(int iSignal)
{
   GLOBAL_SemaphorePost(PID_HUE);
}

//
// Function:   hue_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void hue_ReceiveSignalUser2(int iSignal)
{
}

//
//  Function:   hue_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool hue_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &hue_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "HUE", "hue-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &hue_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "HUE", "hue-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &hue_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "HUE", "hue-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &hue_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "HUE", "hue-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &hue_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "HUE", "hue-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // Pipe error
  if( signal(SIGPIPE, &hue_ReceiveSignalPipe) == SIG_ERR)
  {
     LOG_Report(errno, "HUE", "hue-ReceiveSignalPipe(): SIGPIPE ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGPIPE);
  }
  return(fCC);
}

/*------  Local functions separator ------------------------------------
___________________CONFIG_DEBUG(){};
------------------------------x----------------------------------------*/

//
// Function:   hue_LogJsonObjects 
// Purpose:    Log all JSON elements in this object to LOG file 
//
// Parms:      Object
// Returns:    Nr of elements in this object
// Note:       LOG_Report(-1, ...) does NOT add a trailing CRLF
//
static int hue_LogJsonObjects(char *pcObj)
{
   bool     fOk, fValue;
   int      iIdx, iNr, iValue, iTotal=0;
   double   flValue;
   char    *pcObjNew;
   char    *pcBuffer;
   char    *pcValue;

   pcBuffer = safemalloc(MAX_ARG_LENZ);
   pcValue  = safemalloc(MAX_ARG_LENZ);
   //
   LOG_Report(0, "HUE", "____CURRENT OBJECT____\n%s\n", pcObj); 
   //
   iNr = GEN_JsonGetNumberOfElements(pcObj);
   LOG_Report(0, "HUE", "(--):New Object has %2d elements__________________________________", iNr);
   for(iIdx=0; iIdx<iNr; iIdx++)
   {
      if( (fOk = GEN_JsonGetElementName(pcObj, iIdx, pcBuffer, MAX_ARG_LEN, INCLUDE_DQ)) )
      {
         PRINTF("hue-LogJsonObjects():Element=[%s]" CRLF, pcBuffer);
         switch(GEN_JsonGetObjectType(pcObj, iIdx))
         {
            default:
            case JSON_TYPE_NONE:
               LOG_Report(0, "HUE", "(%02d):ERROR: Type=None[%s]", iIdx, pcBuffer);
               break;

            case JSON_TYPE_OBJECT:
               iTotal++;
               LOG_Report(0, "HUE", "(%02d):Type=Object[%s]", iIdx, pcBuffer);
               if( (pcObjNew = GEN_JsonGetElementObject(pcObj, 0, iIdx)) )
               {
                  iTotal += hue_LogJsonObjects(pcObjNew);
                  LOG_Report(0, "HUE", "(--):End of Object_______________________________________________\n");
               }
               else
               {
                  LOG_Report(0, "HUE", "(%02d):Type=Object[%s]:ERROR: No more objects !", iIdx, pcBuffer);
               }
               break;

            case JSON_TYPE_ARRAY:
               iTotal++;
               LOG_Report(0, "HUE", "(%02d):Type=Array[%s]", iIdx, pcBuffer);
               break;

            case JSON_TYPE_BOOL:
               iTotal++;
               if( GEN_JsonGetBoolean(pcObj, pcBuffer, &fValue))
               {
                  LOG_Report(0, "HUE", "(%02d):Type=Bool[%s]=%d", iIdx, pcBuffer, fValue);
               }
               else
               {
                  LOG_Report(0, "HUE", "(%02d):Type=Bool[%s] has no value", iIdx, pcBuffer);
               }
               break;

            case JSON_TYPE_STRING:
               iTotal++;
               if( GEN_JsonGetStringCopy(pcObj, pcBuffer, pcValue, MAX_ARG_LEN))
               {
                  LOG_Report(0, "HUE", "(%02d):Type=String[%s]=[%s]", iIdx, pcBuffer, pcValue);
               }
               else
               {
                  LOG_Report(0, "HUE", "(%02d):Type=String[%s] has no value", iIdx, pcBuffer);
               }
               break;

            case JSON_TYPE_INTEGER:
               iTotal++;
               if( GEN_JsonGetInteger(pcObj, pcBuffer, &iValue))
               {
                  LOG_Report(0, "HUE", "(%02d):Type=Integer[%s]=%d", iIdx, pcBuffer, iValue);
               }
               else
               {
                  LOG_Report(0, "HUE", "(%02d):Type=Integer[%s] has no value", iIdx, pcBuffer);
               }
               break;

            case JSON_TYPE_FLOAT:
               iTotal++;
               if( GEN_JsonGetDouble(pcObj, pcBuffer, &flValue))
               {
                  LOG_Report(0, "HUE", "(%02d):Type=Float[%s]=%.3f", iIdx, pcBuffer, flValue);
               }
               else
               {
                  LOG_Report(0, "HUE", "(%02d):Type=Float[%s] has no value", iIdx, pcBuffer);
               }
               break;
         }
      }
      else
      {
         LOG_Report(0, "HUE", "(%02d):ERROR: Not an element!", iIdx);
      }
   }
   safefree(pcValue);
   safefree(pcBuffer);
   return(iTotal);
}


//
//  Function:  hue_LogConfiguration
//  Purpose:   Log all used configs if changed
//
//  Parms:     Device type
//  Returns:   
//
static void hue_LogConfiguration(char cDevType)
{
   int      x;
   HUECFG  *pstHueCfg;
   HUECFG  *pstHue;

   pstHueCfg = HUE_GetDeviceConfig(cDevType);
   for(x=0; x<MAX_HUE_DEVICES; x++)
   {
      pstHue = &(pstHueCfg[x]);
      //
      // Log the config item if the checksum has changed
      //
      if( !hue_SumConfiguration(pstHue))
      {
         if(RPI_GetFlag(FLAGS_LOG_BINCFG)) LOG_ListData("hue-LogConfiguration():", (char *)pstHue, sizeof(HUECFG));
         //
         LOG_Report(0, "HUE", "HUE Bridge Configuration():Index=%2d [%s]", x, pstHue->pcDevType);
         LOG_Report(0, "HUE", "Reachable   : %d", pstHue->fReach);
         LOG_Report(0, "HUE", "On          : %d", pstHue->fOn);
         LOG_Report(0, "HUE", "Changed     : %d", pstHue->fChanged);
         LOG_Report(0, "HUE", "Bri         : %d", pstHue->iBri);
         LOG_Report(0, "HUE", "Hue         : %d", pstHue->iHue);
         LOG_Report(0, "HUE", "Sat         : %d", pstHue->iSat);
         LOG_Report(0, "HUE", "Ct          : %d", pstHue->iCt);
         LOG_Report(0, "HUE", "CtMode      : %d", pstHue->tColorMode);
         LOG_Report(0, "HUE", "CtMin       : %d", pstHue->iCtMin);
         LOG_Report(0, "HUE", "CtMax       : %d", pstHue->iCtMax);
         LOG_Report(0, "HUE", "Device Id   : %s", pstHue->cDevId);
         LOG_Report(0, "HUE", "Unique Id   : %s", pstHue->cUniqId);
         LOG_Report(0, "HUE", "Name        : %s", pstHue->cName);
         LOG_Report(0, "HUE", "Group       : %s", pstHue->cGroup);
         LOG_Report(0, "HUE", "Lights      : %s", pstHue->cLights);
         LOG_Report(0, "HUE", "Model       : %s", pstHue->cModel);
         LOG_Report(0, "HUE", "Effect      : %s", pstHue->cEffect);
         LOG_Report(0, "HUE", "Mode        : %s", pstHue->cMode);
         LOG_Report(0, "HUE", "Color Mode  : %s", pstHue->cColMode);
         LOG_Report(0, "HUE", "Type        : %s", pstHue->cType);
         LOG_Report(0, "HUE", "");
      }
   }
}

//
// Function:   hue_SumConfiguration
// Purpose:    Checksum configuration
//
// Parms:      Config ptr
// Returns:    TRUE if checksum okee
// Note:       Updates the data with the correct checksum as well. 
//
static bool hue_SumConfiguration(HUECFG *pstHueCfg)
{
   int      x, iOldSum, iSum=0;
   int      iLen=sizeof(HUECFG);
   u_int8  *pubData=(u_int8 *)pstHueCfg;

   iOldSum              = pstHueCfg->iChecksum; 
   pstHueCfg->iChecksum = 0; 
   //
   for(x=0; x<iLen; x++)
   {
      iSum += (int) *pubData++;
   }
   pstHueCfg->iChecksum = iSum;
   return((iSum == iOldSum));
}

//
//  Function:  hue_ListModConfigurations
//  Purpose:   List all used configs to STDOUT if changed
//
//  Parms:     Device type
//  Returns:   
//
static void hue_ListModConfigurations(char cDevType)
{
   bool     fSum;
   int      x;
   HUECFG  *pstHueCfg;
   HUECFG  *pstHue;

   pstHueCfg = HUE_GetDeviceConfig(cDevType);
   for(x=0; x<MAX_HUE_DEVICES; x++)
   {
      pstHue = &(pstHueCfg[x]);
      fSum   = hue_SumConfiguration(pstHue);
      //
      if(!fSum)
      {
         LOG_printf("hue-ListModConfigurations():Index=%2d [%c] Bad Checksum" CRLF, x, cDevType);
         LOG_DumpData("hue-ListModConfigurations():", (char *)pstHue, sizeof(HUECFG));
      }
      else if(pstHue->iChecksum)
      {
         LOG_printf("hue-ListModConfigurations():Index=%2d [%c]" CRLF, x, cDevType);
         hue_ListDeviceConfig(pstHue);
      }
   }
}

//
//  Function:  hue_ListAllConfigurations
//  Purpose:   Unconditionally list all used configs to STDOUT
//
//  Parms:     Device type
//  Returns:   
//
static void hue_ListAllConfigurations(char cDevType)
{
   int      x;
   HUECFG  *pstHueCfg;
   HUECFG  *pstHue;

   pstHueCfg = HUE_GetDeviceConfig(cDevType);
   for(x=0; x<MAX_HUE_DEVICES; x++)
   {
      pstHue = &(pstHueCfg[x]);
      LOG_printf("hue-ListAllConfigurations():Index=%2d [%c]" CRLF, x, cDevType);
      hue_ListDeviceConfig(pstHue);
   }
}

//
//  Function:  hue_ListDeviceConfig
//  Purpose:   List single device config data
//
//  Parms:     Device config
//  Returns:   
//
static void hue_ListDeviceConfig(HUECFG *pstHue)
{
   if(pstHue)
   {
      LOG_printf("Device      : %s" CRLF, pstHue->pcDevType);
      LOG_printf("Reachable   : %d" CRLF, pstHue->fReach);
      LOG_printf("On          : %d" CRLF, pstHue->fOn);
      LOG_printf("Changed     : %d" CRLF, pstHue->fChanged);
      LOG_printf("Bri         : %d" CRLF, pstHue->iBri);
      LOG_printf("Hue         : %d" CRLF, pstHue->iHue);
      LOG_printf("Sat         : %d" CRLF, pstHue->iSat);
      LOG_printf("Ct          : %d" CRLF, pstHue->iCt);
      LOG_printf("CtMin       : %d" CRLF, pstHue->iCtMin);
      LOG_printf("CtMax       : %d" CRLF, pstHue->iCtMax);
      LOG_printf("Device Id   : %s" CRLF, pstHue->cDevId);
      LOG_printf("Unique Id   : %s" CRLF, pstHue->cUniqId);
      LOG_printf("Name        : %s" CRLF, pstHue->cName);
      LOG_printf("Group       : %s" CRLF, pstHue->cGroup);
      LOG_printf("Lights      : %s" CRLF, pstHue->cLights);
      LOG_printf("Model       : %s" CRLF, pstHue->cModel);
      LOG_printf("Effect      : %s" CRLF, pstHue->cEffect);
      LOG_printf("Mode        : %s" CRLF, pstHue->cMode);
      LOG_printf("Color Mode  : %s" CRLF, pstHue->cColMode);
      LOG_printf("Type        : %s" CRLF, pstHue->cType);
   }
}

/*------  Local functions separator ------------------------------------
__________________COMMENTED_OUT(){};
------------------------------x----------------------------------------*/

#ifdef COMMENTED_OUT

//
// Function:   hue_BackupReadConfig
// Purpose:    Backup the device type configuration and read current bridge config
//
// Parms:      Device type
// Returns:    New backup struct ptr
// Note:       Backup size is the complete config (MAX_HUE_DEVICES)
//
static HUECFG *hue_BackupReadConfig(char cDevType)
{
   HUECFG *pstHueBup;
   HUECFG *pstHueCfg;
   
   pstHueBup = safemalloc(sizeof(HUECFG) * MAX_HUE_DEVICES);
   pstHueCfg = HUE_GetDeviceConfig(cDevType);
   if(pstHueCfg)
   { 
      //
      // Backup config
      //
      GEN_MEMCPY(pstHueBup, pstHueCfg, sizeof(HUECFG) * MAX_HUE_DEVICES);
      //
      // Read current bridge config
      //
      if( !hue_ReadBridgeConfig(cDevType) )
      {
         LOG_Report(0, "HUE", "hue-BackupConfig(): Error reading config (%c)", cDevType);
         safefree(pstHueBup);
         pstHueBup = NULL;
      }
   }
   return(pstHueBup);
}

//
//  Function:  hue_ListDevice
//  Purpose:   List single device
//
//  Parms:     Device config string (f.i. "G3")
//  Returns:   
//
static void hue_ListDevice(char *pcDev)
{
   HUECFG *pstHue;

   pstHue = HUE_GetDeviceStructure(pcDev);
   hue_ListDeviceConfig(pstHue);
}

//
// Function:   hue_MotionTrigger
// Purpose:    Motion trigger
//
// Parms:      
// Returns:    TRUE if OKee
//
static bool hue_MotionTrigger()
{
   bool     fCc=FALSE;
   HUECFG  *pstHue;
   
   pstHue = hue_GetDeviceByIdt(HUE_GARDEN_DOOR);
   if(pstHue)
   {
      PRINTF("hue-MotionTrigger():ON" CRLF);
      pstHue->fChanged = TRUE;
      pstHue->iBri     = 254;
      pstHue->iHue     = HUE_HUE_COLOR_WHITE;
      pstHue->iSat     = HUE_SAT_COLOR_WHITE;
      pstHue->iCt      = 254;
      fCc = hue_UpdateBridgeDevice(pstHue);
   }
   return(fCc);
}

#endif   //COMMENTED_OUT


