/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           par_defs.h
 *  Purpose:            All defines for extracting rpi motion and archive data
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    12 Jun 2021:      Created
 *    20 Oct 2021:      Add HUE Timers
 *    12 Nov 2021:      Remove mail client
 *    13 Nov 2021:      Add external msmtp mail client
 *    30 Nov 2021:      Add Ip/Mac data
 *    10 Dec 2021:      Add Core temperature and Wifi Ping flag
 *    01 Jan 2022:      Add Ping Camera
 *    12 Jan 2022:      Add motion thresholds
 *    12 Oct 2023:      Flexible WAN retrieval
 *    09 Dec 2024:      Add Outlet parms
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// Macro elements:                  Example
// 
//       a  Enum              x     PAR_VERSION
//       b  iFunction         x     WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL
//       c  pcJson            x     "Version"
//       d  pcHtml            x     "vrs="
//       e  pcOption          x     "-v"
//       f  iValueOffset      x     offsetof(RPIMAP, G_pcSwVersion)
//       g  iValueSize        x     MAX_PARM_LEN
//       h  iChangedOffset    x     ALWAYS_CHANGED or offsetof(RPIMAP, G_ubVersionChanged)
//       i  iChanged          x     0
//       j  pcDefault         x     "v1.00-cr103"
//       k  pfHttpFun         x     http_CollectDebug
//
// Note: 
//       WB    (WarmBoot)  : the default value is restored on every restart of the App.
//       PAR_A (Ascii)     : ASCII parameter
//       PAR_F (Float)     : Floating point parameter (double representation)
//       PAR_B (Bcd)       : Integer parameter (BCD representation) 
//       PAR_H (Hex)       : Integer parameter (HEX representation) 
//
//       JSN_TXT           : JSON Text representation    ("Variable")
//       JSN_INT           : JSON Number representation  ( Variable )
//
//
// Macro    a                 b                                                                    c                 d        e        f                                      g                  h                                     i           j                        k
//          Enum,             ____iFunction___________________________________________________     pcJson,           pcHtml   pcOption iValueOffset                           iValueSize        iChangedOffset                         iChanged    pcDefault                pfHttpFun
//                               Txt/Int
EXTRACT_PAR(PAR_COMMAND,     (WB|PAR_A|JSN_TXT|PAR_ALL|PAR____|PAR____|PAR____|PAR____|PAR____),   "Command",        "cmd=",  "",      offsetof(RPIMAP, G_pcCommand),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubCommandChanged),  0,          "",                      NULL)
EXTRACT_PAR(PAR_HOSTNAME,    (   PAR_A|JSN_TXT|PAR_ALL|PAR____|PAR____|PAR____|PAR____|PAR____),   "Hostname",       "",      "",      offsetof(RPIMAP, G_pcHostname),        MAX_URL_LEN,      ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_VERSION,     (WB|PAR_A|JSN_TXT|PAR_ALL|PAR____|PAR____|PAR____|PAR____|PAR____),   "Version",        "vrs=",  "",      offsetof(RPIMAP, G_pcVersionSw),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,          VERSION,                 NULL)
EXTRACT_PAR(PAR_WWW_DIR,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____),   "WwwDir",         "",      "",      offsetof(RPIMAP, G_pcWwwDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          RPI_PUBLIC_WWW,          NULL)
EXTRACT_PAR(PAR_PING_MAC,    (   PAR_A|JSN_TXT|PAR____|PAR_ERR|PAR____|PAR____|PAR____|PAR____),   "PingMac",        "",      "",      offsetof(RPIMAP, G_pcPingMac),         MAX_MAC_LEN,      NEVER_CHANGED,                         0,          "",                      NULL)
EXTRACT_PAR(PAR_PING_IP,     (   PAR_A|JSN_TXT|PAR____|PAR_ERR|PAR____|PAR____|PAR____|PAR____),   "PingIp",         "",      "",      offsetof(RPIMAP, G_pcPingIp),          MAX_ADDR_LEN,     NEVER_CHANGED,                         0,          "",                      NULL)
EXTRACT_PAR(PAR_PING_CAM,    (   PAR_A|JSN_TXT|PAR____|PAR_ERR|PAR____|PAR____|PAR____|PAR____),   "PingCam",        "",      "",      offsetof(RPIMAP, G_pcPingCam),         MAX_ADDR_LEN,     NEVER_CHANGED,                         0,          "",                      NULL)
EXTRACT_PAR(PAR_HOST_IP,     (WB|PAR_A|JSN_TXT|PAR____|PAR_ERR|PAR____|PAR____|PAR____|PAR____),   "HostIp",         "",      "",      offsetof(RPIMAP, G_pcHostIp),          MAX_ADDR_LEN,     NEVER_CHANGED,                         0,          "",                      NULL)
EXTRACT_PAR(PAR_HOST_PORT,   (WB|PAR_A|JSN_TXT|PAR____|PAR_ERR|PAR____|PAR____|PAR____|PAR____),   "HostPort",       "",      "",      offsetof(RPIMAP, G_pcHostPort),        MAX_PARM_LEN,     NEVER_CHANGED,                         0,          "8080",                  NULL)
EXTRACT_PAR(PAR_DEFAULT,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____),   "Default",        "",      "",      offsetof(RPIMAP, G_pcDefault),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          RPI_PUBLIC_DEFAULT,      NULL)
EXTRACT_PAR(PAR_CORE_TEMP,   (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____),   "CoreTemp",       "",      "",      offsetof(RPIMAP, G_flCpuTemperature),  sizeof(double),   ALWAYS_CHANGED,                        0,          "0.0",                   NULL)
EXTRACT_PAR(PAR_WIFI_NOPING, (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____),   "WifiNoPing",     "",      "",      offsetof(RPIMAP, G_iSkipWifiPings),    sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_HERON_SECS,  (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_MOT),   "Heron",          "",      "",      offsetof(RPIMAP, G_iHeronSecs),        sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     http_CollectHeron)
EXTRACT_PAR(PAR_WAN_URL,     (   PAR_A|JSN_TXT|PAR_ALL|PAR____|PAR____|PAR____|PAR____|PAR____),   "WanUrl",         "",      "",      offsetof(RPIMAP, G_pcWanUrl),          MAX_URL_LEN,      ALWAYS_CHANGED,                        0,          "4.icanhazip.com",       NULL)
//
EXTRACT_PAR(PAR_THOLD_MAG,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_MOT),   "TholdMag",       "",      "",      offsetof(RPIMAP, G_iTholdMag),         sizeof(int),      ALWAYS_CHANGED,                        0,          "10",                    NULL)
EXTRACT_PAR(PAR_THOLD_CNT,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_MOT),   "TholdCnt",       "",      "",      offsetof(RPIMAP, G_iTholdCnt),         sizeof(int),      ALWAYS_CHANGED,                        0,          "25",                    NULL)
EXTRACT_PAR(PAR_THOLD_MOT,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_MOT),   "TholdMot",       "",      "",      offsetof(RPIMAP, G_iTholdMot),         sizeof(int),      ALWAYS_CHANGED,                        0,          "300",                   NULL)
//
// PiKrellCam parameters:
// Run State Variables
EXTRACT_PAR(PAR_MIN_DAWN,    (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR____|PAR____),   "MinDawn",        "",      "",      offsetof(RPIMAP, G_iMinuteDawn),       sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_MIN_SRISE,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR____|PAR____),   "MinSunrise",     "",      "",      offsetof(RPIMAP, G_iMinuteSunrise),    sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_MIN_SSET,    (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR____|PAR____),   "MinSunset",      "",      "",      offsetof(RPIMAP, G_iMinuteSunset),     sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_MIN_DUSK,    (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR____|PAR____),   "MinDusk",        "",      "",      offsetof(RPIMAP, G_iMinuteDusk),       sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_RUN_STATE,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR____|PAR____),   "RunState",       "",      "",      offsetof(RPIMAP, G_iRunState),         sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
//
EXTRACT_PAR(PAR_CALLER,      (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "Caller",         "",      "",      offsetof(RPIMAP, G_pcCaller),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_LOG,         (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "LogFile",        "",      "",      offsetof(RPIMAP, G_pcLogFile),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_FIFO,        (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "Fifo",           "",      "",      offsetof(RPIMAP, G_pcFifo),            MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_MEDIA,       (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "MediaDir",       "",      "",      offsetof(RPIMAP, G_pcMediaDir),        MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_ARCHIVE,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "ArchDir",        "",      "",      offsetof(RPIMAP, G_pcArchiveDir),      MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_VIDEO,       (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "VideoDir",       "",      "",      offsetof(RPIMAP, G_pcVideoDir),        MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_THUMB,       (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "ThumbDir",       "",      "",      offsetof(RPIMAP, G_pcThumbDir),        MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_LOOP,        (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "LoopDir",        "",      "",      offsetof(RPIMAP, G_pcLoopDir),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_STILL,       (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "StillDir",       "",      "",      offsetof(RPIMAP, G_pcStillDir),        MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_TIMELAPSE,   (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR____),   "TimelDir",       "",      "",      offsetof(RPIMAP, G_pcTimelapseDir),    MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_REC_FILE,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "RecFile",        "",      "",      offsetof(RPIMAP, G_pcRecordingFile),   MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_LAST_VID,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "VideoFile",      "",      "",      offsetof(RPIMAP, G_pcLastVideoFile),   MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_LAST_THB,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "ThumbFile",      "",      "",      offsetof(RPIMAP, G_pcLastThumbFile),   MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_LAST_PIX,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "StillFile",      "",      "",      offsetof(RPIMAP, G_pcLastStillFile),   MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_LOOP_FILE,   (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "LoopFile",       "",      "",      offsetof(RPIMAP, G_pcLoopMotionFile),  MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_MOT_STATE,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "MotState",       "",      "",      offsetof(RPIMAP, G_iMotionState),      sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_ARC_COUNT,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "ArcCount",       "",      "",      offsetof(RPIMAP, G_iArchiveCount),     sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_VERBOSE,     (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "Verbose",        "",      "",      offsetof(RPIMAP, G_iVerbose),          sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_FREEARCH,    (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PIX|PAR_MOT),   "FreeArch",       "",      "",      offsetof(RPIMAP, G_flFreeArchive),     sizeof(double),   ALWAYS_CHANGED,                        0,          "20.0",                  NULL)
// Hue data
EXTRACT_PAR(PAR_HUE_CMD,     (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_HUE|PAR____),   "HueCmd",         "cmd=",  "",      offsetof(RPIMAP, G_iHueCmd),           sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     http_CollectHueCommand)
EXTRACT_PAR(PAR_HUE_DEVICE,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_HUE|PAR____),   "HueDev",         "dev=",  "",      offsetof(RPIMAP, G_pcHueDevice),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_HUE_IP,      (   PAR_A|JSN_TXT|PAR____|PAR____|PAR_ERR|PAR____|PAR_HUE|PAR____),   "HueIp",          "ip=",   "",      offsetof(RPIMAP, G_pcHueIp),           MAX_ADDR_LEN,     NEVER_CHANGED,                         0,          RPI_HUE_IP,              NULL)
EXTRACT_PAR(PAR_HUE_PORT,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR_ERR|PAR____|PAR_HUE|PAR____),   "HuePort",        "port=", "",      offsetof(RPIMAP, G_pcHuePort),         MAX_PARM_LEN,     NEVER_CHANGED,                         0,          "80",                    NULL)
EXTRACT_PAR(PAR_HUE_USER,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_HUE|PAR____),   "HueUser",        "user=", "",      offsetof(RPIMAP, G_pcHueUser),         MAX_USER_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_HUE_BRI,     (WB|PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_HUE|PAR____),   "BridgeBri",      "bri=",  "",      offsetof(RPIMAP, G_iHueBri),           sizeof(int),      ALWAYS_CHANGED,                        0,          "-1",                    NULL)
EXTRACT_PAR(PAR_HUE_HUE,     (WB|PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_HUE|PAR____),   "BridgeHue",      "hue=",  "",      offsetof(RPIMAP, G_iHueHue),           sizeof(int),      ALWAYS_CHANGED,                        0,          "-1",                    NULL)
EXTRACT_PAR(PAR_HUE_SAT,     (WB|PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_HUE|PAR____),   "BridgeSat",      "sat=",  "",      offsetof(RPIMAP, G_iHueSat),           sizeof(int),      ALWAYS_CHANGED,                        0,          "-1",                    NULL)
EXTRACT_PAR(PAR_HUE_CT,      (WB|PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_HUE|PAR____),   "BridgeCt",       "ct=",   "",      offsetof(RPIMAP, G_iHueCt),            sizeof(int),      ALWAYS_CHANGED,                        0,          "-1",                    NULL)
EXTRACT_PAR(PAR_HUE_DEL_DAWN,(   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "DeltaDawn",      "",      "",      offsetof(RPIMAP, G_iSecsDeltaDawn),    sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     http_CollectDateTime)
EXTRACT_PAR(PAR_HUE_DEL_DUSK,(   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "DeltaDusk",      "",      "",      offsetof(RPIMAP, G_iSecsDeltaDusk),    sizeof(int),      ALWAYS_CHANGED,                        0,          "-3600",                 http_CollectDateTime)
EXTRACT_PAR(PAR_HUE_TMR_DAWN,(   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "TimerDawn",      "",      "",      offsetof(RPIMAP, G_iSecsTimerDawn),    sizeof(int),      ALWAYS_CHANGED,                        0,          "7200",                  http_CollectDateTime)
EXTRACT_PAR(PAR_HUE_TMR_DUSK,(   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "TimerDusk",      "",      "",      offsetof(RPIMAP, G_iSecsTimerDusk),    sizeof(int),      ALWAYS_CHANGED,                        0,          "7200",                  http_CollectDateTime)
EXTRACT_PAR(PAR_HUE_WARN_D,  (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "WarnDay",        "",      "",      offsetof(RPIMAP, G_iSecsWarnDay),      sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     http_CollectDateTime)
EXTRACT_PAR(PAR_HUE_WARN_N,  (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "WarnNight",      "",      "",      offsetof(RPIMAP, G_iSecsWarnNight),    sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     http_CollectDateTime)
EXTRACT_PAR(PAR_HUE_TIM_DARK,(   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "TimeDark",       "",      "",      offsetof(RPIMAP, G_pcHueTimeDark),     MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,          "23:30",                 http_CollectDateTime)
EXTRACT_PAR(PAR_HUE_OUT_ON,  (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "WcdSecsOn",      "",      "",      offsetof(RPIMAP, G_iSecsOutletOn),     sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
EXTRACT_PAR(PAR_HUE_OUT_OFF, (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_DAT|PAR_HUE|PAR____),   "WcdSecsOff",     "",      "",      offsetof(RPIMAP, G_iSecsOutletOff),    sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
// Mailx
EXTRACT_PAR(PAR_MAILX_SUBJ,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_EML|PAR____),   "MailSubj",       "",      "-s",    offsetof(RPIMAP, G_pcMailxSubj),       MAX_MAIL_LEN,     NEVER_CHANGED,                         0,          "Misc",                  NULL)
EXTRACT_PAR(PAR_MAILX_BODY,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_EML|PAR____),   "MailBody",       "",      "-d",    offsetof(RPIMAP, G_pcMailxBody),       MAX_MAIL_LEN,     NEVER_CHANGED,                         0,          "",                      NULL)
EXTRACT_PAR(PAR_MAILX_ATTM,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_EML|PAR____),   "MailAttm",       "",      "",      offsetof(RPIMAP, G_pcMailxAttm),       MAX_MAIL_LEN,     NEVER_CHANGED,                         0,          "",                      NULL)
EXTRACT_PAR(PAR_MAILX_TO,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_EML|PAR____),   "MailTo",         "",      "",      offsetof(RPIMAP, G_pcMailxTo),         MAX_MAIL_LEN,     NEVER_CHANGED,                         0,          "peter@patrn.nl",        NULL)
EXTRACT_PAR(PAR_MAILX_CC,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_EML|PAR____),   "MailCc",         "",      "",      offsetof(RPIMAP, G_pcMailxCc),         MAX_MAIL_LEN,     NEVER_CHANGED,                         0,          "peter.hillen@ziggo.nl", NULL)

