/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           par_hue.h
 *  Purpose:            All defines for extracting HUE data
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Oct 2021:      New
 *    27 Oct 2021:      Add Groups and Scenes
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
                                                                                                                                    
//          enum              pcElm1            pcElm2         pcElm3         pcElm4         pcElm5         iOffset                       iSize
//
// ALL
EXTRACT_HUE(A_BRIDGE_NAME,   "name",            NULL,          NULL,          NULL,          NULL,          offsetof(HUECFG, cName),      HUE_DATA_LENZ  )
EXTRACT_HUE(A_BRIDGE_TYPE,   "type",            NULL,          NULL,          NULL,          NULL,          offsetof(HUECFG, cType),      HUE_DATA_LENZ  )
// LIGHTS
EXTRACT_HUE(L_BRIDGE_ON,     "state",           "on",          NULL,          NULL,          NULL,          offsetof(HUECFG, fOn),        sizeof(bool)   )
EXTRACT_HUE(L_BRIDGE_REACH,  "state",           "reachable",   NULL,          NULL,          NULL,          offsetof(HUECFG, fReach),     sizeof(bool)   )
EXTRACT_HUE(L_BRIDGE_BRI,    "state",           "bri",         NULL,          NULL,          NULL,          offsetof(HUECFG, iBri),       sizeof(int)    )
EXTRACT_HUE(L_BRIDGE_HUE,    "state",           "hue",         NULL,          NULL,          NULL,          offsetof(HUECFG, iHue),       sizeof(int)    )
EXTRACT_HUE(L_BRIDGE_SAT,    "state",           "sat",         NULL,          NULL,          NULL,          offsetof(HUECFG, iSat),       sizeof(int)    )
EXTRACT_HUE(L_BRIDGE_CT,     "state",           "ct",          NULL,          NULL,          NULL,          offsetof(HUECFG, iCt),        sizeof(int)    )
EXTRACT_HUE(L_BRIDGE_EFFECT, "state",           "effect",      NULL,          NULL,          NULL,          offsetof(HUECFG, cEffect),    HUE_DATA_LENZ  )
EXTRACT_HUE(L_BRIDGE_CMODE,  "state",           "colormode",   NULL,          NULL,          NULL,          offsetof(HUECFG, cColMode),   HUE_DATA_LENZ  )
EXTRACT_HUE(L_BRIDGE_MODE,   "state",           "mode",        NULL,          NULL,          NULL,          offsetof(HUECFG, cMode),      HUE_DATA_LENZ  )
EXTRACT_HUE(L_BRIDGE_MODEL,  "modelid",         NULL,          NULL,          NULL,          NULL,          offsetof(HUECFG, cModel),     HUE_DATA_LENZ  )
EXTRACT_HUE(L_BRIDGE_UNIQID, "uniqueid",        NULL,          NULL,          NULL,          NULL,          offsetof(HUECFG, cUniqId),    HUE_DATA_LENZ  )
EXTRACT_HUE(L_BRIDGE_CT_MIN, "capabilities",    "control",     "ct",          "min",         NULL,          offsetof(HUECFG, iCtMin),     sizeof(int)    )
EXTRACT_HUE(L_BRIDGE_CT_MAX, "capabilities",    "control",     "ct",          "max",         NULL,          offsetof(HUECFG, iCtMax),     sizeof(int)    )
// GROUPS
EXTRACT_HUE(G_BRIDGE_ON,     "action",          "on",          NULL,          NULL,          NULL,          offsetof(HUECFG, fOn),        sizeof(bool)   )
EXTRACT_HUE(G_BRIDGE_BRI,    "action",          "bri",         NULL,          NULL,          NULL,          offsetof(HUECFG, iBri),       sizeof(int)    )
EXTRACT_HUE(G_BRIDGE_HUE,    "action",          "hue",         NULL,          NULL,          NULL,          offsetof(HUECFG, iHue),       sizeof(int)    )
EXTRACT_HUE(G_BRIDGE_SAT,    "action",          "sat",         NULL,          NULL,          NULL,          offsetof(HUECFG, iSat),       sizeof(int)    )
EXTRACT_HUE(G_BRIDGE_CT,     "action",          "ct",          NULL,          NULL,          NULL,          offsetof(HUECFG, iCt),        sizeof(int)    )
EXTRACT_HUE(G_BRIDGE_EFFECT, "action",          "effect",      NULL,          NULL,          NULL,          offsetof(HUECFG, cEffect),    HUE_DATA_LENZ  )
EXTRACT_HUE(G_BRIDGE_CMODE,  "action",          "colormode",   NULL,          NULL,          NULL,          offsetof(HUECFG, cColMode),   HUE_DATA_LENZ  )
EXTRACT_HUE(G_BRIDGE_LGT,    "lights",          NULL,          NULL,          NULL,          NULL,          offsetof(HUECFG, cLights),    HUE_DATA_LENZ  )
// SCENES
EXTRACT_HUE(S_BRIDGE_GRP,    "group",           NULL,          NULL,          NULL,          NULL,          offsetof(HUECFG, cGroup),     HUE_DATA_LENZ  )
EXTRACT_HUE(S_BRIDGE_LGT,    "lights",          NULL,          NULL,          NULL,          NULL,          offsetof(HUECFG, cLights),    HUE_DATA_LENZ  )
