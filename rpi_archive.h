/*  (c) Copyright:  2021  Patrn, Confidential Data
 *
 *  Workfile:           rpi_archive.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for Archive thread
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    15 Jun 2021:      Created
 *    17 Jan 2022:      Add csv file data
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_ARCH_H_
#define _RPI_ARCH_H_

typedef enum _archive_csv_
{
   ARC_STATE_INIT = 0,        // Init
   ARC_STATE_TIMESTAMP,       // Add timestamp data
   ARC_STATE_SETTINGS,        // Add Settings data
   ARC_STATE_FRAMES,          // Add number of frames
   ARC_STATE_MAGCNT,          // Add Mag and Cnt data
   ARC_STATE_MOTION,          // Add motion data
   ARC_STATE_TRIGGER,         // Add Motion trigger yesno
   ARC_STATE_CLOSE,           // Close for exit
   //
   NUM_ARC_STATES
}  ARCCSV;
//
int ARCH_Init                 (void);

#endif /* _RPI_ARCH_H_ */
