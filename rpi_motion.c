/*  (c) Copyright:  2021 Patrn, Confidential Data
 *
 *  Workfile:           rpi_motion.c
 *  Purpose:            This utility is executed to handle both motion and archive signals.
 *                      
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Jun 2021:      Created
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    03 Dec 2021:      Keep track of the motion events file
 *    13 Dec 2021:      Save the frames with the highest count
 *    02 Feb 2022:      Signal Hue on motion (part 1)
 *    07 Mar 2022:      Replace atoi() by strtol()
 *    01 Jun 2022:      Add PidList timestamps
 *    20 Nov 2022:      Verbose HUE signal motion
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_main.h"
#include "rpi_func.h"
#include "rpi_mailx.h"
#include "rpi_hue.h"
#include "rpi_motion.h"
//
//#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static int     motn_Execute               (void);
//
static bool    motn_CheckDirectory        (void);
static long    motn_ParseMotionEventsFile (PIKMOT *, long);
static int     motn_ReadMotionRegion      (PIKREG *, char *);
static void    motn_SignalHue             (HUECMD);
static void    motn_SummarizeRegionData   (PIKMOT *);
static void    motn_TrackMotionEventsFile (void);
//
static int     motn_AcquireMotion         (PIKMOT *, char *, int);
static int     motn_EventHandler          (PIKMOT *, char *, int);
static int     motn_EventHeaderStart      (PIKMOT *, char *, int);
static int     motn_EventVideo            (PIKMOT *, char *, int);
static int     motn_EventFrame            (PIKMOT *, char *, int);
static int     motn_EventPreset           (PIKMOT *, char *, int);
static int     motn_EventMagnitudeLimit   (PIKMOT *, char *, int);
static int     motn_EventMagnitudeCount   (PIKMOT *, char *, int);
static int     motn_EventBurstCount       (PIKMOT *, char *, int);
static int     motn_EventBurstFrames      (PIKMOT *, char *, int);
static int     motn_EventHeaderEnd        (PIKMOT *, char *, int);
static int     motn_EventMotionStart      (PIKMOT *, char *, int);
static int     motn_EventMotionEnd        (PIKMOT *, char *, int);
static int     motn_EventEnd              (PIKMOT *, char *, int);
//
static bool    motn_SignalRegister        (sigset_t *);
static void    motn_ReceiveSignalSegmnt   (int);
static void    motn_ReceiveSignalInt      (int);
static void    motn_ReceiveSignalTerm     (int);
static void    motn_ReceiveSignalUser1    (int);
static void    motn_ReceiveSignalUser2    (int);

//
// pikrellcam stores run-time variables in the filesystem as run/pikrellcam/state :
//
//             motion_enable off
//             show_preset off
//             show_vectors off
//             preset 1 3
//             magnitude_limit 5
//             magnitude_count 5
//             burst_count 350
//             burst_frames 3
//             video_record_state stop
//             video_last /mnt/rpipix/videos/loop_2018-10-15_19.10.59_0.mp4
//             video_last_frame_count 22
//             video_last_time 0.92
//             video_last_fps 24.01
//             audio_last_frame_count 43986
//             audio_last_rate 47999
//             still_last none
//             show_timelapse off
//             timelapse_period 60
//             timelapse_active off
//             timelapse_hold off
//             timelapse_jpeg_last none
//             timelapse_converting none
//             timelapse_video_last none
//             current_minute 1180
//             dawn 445
//             sunrise 479
//             sunset 1117
//             dusk 1151
//             multicast_group_IP 225.0.0.55
//             multicast_group_port 22555
//
static PIKEVT stEventList[] = 
{
   {  "<header>",             motn_EventHeaderStart       },
   {  "video",                motn_EventVideo             },
   {  "frame",                motn_EventFrame             },
   {  "preset",               motn_EventPreset            },
   {  "magnitude_limit",      motn_EventMagnitudeLimit    },
   {  "magnitude_count",      motn_EventMagnitudeCount    },
   {  "burst_count",          motn_EventBurstCount        },
   {  "burst_frames",         motn_EventBurstFrames       },
   {  "</header>",            motn_EventHeaderEnd         },
   {  "<motion",              motn_EventMotionStart       },
   {  "</motion>",            motn_EventMotionEnd         },
   {  "<end>",                motn_EventEnd               }
};
static int iNumEvents = sizeof(stEventList) / sizeof(PIKEVT);
//
static FILE   *ptEventFile    = NULL;
static char   *pcEventBuffer  = NULL;


//=============================================================================
// Global Data 
//=============================================================================
//
//    Motion data capture
//
//    PIKMOT pstMap->G_stMotion:
//    ---------------------------------------------
//    iMotionEvents                 : bitfields
//    iFrame                        : frame index 0..?
//    iBlocksHor                    : frame hor ver
//    iBlocksVer                    : frame hor ver
//    iMagLimit                     : magnitude limit
//    iMagCount                     : magnitude count
//    iBurstCount                   : burst count
//    iBurstFrames                  : burst frames
//    stFrame                       : motion frame data regions
//    cMotionFile[MAX_PATH_LENZ]    : motion filename
//
//    PIKFRM stFrame:
//    ---------------------------------------------
//    flSeconds                     : motion offset in secs
//    iBurst                        : Burst trigger
//    iAudio                        : Audio trigger
//    iExternal                     : External trigger
//    stCanvas                      ; Whole canvas frame vector data
//    stRegion[10]                  : Single frame region 0..9 vector data
//
//    PIKREG stRegion:
//    ---------------------------------------------
//    iX                            : X vector 
//    iY                            : Y vector
//    idX                           : dX
//    idY                           : dY
//    iMagnitude                    : Magnitude Sum
//    iCount                        : Count     Sum
//
// Local arguments
//
static int     fThreadRunning       = TRUE;

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   MOTN_Init
// Purpose:    Handle motion detect for PiKrellCam
//
// Parms:      
// Returns:    Exit codes
// Note:       Calld from main() to split off thread and let it handle the 
//             motion detection. The main() will use SIGUSR1 to trigger motion
//             handling.
//
int MOTN_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_MOTION, tPid);
         GLOBAL_PidSetInit(PID_MOTION, MOTN_Init);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = motn_Execute();
         GLOBAL_PidPut(PID_MOTION, 0);
         LOG_Report(0, "MOT", "MOTN-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("MOTN-Init(): Error!" CRLF);
         LOG_Report(errno, "MOT", "MOTN-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_MOT_INI);
}

//
// Function:   motn_Execute
// Purpose:    Handle motion detect for PiKrellCam
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int motn_Execute()
{
   bool     fParseMotionFile=FALSE;
   int      iOpt, iCc=EXIT_CC_OKEE;
   int      iTimeout;
   u_int32  ulSecs, ulAqTimeoutSecs=0;
   long     lEventFilePos;
   sigset_t tBlockset;
   PIKMOT  *pstMotion;

   if(motn_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "MOT", "motn-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_MOTION);
   //
   //
   // Make sure the motion data dir exists
   //
   if( (fThreadRunning = motn_CheckDirectory()) == FALSE)
   {
      LOG_Report(0, "MOT", "motn-Execute():Unable to create Motion Data directory");
      iCc = EXIT_CC_GEN_ERROR;
   }
   //
   // Allocate memory to read PiKrellCam EventFile
   //
   pstMotion     = safemalloc(sizeof(PIKMOT));
   pcEventBuffer = safemalloc(EVENT_BUFFER_LENZ);
   iTimeout      = EVENT_POLL_IDLE;
   //
   // Init ready: await further actions
   //
   GLOBAL_PidTimestamp(PID_MOTION, 0);
   GLOBAL_PidSaveGuard(PID_MOTION, GLOBAL_MOT_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_MOT_INI);
   //
   // Main loop of Motion: wait for the semaphore posted by the Host 
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_MOTION, iTimeout);
      switch(iOpt)
      {
         case 0:
            GLOBAL_PidTimestamp(PID_MOTION, 0);
            //
            // Check if HOST reports Midnight
            // Turn OFF Motion logging 
            //
            if(GLOBAL_GetSignalNotification(PID_MOTION, GLOBAL_HST_ALL_MID)) 
            {
               PRINTF("motn-Execute():Midnight" CRLF);
               LOG_Report(0, "MOT", "motn-Execute():Midnight");
               RPI_SetFlag(FLAGS_LOG_MOTION, FALSE);
            }
            //
            // Check if HOST has a motion detect event
            //
            if(GLOBAL_GetSignalNotification(PID_MOTION, GLOBAL_HST_MOT_RUN)) 
            {
               PRINTF("motn-Execute():GLOBAL_HST_MOT_RUN" CRLF);
               RPI_LogTimestamp("motn-Execute()", GLOBAL_GetParameter(PAR_LOOP_FILE), &(pstMap->G_stTs));
               if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "MOT", "Motion acquisition started.");
               //
               // This is the PiKrellCam ON_MOTION_BEGIN Event:
               //
               // Try to open the PiKrellCam Motion Event file, if not yet done.
               // Note: If PiKrellCam does not have any motion yet, the <pcPiKrellEvents>
               //       file does not exist. Opening will prove impossible so to prevent 
               //       flooding the LOG file, we will NOT use safefopen() here.
               //
               if(ptEventFile == NULL) ptEventFile = fopen((char *)pcPiKrellEvents, "r");
               //
               // LOG the open/close state of the motion-event file 
               //
               motn_TrackMotionEventsFile();
               //
               // Start motion acquisition: reset the storage first
               // Keep parsing the motion-events file until the end-of-motion has been detected
               // Keep track of highest count
               //
               GEN_MEMSET(pstMotion, 0x00, sizeof(PIKMOT));
               //
               pstMotion->ulMotionSecs = RTC_GetSystemSecs();
               ulAqTimeoutSecs         = pstMotion->ulMotionSecs + ACQUSITION_TIMEOUT;
               lEventFilePos           = 0;
               iTimeout                = EVENT_POLL_ACTIVE;
               fParseMotionFile        = TRUE;
            }
            //
            // Check if HUE reports mission completed
            //
            if(GLOBAL_GetSignalNotification(PID_MOTION, GLOBAL_HUE_MOT_NFY)) 
            {
               PRINTF("motn-Execute():Hue notification" CRLF);
               if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "MOT", "motn-Execute():Hue notifies motion lightning completed");
            }
            break;

         case 1:
            //
            // Timeout: 
            //
            PRINTF("motn-Execute():Timeout" CRLF);
            if(fParseMotionFile) 
            {
               //
               // We are in the process of reading the motion-event file from PiKrellCam
               //
               lEventFilePos = motn_ParseMotionEventsFile(pstMotion, lEventFilePos);
               //
               // Keep track of the Motion aquisition.
               // Normally, the <motion-events> file will have the <end> tag to complete the motion aquisition.
               // If this does NOT happen within the aquisition timeout, report and terminate motion aquisition
               // 
               if(pstMotion->iMotionEvents & MOT_END)
               {
                  //
                  // The motion-event acquisition has complete: 
                  //
                  //    - Summarize this motion-event to todays SUM
                  //    - Copy the acquisition to the Global G_stMotion structure
                  //    - Notify the host and restart for more motion-events
                  //
                  if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "MOT", "Motion acquisition completed.");
                  RPI_LogTimestamp("motn-Execute()", GLOBAL_GetParameter(PAR_LOOP_FILE), &(pstMap->G_stTs));
                  PRINTF("motn-Execute():Motion-events END: %d frames" CRLF, pstMotion->iNumFrames);
                  motn_SummarizeRegionData(pstMotion);
                  pstMap->G_stMotion = *pstMotion;
                  //  Turn OFF motion logging
                  RPI_SetFlag(FLAGS_LOG_MOTION, FALSE);
                  motn_SignalHue(HUE_CMD_WARNING_OFF);
                  GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_MOT_HST_NFY);
                  fParseMotionFile  = FALSE;
                  ulAqTimeoutSecs   = 0;
                  iTimeout          = EVENT_POLL_IDLE;
               }
               else
               {
                  //
                  // Check if motion acquisition does not take too long
                  //
                  ulSecs = RTC_GetSystemSecs();
                  if( ulAqTimeoutSecs && (ulSecs > ulAqTimeoutSecs))
                  {
                     //
                     // Motion acquisition takes too long: abort
                     //
                     LOG_Report(0, "MOT", "Motion acquisition ABORTED: Timeout");
                     pstMotion->iMotionEvents |= MOT_END;
                  }
               }
            }
            if(GLOBAL_GetSignalNotification(PID_MOTION, GLOBAL_HST_MOT_PEN)) 
            {
               //
               // We have a ON_MOTION_END signal from the host: Abort the motion-event search for 
               // the <end>
               // Also, terminate any warning lights
               //
               if(fParseMotionFile)
               {
                  LOG_Report(0, "MOT", "Motion acquisition ABORTED: No <end> directive encountered yet!");
               }
               else
               {
                  //pwjh LOG_Report(0, "MOT", "Motion acquisition MISSED completion: handle it anyway. Turn ON motion logging!");
                  //pwjh RPI_SetFlag(FLAGS_LOG_MOTION, TRUE);
                  LOG_Report(0, "MOT", "Motion acquisition MISSED completion: handle it anyway!");
                  fParseMotionFile = TRUE;
               }
               pstMotion->iMotionEvents |= MOT_END;
            }
            break;

         default:
         case -1:
            LOG_Report(errno, "MOT", "motn-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("motn-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   if(ptEventFile)   safefclose(ptEventFile);
   if(pcEventBuffer) safefree(pcEventBuffer);
   if(pstMotion)     safefree(pstMotion);
   //
   GLOBAL_SemaphoreDelete(PID_MOTION);
   return(iCc);
}
/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   motn_CheckDirectory
// Purpose:    Check if the motion data dir exists, if not create it
//             
// Parms:      
// Returns:    TRUE if all is OKee
// Note:       Permissions = 0775 (drwxrwxr-x)
// 
static bool motn_CheckDirectory()
{
   bool  fOkee=TRUE;

   if(!RPI_DirectoryExists((char *)pcMotionLoopPath))  fOkee = RPI_DirectoryCreate((char *)pcMotionLoopPath, ATTR_RWXRWXR_X, TRUE);
   return(fOkee);
}

// 
// Function:   motn_ParseMotionEventsFile
// Purpose:    Verify if we have sufficient motion in this loop_video to trigger
//             the Archive. Read the motion data from /run/pikrellcam/motion-events
// 
// Parameters: Motion data, file position
// Returns:    Last file position
// Note:       PiKrellcam will process motion detection (if enabled) and update the             
//             motion-events file accordingly. The end of the motion detection is marked 
//             by terminating the motion-events file with <end>
//             Only then we can process with parsing the file and filling the global
//             G_stMotion structure.
// 
static long motn_ParseMotionEventsFile(PIKMOT *pstMot, long lFilePos)
{
   int      iLen, iAqState=MOT_STATE_IDLE;
   char    *pcRes;

   if(ptEventFile)
   {
      if( fseek(ptEventFile, lFilePos, SEEK_SET) != 0)
      {
         PRINTF("motn-ParseMotionEventsFile():ERROR Seek to %ld! Pos=%ld" CRLF, lFilePos, ftell(ptEventFile));
         LOG_Report(errno, "MOT", "motn-ParseMotionEventsFile():ERROR Seek to %ld! Pos=%ld ", lFilePos, ftell(ptEventFile));
         rewind(ptEventFile);
         pstMot->iMotionEvents = 0;
      }
      do
      {
         pcRes = fgets(pcEventBuffer, EVENT_BUFFER_LEN, ptEventFile);
         if(pcRes) 
         {
            //
            // Skip trailing LF
            //
            GEN_RemoveChar(pcRes, CR);
            GEN_RemoveChar(pcRes, LF);
            pcRes = GEN_TrimAll(pcRes);
            iLen  = GEN_STRLEN(pcRes);
            //
            if(iLen && RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "MOT", "motn-ParseMotionEventsFile():[%s]", pcRes);
            PRINTF("motn-ParseMotionEventsFile{}:Event:[%s]" CRLF, pcRes);
            if( (pstMot->iMotionEvents & VALID_ACQUISITION) == VALID_ACQUISITION) 
            {
               //
               // We have acquired 
               // Start motion region data acquisition 
               //          "<motion xxx>\n"
               //          "f  89  27  -8   0   8  202\n"   <--+
               //          "...."\n"                           |
               //          "2  85  26  -8   1   8   92\n"      +--- We are in this region
               //          "3  93  28  -8   0   8  110\n"      |
               //          "b 3467"\n"                         |
               //          "...."\n"                        <--+
               //          "</motion>\n"
               //
               // Handle motion type:
               //    f,a,b,e,0...9
               //
               motn_AcquireMotion(pstMot, pcRes, iLen);
            }
            iAqState = motn_EventHandler(pstMot, pcRes, iLen);
         }
      }
      while(pcRes);
      //
      // No more data in event-state file: Check acquisition progress
      //
      switch(iAqState)
      {
         default:
         case MOT_STATE_IDLE:
         case MOT_STATE_BUSY:
            lFilePos = ftell(ptEventFile);
            break;

         case MOT_STATE_DONE:
            PRINTF("motn-ParseMotionEventsFile():END of Aquisition" CRLF, lFilePos);
            lFilePos = 0;
            break;

         case MOT_STATE_ERROR:
            PRINTF("motn-ParseMotionEventsFile():Aquisition ERROR: Reset" CRLF, lFilePos);
            lFilePos = -1;
            break;
      }
   }
   else
   {
      PRINTF("motn-ParseMotionEventsFile():Event file %s not open" CRLF, pcPiKrellEvents);
   }
   motn_TrackMotionEventsFile();
   return(lFilePos);
}

//
// Function:   motn_ReadMotionRegion
// Purpose:    Read a single motion region
//             
// Parms:      Frame ptr, Frame
// Returns:    CC, 0=OK, -1=error 
// Note:       Input from the PiKrellCam (or PiPatrnCam) utility:
//             "...."
//             "<motion  0.375>
//             "f  89  27  -8   0   8  202
//        pcFrame^
//             "2  85  26  -8   1   8   92
//             "3  93  28  -8   0   8  110
//             "</motion>
//             "...."
//
static int motn_ReadMotionRegion(PIKREG *pstReg, char *pcFrame)
{
   int   iNr, iCc=0;
   int   iX, iY, idX, idY, iM, iC;

   //
   // Extract X Y dX dY Magn Count
   //
   iNr = GEN_SSCANF(pcFrame, "%d %d %d %d %d %d", &iX, &iY, &idX, &idY, &iM, &iC);
   if(iNr == 6)
   {
      //
      // We have the correct amount of variables. Save the frame 
      //
      pstReg->iX         = iX;
      pstReg->iY         = iY;
      pstReg->idY        = idY;
      pstReg->idY        = idY;
      //
      pstReg->iMagnitude = iM;
      pstReg->iCount     = iC;
      //
      PRINTF("motn-ReadMotionRegion():%s" CRLF, pcFrame);
   }
   else
   {
         LOG_Report(0, "MOT", "motn-ReadMotionRegion():Scan ERROR:%s", pcFrame);
         iCc = -1;
   }
   return(iCc);
}

//
// Function:   motn_SignalHue
// Purpose:    Signal the HUE Bridge to turn on warning lights
//             
// Parms:      HUE Command
// Returns:     
// Note:       Report motion to HUE only if FLAGS_HUE_MOTION set.
//
static void motn_SignalHue(HUECMD tCmd)
{
   static bool fReportedMotion = FALSE;

   if(RPI_GetFlag(FLAGS_HUE_MOTION)) 
   {
      LOG_Report(0, "MOT", "Motn_SignalHue():Signal %d", tCmd);
      pstMap->G_iHueCmd = tCmd;
      GLOBAL_SetSignalNotification(PID_HUE, GLOBAL_MOT_HUE_RUN);
      fReportedMotion = FALSE;
   }
   else
   {
      if(!fReportedMotion) LOG_Report(0, "MOT", "Motn-Motn_SignalHue():Motion signal to HUE is DISABLED");
      fReportedMotion = TRUE;
   }
}

//
// Function:   motn_SummarizeRegionData
// Purpose:    Summarize region data from all frames 
//             
// Parms:      Motion data
// Returns:     
// Note:       Sum this motion data to the days-total:
//
//                G_iRegionMagnitude[MAX_MOTION_REGIONS]
//                G_iRegionCount    [MAX_MOTION_REGIONS]
//
//                G_stMotion := Motion data 
//
static void motn_SummarizeRegionData(PIKMOT *pstMot)
{
   int      iReg; 
   PIKREG  *pstReg;

   //
   // Summarize Motions per region
   //
   for(iReg=0; iReg<MAX_MOTION_REGIONS; iReg++)
   {
      pstReg = &(pstMot->stFrame.stRegion[iReg]);
      //
      // Add/Count motion on each of the criteria
      //
      pstMap->G_iRegionMagnitude[iReg] += pstReg->iMagnitude;
      pstMap->G_iRegionCount[iReg]     += pstReg->iCount;
   }
}

//
// Function:   motn_TrackMotionEventsFile
// Purpose:    Keep track of the PiKrellCam motion-events file
//             
// Parms:      
// Returns:     
// Note:       iEventsLogged =
//                0: Init
//               +1: Event file open
//               -1: Event file NOT open
//
static void motn_TrackMotionEventsFile()
{
   static int iEventsLogged=0;

   if(ptEventFile)
   {
      //
      // PiKrellCam Events file OKee, report it once
      //
      if(iEventsLogged < 0)
      {
         LOG_Report(0, "MOT", "motn-TrackMotionEventsFile():Motion events file %s OKee", pcPiKrellEvents);
         PRINTF("motn-TrackMotionEventsFile():Motion events file %s OKee" CRLF, pcPiKrellEvents);
         iEventsLogged = +1;
      }
   }
   else
   {
      //
      // PiKrellCam Events file not found: report it once
      //
      if(iEventsLogged > 0)
      {
         LOG_Report(0, "MOT", "motn-TrackMotionEventsFile():No motion events file %s", pcPiKrellEvents);
         PRINTF("motn-TrackMotionEventsFile():No motion events file %s" CRLF, pcPiKrellEvents);
         iEventsLogged = -1;
      }
   }
}


/*----------------------------------------------------------------------
________________EVENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   motn_AcquireMotion
// Purpose:    Acquire the PiLrellCam motion data
//             
// Parms:      Motion storage, Event data, size
// Returns:    CC, 0=OK, -1=error 
// Note:       Input from the PiKrellCam utility
//             "<header>"
//             "...."
//             "<motion  0.375>
//             "f  89  27  -8   0   8  202   <--+
//             "...."                           |
//             "2  85  26  -8   1   8   92      +--- We are in this region
//             "3  93  28  -8   0   8  110      |
//             "b 3467"                         |
//             "...."                        <--+
//             "</motion>
//             "...."
//             "</header>"
//
static int motn_AcquireMotion(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   int      iNr, iVal, iCc=0;
   int      iMot, iMotMax;
   PIKFRM  *pstFrm;
   PIKREG   stActReg;

   pstFrm = &(pstMot->stFrame);
   PRINTF("motn-AcquireMotion()" CRLF);
   //
   switch(*pcEvent)
   {
      case '<':
         // Next directive
         break;

      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
         //
         // region 0...9
         // n x y dx dy magnitude count - where the first character code n is a digit.
         // These lines are for motion vectors for a motion region detects. There will be a line for each region 
         // having motion and no line for regions not having motion. Just like the overall frame vector, for a 
         // motion region to have motion, the configured magnitude and count limits must be met. For the example
         // in the note above there was motion in regions 2 and 3.
         //
         iNr = (int)*pcEvent - (int)'0';
         if( motn_ReadMotionRegion(&stActReg, &pcEvent[1]) == 0)
         {
            iMot    = stActReg.iCount * stActReg.iMagnitude;
            iMotMax = pstFrm->stRegion[iNr].iCount * pstFrm->stRegion[iNr].iMagnitude;
            //
            if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "MOT", "motn-AcquireMotion():Reg%d=(%d, %d, %d)", iNr, stActReg.iMagnitude, stActReg.iCount, iMot);
            if(RPI_GetFlag(FLAGS_PRN_MOTION)) LOG_printf("motn-AcquireMotion():Reg%d=(%d, %d, %d)" CRLF, iNr, stActReg.iMagnitude, stActReg.iCount, iMot);
            //
            if(iMot > iMotMax)
            {
               if(RPI_GetFlag(FLAGS_LOG_MOTION)) LOG_Report(0, "MOT", "motn-AcquireMotion():Signal Max Reg%d=%d", iNr, iMot);
               if(RPI_GetFlag(FLAGS_PRN_MOTION)) LOG_printf(0, "MOT", "motn-AcquireMotion():Signal Max Reg%d=%d" CRLF, iNr, iMot);
               //
               // This Frame has more movement: save it
               //
               pstFrm->stRegion[iNr] = stActReg;
               pstMap->G_iRegionCount[iNr]++;
               //
               // Signal HUE thread to turn some lights ON if not done so
               //
               motn_SignalHue(HUE_CMD_UPDATE_MOTION);
            }
         }
         break;

      case 'a':
         //
         // a level - shows an audio trigger if an audio level exceeded the configured audio_trigger_level value. 
         // If there was only an audio trigger, then the overall motion frame vector 'f' line will show a zero vector.
         //
         if( GEN_SSCANF(&pcEvent[1], "%d", &iVal) == 1)
         {
            pstFrm->iAudio = iVal;
         }
         break;

      case 'b':
         //
         // b - this line shows burst counts. If no regions individually passed detection magnitude and count 
         // limits, the overall frame vector must pass for a burst count detect so a burst detect with no region 
         // detects still always has a frame vector.
         //
         if( GEN_SSCANF(&pcEvent[1], "%d", &iVal) == 1)
         {
            pstFrm->iBurst = iVal;
         }
         break;

      case 'e':
         //
         // e code - shows there was an external trigger (motion trigger command into the FIFO). The code will either 
         // be "FIFO" or a custom code supplied in the external trigger command. If there was only an external trigger, 
         // then the overall motion frame vector 'f' line will show a zero vector. 
         //
         if( GEN_SSCANF(&pcEvent[1], "%d", &iVal) == 1)
         {
            pstFrm->iExternal = iVal;
         }
         break;

      case 'f':
         //
         // f x y dx dy magnitude count - where the first character code is 'f'.
         // This line is the data for the total frame vector (composite of all the motion region vectors). 
         // There is always a frame vector reported but may be a zero vector for audio or external only detects. 
         // A non-zero frame vector passes the configured magnitude and count limits and there can be a passing 
         // frame vector without any passing region vectors.
         //
         if( motn_ReadMotionRegion(&stActReg, &pcEvent[1]) == 0)
         {
            if(stActReg.iCount > pstFrm->stCanvas.iCount)
            {
               //
               // This Frame has more movement: save it
               //
               pstFrm->stCanvas = stActReg;
            }
         }
         break;

      default:
         PRINTF("motn-AcquireMotion(): Unknown <%s>" CRLF, pcEvent);
         iCc = -1;
         break;
   }
   return(iCc);
}

//
// Function:   motn_EventHandler
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Result struct, Event data, size
// Returns:    Motion state
// Note:       Input from the PiKrellCam (or PiPatrnCam) utility
//
//             "<header>\n"
//             "video /mnt/rpipix/videos/loop_2018-08-01_18.56.41_0.mp4\n"
//             "frame 121 68\n"
//             "preset 1 2\n"
//             "magnitude_limit 6\n"
//             "magnitude_count 6\n"
//             "burst_count 400\n"
//             "burst_frames 3\n"
//             "</header>\n"
//             "<motion  0.375>\n"
//             "f  89  27  -8   0   8  202\n"
//             "2  85  26  -8   1   8   92\n"
//             "3  93  28  -8   0   8  110\n"
//             "</motion>\n"
//             "<motion  2.075>\n"
//             "...."\n"
//             "</motion>\n"
//             "<end>\n"
//
static int motn_EventHandler(PIKMOT *pstMot, char *pcEventBuffer, int iLen)
{
   int            i, iSize, iState=MOT_STATE_ERROR;
   PFEVT          pfAddr;
   const PIKEVT  *pstEvt=stEventList;

   for(i=0; i<iNumEvents; i++)
   {
      ///PRINTF("motn-EventHandler():Check:%s" CRLF, pstEvt->pcEvent);
      if(GEN_STRNCMPI(pstEvt->pcEvent, pcEventBuffer, GEN_STRLEN(pstEvt->pcEvent)) == 0)
      {
         //PRINTF("motn-EventHandler():Event=%s:MATCH" CRLF, pstEvt->pcEvent);
         //
         // Event found: handle it
         //
         if( (pfAddr = pstEvt->pfEvtCb) )
         {
            //
            // Run the event handler: 
            //
            iSize          = GEN_STRLEN(pstEvt->pcEvent);
            pcEventBuffer += iSize;
            iLen          -= iSize;
            iState         = pfAddr(pstMot, pcEventBuffer, iLen);
         }
         break;
      }
      pstEvt++;
   }
   return(iState);
}

//
// Function:   motn_EventHeaderStart
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
// Note:       Input from the PiKrellCam (or PiPatrnCam) utility
//             "<header>"
//             "...."
//             "<motion  0.375>
//             "f  89  27  -8   0   8  202
//             "...."                     
//             "2  85  26  -8   1   8   92
//             "3  93  28  -8   0   8  110
//             "...."                     
//             "b 3467"                     
//             "</motion>
//             "...."
//             "</header>"
//
//
static int motn_EventHeaderStart(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   pstMot->iMotionEvents |= MOT_HEADER_START;
   //
   PRINTF("motn-EventHeaderStart():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventVideo
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//
static int motn_EventVideo(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   pcEvent = RPI_FindFilename(pcEvent);
   GEN_STRNCPY(pstMot->cMotionFile, pcEvent, MAX_PATH_LEN);
   pstMot->iMotionEvents |= MOT_VIDEO;
   //
   PRINTF("motn-EventVideo():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventFrame
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//
static int motn_EventFrame(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   char *pcVal;

   pstMot->iMotionEvents |= MOT_FRAME;
   //
   // Extract number of hor and ver Macroblocks
   //
   pcVal = GEN_FindDelimiter(pcEvent, DELIM_NEXTFIELD);
   pstMot->iBlocksHor = (int)strtol(pcVal, NULL, 0);
   //
   pcVal = GEN_FindDelimiter(pcEvent, DELIM_NEXTFIELD);
   pstMot->iBlocksVer = (int)strtol(pcVal, NULL, 0);
   //
   PRINTF("motn-EventFrame():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventPreset
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//
static int motn_EventPreset(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   PRINTF("motn-EventPreset():%s" CRLF, pcEvent);
   pstMot->iMotionEvents |= MOT_PRESET;
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventMagnitudeLimit
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//
static int motn_EventMagnitudeLimit(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   char *pcVal;

   pstMot->iMotionEvents |= MOT_MAG_LIMIT;
   //
   // Extract number
   //
   pcVal = GEN_FindDelimiter(pcEvent, DELIM_NEXTFIELD);
   pstMot->iMagLimit = (int)strtol(pcVal, NULL, 0);
   //
   PRINTF("motn-EventMagnitudeLimit():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventMagnitudeCount
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//
static int motn_EventMagnitudeCount(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   char *pcVal;

   pstMot->iMotionEvents |= MOT_MAG_COUNT;
   //
   // Extract number
   //
   pcVal = GEN_FindDelimiter(pcEvent, DELIM_NEXTFIELD);
   pstMot->iMagCount = (int)strtol(pcVal, NULL, 0);
   //
   PRINTF("motn-EventMagnitudeCount():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventBurstCount
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//
static int motn_EventBurstCount(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   char *pcVal;

   pstMot->iMotionEvents |= MOT_BURST_COUNT;
   pcVal = GEN_FindDelimiter(pcEvent, DELIM_NEXTFIELD);
   pstMot->iBurstCount = (int)strtol(pcVal, NULL, 0);
   //
   PRINTF("motn-EventBurstCount():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventBurstFrames
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//
static int motn_EventBurstFrames(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   char *pcVal;

   pstMot->iMotionEvents |= MOT_BURST_FRAMES;
   //
   // Extract number
   //
   pcVal = GEN_FindDelimiter(pcEvent, DELIM_NEXTFIELD);
   pstMot->iBurstFrames = (int)strtol(pcVal, NULL, 0);
   //
   PRINTF("motn-EventBurstFrames():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventHeaderEnd
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//             "<header>"
//             "...."
//             "<motion  0.375>
//             "f  89  27  -8   0   8  202
//             "...."                     
//             "2  85  26  -8   1   8   92
//             "3  93  28  -8   0   8  110
//             "...."                     
//             "</motion>
//             "...."
//    Here --> "</header>"
//
static int motn_EventHeaderEnd(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   pstMot->iMotionEvents |= MOT_HEADER_END;
   //
   PRINTF("motn-EventHeaderEnd():%s" CRLF, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventMotionStart
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//             "<header>"
//             "...."
//     Here--> "<motion  0.375>
//             "f  89  27  -8   0   8  202
//             "...."                     
//             "2  85  26  -8   1   8   92
//             "3  93  28  -8   0   8  110
//             "...."                     
//             "</motion>
//             "...."
//             "</header>"
//
static int motn_EventMotionStart(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   char    *pcVal;
   PIKFRM  *pstFrm;

   pstMot->iMotionEvents |= MOT_MOTION_START;
   pstMot->iNumFrames++;
   //
   // Extract number
   //
   pcVal  = GEN_FindDelimiter(pcEvent, DELIM_NEXTFIELD);
   pstFrm = &(pstMot->stFrame);
   //
   pstFrm->flSeconds = atof(pcVal);
   //
   PRINTF("motn-EventMotionStart():Frame=%d:%s" CRLF, pstMot->iNumFrames, pcEvent);
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventMotionEnd
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//             "<header>"
//             "...."
//             "<motion  0.375>
//             "f  89  27  -8   0   8  202
//             "...."                     
//             "2  85  26  -8   1   8   92
//             "3  93  28  -8   0   8  110
//             "...."                     
//             "b 3467"                     
//     Here--> "</motion>
//             "...."
//             "</header>"
//
static int motn_EventMotionEnd(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   pstMot->iMotionEvents &= ~MOT_MOTION_START;
   return(MOT_STATE_BUSY);
}

//
// Function:   motn_EventEnd
// Purpose:    Handle the PiLrellCam motion_event
//             
// Parms:      Motion storage, Event data, size
// Returns:    Motion state
//             "<header>"
//             "...."
//             "<motion  0.375>
//             "f  89  27  -8   0   8  202
//             "...."                     
//             "2  85  26  -8   1   8   92
//             "3  93  28  -8   0   8  110
//             "...."                     
//             "b 3467"                     
//             "</motion>
//             "...."
//             "</header>"
//     Here--> "<end>
//
static int motn_EventEnd(PIKMOT *pstMot, char *pcEvent, int iLen)
{
   pstMot->iMotionEvents |= MOT_END;
   //
   PRINTF("motn-EventEnd():%s" CRLF, pcEvent);
   return(MOT_STATE_DONE);
}


/*----------------------------------------------------------------------
_______________SIGNAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   motn_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool motn_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &motn_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "MOT", "motn-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &motn_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "MOT", "motn-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &motn_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "MOT", "motn-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &motn_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "MOT", "motn-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &motn_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "MOT", "motn-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   motn_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void motn_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   motn_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void motn_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "MOT", "motn-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_MOTION);
}

//
// Function:   motn_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void motn_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "MOT", "motn-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_MOTION);
}

//
// Function:   motn_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void motn_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   motn_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void motn_ReceiveSignalUser2(int iSignal)
{
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT



#endif   //COMMENT
